# Add Rules for Moshi library code by KED
# Squareup Moshi based on documentation form project github page https://github.com/square/moshi

-dontwarn okio.**
-dontwarn com.squareup.moshi.**
-keepattributes *Annotation*
-keepclasseswithmembers class * {
    @com.squareup.moshi.* <methods>;
}
-keepclassmembers class ** {
  @com.squareup.moshi.FromJson *;
  @com.squareup.moshi.ToJson *;
}
-keep @com.squareup.moshi.JsonQualifier interface *

# Additional rules are needed if you are using Kotlin:
#-keepclassmembers class kotlin.Metadata {
#    public <methods>;
#}

#If you are using the codegen API (i.e. JsonClass(generateAdapter = true)):
#-keep class **JsonAdapter {
#    <init>(...);
#    <fields>;
#}
#-keepnames @com.squareup.moshi.JsonClass class *