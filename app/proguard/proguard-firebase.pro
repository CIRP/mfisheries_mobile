# Add Rules for Keeping Firebase library code by KED
# Development of rules based on the Crashlytics 2

-keep class com.google.firebase.** { *; }
-keep class com.google.firebase.perf.** { *; }
-keepattributes SourceFile, LineNumberTable, *Annotation*