#Proguard

## Description
This folder contains the proguard files that are used to strip unused files and classes from libraries.
One recommendation to make rules more manageable is to use a file for each library that is used.

The rules used in this project is based on github project by Krschultz. https://github.com/krschultz/android-proguard-snippets

Additional rules are added based on the existing rules from the previously existing proguard-rules.pro file