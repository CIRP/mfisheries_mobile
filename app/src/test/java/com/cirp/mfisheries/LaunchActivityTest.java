package com.cirp.mfisheries;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.Robolectric;
import org.robolectric.RobolectricTestRunner;

import static org.junit.Assert.assertNotNull;

@RunWith(RobolectricTestRunner.class)
public class LaunchActivityTest {
    private SettingsActivity activity;

    @Before
    public void setUp() {
        activity = Robolectric
                .setupActivity( SettingsActivity.class );

    }

    @Test
    public void shouldNotBeNull() {
        assertNotNull( activity );
    }
}
