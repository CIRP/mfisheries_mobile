package com.cirp.mfisheries.core;

import android.app.Activity;

import com.cirp.mfisheries.SettingsActivity;
import com.cirp.mfisheries.core.module.Module;
import com.cirp.mfisheries.core.module.ModuleFactory;
import com.cirp.mfisheries.util.ModuleUtil;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.Robolectric;
import org.robolectric.RobolectricTestRunner;

import static org.junit.Assert.assertNotNull;

@RunWith(RobolectricTestRunner.class)
public class ModuleTests {
	
	@Test
	public void verifyModules(){
		Activity activity = Robolectric.setupActivity(SettingsActivity.class);
		
		String [] modulesStrings = ModuleUtil.MODULE_ORDER;
		ModuleFactory factory = ModuleFactory.getInstance(activity);
		assertNotNull(factory);
		for (String moduleName: modulesStrings){
			Module m = factory.getModule(moduleName);
			assertNotNull(m);
		}
	}
}

