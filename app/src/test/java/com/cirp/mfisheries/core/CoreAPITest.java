package com.cirp.mfisheries.core;

import android.app.Activity;
import android.location.Location;

import com.cirp.mfisheries.SettingsActivity;
import com.cirp.mfisheries.alerts.SendAlertActivity;
import com.cirp.mfisheries.alerts.model.AlertDM;
import com.cirp.mfisheries.alerts.model.GroupAdapter;
import com.cirp.mfisheries.alerts.model.GroupDM;
import com.cirp.mfisheries.core.location.LocationService;
import com.cirp.mfisheries.core.location.TrackPointDM;
import com.cirp.mfisheries.core.module.Module;
import com.cirp.mfisheries.core.module.ModuleFactory;
import com.cirp.mfisheries.lek.LEKActivity;
import com.cirp.mfisheries.lek.models.LEKPost;
import com.cirp.mfisheries.sos.SOSService;
import com.cirp.mfisheries.util.CountryUtil;
import com.cirp.mfisheries.util.FileUtil;
import com.cirp.mfisheries.util.ModuleUtil;
import com.cirp.mfisheries.util.NetUtil;
import com.cirp.mfisheries.util.PrefsUtil;
import com.cirp.mfisheries.util.SMSUtil;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.Robolectric;
import org.robolectric.RobolectricTestRunner;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertNotNull;
import static junit.framework.Assert.assertNull;
import static junit.framework.Assert.assertTrue;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.fail;


@RunWith(RobolectricTestRunner.class)
public class CoreAPITest {

	private Activity activity;
	private String[] moduleNames;
	private ModuleFactory moduleFactory;

	@Before
	public void setup() {
		//ShadowGooglePlayServicesUtil.setIsGooglePlayServicesAvailable(ConnectionResult.SUCCESS);
		moduleNames = ModuleUtil.MODULE_ORDER;
		List<String> selectedModules = Arrays.asList(moduleNames);
		FileUtil.saveModules(selectedModules);
		activity = Robolectric.buildActivity(SettingsActivity.class).create().get();
		moduleFactory = ModuleFactory.getInstance(activity.getApplicationContext());
	}

	//Tests that each API endpoint used for retrieving data exists at the expected URL.
	@Test
	public void doGetEndpointsExist() {
		assertTrue(NetUtil.exists(String.format("%s%s&%s=%s",NetUtil.API_URL, NetUtil.GET_COUNTRIES_PATH, NetUtil.API_KEY_PARAM, NetUtil.API_KEY)));
	}

	//Tests that the countries the app is available to can be loaded from the server and the countries received are supported in the app.
	@Test
	public void areCountriesAvailable() {
		CountryUtil.getInstance(activity).retrieveCountriesFromServer(new CountryUtil.CountryRetrievedListener() {
			@Override
			public void processCountries(List<Country> countries) {
				assertNotNull(countries);
				assertNotEquals(countries.size(), 0);
			}
		});
	}
	
	@Test
	public void areCountryLocationsAvailable() {
		CountryUtil.getInstance(activity).retrieveCountryLocationsFromServer(new CountryUtil.CountryLocRetrievedListener() {
			@Override
			public void processCountryLocations(List<CountryLoc> countryLocs) {
				assertNotNull(countryLocs);
				assertNotEquals(countryLocs.size(), 0);
			}
		});
	}

	//Ensures the phone numbers and email addresses of the authority for each country is available.
//	@Test
	public void hasAuthorityData() {
		Ion.with(activity)
				.load(NetUtil.API_URL + NetUtil.GET_COUNTRIES_PATH)
				.setHeader(NetUtil.API_KEY_PARAM, NetUtil.API_KEY)
				.asJsonObject()
				.setCallback(new FutureCallback<JsonObject>() {
					@Override
					public void onCompleted(Exception e, JsonObject result) {
						if (e != null)
							fail("Auth not available " + e.getMessage());
						else {
							int status = result.get("status").getAsInt();
							if (status != 200) {
								fail("Error Occurred reading authorities from server");
							} else {
								JsonArray array = result.getAsJsonArray("data");
								for (int i = 0; i < array.size(); i++) {
									JsonObject object = array.get(i).getAsJsonObject();
									assertTrue(object.has("authority"));
									assertTrue(object.getAsJsonObject("authority").has("mobileNum"));
									assertTrue(object.getAsJsonObject("authority").has("email"));
								}
							}
						}
					}
				});
	}

	//Ensures that modules are available on the server for each available country.
	@Test
	public void canGetModules() {
		int[] countryIds = new int[]{1, 2, 3, 4};
		// For each country code
		for (int cid : countryIds) {
			final String url = NetUtil.API_URL + NetUtil.GET_MODULES_PATH + "?countryid=" + cid;
			Ion.with(activity)
					.load(url)
					.setHeader(NetUtil.API_KEY_PARAM, NetUtil.API_KEY)
					.asJsonArray()
					.setCallback(new FutureCallback<JsonArray>() {
						@Override
						public void onCompleted(Exception e, JsonArray result) {
							if (e != null)
								fail("Modules not available " + e.getMessage());
							else {
								assertNotEquals(result.size(), 0);
								for (int i = 0; i < result.size(); i++) {
									JsonObject object = result.get(i).getAsJsonObject();
									assertNotNull(object.get("module").getAsString());
								}
							}
						}
					});
		}

	}

	//Tests that the downloadable module data is available at the expected URLs.
	@Test
	public void doModuleDownloadsExist() {
		String [] countryPaths = {
				"static/country%5Fmodules/atb",
				"static/country%5Fmodules/dominica",
				"static/country%5Fmodules/grenada",
				"static/country%5Fmodules/skn",
				"static/country%5Fmodules/slu",
				"static/country%5Fmodules/svg",
				"static/country%5Fmodules/tobago",
				"static/country%5Fmodules/trinidad"
		};
		for (String country: countryPaths) {
			System.out.print("Running Test for: " + country);
			PrefsUtil.setPath(activity, country);
			for (String moduleId : moduleNames) {
				System.out.println("Loading module: " + moduleId);
				Module module = moduleFactory.getModule(moduleId);
				assertNotNull(module);
				if (module.hasDownload()) {
					assertTrue(NetUtil.exists(module.getDataLoc()));
				}
			}
		}
	}

//	@Test
	public void canSendTrack() {
		Location loc = new Location("");
		loc.setLatitude(11.2956943);
		loc.setLongitude(-60.8228397);
		loc.setBearing(231);
		loc.setSpeed(100);

		Location loc2 = new Location("");
		loc2.setLatitude(22.2956943);
		loc2.setLongitude(60.8228397);
		loc2.setBearing(31);
		loc2.setSpeed(10);

		ArrayList<TrackPointDM> points = new ArrayList<>();

//		LocationService service = Robolectric.buildService(LocationService.class).attach().create().get();
		
		LocationService service = null;
		if (service != null) {
			TrackPointDM trackPoint1 = service.createTrackPoint(loc, 1);
			TrackPointDM trackPoint2 = service.createTrackPoint(loc2, 1);
			points.add(trackPoint1);
			points.add(trackPoint2);
			
			JsonArray jsonArray = service.getJsonElements(points);
			assertNotNull(jsonArray);
			
			service.sendTracks(points, new FutureCallback<JsonObject>() {
				@Override
				public void onCompleted(Exception e, JsonObject result) {
					assertNull(e);
					int status = result.get("status").getAsInt();
					assertEquals(201, status);
				}
			});
		}
	}

//	@Test
	public void canSendSOS() {
		Location location = new Location("");
		location.setLatitude(11.2956943);
		location.setLongitude(-60.8228397);
		location.setBearing(231);
		location.setSpeed(100);

		TrackPointDM trackPointDM = SMSUtil.createSOS(location, activity);

		ArrayList<TrackPointDM> points = new ArrayList<>();
		points.add(trackPointDM);

//		SOSService service = Robolectric.buildService(SOSService.class).attach().create().get();
		SOSService service = null;
		if (service != null) {
			JsonArray jsonArray = service.getJsonElements(points);
			assertNotNull(jsonArray);
			
			service.sendSOS(jsonArray, new FutureCallback<JsonObject>() {
				@Override
				public void onCompleted(Exception e, JsonObject result) {
					if (e != null)
						fail("SOS not sent " + e.getMessage());
					int status = result.get("status").getAsInt();
					assertEquals(201, status);
				}
			});
		}
	}

//	@Test
	public void canSubscribe() {
		GroupAdapter groupAdapter = new GroupAdapter(activity, new ArrayList<GroupDM>());
		GroupDM groupDM = new GroupDM(1, "group", false, 1);
		groupAdapter.subscribe(groupDM, null);
	}

//	@Test
	public void canSendAlert() {
		SimpleDateFormat format = new SimpleDateFormat("yyyy-M-d hh:mm:ss", Locale.ENGLISH);

		AlertDM alert = new AlertDM(1, 1, "10.281", "-61.459", format.format(new Date()), "lorem ipsum");

		SendAlertActivity activity = Robolectric.buildActivity(SendAlertActivity.class).create().get();

		JsonObject jsonObject = alert.getJsonObject();
		assertNotNull(jsonObject);

		activity.sendAlert(jsonObject, new FutureCallback<JsonObject>() {
			@Override
			public void onCompleted(Exception e, JsonObject result) {
				if (e == null && result != null) {
					int status = result.get("status").getAsInt();
					assertEquals(200, status);
				} else {
					assertNull(e);
				}
			}
		});
	}

//	@Test
	public void canSendLEK() {
		LEKPost lekPost = new LEKPost();
		lekPost.latitude = "10.281";
		lekPost.longitude = "-61.459";
		lekPost.aDate = new Date().toString();
		lekPost.text = "lorem ipsum dolor";
		lekPost.timestamp = new Date().toString();
		lekPost.filetype = "text/*";
		lekPost.userid = "1";
		lekPost.countryid = "1";

		LEKActivity lekActivity = Robolectric.buildActivity(LEKActivity.class).create().get();
		lekActivity.sendLEK(lekPost, null, new FutureCallback<JsonObject>() {
			@Override
			public void onCompleted(Exception e, JsonObject result) {
				if (e != null)
					fail("LEK not sent " + e.getMessage());
				else {
					int status = result.get("status").getAsInt();
					assertEquals(201, status);
				}
			}
		});
	}

}