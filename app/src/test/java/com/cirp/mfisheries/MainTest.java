package com.cirp.mfisheries;

import android.app.AlertDialog;
import android.content.Intent;
import android.os.Bundle;
import android.telephony.SmsManager;
import android.util.Log;
import android.widget.Adapter;
import android.widget.GridView;

import com.cirp.mfisheries.core.Country;
import com.cirp.mfisheries.core.fcm.NotifyCloudMessagingService;
import com.cirp.mfisheries.core.module.Module;
import com.cirp.mfisheries.core.module.ModuleActivity;
import com.cirp.mfisheries.core.module.ModuleAdapter;
import com.cirp.mfisheries.core.module.ModuleFactory;
import com.cirp.mfisheries.util.FileUtil;
import com.cirp.mfisheries.util.ModuleUtil;
import com.cirp.mfisheries.util.NetUtil;
import com.cirp.mfisheries.util.SMSUtil;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;
import com.squareup.moshi.JsonAdapter;
import com.squareup.moshi.Moshi;
import com.squareup.moshi.Types;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.Robolectric;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.Shadows;
import org.robolectric.shadows.ShadowAlertDialog;
import org.robolectric.shadows.ShadowLog;
import org.robolectric.shadows.ShadowSmsManager;

import java.io.IOException;
import java.lang.reflect.Type;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

import static junit.framework.Assert.assertNotNull;
import static org.hamcrest.CoreMatchers.instanceOf;
import static org.hamcrest.core.IsEqual.equalTo;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

@RunWith(RobolectricTestRunner.class)
public class MainTest {

	private MainActivity activity;
	private String[] moduleNames;
	private ModuleFactory moduleFactory;

	@Before
	public void setup() {
		ShadowLog.stream = System.out;

		moduleNames = ModuleUtil.MODULE_ORDER;
		List<String> selectedModules = Arrays.asList(moduleNames);
		FileUtil.saveModules(selectedModules);
		activity = Robolectric.setupActivity(MainActivity.class);
		moduleFactory = ModuleFactory.getInstance(activity.getApplicationContext());
	}

	//Ensures that all available modules extend the Module class.
	@Test
	public void doModulesExtendClass() {
		for (String name : moduleNames) {
			System.out.println(name + "");
			Module module = moduleFactory.getModule(name);
			assertNotNull(module);
			assertThat(module, instanceOf(Module.class));
		}
	}

	//Tests that activities displayed for each module extends the ModuleActivity class.
    @Test
    public void doActivitiesDisplayedExtendClass() {
		// Set all Modules to be displayed
		activity.displayModules(Arrays.asList(ModuleUtil.MODULE_ORDER));
  
		// Load the GridView that displays Module icons
		GridView gridView = activity.findViewById(R.id.gridView);
        assertNotNull(gridView);
        
        ModuleAdapter adapter = (ModuleAdapter)gridView.getAdapter();
        int size = adapter.getCount();
        assertEquals(size, Arrays.asList(ModuleUtil.MODULE_ORDER).size());
        
        for(int i = 0; i < size; i++) {
			Module module = moduleFactory.getModule(adapter.getItem(i));
			System.out.println(module.getName() + "");
			
	        assertNotNull(module);
			module.display();

			try {
				Intent intent = Shadows.shadowOf(activity).peekNextStartedActivityForResult().intent;
				Class<?> clazz = Class.forName(intent.getComponent().getClassName());
				Object activity2 = clazz.newInstance();
				assertTrue(activity2 instanceof ModuleActivity);
			} catch (Exception e) {
				e.printStackTrace();
			}
        }
    }

	//Ensures that each module can be opened without any additional module installed.
	@Test
	public void canModulesOpen() {
		activity.displayModules(Arrays.asList(ModuleUtil.MODULE_ORDER));
		GridView gridView = activity.findViewById(R.id.gridView);
		ModuleAdapter adapter = (ModuleAdapter)gridView.getAdapter();
		int size = adapter.getCount();
		for(int i = 0; i < size; i++) {
			Module module = moduleFactory.getModule(adapter.getItem(i));
			if(module == null) {
				fail("Module " + adapter.getItem(i) + " is not returned by factory");
			}
			else {
				module.display();
			}
		}
	}

	//Tests that all modules available on the server are available in the ModuleFactory.
	@Test
	public void isFactorySetupForAllModules() {
		Ion.with(activity)
				.load(NetUtil.API_URL + NetUtil.GET_MODULES_PATH + "1")
				.setHeader(NetUtil.API_KEY_PARAM, NetUtil.API_KEY)
				.asJsonObject()
				.setCallback(new FutureCallback<JsonObject>() {
					@Override
					public void onCompleted(Exception e, JsonObject result) {
						JsonArray array = result.getAsJsonArray("data");
						for (int i = 0; i < array.size(); i++) {
							JsonObject object = array.get(i).getAsJsonObject();
							Module module = moduleFactory.getModule(object.get("name").getAsString());
							assertTrue(module != null);
						}
					}
				});
	}

	//Ensures the user can select their country through an AlertDialog and a list of modules is shown upon selection.
//	@Test
	public void canSelectCountries(){
		Ion.with(activity)
				.load(NetUtil.API_URL + NetUtil.GET_COUNTRIES_PATH)
				.setHeader(NetUtil.API_KEY_PARAM, NetUtil.API_KEY)
				.asJsonObject()
				.setCallback(new FutureCallback<JsonObject>() {
					@Override
					public void onCompleted(Exception e, JsonObject result) {
						int status = result.get("status").getAsInt();
						System.out.println("Status: " + status);
						if (status == 200) {
							JsonArray array = result.getAsJsonArray("data");

							Moshi moshi = new Moshi.Builder().build();
							Type listMyData = Types.newParameterizedType(List.class, Country.class);
							JsonAdapter<List<Country>> jsonAdapter = moshi.adapter(listMyData);

							List<Country> countries = null;
							try {
								countries = jsonAdapter.fromJson(array.toString());
							} catch (IOException e1) {
								fail("Json not decoded correctly: " + e.getMessage());
							}

							activity.selectCountry(countries);
							AlertDialog alert = ShadowAlertDialog.getLatestAlertDialog();
							ShadowAlertDialog sAlert = Shadows.shadowOf(alert);

							Adapter adapter = sAlert.getAdapter();
							int count = adapter.getCount();
							for (int i = 0; i < count; i++) {
								System.out.println(adapter.getItem(i));
								sAlert.clickOnItem(i);

								AlertDialog moduleAlert = ShadowAlertDialog.getLatestAlertDialog();
								ShadowAlertDialog sModuleAlert = Shadows.shadowOf(moduleAlert);

								Log.d("MainTest", sModuleAlert.getTitle() + ", " + activity.getString(R.string.dialog_modules));
								assertTrue(sModuleAlert.getTitle().equals(activity.getString(R.string.dialog_modules)));
								sModuleAlert.dismiss();
								sAlert.show();
							}
						}
						else{
							fail("Countries not received from server");
						}
					}
				});
	}

	//Ensures that the add module dialog is displayed when the button is selected.
	@Test
	public void isAddModuleDialogDisplayed() {
		activity.selectModules(ModuleUtil.MODULE_ORDER);

		AlertDialog alert = ShadowAlertDialog.getLatestAlertDialog();
		ShadowAlertDialog sAlert = Shadows.shadowOf(alert);
		assertThat(sAlert.getTitle().toString(), equalTo(activity.getString(R.string.dialog_modules)));
	}

	//Verifies that the user can add new modules during runtime.
	@Test
	public void canAddModules() {
		activity.selectModules(ModuleUtil.MODULE_ORDER);

		AlertDialog alert = ShadowAlertDialog.getLatestAlertDialog();
		ShadowAlertDialog sAlert = Shadows.shadowOf(alert);

		List<String> modules = activity.getSelectedModules();
		if(modules.size() == 0) {
			Adapter adapter = sAlert.getAdapter();
			int count = adapter.getCount();
			for (int i = 0; i < count; i++) {
				sAlert.clickOnItem(i);
			}
		}
		sAlert.clickOn(android.R.id.button1);
		System.out.println(activity.getSelectedModules());
	}

	//Verifies that the user can remove modules during runtime.
	@Test
	public void canRemoveModules() {
		activity.selectModules(ModuleUtil.MODULE_ORDER);

		AlertDialog alert = ShadowAlertDialog.getLatestAlertDialog();
		ShadowAlertDialog sAlert = Shadows.shadowOf(alert);

		List<String> modules = activity.getSelectedModules();
		if(modules.size() != 0) {
			Adapter adapter = sAlert.getAdapter();
			int count = adapter.getCount();
			for (int i = 0; i < count; i++) {
				sAlert.clickOnItem(i);
			}
		}
		sAlert.clickOn(android.R.id.button1);
		System.out.println(activity.getSelectedModules());
	}

	//Verifies that the Add Module tile is displayed when an odd number of modules is selected.
	@Test
	public void isAddModuleBlockDisplayed() {
		List<String> modules = new LinkedList<>(Arrays.asList(ModuleUtil.MODULE_ORDER));
		if (modules.size() % 2 == 0) {
			modules.remove(0);
		}

		activity.displayModules(modules);

		//View view = activity.findViewById(R.id.placeholder);
		GridView view = activity.findViewById(R.id.gridView);
		ModuleAdapter adapter = (ModuleAdapter) view.getAdapter();

		assertTrue(adapter.getCount() % 2 == 0);
	}

//	@Test
	public void isSMSSent() {
		String message = "Testing123";
		String phoneNumber = "1231231222";

//		SMSUtil.sendSMS(activity, message, phoneNumber);
		SMSUtil.composeSMSMessage(activity, message, phoneNumber);

		ShadowSmsManager shadowSmsManager = Shadows.shadowOf(SmsManager.getDefault());
		ShadowSmsManager.TextMultipartParams lastSentTextMessageParams = shadowSmsManager.getLastSentMultipartTextMessageParams();

		assertEquals(phoneNumber, lastSentTextMessageParams.getDestinationAddress());
		assertEquals(message, lastSentTextMessageParams.getParts().get(0));
	}

//	@Test
	public void canReceiveAlert(){
	
//		NotifyCloudMessagingService service = Robolectric.buildService(NotifyCloudMessagingService.class).attach().create().get();
		NotifyCloudMessagingService service = null;
		Bundle bundle = new Bundle();

		String string = "{'msg':'hello','user':'1','time':'','lat':'12','lng':'34.12'}";
		bundle.putString("message", string);
		service.onMessageReceived(null);
	}
}
