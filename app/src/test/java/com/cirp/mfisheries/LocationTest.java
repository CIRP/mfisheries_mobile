package com.cirp.mfisheries;


import android.app.Activity;

import com.cirp.mfisheries.core.location.Geofencer;
import com.cirp.mfisheries.core.location.LocationService;
import com.cirp.mfisheries.util.FileUtil;
import com.cirp.mfisheries.util.LocationUtil;
import com.cirp.mfisheries.util.ModuleUtil;
import com.cirp.mfisheries.util.PrefsUtil;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.Robolectric;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.android.controller.ServiceController;
import org.robolectric.shadows.ShadowLog;

import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.assertEquals;


@RunWith(RobolectricTestRunner.class)
public class LocationTest {

    private Activity activity;

    @Before
    public void setup() {
        ShadowLog.stream = System.out;

        String[] moduleNames = ModuleUtil.MODULE_ORDER;
        List<String> selectedModules = Arrays.asList(moduleNames);
        FileUtil.saveModules(selectedModules);

        activity = Robolectric.buildActivity(SettingsActivity.class).create().get();
        PrefsUtil.setCountry(activity, "Trinidad");
    }

    @Test
    public void testGeofencer(){
        assertEquals(true, Geofencer.isWithinCountry(activity, 10.455657, -61.228503));
        assertEquals(false, Geofencer.isWithinCountry(activity, 10.420475, -61.596461));
    }

    @Test
    public void canLocationServiceStart(){
        ServiceController<LocationService> controller = Robolectric.buildService(LocationService.class);
        controller.create();
    }

    @Test
    public void areCoordinatesConverted(){
        String lat = LocationUtil.convertLatitude(12.232);
        assertEquals("12° 13' 55.20\" N", lat);

        String lng = LocationUtil.convertLongitude(-10.23);
        assertEquals("10° 13' 48.00\" W", lng);
    }

}
