package com.cirp.mfisheries;

import android.location.Location;

import com.cirp.mfisheries.core.location.Geofencer;
import com.cirp.mfisheries.core.location.LocationService;
import com.cirp.mfisheries.core.module.ModuleFactory;
import com.cirp.mfisheries.util.FileUtil;
import com.cirp.mfisheries.util.ModuleUtil;
import com.cirp.mfisheries.util.PrefsUtil;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.Robolectric;
import org.robolectric.RobolectricTestRunner;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Arrays;
import java.util.List;

import static junit.framework.Assert.assertFalse;
import static junit.framework.Assert.assertNotNull;
import static junit.framework.Assert.assertNull;
import static junit.framework.Assert.fail;
import static org.junit.Assert.assertTrue;

@RunWith(RobolectricTestRunner.class)
//@Config(constants = BuildConfig.class, sdk = 21)
public class TrackingTests {

	private MainActivity activity;
	private String[] moduleNames;
	private ModuleFactory moduleFactory;

	@Before
	public void setup() {
		//ShadowGooglePlayServicesUtil.setIsGooglePlayServicesAvailable(ConnectionResult.SUCCESS);
		moduleNames = ModuleUtil.MODULE_ORDER;
		List<String> selectedModules = Arrays.asList(moduleNames);
		FileUtil.saveModules(selectedModules);
		activity = Robolectric.buildActivity(MainActivity.class).create().get();
		moduleFactory = ModuleFactory.getInstance(activity.getApplicationContext());

		PrefsUtil.setCountry(activity, "trinidad");
	}

	public boolean exists(String url) {
		try {
			URL u = new URL(url);
			HttpURLConnection huc = (HttpURLConnection) u.openConnection();
			huc.setRequestMethod("GET");
			huc.connect();
			int code = huc.getResponseCode();
			System.out.println(code);

			return code == 200;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}

	public JsonArray getJSONFor(String position) throws IOException {
		String urlStr = "http://localhost:8080/" + position + ".json";
		assertTrue(exists(urlStr));

		URL u = new URL(urlStr);
		HttpURLConnection c = (HttpURLConnection) u.openConnection();
		c.setRequestMethod("GET");
		c.setRequestProperty("Content-length", "0");
		c.setUseCaches(false);
		c.setAllowUserInteraction(false);
		c.setConnectTimeout(1000);
		c.setReadTimeout(1000);
		c.connect();

		BufferedReader br = new BufferedReader(new InputStreamReader(c.getInputStream()));
		StringBuilder sb = new StringBuilder();
		String line;
		while ((line = br.readLine()) != null) {
			sb.append(line).append("\n");
		}
		br.close();
		String jsonString = sb.toString();

		JsonElement json = new JsonParser().parse(jsonString);
		JsonArray result = json.getAsJsonArray();
		assertTrue(result.size() > 0);

		return result;
	}

	    @Test
	public void testInsideGeo() throws IOException {
		JsonArray result = getJSONFor("inside");
		assertNotNull(result);
		assertNull(Geofencer.loadJsonFromFile(activity));
		assertNotNull(Geofencer.loadJsonFromAsset(activity));

		for (int i = 0; i < result.size(); i++) {
			JsonObject loc = result.get(i).getAsJsonObject();
			int longitude = (int) (loc.get("Longitude").getAsFloat());
			int latitude = (int) (loc.get("Latitude").getAsFloat());

			System.out.printf("%f, %f \n", loc.get("Longitude").getAsFloat(), loc.get("Latitude").getAsFloat());
			Boolean withinCountry = Geofencer.isWithinCountry(activity, latitude, longitude);
			if (withinCountry == null) {
				fail("Failed to test filepath");
			} else {
				assertTrue(withinCountry);
			}
		}
	}

	    @Test
	public void testOutsideGeo() throws IOException {
		JsonArray result = getJSONFor("outside");
		assertNotNull(result);
		assertNull(Geofencer.loadJsonFromFile(activity));
		assertNotNull(Geofencer.loadJsonFromAsset(activity));

		for (int i = 0; i < result.size(); i++) {
			JsonObject loc = result.get(i).getAsJsonObject();
			int longitude = (int) (loc.get("Longitude").getAsFloat());
			int latitude = (int) (loc.get("Latitude").getAsFloat());

			System.out.printf("%f, %f \n", loc.get("Longitude").getAsFloat(), loc.get("Latitude").getAsFloat());
			Boolean withinCountry = Geofencer.isWithinCountry(activity, latitude, longitude);
			if (withinCountry == null) {
				fail("Failed to test filepath");
			} else {
				assertFalse(withinCountry);
			}
		}
	}


	@Test
	public void testSendServer() throws IOException {
		LocationService locService = new LocationService();
		JsonArray result = getJSONFor("outside");
		assertNotNull(result);
		assertNull(Geofencer.loadJsonFromFile(activity));
		assertNotNull(Geofencer.loadJsonFromAsset(activity));

		for (int i = 0; i < result.size(); i++) {
			JsonObject jsonLoc = result.get(i).getAsJsonObject();
			Location loc = new Location("");
			loc.setAccuracy(1);
			loc.setBearing(0);
			loc.setSpeed(0);
			loc.setAltitude(jsonLoc.get("Altitude").getAsFloat());
			loc.setLatitude(jsonLoc.get("Latitude").getAsFloat());
			loc.setLongitude(jsonLoc.get("Longitude").getAsFloat());
			System.out.printf("Storing (%f, %f) \n", loc.getLongitude(), loc.getLatitude());
//            locService.saveTrack(loc, "track");
		}
	}


}
