package com.cirp.mfisheries;

import android.app.Application;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.widget.GridView;

import com.cirp.mfisheries.core.module.Module;
import com.cirp.mfisheries.core.module.ModuleAdapter;
import com.cirp.mfisheries.core.module.ModuleFactory;
import com.cirp.mfisheries.util.FileUtil;
import com.cirp.mfisheries.util.ModuleUtil;

import org.robolectric.Robolectric;
import org.robolectric.RuntimeEnvironment;
import org.robolectric.Shadows;
import org.robolectric.shadows.ShadowConnectivityManager;
import org.robolectric.shadows.ShadowLog;
import org.robolectric.shadows.ShadowNetworkInfo;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;

import static junit.framework.Assert.assertNotNull;
import static org.junit.Assert.fail;

//@RunWith(RobolectricTestRunner.class)
//@Config(constants = BuildConfig.class, sdk = 21)
public class OfflineTest {

    private ConnectivityManager connectivityManager;
    private ShadowConnectivityManager shadowConnectivityManager;
    private ShadowNetworkInfo shadowOfActiveNetworkInfo;

//    @Before
    public void setUp() {
        ShadowLog.stream = System.out;
        connectivityManager = getConnectivityManager();
        shadowConnectivityManager = Shadows.shadowOf(connectivityManager);
        shadowOfActiveNetworkInfo = Shadows.shadowOf(connectivityManager.getActiveNetworkInfo());

        //turn off network connections
        NetworkInfo networkInfo = ShadowNetworkInfo.newInstance(NetworkInfo.DetailedState.DISCONNECTED, ConnectivityManager.TYPE_MOBILE, 0, false, false);
        shadowConnectivityManager.setActiveNetworkInfo(networkInfo);
    }

//    @Test
    public void canAppOpenOffline() {
        List<String> selectedModules = Arrays.asList(ModuleUtil.MODULE_ORDER);
        FileUtil.saveModules(selectedModules);

        MainActivity activity = Robolectric.buildActivity(MainActivity.class).create().get();
        assertNotNull(activity);
    }

//    @Test
    public void canModulesOpenOffline() {
        List<String> selectedModules = Arrays.asList(ModuleUtil.MODULE_ORDER);
        FileUtil.saveModules(selectedModules);

        MainActivity activity = Robolectric.buildActivity(MainActivity.class).create().get();
        ModuleFactory moduleFactory = ModuleFactory.getInstance(activity.getApplicationContext());

        activity.displayModules(Arrays.asList(ModuleUtil.MODULE_ORDER));
        GridView gridView = activity.findViewById(R.id.gridView);
        ModuleAdapter adapter = (ModuleAdapter)gridView.getAdapter();
        int size = adapter.getCount();
        for(int i = 0; i < size; i++) {
            Module module = moduleFactory.getModule(adapter.getItem(i));
            if(module == null) {
                fail("Module " + adapter.getItem(i) + " is not returned by factory");
            }
            else {
                module.display();
            }
        }
    }

    private ConnectivityManager getConnectivityManager() {
        Application application = RuntimeEnvironment.application;
        if(application == null)
            System.out.println("is null");
        return (ConnectivityManager) RuntimeEnvironment.application.getSystemService(Context.CONNECTIVITY_SERVICE);
    }
}
