package com.cirp.mfisheries.nav;

import android.support.test.filters.LargeTest;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;

import com.cirp.mfisheries.R;

import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.matcher.ViewMatchers.withId;

@RunWith(AndroidJUnit4.class)
@LargeTest
public class UseNavInstrumentationTest {
	
	@Rule
	public ActivityTestRule mActivityRule = new ActivityTestRule<>(NavigationActivity.class);
	
	@Test
	public void viewMarkers(){
		onView(withId(R.id.navigation_my_markers)).perform(click());
	}
	
	@Test
	public void launchNavigation(){
		onView(withId(R.id.navigation_navigate)).perform(click());
	}
	
	@Test
	public void launchAddMarker(){
		onView(withId(R.id.navigation_add_marker)).perform(click());
		
	}
	
	@Test
	public void launchNavMyLoc(){
		onView(withId(R.id.navigation_my_location)).perform(click());
	}
	
	@Test
	public void launchCompass(){
		onView(withId(R.id.navigation_compass_toggle)).perform(click());
	}
}
