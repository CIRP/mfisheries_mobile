package com.cirp.mfisheries.alerts;

import android.support.test.rule.ActivityTestRule;
import android.content.Context;
import android.support.test.filters.MediumTest;
import android.support.test.runner.AndroidJUnit4;

import com.cirp.mfisheries.util.CountryUtil;
import com.cirp.mfisheries.util.NetUtil;
import com.cirp.mfisheries.weather.SourceActivity;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static junit.framework.Assert.assertTrue;

@MediumTest
@RunWith(AndroidJUnit4.class)
public class AlertUtilTest {
	
	@Rule
	public ActivityTestRule<SourceActivity> mActivityRule = new ActivityTestRule<>(SourceActivity.class);
	Context context;
	// Utilities
	private CountryUtil countryUtil;
	private AlertUtils alertUtils;
	private final String TAG = "WeatherUtilTest";
	
	@Before
	public void setup(){
		context = mActivityRule.getActivity().getBaseContext();
		countryUtil = CountryUtil.getInstance(context);
		alertUtils = new AlertUtils(context);
	}
	
	@Test
	public void doEndPointsExist(){
		assertTrue(NetUtil.exists(alertUtils.getGroupsURL()));
//		assertTrue(NetUtil.exists(alertUtils.getAlertAPI()));
	}
}
