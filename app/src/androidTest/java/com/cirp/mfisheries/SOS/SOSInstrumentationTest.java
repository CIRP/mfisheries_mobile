package com.cirp.mfisheries.SOS;

import android.Manifest;
import android.os.Build;
import android.support.test.filters.LargeTest;
import android.support.test.rule.ActivityTestRule;
import android.support.test.rule.ServiceTestRule;
import android.support.test.runner.AndroidJUnit4;

import com.cirp.mfisheries.sos.SOSActivity;
import com.cirp.mfisheries.util.SOSUtil;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static android.support.test.InstrumentationRegistry.getInstrumentation;
import static android.support.test.InstrumentationRegistry.getTargetContext;
import static junit.framework.Assert.assertNotNull;

@RunWith(AndroidJUnit4.class)
@LargeTest
public class SOSInstrumentationTest {

    @Rule
    public ActivityTestRule mActivityRule = new ActivityTestRule<>(SOSActivity.class);

    @Rule
    public ServiceTestRule mServiceRule = new ServiceTestRule();

    @Before
    public void grantPhonePermission(){
        String [] permissions = new String[]{
                Manifest.permission.SEND_SMS,
                Manifest.permission.CALL_PHONE,
                Manifest.permission.ACCESS_FINE_LOCATION
        };

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M){
            for(String permission : permissions){
                getInstrumentation()
                        .getUiAutomation()
                        .executeShellCommand("pm grant "+getTargetContext().getPackageName()+permission);
            }
        }
    }

    private SOSUtil sosUtil = new SOSUtil(getTargetContext(), 10.0, 20.0);


    @Test
    public void testHasMobileNumbers(){
        assertNotNull("Mobile Number for SOS was null", sosUtil.getSOSPrimary());
        assertNotNull("Additional Number for SOS was null", sosUtil.getSOSExtra());
        assertNotNull("Number to call landline was null", sosUtil.getSOSLandPrimary());
    }
    
    @Test
    public void testSMSMessage(){
        assertNotNull("SMS Message was null", sosUtil.generateSOSSMSMessage());
        //TODO check message to ensure that it is in the correct format
    }
    
    @Test
    public void testSMS(){
        sosUtil.performSmsCall();
    }

//    @Test
    public void testVoiceCall(){
        sosUtil.performVoiceCall();
    }

}
