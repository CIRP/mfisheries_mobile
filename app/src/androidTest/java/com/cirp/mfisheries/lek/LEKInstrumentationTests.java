package com.cirp.mfisheries.lek;

import android.support.test.filters.LargeTest;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;


import com.cirp.mfisheries.R;

import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withId;

@RunWith(AndroidJUnit4.class)
@LargeTest
public class LEKInstrumentationTests {
	@Rule
	public ActivityTestRule mActivityRule = new ActivityTestRule<>(LEKActivity.class);
	
	@Test
	public void runLEKActivity(){
		onView(withId(R.id.map)).check(matches(isDisplayed()));
		
//		onView(withId(R.id.lek_fab)).perform(click());
	}
}
