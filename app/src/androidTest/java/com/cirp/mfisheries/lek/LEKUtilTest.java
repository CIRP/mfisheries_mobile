package com.cirp.mfisheries.lek;

import android.content.Context;
import android.support.annotation.Nullable;
import android.support.test.filters.MediumTest;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;
import android.util.Log;

import com.cirp.mfisheries.core.Country;
import com.cirp.mfisheries.lek.models.LEKCategory;
import com.cirp.mfisheries.lek.models.LEKPost;
import com.cirp.mfisheries.util.CountryUtil;
import com.cirp.mfisheries.util.NetUtil;
import com.cirp.mfisheries.util.PrefsUtil;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.List;

import static junit.framework.Assert.assertNotNull;
import static junit.framework.Assert.assertTrue;

@MediumTest
@RunWith(AndroidJUnit4.class)
public class LEKUtilTest {
	
	@Rule
	public ActivityTestRule<LEKActivity> mActivityRule = new ActivityTestRule<>(LEKActivity.class);
	Context context;
	private LEKUtils util;
	private CountryUtil countryUtil;
	private final String TAG = "LEKUtilTest";
	
	@Before
	public void setup() {
		context = mActivityRule.getActivity().getBaseContext();
		util = new LEKUtils(context);
		countryUtil = CountryUtil.getInstance(context);
	}
	@Test
	public void doEndPointsExist(){
		// Test Base API for weather source
		assertTrue(NetUtil.exists(util.getAPI()));
		assertTrue(NetUtil.exists(util.getCategoriesAPI()));
	}
	
	@Test
	public void retrieveCountryCategories(){
		countryUtil.retrieveCountries(new CountryUtil.CountryRetrievedListener() {
			public void processCountries(List<Country> countries) {
				assertTrue(countries.size() > 1);
				for (final Country country : countries){
					PrefsUtil.setCountryId(context, country.getId());
					util.retrieveCategories(new LEKUtils.CategoryListener() {
						public void onReceived(@Nullable List<LEKCategory> categories) {
							assertNotNull(categories);
							Log.d(TAG, "Retrieved Categories for: " + country.getName());
							assertTrue(categories.size() > 0);
							for(LEKCategory cat: categories){
								Log.d(TAG, String.format("Country: %s and ID: %s",  country.getId(),cat.countryid ));
								assertTrue(country.getId().equalsIgnoreCase(cat.countryid));
								Log.d(TAG, String.format("Country Name: %s and Name: %s",  country.getName(),cat.country ));
								assertTrue(country.getName().equalsIgnoreCase(cat.country));
							}
						}
					});
				}
			}
		});
	}
	
	@Test
	public void saveLEKPost(){
		LEKPost post = new LEKPost();
		post.text="This is a TEST";
		post.userid = "2";
		post.countryid = "3";
		post.filepath = "";
		post.filetype="text";
		post.filepath="";
		post.aDate="";
		post.category="environmental";
		post.fullname="mFisheriesTest";
//		util.save(post, new LEKUtils.PostUpdateListener() {
//			@Override
//			public void onUpdated(LEKPost post, boolean status) {
//				assertTrue(status);
//				assertNotNull(post);
//			}
//		});
	}
}
