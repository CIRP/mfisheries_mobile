package com.cirp.mfisheries.core.download;

import android.app.DownloadManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.Uri;
import android.support.test.InstrumentationRegistry;
import android.support.test.filters.LargeTest;
import android.support.test.rule.ActivityTestRule;
import android.support.test.rule.ServiceTestRule;
import android.support.test.runner.AndroidJUnit4;
import android.util.Log;

import com.cirp.mfisheries.MainActivity;
import com.cirp.mfisheries.core.module.Module;
import com.cirp.mfisheries.core.module.ModuleFactory;
import com.cirp.mfisheries.util.FileUtil;
import com.cirp.mfisheries.util.PrefsUtil;
import com.cirp.mfisheries.util.Zipper;

import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeoutException;

import static junit.framework.Assert.assertNotNull;
import static junit.framework.Assert.assertTrue;

@RunWith(AndroidJUnit4.class)
@LargeTest
public class DownloadInstrumentationTest {
	@Rule
	public ActivityTestRule mActivityRule = new ActivityTestRule<>(MainActivity.class);
	@Rule
	public final ServiceTestRule mServiceRule = new ServiceTestRule();
	private CountDownLatch downloadSignal;
	private CountDownLatch extractSignal;
	private static final String TAG = "DownloadInstrumentTester";
	private CountDownLatch installSignal;
	
	
	private Context setCountryDetails(){
		final Context context = mActivityRule.getActivity().getApplicationContext();
		
		PrefsUtil.setCountryId(context,"2");
		PrefsUtil.setCountry(context, "Tobago");
		PrefsUtil.setIsRegistered(context, true); // we set as registered to avoid launching activity
		PrefsUtil.setPath(context, "static/country_modules/tobago");
		return context;
	}
	
	private ArrayList<String>getModulesList(){
		ArrayList<String> modules = new ArrayList<>();
		modules.add("Alerts");
		modules.add("FEWER");
		modules.add("FirstAid");
		modules.add("Messaging");
		modules.add("Navigation");
		modules.add("PhotoDiary");
		modules.add("Podcast");
		modules.add("SOS");
		return modules;
	}
	
//	@Test
	public void getModules(){
		MainActivity mainActivity = (MainActivity) mActivityRule.getActivity();
		List<String> modules = mainActivity.getSelectedModules();
		assertNotNull("Unable to get selected modules", modules);
		assertTrue("No modules was found as selected", modules.size() > 0);
	}
	
//	@Test
	public void testDownloadModuleTasks() throws InterruptedException {
		// Run the tasks involved in download and extraction manually
		final CountDownLatch downloadSignal = new CountDownLatch(1);
		final Context context = setCountryDetails();
		ModuleFactory factory = ModuleFactory.getInstance(context);
		Module module = factory.getModule("Podcast");
		
		DownloadManager downloadManager = (DownloadManager)context.getSystemService(Context.DOWNLOAD_SERVICE);
		assertNotNull(downloadManager);
		DownloadManager.Request request = new android.app.DownloadManager.Request(Uri.parse(module.getDataLoc()));
		//Setting title of request
		request.setTitle("Downloading " + module.getName() + " Module");
		//Setting description of request
		request.setDescription("Downloading modules resources");
		request.setDestinationUri(FileUtil.getAudioUri("/" + module.getId()+ ".zip"));
		downloadManager.enqueue(request);
		
		BroadcastReceiver onComplete = new BroadcastReceiver() {
			@Override
			public void onReceive(Context context, Intent intent) {
				downloadSignal.countDown();
			}
		};
		context.registerReceiver(onComplete, new IntentFilter(DownloadManager.ACTION_DOWNLOAD_COMPLETE));
		downloadSignal.await();
		
		String filePath = FileUtil.getFileUri("/" + module.getId()+ ".zip").toString();
		File file = new File(filePath);
		assertTrue(file.exists());
		
		if (file.exists()) {
			final CountDownLatch extractSignal = new CountDownLatch(1);
			Zipper zipper = new Zipper(FileUtil.getFileUri("/" + module.getId() + ".zip").toString(), FileUtil.getFileUri("").toString());
			zipper.setDeleteAfterUnzip(true);
			zipper.setOnZipFinishedListener(new Zipper.OnZipFinishedListener() {
				@Override
				public void onFinished(boolean status) {
					extractSignal.countDown();
				}
			});
			zipper.unzip();
			extractSignal.await();
			
			filePath = FileUtil.getFileUri("/" + module.getId()).toString();
			file = new File(filePath);
			assertTrue(file.exists());
		}else{
			Log.d(TAG, "No file was downloaded");
		}
	}
	
	// White box testing download functionality
//	@Test
	public void testDownloadModuleFunctionality() {
		final MainActivity mainActivity = (MainActivity) mActivityRule.getActivity();
		final CountDownLatch downloadSignal = new CountDownLatch(1);
		final Context context = setCountryDetails();
		final ArrayList<String> modules = getModulesList();
		assertTrue(modules.size() > 0);
		// Attempt to save modules
		assertTrue(mainActivity.saveSelectedModules(modules));
		// Attempt to compare modules saved and defined
		List<String>receivedModule = mainActivity.getSelectedModules();
		assertTrue(modules.size() == receivedModule.size());
		// Test the Download
		mainActivity.downloadModulesResources(modules);
		
	}
	
//	@Test
	public void launchMainActivityDownloadModule(){
		setCountryDetails();
		ArrayList<String> modules = new ArrayList<>();
		modules.add("Podcast");
		MainActivity mainActivity = (MainActivity) mActivityRule.getActivity();
		mainActivity.loadData(modules);
	}
	
//	@Test
	public void testDownloadService() throws InterruptedException {
		testDownloadService("Podcast", null);
	}

	public void testDownloadService(final String moduleId, BroadcastReceiver onComplete) throws InterruptedException {
		final Context context = InstrumentationRegistry.getTargetContext();
		// Setup Listener
		downloadSignal = new CountDownLatch(1);
		if (onComplete == null) {
			onComplete = new BroadcastReceiver() {
				@Override
				public void onReceive(Context context, Intent intent) {
					Log.d(TAG, "Broadcast Received");
					assertTrue(intent.hasExtra(DownloadService.MODULE_ID));
					assertTrue(intent.getStringExtra(DownloadService.MODULE_ID).equals(moduleId));
					
					assertTrue(intent.hasExtra(DownloadService.DOWNLOAD_STATUS));
					assertTrue(intent.getBooleanExtra(DownloadService.DOWNLOAD_STATUS, false));
					
					assertTrue(intent.hasExtra(DownloadService.DOWNLOAD_PATH));
					String url = intent.getStringExtra(DownloadService.DOWNLOAD_PATH);
					Log.d(TAG, String.valueOf(Uri.parse(url)));
					downloadSignal.countDown();
				}
			};
		}
		context.registerReceiver(onComplete, new IntentFilter(DownloadService.DOWNLOADS_COMPLETE_BROADCAST));
		
		// Start Download
		DownloadService.startDownloadService(context, moduleId);
		downloadSignal.await();
		context.unregisterReceiver(onComplete);
		Log.d(TAG, "Download Listener unregistered");
	}
	
//	@Test
	public void testUnzipService() throws InterruptedException {
		testUnzipService("Podcast", null);
	}
	
	public void testUnzipService(final String moduleId, BroadcastReceiver onComplete) throws InterruptedException {
		String downloadUri = "file:///storage/emulated/0/mFisheries/Podcast.zip";
		final Context context = InstrumentationRegistry.getTargetContext();
		extractSignal = new CountDownLatch(1);
		if (onComplete == null) {
			onComplete = new BroadcastReceiver() {
				public void onReceive(Context context, Intent intent) {
					assertTrue(intent.hasExtra(ZipService.MODULE_ID));
					assertTrue(intent.getStringExtra(ZipService.MODULE_ID).equals(moduleId));
					
					assertTrue(intent.hasExtra(ZipService.EXTRACT_STATUS));
					assertTrue(intent.getBooleanExtra(ZipService.EXTRACT_STATUS, false));
					
					extractSignal.countDown();
				}
			};
		}
		context.registerReceiver(onComplete, new IntentFilter(ZipService.EXTRACT_COMPLETE_BROADCAST));
		ZipService.launchZipService(context, moduleId, false);
		
		extractSignal.await();
		context.unregisterReceiver(onComplete);
	}
	
//	@Test
	public void testDownloadThenInstallService() throws InterruptedException {
		final String moduleId = "Podcast";
		testDownloadService(moduleId, new BroadcastReceiver() {
			@Override
			public void onReceive(Context context, Intent intent) {
				Log.d(TAG, "Download Successful in DownloadThenInstall service");
				downloadSignal.countDown();
			}
		});
		Log.d(TAG, "testDownloadService completed");
		testUnzipService(moduleId, new BroadcastReceiver() {
			@Override
			public void onReceive(Context context, Intent intent) {
				Log.d(TAG, "Extract Successful in DownloadThenInstall service");
				extractSignal.countDown();
			}
		});
		Log.d(TAG, "testUnzipService completed");
	}
	
	@Test
	public void testInstallModuleService() throws InterruptedException {
		final Context context = InstrumentationRegistry.getTargetContext();
		
		ArrayList<String> modules = getModulesList();
		installSignal = new CountDownLatch(1);
		BroadcastReceiver onComplete = new BroadcastReceiver() {
			@Override
			public void onReceive(Context context, Intent intent) {
				String action = intent.getAction();
				Log.d(TAG, String.format("Received %s as the action of the intent", action));
				if (action != null) {
					if (action.equals(InstallModuleService.INTENT_MODULE_COMPLETE)) {
						Log.d(TAG, "Completed Install for: " + intent.getStringExtra(InstallModuleService.STATUS_INSTALL_INFO));
					}
					if (action.equals(InstallModuleService.INTENT_MODULE_START)) {
						Log.d(TAG, "Started download from: " + intent.getStringExtra(InstallModuleService.STATUS_INSTALL_INFO));
					}
					if (action.equals(InstallModuleService.INTENT_MODULES_COMPLETE)) {
						installSignal.countDown();
					}
				}
			}
		};
		context.registerReceiver(onComplete, new IntentFilter(InstallModuleService.INTENT_MODULES_COMPLETE));
		context.registerReceiver(onComplete, new IntentFilter(InstallModuleService.INTENT_MODULE_COMPLETE));
		context.registerReceiver(onComplete, new IntentFilter(InstallModuleService.INTENT_MODULE_START));
		
		InstallModuleService.requestInstallModules(context, modules);
		installSignal.await();
		context.unregisterReceiver(onComplete);
		Log.d(TAG, "Install Listener unregistered");
	}
}
