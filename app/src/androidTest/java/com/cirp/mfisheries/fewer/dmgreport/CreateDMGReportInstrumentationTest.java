package com.cirp.mfisheries.fewer.dmgreport;

import android.Manifest;
import android.content.Context;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationManager;
import android.os.Build;
import android.support.test.filters.LargeTest;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;

import com.cirp.mfisheries.R;
import com.cirp.mfisheries.fewer.dmgReport.CreateReportActivity;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static android.support.test.InstrumentationRegistry.getInstrumentation;
import static android.support.test.InstrumentationRegistry.getTargetContext;
import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.action.ViewActions.closeSoftKeyboard;
import static android.support.test.espresso.action.ViewActions.typeText;
import static android.support.test.espresso.matcher.ViewMatchers.withId;

@RunWith(AndroidJUnit4.class)
@LargeTest
public class CreateDMGReportInstrumentationTest {
	@Rule
	public ActivityTestRule mActivityRule = new ActivityTestRule<>(CreateReportActivity.class);
	
	@Before
	public void grantPhonePermission() {
		String [] permissions = new String[]{
				Manifest.permission.ACCESS_FINE_LOCATION,
				Manifest.permission.CAMERA,
				Manifest.permission.RECORD_AUDIO,
				Manifest.permission.WRITE_EXTERNAL_STORAGE
		};
		
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
			for(String permission : permissions){
				getInstrumentation()
					.getUiAutomation()
					.executeShellCommand("pm grant "+getTargetContext().getPackageName()+permission);
			}
		}
	}
	
//	@Before
//	public void testGPS(){
//		LocationManager locationManager = (LocationManager) getInstrumentation().getContext().getSystemService(Context.LOCATION_SERVICE);
//		locationManager.addTestProvider("Test", false, false, false, false, false, false, false, Criteria.POWER_LOW, Criteria.ACCURACY_FINE);
//		locationManager.setTestProviderEnabled("Test", true);
//
//		// Set up your test
//		Location location = new Location("Test");
//		location.setLatitude(10.0);
//		location.setLongitude(20.0);
//		locationManager.setTestProviderLocation("Test", location);
//
//		// Check if your listener reacted the right way
//		locationManager.removeTestProvider("Test");
//	}
	
	@Test
	public void createReport(){
		String name = "Automated Test";
		mActivityRule.getActivity();
		onView(withId(R.id.name)).perform(typeText(name), closeSoftKeyboard());
		onView(withId(R.id.post_btn)).perform(click());
	}
	
	
}
