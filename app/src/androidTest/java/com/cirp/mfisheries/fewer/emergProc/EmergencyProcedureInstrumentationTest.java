package com.cirp.mfisheries.fewer.emergProc;

import android.support.test.filters.LargeTest;
import android.support.test.runner.AndroidJUnit4;
import android.support.test.rule.ActivityTestRule;

import android.widget.ListView;

import com.cirp.mfisheries.R;
import com.google.firebase.database.ThrowOnExtraProperties;

import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static junit.framework.Assert.assertNotNull;

@RunWith(AndroidJUnit4.class)
@LargeTest

public class EmergencyProcedureInstrumentationTest {
    @Rule
    public ActivityTestRule mActivityRule = new ActivityTestRule<>(EmergencyProceduresActivity.class);

    @Test
    public void playFirstAndLast(){
        final ListView list = mActivityRule.getActivity().findViewById(R.id.listView);
        assertNotNull("List was not loaded", list);
        mActivityRule.getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                int position = 0;
                list.performItemClick(
                        list.getAdapter().getView(position, null, null),
                        position,
                        list.getAdapter().getItemId(position));
            }
        });
    }
}
