package com.cirp.mfisheries.fewer.msreport;


import android.os.SystemClock;
import android.support.test.filters.LargeTest;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;

import com.cirp.mfisheries.R;
import com.cirp.mfisheries.fewer.msreport.model.PersonReport;

import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.Random;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.action.ViewActions.closeSoftKeyboard;
import static android.support.test.espresso.action.ViewActions.typeText;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertNotNull;

@RunWith(AndroidJUnit4.class)
@LargeTest
public class CreateMSReportUITest {
	@Rule
	public ActivityTestRule mActivityRule = new ActivityTestRule<>(CreateMSReportActivity.class);
	
	@Test
	public void enterFields() {
		Random r = new Random();
		int randomId = r.nextInt(80 - 1) + 1;

		mActivityRule.getActivity();
		
		final String title = "Test Name - " + randomId;
		final String description = "Test Description";
		final String contact = "1234567";
		
		onView(withId(R.id.title))
				.perform(typeText(title), closeSoftKeyboard());
		onView(withId(R.id.description))
				.perform(typeText(description), closeSoftKeyboard());
		onView(withId(R.id.contact))
				.perform(typeText(contact), closeSoftKeyboard());
		
		PersonReport report = ((CreateMSReportActivity) mActivityRule.getActivity()).getReport();
		assertNotNull(report);
		assertEquals(report.title, title);
		assertEquals(report.description, description);
		assertEquals(report.contact, contact);
		
		onView(withId(R.id.btn_create))
				.perform(click());
		
		SystemClock.sleep(1000);
	}
}
