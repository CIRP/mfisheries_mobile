package com.cirp.mfisheries.fewer.emergCon;

import android.Manifest;
import android.os.Build;
import android.support.test.filters.LargeTest;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static android.support.test.InstrumentationRegistry.getInstrumentation;
import static android.support.test.InstrumentationRegistry.getTargetContext;

@RunWith(AndroidJUnit4.class)
@LargeTest
public class ViewEmgContactInstrumentationTest {
	@Rule
	public ActivityTestRule mActivityRule = new ActivityTestRule<>(EmergencyContactActivity.class);
	
	@Before
	public void grantPhonePermission() {
		String [] permissions = new String[]{
				Manifest.permission.CALL_PHONE
		};
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
			for(String permission : permissions){
				getInstrumentation()
						.getUiAutomation()
						.executeShellCommand("pm grant "+getTargetContext().getPackageName()+permission);
			}
		}
	}
	
	@Test
	public void viewContacts(){
	
	}
	
	@Test
	public void launchImport(){
	
	}
	
	
}
