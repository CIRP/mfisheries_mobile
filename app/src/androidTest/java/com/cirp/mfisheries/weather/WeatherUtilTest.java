package com.cirp.mfisheries.weather;

import android.content.Context;
import android.support.annotation.Nullable;
import android.support.test.filters.MediumTest;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;
import android.util.Log;

import com.cirp.mfisheries.core.Country;
import com.cirp.mfisheries.util.CountryUtil;
import com.cirp.mfisheries.util.NetUtil;
import com.cirp.mfisheries.weather.models.WeatherReading;
import com.cirp.mfisheries.weather.models.WeatherSrc;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.List;
import java.util.Map;

import static junit.framework.Assert.assertNotNull;
import static junit.framework.Assert.assertTrue;

@MediumTest
@RunWith(AndroidJUnit4.class)
public class WeatherUtilTest {
	
	@Rule
	public ActivityTestRule<SourceActivity> mActivityRule = new ActivityTestRule<>(SourceActivity.class);
	Context context;
	
	// Utilities
	private CountryUtil countryUtil;
	private WeatherUtil weatherUtil;
	private final String TAG = "WeatherUtilTest";
	
	@Before
	public void setup(){
		context = mActivityRule.getActivity().getBaseContext();
		countryUtil = CountryUtil.getInstance(context);
		weatherUtil = new WeatherUtil(context);
	}
	
	@Test
	public void doWeatherEndPointsExist(){
		// Test Base API for weather source
		assertTrue(NetUtil.exists(weatherUtil.getWeatherSrcAPI()));
		assertTrue(NetUtil.exists(weatherUtil.getWeatherReadingAPI()));
		assertTrue(NetUtil.exists(weatherUtil.getWeatherThresholdAPI()));
	}
	
	private void testWeatherSrc(WeatherSrc weatherSrc){
		assertNotNull(weatherSrc);
		Log.d(TAG, "Attempting to find information for the following source: " + weatherSrc.name);
		assertNotNull(weatherSrc.infotype);
		assertTrue(weatherSrc.infotype.length() > 0);
		assertNotNull(weatherSrc.id);
		assertTrue(weatherSrc.id.length() > 0);
		assertNotNull(weatherSrc.lastexecuted);
		assertTrue(weatherSrc.lastexecuted.length() > 0);
	}
	
	@Test
	public void testWeatherSources(){
		countryUtil.retrieveCountries(new CountryUtil.CountryRetrievedListener() {
			public void processCountries(List<Country> countries) {
				assertTrue(countries.size() > 1);
				for (final Country country : countries){
					weatherUtil.retrieveWeatherSource(country.getId(), new WeatherUtil.WeatherSourceListener() {
						public void onReceiveSourceList(@Nullable List<WeatherSrc> weatherSources) {
							assertNotNull(weatherSources);
							Log.d(TAG, "Retrieved Sources for: " + country.getName());
//							assertTrue(String.format("Received Weather source for %s as:%d", country.getId(), weatherSources.size()),weatherSources.size() > 0 );
							for(WeatherSrc weatherSrc: weatherSources){
								assertTrue(country.getId().equalsIgnoreCase(weatherSrc.countryid));
								assertTrue(country.getName().equalsIgnoreCase(weatherSrc.country));
								testWeatherSrc(weatherSrc);
							}
							// Test to see if Information in Cache after request
							List<WeatherSrc> cachedList = weatherUtil.retrieveCachedSources(country.getId());
							assertTrue(cachedList.size() == weatherSources.size());
						}
					});
				}
			}
		});
	}
	
	@Test
	public void testWeatherSrcStoreCache(){
		final String countryid = "1"; // Assuming that there will at least be a country id 1
		weatherUtil.retrieveWeatherSource(countryid, new WeatherUtil.WeatherSourceListener() {
			@Override
			public void onReceiveSourceList(@Nullable List<WeatherSrc> list) {
				assertNotNull(list);
				assertTrue(list.size() > 0);
				assertTrue(weatherUtil.cacheSources(countryid,list));
				List<WeatherSrc> cachedList = weatherUtil.retrieveCachedSources(countryid);
				assertTrue(cachedList.size() == list.size());
			}
		});
	}
	
	
	@Test
	public void testWeatherReadings(){
		Log.d(TAG, "Attempting to Run Weather readings");
		weatherUtil.retrieveCountryWeatherReading("1", new WeatherUtil.WeatherReadingListener() {
			@Override
			public void onReceiveReadingList(@Nullable List<WeatherReading> list) {
				assertNotNull(list);
				assertTrue(list.size() > 0);
				for(WeatherReading weatherReading: list){
					assertNotNull(weatherReading.id);
					assertTrue(weatherReading.id.length() > 0);
					assertNotNull(weatherReading.country);
					assertTrue(weatherReading.country.length() > 0);
					assertNotNull(weatherReading.countryid);
					assertTrue(weatherReading.countryid.length() > 0);
					assertNotNull(weatherReading.sourceid);
					assertTrue(weatherReading.sourceid.length() > 0);
					assertNotNull(weatherReading.readings);
					assertTrue(weatherReading.readings.size() > 0);
					Log.d(TAG, "Size of readings is: " + weatherReading.readings.size());
					
					for(Map<String, String>reading :  weatherReading.readings){
						assertTrue(reading.entrySet().size() > 0);
						Log.d(TAG, reading.entrySet().toString());
						for(Map.Entry<String, String> entry : reading.entrySet()){
							Log.d(TAG, entry.getKey() + "=>" + entry.getValue());
						}
					}
				}
			}
		});
	}


}
