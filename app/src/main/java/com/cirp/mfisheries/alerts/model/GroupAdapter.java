package com.cirp.mfisheries.alerts.model;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.cirp.mfisheries.R;
import com.cirp.mfisheries.alerts.AlertsActivity;
import com.cirp.mfisheries.core.DatabaseHandler;
import com.cirp.mfisheries.util.NetUtil;
import com.cirp.mfisheries.util.PrefsUtil;
import com.google.firebase.messaging.FirebaseMessaging;
import com.google.gson.JsonObject;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;

import java.util.List;

public class GroupAdapter extends ArrayAdapter<GroupDM> {
	private final Context context;
	private final List<GroupDM> groups;
	private final String TAG = "GroupAdapter";

	public GroupAdapter(Context context, List<GroupDM> groups) {
		super(context, R.layout.layout_group, groups);
		this.context = context;
		this.groups = groups;
	}

	@NonNull
	@Override
	public View getView(int position, View convertView, @NonNull ViewGroup parent) {
		final GroupDM group = groups.get(position);
		View rowView = convertView;
		
		if (rowView == null) {
			LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			if (inflater != null) {
				rowView = inflater.inflate(R.layout.layout_group, parent, false);
				ViewHolder viewHolder = new ViewHolder();
				viewHolder.view = rowView;
				viewHolder.groupName = rowView.findViewById(R.id.groupLabel);
				viewHolder.subscribeBtn = rowView.findViewById(R.id.groupSubscribeBtn);
				viewHolder.viewBtn = rowView.findViewById(R.id.viewGroupBtn);
				rowView.setTag(viewHolder);
			}
		}
		
		if (rowView != null) {
			final ViewHolder holder = (ViewHolder) rowView.getTag();
			holder.group = group;
			holder.groupName.setText(group.groupName);
			holder.isSubscribed = group.isSubscribed;
			
			Log.d(TAG, String.format("The group %s, is subscribed: %s and is one way: %s", group.groupName, group.isSubscribed, group.oneWay));
			
			if (!group.isSubscribed) {
				holder.viewBtn.setVisibility(View.GONE);
				holder.subscribeBtn.setVisibility(View.VISIBLE);
				holder.subscribeBtn.setOnClickListener(new View.OnClickListener() {
					@Override
					public void onClick(View view) {
						subscribe(group, holder);
					}
				});
			} else {
				// Re-add to subscribe group to accommodate for users before current implementation
				FirebaseMessaging.getInstance().subscribeToTopic("alert-group-" + String.valueOf(group.id));
				Log.d(TAG, "Attempting to add user to subscribe group: " + "alert-group-" + String.valueOf(group.id));
				holder.subscribeBtn.setVisibility(View.GONE);
				holder.viewBtn.setVisibility(View.VISIBLE);
				holder.viewBtn.setOnClickListener(new View.OnClickListener() {
					@Override
					public void onClick(View view) {
						AlertsActivity.launchAlertListingActivity(context, group);
					}
				});
			}
			
			holder.view.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					if (holder.isSubscribed) {
						AlertsActivity.launchAlertListingActivity(context, group);
					} else {
						if (group.groupName.toLowerCase().contains("public")){
							(new AlertDialog.Builder(context))
									.setTitle("Alerts")
									.setMessage(R.string.alert_view_join_msg)
									.setNeutralButton("View", new DialogInterface.OnClickListener() {
										@Override
										public void onClick(DialogInterface dialogInterface, int i) {
											AlertsActivity.launchAlertListingActivity(context, group);
										}
									})
									.setPositiveButton("Join and View", new DialogInterface.OnClickListener() {
										@Override
										public void onClick(DialogInterface dialogInterface, int i) {
											subscribe(group, holder);
											AlertsActivity.launchAlertListingActivity(context, group);
										}
									})
									.show();
						}else Toast.makeText(context, "You need to Join", Toast.LENGTH_SHORT).show();
					}
				}
			});
		}
		return rowView;
	}

	public void subscribe(final GroupDM group, final ViewHolder holder) {
        FirebaseMessaging.getInstance().subscribeToTopic("alert-group-" + String.valueOf(group.id));
        
        Log.d(TAG, String.format("User %s was subscribed to group 'alert-group-%s'",PrefsUtil.getUserId(context), group.id ));

		JsonObject object = new JsonObject();
		object.addProperty("userid", PrefsUtil.getUserId(context));
		object.addProperty("groupid", group.id);
		object.addProperty("gcm", PrefsUtil.getNotificationToken(context));
		
		final String url = NetUtil.API_URL + NetUtil.SUBSCRIBE;
		
		Log.d(TAG, String.format("Attempting to send %s to %s via POST request", object, url));

		Ion.with(getContext())
			.load("POST", url)
			.setHeader(NetUtil.API_KEY_PARAM, NetUtil.API_KEY)
			.setJsonObjectBody(object)
            .asJsonObject()
            .setCallback(new FutureCallback<JsonObject>() {
                @Override
                public void onCompleted(Exception e, JsonObject result) {
                    if (e == null && result != null) {
//                      Toast.makeText(getContext(), "Subscription successful", Toast.LENGTH_SHORT).show();
//	                    Snackbar.make(holder.view, "Successfully Joined " + group.groupName, Snackbar.LENGTH_LONG).show();
                        Log.d("Alert Group", "Subscription complete " + result.toString());
						group.isSubscribed = true;
						if(holder != null) {
							holder.isSubscribed = true;
							holder.subscribeBtn.setVisibility(View.GONE);
							holder.viewBtn.setVisibility(View.VISIBLE);
							holder.viewBtn.setOnClickListener(new View.OnClickListener() {
								@Override
								public void onClick(View view) {
									Intent intent = new Intent(context, AlertsActivity.class);
									intent.putExtra("groupName", group.groupName);
									intent.putExtra("groupId", group.id);
									intent.putExtra("oneWay", group.oneWay);
									context.startActivity(intent);
								}
							});
						}
						saveSubscription(group);
                    } else {
	                    if (e != null) e.printStackTrace();
	                    Toast.makeText(getContext(), "Sorry, cannot join", Toast.LENGTH_SHORT).show();
                    }
                }
            });
	}
	
	private void saveSubscription(final GroupDM group) {
		(new Thread() {
			public void run() {
				try {
					final DatabaseHandler database = new DatabaseHandler(getContext(), 6);
					database.open();
					database.groupDBHelper.updateGroupSubscription(group);
					database.close();
				} catch (Exception exc) {
					Log.d("Alert Group", "Error updating subscription" + exc.getLocalizedMessage());
				}
			}
		}).start();
		
	}
	
	public static class ViewHolder {
		public View view;
		public GroupDM group;
		public TextView groupName;
		Button subscribeBtn;
		boolean isSubscribed;
		Button viewBtn;
	}
}


