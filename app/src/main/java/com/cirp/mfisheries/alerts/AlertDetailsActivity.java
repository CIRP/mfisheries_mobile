package com.cirp.mfisheries.alerts;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.cirp.mfisheries.R;
import com.cirp.mfisheries.alerts.model.AlertDM;
import com.cirp.mfisheries.core.module.ModuleActivity;
import com.cirp.mfisheries.nav.MapMarkerDM;
import com.cirp.mfisheries.util.LocationUtil;

public class AlertDetailsActivity extends ModuleActivity {

    public Context context;
    private TextView username;
    private TextView location;
    private TextView time;
    private TextView content;
    private TextView desc;
    private TextView severity;
    private TextView isPublic;
    private TextView isVerified;
    private TextView likes;
    private TextView dislikes;
    
    private static final String TAG = "AlertDetailsActivity";


    @SuppressLint("SetTextI18n")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        
        context = getApplicationContext();
        
        username = findViewById(R.id.alert_username);
        location = findViewById(R.id.alert_location);
        time = findViewById(R.id.alert_time);
        content = findViewById(R.id.alert_content);
        desc = findViewById(R.id.alert_desc);
        severity = findViewById(R.id.alert_severity);
        isPublic = findViewById(R.id.alert_isPublic);
        isVerified= findViewById(R.id.alert_verified);
        likes = findViewById(R.id.alert_likes);
        dislikes = findViewById(R.id.alert_dislikes);
        Log.d(TAG, "Getting Intent");
        Intent intent = getIntent();

        if(intent.getExtras()!=null){
            Log.d(TAG, "get Extras was not null");
            final AlertDM alert = intent.getExtras().getParcelable("alert");
            if (alert != null){
                Log.d(TAG,"Received from extras: " + alert.getJsonObject().toString());
                
                username.setText("User: "+alert.fullname);
                location.setText("Location: "+alert.latitude +", "+alert.longitude);
                content.setText("Message: "+alert.messagecontent);
                desc.setText("Description: "+alert.messageDescription);
                severity.setText("Severity: "+alert.messageSeverity);
                time.setText("Time: "+alert.deviceTimestamp);
                isVerified.setText("Verified: "+alert.isVerified);
                isPublic.setText("Public: "+alert.isPublic);
                likes.setText("Likes: "+alert.likes);
                dislikes.setText("Dislikes: "+alert.dislikes);
                
                final View mapView = findViewById(R.id.alertMapView);
                
                if (alert.latitude != null && alert.longitude != null){
                    // Launch a map when user selects the location
                    location.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            displayLocation(alert);
                        }
                    });
                    
                    // if the location of the alert is specified, then load the image of location
                    Log.d(TAG, "We have a location of the alert and will attempt to load information");
                    try {
                        final String url = "http://maps.google.com/maps/api/staticmap?center=" +
                                alert.latitude + "," + alert.longitude +
                                "&zoom=" + 10 + "&scale=2&size=" + 960 + "x" + 960 +
                                "&sensor=false&markers=color:red%7C" +
                                alert.latitude + "," + alert.longitude;
                        Log.d(TAG, "Attempting to show Map Preview of : " + url);
                        
                        Glide.with(this)
                                .load(url)
                                .into((ImageView) findViewById(R.id.alertMapView));
                        
                        mapView.setVisibility(View.VISIBLE);
                        mapView.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                displayLocation(alert);
                            }
                        });
                        
                    }catch(Exception e){e.printStackTrace();}
                }else{
                    location.setVisibility(View.GONE);
                    mapView.setVisibility(View.GONE);
                }
            }
        }
    }
    
    @Override
    public int getLayoutResourceId() {
        return R.layout.activity_alert_details;
    }
    @Override
    public int getColor(){
        return R.color.yellow;
    }
    
    @Override
    public int getColorDark(){
        return R.color.yellowDark;
    }
    
    public static void launch(final AlertDM alert, final Context context){
        Intent intent = new Intent(context, AlertDetailsActivity.class);
        intent.putExtra("alert",alert);
        context.startActivity(intent);
    }
    
    public void displayLocation(AlertDM alert){
        final MapMarkerDM loc = new MapMarkerDM();
        loc.latitude = alert.latitude;
        loc.longitude = alert.longitude;
        loc.name = alert.messagecontent;
        loc.details = alert.messageDescription;
        LocationUtil.displayLocation(context, loc, location);
    }
}
