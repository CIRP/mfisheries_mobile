package com.cirp.mfisheries.alerts.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.cirp.mfisheries.core.module.BaseModel;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.squareup.moshi.JsonAdapter;
import com.squareup.moshi.Moshi;

@SuppressWarnings("WeakerAccess")
public class AlertDM extends BaseModel implements Parcelable {
	
	public int id;
	public int userid;
	public int groupid;
	public String username;
	public String latitude;
	public String longitude;
	public String deviceTimestamp;
	public String messagecontent;
	public String messageDescription;
	public String messageSeverity;
	public boolean isPublic;
	public boolean isVerified;
	public int likes;
	public int dislikes;
	public String fullname;
	
	public AlertDM() {
		this.likes = 0;
		this.dislikes = 0;
		this.isPublic = false;
		this.isVerified = false;
	}
	
	public AlertDM(int userid, int groupid, String latitude, String longitude, String deviceTimestamp, String messagecontent) {
		this();
		this.userid = userid;
		this.groupid = groupid;
		this.latitude = latitude;
		this.longitude = longitude;
		this.deviceTimestamp = deviceTimestamp;
		this.messagecontent = messagecontent;
		this.isPublic = false;
		this.isVerified = false;
	}
	
	public AlertDM(int userid, int groupid, String latitude, String longitude, String deviceTimestamp, String messagecontent, String description, String severity) {
		this();
		this.userid = userid;
		this.groupid = groupid;
		this.latitude = latitude;
		this.longitude = longitude;
		this.deviceTimestamp = deviceTimestamp;
		this.messagecontent = messagecontent;
		this.isPublic = false;
		this.isVerified = false;
		this.messageDescription = description;
		this.messageSeverity = severity;
	}
	
	public boolean equals(Object object){
		if (object instanceof AlertDM){
			AlertDM alert = (AlertDM)object;
			if (alert.id != 0 && this.id != 0)return alert.id == this.id;
			if (alert.deviceTimestamp != null && this.deviceTimestamp != null)
				return alert.deviceTimestamp.equals(this.deviceTimestamp);
		}
		return false;
	}
	
	public JsonObject getJsonObject() {
		try {
			Moshi moshi = new Moshi.Builder().build();
			JsonAdapter<AlertDM> jsonAdapter = moshi.adapter(AlertDM.class);
			String json = jsonAdapter.toJson(this);
			JsonParser parser = new JsonParser();
			return (JsonObject) parser.parse(json);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	
	protected AlertDM(Parcel in) {
		id = in.readInt();
		userid = in.readInt();
		groupid = in.readInt();
		username = in.readString();
		latitude = in.readString();
		longitude = in.readString();
		deviceTimestamp = in.readString();
		messagecontent = in.readString();
		messageDescription = in.readString();
		messageSeverity = in.readString();
		isPublic = in.readByte() != 0x00;
		isVerified = in.readByte() != 0x00;
		likes = in.readInt();
		dislikes = in.readInt();
		fullname = in.readString();
	}
	
	@Override
	public int describeContents() {
		return 0;
	}
	
	@Override
	public void writeToParcel(Parcel dest, int flags) {
		dest.writeInt(id);
		dest.writeInt(userid);
		dest.writeInt(groupid);
		dest.writeString(username);
		dest.writeString(latitude);
		dest.writeString(longitude);
		dest.writeString(deviceTimestamp);
		dest.writeString(messagecontent);
		dest.writeString(messageDescription);
		dest.writeString(messageSeverity);
		dest.writeByte((byte) (isPublic ? 0x01 : 0x00));
		dest.writeByte((byte) (isVerified ? 0x01 : 0x00));
		dest.writeInt(likes);
		dest.writeInt(dislikes);
		dest.writeString(fullname);
	}
	
	@SuppressWarnings("unused")
	public static final Parcelable.Creator<AlertDM> CREATOR = new Parcelable.Creator<AlertDM>() {
		@Override
		public AlertDM createFromParcel(Parcel in) {
			return new AlertDM(in);
		}
		
		@Override
		public AlertDM[] newArray(int size) {
			return new AlertDM[size];
		}
	};
}