package com.cirp.mfisheries.alerts;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AlertDialog;
import android.view.MenuItem;
import android.view.View;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.cirp.mfisheries.R;
import com.cirp.mfisheries.alerts.model.GroupAdapter;
import com.cirp.mfisheries.alerts.model.GroupDM;
import com.cirp.mfisheries.core.Country;
import com.cirp.mfisheries.core.CountryAdapter;
import com.cirp.mfisheries.core.module.ModuleActivity;
import com.cirp.mfisheries.util.CountryUtil;
import com.cirp.mfisheries.util.PrefsUtil;

import java.util.List;

public class AlertsGroupActivity extends ModuleActivity {
	
	private final String TAG = "AlertGroups";
	private SwipeRefreshLayout swLayout;
	private GroupUtils utils;
	
	private String countryid;
	private String countryName;
	private TextView cNameView;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		module = new Alerts(this);
		utils = new GroupUtils(getBaseContext());
		
		setUpSwipeRefresh();
		
		countryid = PrefsUtil.getCountryId(this);
		countryName = PrefsUtil.getCountry(this);
		if (!countryid.equals("-1") && !countryName.equals("")) {
			cNameView = findViewById(R.id.countryname);
			displayCountrySelection(countryName);
		}else{
//			Toast.makeText(this, "Country was incorrectly configured. Select Country and try again", Toast.LENGTH_LONG).show();
//			finish();
			super.ensureRegistered();
		}
	}
	
	private void setUpSwipeRefresh(){
		swLayout = findViewById(R.id.alert_group_swiperefresh);
		swLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
			@Override
			public void onRefresh() {
				retrieveGroups();
			}
		});
	}
	
	@Override
	public void onResume() {
		super.onResume();
		retrieveGroups();
	}

	private void retrieveGroups(){
		int uId = PrefsUtil.getUserId(this);
		if (swLayout != null) swLayout.setRefreshing(true);
		
		utils.retrieveByUserID(new GroupUtils.OnRecordsReceiveListener() {
			@Override
			public void onRecordsReceived(List<GroupDM> records) {
				swLayout.setRefreshing(false);
				if (records != null && records.size() > 0){
					displayGroups(records);
				}else{
					Snackbar.make(swLayout, "No alert groups are available.", Snackbar.LENGTH_SHORT).show();
				}
			}
		}, String.valueOf( uId ));
	}

	//displaying groups from the database
	public void displayGroups(List<GroupDM> groups) {
		final ListView listView = findViewById(R.id.groupList);
		final GroupAdapter groupAdapter = new GroupAdapter(this, groups);
		listView.post(new Runnable() {
			@Override
			public void run() {
				listView.setAdapter(groupAdapter);
				ProgressBar progressBar = findViewById(R.id.progressBar);
				progressBar.setVisibility(View.GONE);
			}
		});
	}

	@Override
	public int getLayoutResourceId() {
		return R.layout.activity_alerts_group;
	}

	@Override
	public int getColor(){
		return R.color.yellow;
	}

	@Override
	public int getColorDark(){
		return R.color.yellowDark;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		if (item.getItemId() == R.id.menu_refresh)
			retrieveGroups();
		return super.onOptionsItemSelected(item);
	}
	
	public void chooseCountry(View view){
		CountryUtil.getInstance(this).retrieveCountries(new CountryUtil.CountryRetrievedListener() {
			@Override
			public void processCountries(List<Country> countries) {
				if (countries != null){
					displayCountries(countries);
				}else{
					Toast.makeText(AlertsGroupActivity.this, "Unable to retrieve countries", Toast.LENGTH_SHORT).show();
				}
			}
		});
	}
	
	private void displayCountries(final List<Country> countries){
		final CountryAdapter arrayAdapter = new CountryAdapter(this, countries);
		
		new AlertDialog.Builder(this)
				.setTitle(getResources().getString(R.string.dialog_country))
				.setAdapter(arrayAdapter,
						new DialogInterface.OnClickListener() {
							@Override
							public void onClick(DialogInterface dialog, int which) {
								Country selectedCountry = countries.get(which);
								countryid = selectedCountry.getId();
								countryName = selectedCountry.getName();
								displayCountrySelection(countryName);
								retrieveGroups();
							}
						})
				.show();
	}
	
	private void displayCountrySelection(String country){
		cNameView.setText(country);
	}
	
}
