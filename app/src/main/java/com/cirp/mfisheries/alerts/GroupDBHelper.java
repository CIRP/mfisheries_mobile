package com.cirp.mfisheries.alerts;

import android.content.ContentValues;
import android.database.Cursor;
import android.util.Log;

import com.cirp.mfisheries.alerts.model.GroupDM;
import com.cirp.mfisheries.core.DatabaseHandler;

import java.util.ArrayList;
import java.util.List;

@SuppressWarnings({"WeakerAccess", "UnusedReturnValue"})
public class GroupDBHelper {

	private static final String GROUP_TABLE = "GroupTable";
	private static final String GROUP_ID = "groupId";
	private static final String GROUP_REC_ID = "id";
	private static final String GROUP_NAME = "groupName";
	private static final String GROUP_SUBSCRIPTION = "isSubscribed";
	private static final String GROUP_ONE_WAY = "oneWay";
	private static final String TAG = "GroupDBHelper";

	public static String CREATE_GROUP_TABLE =
			"CREATE TABLE " + GROUP_TABLE + " ("
					+ GROUP_REC_ID + " INTEGER PRIMARY KEY,"
					+ GROUP_ID + " INTEGER,"
					+ GROUP_NAME + " TEXT,"
					+ GROUP_SUBSCRIPTION + " TEXT,"
					+ GROUP_ONE_WAY + " INTEGER,"
					+ "UNIQUE("+GROUP_ID+") ON CONFLICT REPLACE"
					+ ")";


	public float addGroup(GroupDM groupDM) {
		if (findGroup(groupDM.id) != null) {
			Log.d(TAG, "Group was already added to the database");
			return 0;
		} else {
			Log.d(TAG, "Attempting to add the group to the database");
			try {
				ContentValues groupValues = new ContentValues();
				groupValues.put(GROUP_ID, groupDM.id);
				groupValues.put(GROUP_NAME, groupDM.groupName);
				groupValues.put(GROUP_SUBSCRIPTION, groupDM.isSubscribed + "");
				groupValues.put(GROUP_ONE_WAY, groupDM.oneWay + "");
				return DatabaseHandler.database.insert(GROUP_TABLE, null, groupValues);
			} catch (Exception ex) {
				Log.e(TAG, "Error adding alert group to database:");
				ex.printStackTrace();
			}
		}
		return 0;
	}

	public List<GroupDM> getAllGroups() {
		List<GroupDM> groups = new ArrayList<>();
		try {
			String selectQuery = "SELECT  * FROM " + GROUP_TABLE;
			Cursor cursor = DatabaseHandler.database.rawQuery(selectQuery, null);
			if (cursor.moveToFirst()) {
				do {
					//TODO Remove using index and use column name instead
					GroupDM group = new GroupDM();
					group.id = cursor.getInt(1);
					group.groupName = cursor.getString(2);
					group.isSubscribed = Boolean.parseBoolean(cursor.getString(3));
					group.oneWay = cursor.getInt(4);
					groups.add(group);
				}
				while (cursor.moveToNext());
			}
			cursor.close();
		} catch (Exception ex) {
			Log.d(TAG, "get all groups, Error: " + ex.getLocalizedMessage());
		}
		return groups;
	}


	public GroupDM findGroup(int groupId) {
		GroupDM groupDM = null;
		try {
			String selectQuery = "SELECT  * FROM " + GROUP_TABLE + " WHERE '" + GROUP_ID + "' = " + groupId;
			Cursor cursor = DatabaseHandler.database.rawQuery(selectQuery, null);
			if (cursor.moveToFirst()) {
				groupDM = new GroupDM();
				groupDM.id = cursor.getInt(0);
				groupDM.groupName = cursor.getString(1);
				groupDM.isSubscribed = Boolean.parseBoolean(cursor.getString(2));
				groupDM.oneWay = cursor.getInt(3);
			}
			cursor.close();
		} catch (Exception ex) {
			Log.wtf(TAG, "Alert DB Helper, get Alerts, Error " + ex.getLocalizedMessage());
		}
		return groupDM;
	}
	
	public void updateGroupSubscription(GroupDM group) {
		updateGroupSubscription(group.groupName, group.isSubscribed);
	}

	public void updateGroupSubscription(String groupName, boolean isSubscribed) {
		String updateQuery = "UPDATE " + GROUP_TABLE + " SET " + GROUP_SUBSCRIPTION + " = \" " + isSubscribed + "\"  WHERE " + GROUP_NAME + " = \"" + groupName + "\"";
		DatabaseHandler.database.execSQL(updateQuery);
	}
}
