package com.cirp.mfisheries.alerts.model;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.annotation.DrawableRes;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v7.widget.CardView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.TextView;

import com.cirp.mfisheries.App;
import com.cirp.mfisheries.R;
import com.cirp.mfisheries.alerts.AlertDetailsActivity;
import com.cirp.mfisheries.alerts.AlertUtils;
import com.cirp.mfisheries.core.AppDatabase;
import com.cirp.mfisheries.nav.MapMarkerDM;
import com.cirp.mfisheries.util.LocationUtil;
import com.cirp.mfisheries.util.PrefsUtil;

import java.util.List;

public class AlertAdapter extends ArrayAdapter<AlertDM> {
	private static final String TAG = "AlertAdapter";
	
	private final Context context;
	private final List<AlertDM> alerts;
	private final AlertUtils alertUtils;
	private AppDatabase database;
	private View rowView;
	
	public AlertAdapter(Context context, List<AlertDM> alerts) {
		super(context, R.layout.layout_alert, alerts);
		this.context = context;
		this.alerts = alerts;
		database = App.getDatabase(context);
		alertUtils = new AlertUtils(context);
	}

	@NonNull
	@Override
	public View getView(int position, View convertView, @NonNull ViewGroup parent) {
		rowView = convertView;
		if (rowView == null) {
			LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			if (inflater != null) {
				rowView = inflater.inflate(R.layout.layout_alert, parent, false);
				
				ViewHolder viewHolder = new ViewHolder();
				viewHolder.view = (CardView) rowView;
				viewHolder.msgDate = rowView.findViewById(R.id.msgDate);
				viewHolder.msgSender = rowView.findViewById(R.id.msgSender);
				viewHolder.msgBody = rowView.findViewById(R.id.msgBody);
				viewHolder.msgLocation = rowView.findViewById(R.id.msgLocation);
				viewHolder.likeButton = rowView.findViewById(R.id.likeButton);
				viewHolder.dislikeButton = rowView.findViewById(R.id.dislikeButton);
				viewHolder.shareButton = rowView.findViewById(R.id.shareButton);
				viewHolder.deleteButton = rowView.findViewById(R.id.deleteButton);
				viewHolder.verified = rowView.findViewById(R.id.verified);
				viewHolder.msgType = rowView.findViewById(R.id.msgType);
				viewHolder.msgSeverity = rowView.findViewById(R.id.msgSeverity);
				
				rowView.setTag(viewHolder);
			}
		}
		
		final AlertDM alert = alerts.get(position);
		final ViewHolder holder = (ViewHolder) rowView.getTag();

		if(alert.isVerified){
			holder.view.setCardBackgroundColor(context.getResources().getColor(R.color.transGreen));
			holder.verified.setVisibility(View.VISIBLE);
		} else {
			holder.view.setCardBackgroundColor(context.getResources().getColor(android.R.color.white));
			holder.verified.setVisibility(View.GONE);
		}

		holder.alert = alert;
		holder.msgDate.setText(alert.deviceTimestamp);
		holder.msgSender.setText(alert.fullname);
		holder.msgBody.setText(alert.messagecontent);
		holder.msgType.setText(alert.messageDescription);
		holder.msgSeverity.setText(alert.messageSeverity);

		// Onclick listener to launch more details
		View.OnClickListener listener = new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				Log.d(TAG, "Attempting to view: " + alert.getJsonObject().toString());
				AlertDetailsActivity.launch(alert, context);
			}
		};
		holder.msgBody.setOnClickListener(listener);
		holder.msgSender.setOnClickListener(listener);
		holder.msgDate.setOnClickListener(listener);
		
		
		//set filepath
		String loc = LocationUtil.getLocationString(Double.parseDouble(alert.latitude), Double.parseDouble(alert.longitude));
		MapMarkerDM mapMarkerDM = getMapMarker(alert);
		holder.msgLocation.setTag(mapMarkerDM);
		holder.msgLocation.setText(loc);
		holder.msgLocation.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				openMap(view);

			}
		});

		//like and dislike setup
		holder.likeButton.setText(String.valueOf(alert.likes));
		holder.likeButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				if (alert.userid == PrefsUtil.getUserId(context)){
					Snackbar.make(rowView, "You cannot rate your own alert.", Snackbar.LENGTH_SHORT).show();
				}else{
					try {
						int icon = R.drawable.ic_arrow_upward_blue;
						boolean status = true;

						// First Time we selecting like option
						if (holder.like == null){
							holder.alert.likes += 1;
						} else if (!holder.like.isState()) {
							holder.alert.likes += 1;
							status = true;
						}else{ // Option deselected (i.e. dislike)
							holder.alert.likes -= 1;
							status = false;
						}

						holder.like = new Like(alert.id, status); // true is liked and false is disliked
						if (status) {
							if (holder.dislike != null && !holder.dislike.isState()) {
								holder.dislike.setState(true);
								holder.alert.dislikes -= 1;
							}
						}else icon = R.drawable.ic_arrow_upward_black;

						updateAlert(holder.alert);
						updateLike(alert.id, status, holder.alert.likes, (Button) v, icon, "like");
						notifyDataSetChanged();
					}catch (Exception e){ e.printStackTrace(); }
				}
			}
		});

		// Update colour icon based on state of like
		holder.like = database.likeDao().findById(alert.id, true);
		if(holder.like != null) {
			holder.likeButton.setCompoundDrawablesWithIntrinsicBounds
					(context.getResources().getDrawable(R.drawable.ic_arrow_upward_blue), null, null, null);
		}
		else {
			holder.likeButton.setCompoundDrawablesWithIntrinsicBounds
					(context.getResources().getDrawable(R.drawable.ic_arrow_upward_black), null, null, null);
		}

		holder.dislikeButton.setText(String.valueOf(alert.dislikes));
		holder.dislikeButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				if (alert.userid == PrefsUtil.getUserId(context)){
					Snackbar.make(rowView, "You cannot rate your own alert.", Snackbar.LENGTH_SHORT).show();
				} else {
					try {
						int icon = R.drawable.ic_arrow_downward_blue;
						boolean status = false; // false is (enable) and true is disable

						// First Time we selecting like option
						if (holder.dislike == null){
							holder.alert.dislikes += 1;
						} else if (holder.dislike.isState()) { // If we select a downvote/dislike again (when it was deselected) i.e. reselect
							holder.alert.dislikes += 1;
							status = false;

						}else{ // Option deselected
							holder.alert.dislikes -= 1;
							status = true;
						}

						holder.dislike = new Like(alert.id, status); // true is liked and false is disliked
						if (!status){
							if (holder.like != null && holder.like.isState()){
								holder.like.setState(false);
								holder.alert.likes -= 1;
							}
						}else icon = R.drawable.ic_arrow_downward_black;

						updateAlert(holder.alert);
						updateLike(alert.id, status, holder.alert.dislikes, (Button) v, icon, "dislike");
						notifyDataSetChanged();
					}catch (Exception e){ e.printStackTrace(); }
				}
			}
		});

		//
		holder.dislike = database.likeDao().findById(alert.id, false);
		if(holder.dislike != null) {
			holder.dislikeButton.setCompoundDrawablesWithIntrinsicBounds
					(context.getResources().getDrawable(R.drawable.ic_arrow_downward_blue), null, null, null);
		}
		else {
			holder.dislikeButton.setCompoundDrawablesWithIntrinsicBounds
					(context.getResources().getDrawable(R.drawable.ic_arrow_downward_black), null, null, null);
		}

		
		// Specify Share operation
		holder.shareButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				shareAlert(alert);
			}
		});
		
		
		// Delete Operation
		if (alert.userid == PrefsUtil.getUserId(context)){
			holder.deleteButton.setVisibility(View.VISIBLE);
			holder.deleteButton.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View view) {
					deleteAlert(alert);
				}
			});
		}else{
			holder.deleteButton.setVisibility(View.INVISIBLE);
		}
		return rowView;
	}
	private void shareAlert(final AlertDM alert){
		String share = "";

		String locationText = String.valueOf("Location : https://www.google.com/maps/search/?api=1&query="+alert.latitude+","+alert.longitude);

		share += "Date: "+ alert.deviceTimestamp +"\n\n";
		share += "Sender: "+ alert.username +"\n\n";
//		share += "Location\n\tLatitude: "+alert.latitude+"\n\tLongitude: "+alert.longitude+"\n\n";
		share += locationText;
		share += "\n\nType: "+ alert.messageDescription +"\n\n";
		share += "Severity: "+ alert.messageSeverity +"\n\n";
		share += "Message:\n"+ alert.messagecontent +"\n\n";
		share += "Verified: "+ alert.isVerified +"\n\n";
		
		Intent shares = new Intent(Intent.ACTION_SEND);
		shares.setType("text/plain");
		shares.putExtra(Intent.EXTRA_SUBJECT, "Alert from: "+ alert.username);
		shares.putExtra(Intent.EXTRA_TEXT,share);
		context.startActivity(Intent.createChooser(shares,"Share Via"));
	}
	
	private void deleteAlert(final AlertDM alert){
		if (alert.userid == PrefsUtil.getUserId(context)) {
			new AlertDialog.Builder(context)
					.setTitle("Alerts")
					.setMessage("Are you sure you want to delete the alert?")
					.setPositiveButton("Delete", new DialogInterface.OnClickListener() {
						@Override
						public void onClick(final DialogInterface dialogInterface, int i) {
							alertUtils.delete(alert, new AlertUtils.AlertUpdateListener() {
								@Override
								public void onUpdatedAlert(AlertDM alert, boolean status) {
									if (status){
										Snackbar.make(rowView, "Alert deleted", Snackbar.LENGTH_SHORT).show();
										alerts.remove(alert);
										notifyDataSetChanged();
									}
									else Snackbar.make(rowView, "Unable to deleted alert.", Snackbar.LENGTH_SHORT).show();
								}
							});
						}
					})
					.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
						@Override
						public void onClick(DialogInterface dialogInterface, int i) {
							dialogInterface.dismiss();
						}
					})
					.show();
		}else{
			Snackbar.make(rowView, "Unable to deleted another users' alert .", Snackbar.LENGTH_SHORT).show();
		}
	}

	private void updateLike(int alertId, boolean state, int count, Button button, @DrawableRes int icon, String updating){
		
		Like like = database.likeDao().findById(alertId, true);
		Like dislike = database.likeDao().findById(alertId, false);
		
		// Clear records before adding new one
		if (like != null)database.likeDao().delete(like);
		if (dislike != null)database.likeDao().delete(dislike);
		
		if (updating.equalsIgnoreCase("like") && state) {
			database.likeDao().insertAll(new Like(alertId, true));
		}
		if (updating.equalsIgnoreCase("dislike") && !state) {
			database.likeDao().insertAll(new Like(alertId, false));
		}
		
		button.setText(String.valueOf(count));
		button.setCompoundDrawablesWithIntrinsicBounds(context.getResources().getDrawable(icon), null, null, null);
	}

	@NonNull
	private MapMarkerDM getMapMarker(AlertDM alert) {
		MapMarkerDM mapMarkerDM = new MapMarkerDM();
		mapMarkerDM.name = alert.userid + "";
		mapMarkerDM.details = alert.messagecontent;
		mapMarkerDM.latitude = alert.latitude;
		mapMarkerDM.longitude = alert.longitude;
		return mapMarkerDM;
	}

	private void updateAlert(AlertDM alert) {
		alertUtils.update(alert, new AlertUtils.AlertUpdateListener() {
			@Override
			public void onUpdatedAlert(AlertDM alert, boolean status) {
				if (status)Snackbar.make(rowView, "Alert updated", Snackbar.LENGTH_SHORT).show();
				else Snackbar.make(rowView, "Unable to update alert.", Snackbar.LENGTH_SHORT).show();
			}
		});
	}

	private void openMap(View view) {
		final MapMarkerDM list = (MapMarkerDM) view.getTag();
		LocationUtil.displayLocation(context, list, view);
	}
	
	private static class ViewHolder {
		public CardView view;
		public AlertDM alert;
		TextView msgDate;
		TextView msgSender;
		TextView msgBody;
		TextView msgLocation;
		Button likeButton;
		Button dislikeButton;
		Button shareButton;
		Button deleteButton;
		TextView verified;
		public Like like;
		Like dislike;
		TextView msgType;
		TextView msgSeverity;
	}
}

