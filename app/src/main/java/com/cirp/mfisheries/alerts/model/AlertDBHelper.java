package com.cirp.mfisheries.alerts.model;

import android.content.ContentValues;
import android.database.Cursor;
import android.util.Log;

import com.cirp.mfisheries.core.DatabaseHandler;

import java.util.ArrayList;
import java.util.List;

@SuppressWarnings("UnusedReturnValue")
public class AlertDBHelper {

	private static final String ALERT_TABLE = "AlertTable";
	private static final String ALERT_ID = "id";
	private static final String ALERT_LATITUDE = "latitude";
	private static final String ALERT_LONGITUDE = "longitude";
	private static final String ALERT_TIMESTAMP = "timestamp";
	private static final String ALERT_USER = "user";
	private static final String ALERT_USER_ID = "userId";
	private static final String ALERT_MESSAGE = "message";
	private static final String ALERT_DESCRIPTION = "description";
	private static final String ALERT_SEVERITY = "severity";
	private static final String ALERT_GROUP_ID = "groupId";
	
	
	private final String TAG = "AlertDBHelper";

	public static String CREATE_ALERT_TABLE =
			"CREATE TABLE " + ALERT_TABLE + " ("
					+ ALERT_ID + " INTEGER PRIMARY KEY autoincrement,"
					+ ALERT_LATITUDE + " TEXT,"
					+ ALERT_LONGITUDE + " TEXT,"
					+ ALERT_TIMESTAMP + " TEXT,"
					+ ALERT_USER + " TEXT,"
					+ ALERT_USER_ID + " INTEGER,"
					+ ALERT_MESSAGE + " TEXT,"
					+ ALERT_DESCRIPTION + " TEXT,"
					+ ALERT_SEVERITY + " TEXT,"
					+ ALERT_GROUP_ID + " INTEGER,"
					+ "UNIQUE(" + ALERT_TIMESTAMP + "," + ALERT_USER_ID + "," + ALERT_GROUP_ID +") "
					+ "ON CONFLICT REPLACE"
					+ ")";

	public float addAlert(AlertDM alert) {
		try {
			ContentValues alert_values = new ContentValues();
			alert_values.put(ALERT_LATITUDE, alert.latitude);
			alert_values.put(ALERT_LONGITUDE, alert.longitude);
			alert_values.put(ALERT_TIMESTAMP, alert.deviceTimestamp);
			alert_values.put(ALERT_USER, alert.username);
			alert_values.put(ALERT_USER_ID, alert.userid);
			alert_values.put(ALERT_MESSAGE, alert.messagecontent);
			alert_values.put(ALERT_GROUP_ID, alert.groupid);
			return DatabaseHandler.database.insert(ALERT_TABLE, null, alert_values);
		} catch (Exception ex) {
			Log.wtf(TAG, "Add Alert, Error adding alert" + ex.getLocalizedMessage());
		}
		return 0;
	}

	public int getAlertCount() {
		try {
			String alerts_query = "SELECT * FROM " + ALERT_TABLE;
			Cursor cursor = DatabaseHandler.database.rawQuery(alerts_query, null);
			int count = cursor.getCount();
			cursor.close();
			return count;
		} catch (Exception ex) {
			Log.wtf(TAG, "Offline Track DB Helper, Get Offline Track Count, Error " + ex.getLocalizedMessage());
		}
		return 0;
	}


	public List<AlertDM> getAllAlerts() {
		List<AlertDM> alertDMs = new ArrayList<>();
		try {
			String selectQuery = "SELECT  * FROM " + ALERT_TABLE;
			Cursor cursor = DatabaseHandler.database.rawQuery(selectQuery, null);
			if (cursor.moveToFirst()) {
				do {
					AlertDM alert = new AlertDM();
					alert.userid = cursor.getInt(0);
					alert.latitude = cursor.getString(1);
					alert.longitude = cursor.getString(2);
					alert.deviceTimestamp = cursor.getString(3);
					alert.username = cursor.getString(4);
					alert.userid = cursor.getInt(5);
					alert.messagecontent = cursor.getString(6);
					alert.groupid = cursor.getInt(7);
					alertDMs.add(alert);
				}
				while (cursor.moveToNext());
			}
			cursor.close();
		} catch (Exception ex) {
			Log.wtf(TAG, "Get Alerts, Error " + ex.getLocalizedMessage());
		}
		return alertDMs;
	}

	public List<AlertDM> getAlertsByGroupId(int groupId) {
		List<AlertDM> alerts = new ArrayList<>();
		try {
			String selectQuery = "SELECT  * FROM " + ALERT_TABLE + " WHERE " + ALERT_GROUP_ID + " = " + groupId + " ORDER BY " +
					ALERT_TIMESTAMP + " DESC";
			Cursor cursor = DatabaseHandler.database.rawQuery(selectQuery, null);
			if (cursor.moveToFirst()) {
				do {
					AlertDM alert = new AlertDM();
					alert.userid = cursor.getInt(0);
					alert.latitude = cursor.getString(1);
					alert.longitude = cursor.getString(2);
					alert.deviceTimestamp = cursor.getString(3);
					alert.username = cursor.getString(4);
					alert.userid = cursor.getInt(5);
					alert.messagecontent = cursor.getString(6);
					alert.groupid = cursor.getInt(7);
					alerts.add(alert);
				}
				while (cursor.moveToNext());
			}
			cursor.close();
		} catch (Exception ex) {
			Log.wtf(TAG, "Get Alerts, Error " + ex.getLocalizedMessage());
		}
		return alerts;
	}
}