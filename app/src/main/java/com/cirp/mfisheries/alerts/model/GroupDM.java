package com.cirp.mfisheries.alerts.model;

import com.cirp.mfisheries.core.module.BaseModel;

public class GroupDM  extends BaseModel{

	public int id;
	public String groupName;
	public boolean isSubscribed;
	public int oneWay;

	public GroupDM(int id, String groupName, boolean isSubscribed, int oneWay) {
		this.id = id;
		this.groupName = groupName;
		this.isSubscribed = isSubscribed;
		this.oneWay = oneWay;
	}

	public GroupDM() {
	}
}
