package com.cirp.mfisheries.alerts;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.widget.SwipeRefreshLayout;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.cirp.mfisheries.R;
import com.cirp.mfisheries.alerts.model.AlertAdapter;
import com.cirp.mfisheries.alerts.model.AlertDM;
import com.cirp.mfisheries.alerts.model.GroupDM;
import com.cirp.mfisheries.core.DatabaseHandler;
import com.cirp.mfisheries.core.module.ModuleActivity;
import com.cirp.mfisheries.util.NetUtil;
import com.cirp.mfisheries.util.PrefsUtil;
import com.google.firebase.messaging.FirebaseMessaging;
import com.google.gson.JsonObject;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;

import java.util.List;

public class AlertsActivity extends ModuleActivity {

	private static final String TAG = "AlertActivity";
	private static final int CREATE_ALERT_REQUEST = 102;
	private String group;
	private int groupId;
	private SwipeRefreshLayout swLayout;
	private AlertUtils alertUtils;
	
	public static void launchAlertListingActivity(Context context, GroupDM group) {
		Intent intent = new Intent(context, AlertsActivity.class);
		intent.putExtra("groupName", group.groupName);
		intent.putExtra("groupId", group.id);
		intent.putExtra("oneWay", group.oneWay);
		context.startActivity(intent);
	}
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		module = new Alerts(this);
		
		Intent intent = getIntent();
		group = intent.getStringExtra("groupName");
		alertUtils = new AlertUtils(this);
		//noinspection UnnecessaryUnboxing
		groupId = (Integer.valueOf(intent.getIntExtra("groupId", 0))).intValue();
		int oneWay = intent.getIntExtra("oneWay", 0);
		if (groupId == 0) finish(); // TODO temp fix until better back button handling
		
		if (getSupportActionBar() != null) getSupportActionBar().setTitle(group);

		setUpSwipeRefresh();

		FloatingActionButton fab = findViewById(R.id.alertsFab);
		fab.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent intent = new Intent(AlertsActivity.this, SendAlertActivity.class);
				intent.putExtra("groupId", groupId);
				intent.putExtra("group", group);
				startActivityForResult(intent, CREATE_ALERT_REQUEST);
			}
		});
		// If Group is broadcast only, then remove button to add alert
		if (oneWay == 1) fab.setVisibility(View.GONE);
	}

	@Override
	public void onResume(){
		super.onResume();
		// Start retrieve Alerts process
		getAlerts();
	}

	private void setUpSwipeRefresh(){
		swLayout = findViewById(R.id.alert_swiperefresh);
		swLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
			@Override
			public void onRefresh() {
				getAlerts();
			}
		});
	}
	
	public void getAlerts() {
		swLayout.setRefreshing(true);
		alertUtils.retrieve(new AlertUtils.AlertReceiveListener() {
			@Override
			public void onReceivedAlert(List<AlertDM> alerts) {
				swLayout.setRefreshing(false);
				if (alerts != null)displayAlerts(alerts);
				else Toast.makeText(getBaseContext(), "Unable to retrieve Alerts", Toast.LENGTH_SHORT).show();
			}
		}, groupId);
	}

	private void displayAlerts(List<AlertDM> alerts) {
		//TODO Convert to RecyclerView
		ListView listView = findViewById(R.id.msgList);
		listView.setAdapter(new AlertAdapter(AlertsActivity.this, alerts));

		listView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
			@Override
			public boolean onItemLongClick(AdapterView<?> adapterView, View view, int i, long l) {
				AlertDM alert = (AlertDM) adapterView.getItemAtPosition(i);


				Intent detailsIntent = new Intent(getApplicationContext(),AlertDetailsActivity.class);
				detailsIntent.putExtra("Alert", alert);
				return true;
			}
		});

		ProgressBar progressBar =  findViewById(R.id.progressBar);
		progressBar.setVisibility(View.GONE);
		if (alerts.size() < 1){
			Snackbar.make(listView, "No Alerts Available", Snackbar.LENGTH_LONG).show();
		}
	}

	public void getLocalAlerts() {
		try {
			DatabaseHandler database = new DatabaseHandler(AlertsActivity.this, 4);
			database.open();
			List<AlertDM> messages = database.alertDBHelper.getAlertsByGroupId(groupId);
			Log.d(TAG, "Local Alerts Found: " + messages.size());
			database.close();

			displayAlerts(messages);

		} catch (Exception ex) {
			ex.printStackTrace();
			Log.wtf(TAG, "Alerts, Error loading alerts by group in act" + ex.getLocalizedMessage());
		}
	}
	
	public void unSubscribeFromGroup() {
        FirebaseMessaging.getInstance().unsubscribeFromTopic(String.valueOf(groupId));

		int userid = PrefsUtil.getUserId(this);
		JsonObject object = new JsonObject();
		object.addProperty("userid", userid);
		object.addProperty("gcm", PrefsUtil.getToken(this));

		Log.d(TAG, NetUtil.API_URL + NetUtil.SUBSCRIBE +"/" + userid + "/" + groupId);
        Log.d(TAG, object.toString());
		//building JSON
		Ion.with(getApplicationContext())
				.load("DELETE", NetUtil.API_URL + NetUtil.SUBSCRIBE +"/" + userid + "/" + groupId)
				.setHeader(NetUtil.API_KEY_PARAM, NetUtil.API_KEY)
				.setJsonObjectBody(object)
				.asJsonObject()
				.setCallback(new FutureCallback<JsonObject>() {
					@Override
					public void onCompleted(Exception e, JsonObject result) {
						if (e == null && result != null) {
							Log.d(TAG, "Unsubscribed from groups successfully with result: " + result.toString());
							Toast.makeText(getApplicationContext(), "Unsubscribed Successfully", Toast.LENGTH_SHORT).show();
							unsubscribeCachedRecords();
							finish();
						} else {
							Toast.makeText(getApplicationContext(), "Sorry, Cannot unsubscribe, Try again", Toast.LENGTH_SHORT).show();
						}
					}
				});
	}
	
	public void unsubscribeCachedRecords() {
		(new Thread() {
			public void run() {
				try {
					DatabaseHandler database = new DatabaseHandler(getApplicationContext(), 6);
					database.open();
					database.groupDBHelper.updateGroupSubscription(group, false);
					database.close();
				} catch (Exception e) {
					e.printStackTrace();
					Log.d(TAG, "Unable to update records in the database");
					e.printStackTrace();
				}
			}
		}).start();
	}

	@Override
	public int getLayoutResourceId() {
		return R.layout.activity_alerts;
	}

	@Override
	public int getColor(){
		return R.color.yellow;
	}

	@Override
	public int getColorDark(){
		return R.color.yellowDark;
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.menu_alerts, menu);
		return true;
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		int id = item.getItemId();
		if (id == R.id.unsubscribe) {
			unSubscribeFromGroup();
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
}