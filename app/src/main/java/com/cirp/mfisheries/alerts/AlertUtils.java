package com.cirp.mfisheries.alerts;

import android.content.Context;
import android.util.Log;

import com.cirp.mfisheries.alerts.model.AlertDM;
import com.cirp.mfisheries.core.DatabaseHandler;
import com.cirp.mfisheries.util.BaseUtils;
import com.cirp.mfisheries.util.NetUtil;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;
import com.squareup.moshi.JsonAdapter;
import com.squareup.moshi.Moshi;
import com.squareup.moshi.Types;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

public class AlertUtils extends BaseUtils {
	private static final String TAG = "AlertUtil";
	final long MAX_DAYS = 5;
	
	public AlertUtils(Context context) {
		super(context);
		init();
	}
	
	private void init() {
		records = new ArrayList<>();
	}
	
	
	public void retrieve(final AlertReceiveListener listener, final int groupId){
		if (!canUseCache()){
			final String url = getAlertsByGroupId(groupId);
			Log.d(TAG, "Requesting information from:" + url);
			Ion.with(context)
					.load(url)
					.setHeader(NetUtil.API_KEY_PARAM, NetUtil.API_KEY)
					.asJsonArray()
					.setCallback(new FutureCallback<JsonArray>() {
						@Override
						public void onCompleted(Exception e, JsonArray result) {
							// If no error from request, we display alerts else we load from the local storage
							if (e == null && result != null) {
								try {
									// Retrieved data and format into a list => https://github.com/square/moshi#user-content-parse-json-arrays
									Moshi moshi = new Moshi.Builder().build();
									Type listOfAlertDMType = Types.newParameterizedType(List.class, AlertDM.class);
									JsonAdapter<List<AlertDM>> jsonAdapter = moshi.adapter(listOfAlertDMType);
									List<AlertDM> alerts = jsonAdapter.fromJson(result.toString());
									// Display the list of alerts received from server
									listener.onReceivedAlert(alerts);
									if (alerts != null){
										records.clear();
										records.addAll(alerts);
										// Store the alert received
										cache(alerts);
									}
									
								}
								catch (Exception exc){
									//Toast.makeText(AlertsActivity.this, "Error Occurred When Processing Alerts.", Toast.LENGTH_SHORT).show();
									exc.printStackTrace();
								}
							} else {
								Log.d(TAG, "Error: " + e.getMessage());
								getCache(groupId, listener);
								e.printStackTrace();
							}
						}
					});
		}
	}
	
	private void cache(final List<AlertDM> alerts) {
		(new Thread(){
			public void run(){
				DatabaseHandler database = new DatabaseHandler(context, 4);
				database.open();
				try{
					for(AlertDM alert : alerts){
						database.alertDBHelper.addAlert(alert);
					}
				}catch(Exception e){
					e.printStackTrace();
				}
				database.close();
			}
		}).start();
	}
	
	private void getCache(final int groupId, AlertReceiveListener listener){
		try {
			DatabaseHandler database = new DatabaseHandler(context, 4);
			database.open();
			List<AlertDM> messages = database.alertDBHelper.getAlertsByGroupId(Integer.valueOf(groupId));
			Log.d(TAG, "Local Alerts Found: " + messages.size());
			database.close();
			listener.onReceivedAlert(messages);
		} catch (Exception ex) {
			ex.printStackTrace();
			Log.wtf(TAG, "Alerts, Error loading alerts by group in act" + ex.getLocalizedMessage());
			listener.onReceivedAlert(null);
		}
	}
	
	public void get(final AlertDM alert, final AlertUpdateListener listener){
		try{
			final String url = getAlertAPI(alert);
			Ion.with(context)
					.load( url)
					.setHeader(NetUtil.API_KEY_PARAM, NetUtil.API_KEY)
					.asJsonObject()
					.setCallback(new FutureCallback<JsonObject>() {
						@Override
						public void onCompleted(Exception e, JsonObject result) {
							if (e == null && result != null) {
								Log.d(TAG, result.toString() + "");
								
							}else   {
								listener.onUpdatedAlert(null, false);
								if (e != null)e.printStackTrace();
							}
						}
					});
		}catch (Exception e){
			e.printStackTrace();
			listener.onUpdatedAlert(alert, false);
		}
	}
	
	public void save(final AlertDM alert, final AlertUpdateListener listener){
		try {
			JsonObject jsonObject = alert.getJsonObject();
			final String url = getAlertAPI();
			Log.d(TAG, String.format("Attempting to store alert %s  to: %s", jsonObject.toString(), url ));
			Ion.with(context)
					.load("POST", url)
					.setHeader(NetUtil.API_KEY_PARAM, NetUtil.API_KEY)
					.setJsonObjectBody(jsonObject)
					.asJsonObject()
					.setCallback(new FutureCallback<JsonObject>() {
						@Override
						public void onCompleted(Exception e, JsonObject result) {
							if (e == null && result != null){
								Log.d(TAG, result.toString() + "");
								listener.onUpdatedAlert(alert, true);
							}else{
								listener.onUpdatedAlert(alert, false);
								if (e != null)e.printStackTrace();
							}
						}
					});
		}catch (Exception e){
			e.printStackTrace();
			listener.onUpdatedAlert(alert, false);
		}
	}
	
	public void update(final AlertDM alert, final AlertUpdateListener listener){
		try {
			JsonObject jsonObject = alert.getJsonObject();
			String url = getAlertAPI(alert);
			Log.d(TAG, "Attempting to update information at: " + url);
			Ion.with(context)
					.load("PUT", url)
					.setHeader(NetUtil.API_KEY_PARAM, NetUtil.API_KEY)
					.setJsonObjectBody(jsonObject)
					.asJsonObject()
					.setCallback(new FutureCallback<JsonObject>() {
						@Override
						public void onCompleted(Exception e, JsonObject result) {
							if (e == null && result != null) {
								Log.d(TAG, result.toString() + "");
								listener.onUpdatedAlert(alert, true);
							}else{
								listener.onUpdatedAlert(alert, false);
								if (e != null)e.printStackTrace();
							}
						}
					});
		}catch (Exception e ) {
			e.printStackTrace();
			listener.onUpdatedAlert(alert, false);
		}
	}
	
	public void delete(final AlertDM alert, final AlertUpdateListener listener) {
		try {
			JsonObject jsonObject = alert.getJsonObject();
			String url = getAlertAPI(alert);
			Log.d(TAG, "Attempting to Delete information at: " + url);
			Ion.with(context)
					.load("DELETE", url)
					.setHeader(NetUtil.API_KEY_PARAM, NetUtil.API_KEY)
					.setJsonObjectBody(jsonObject)
					.asString()
					.setCallback(new FutureCallback<String>() {
						@Override
						public void onCompleted(Exception e, String result) {
							if (e == null) {
								listener.onUpdatedAlert(alert, true);
							}else{
								listener.onUpdatedAlert(alert, false);
								e.printStackTrace();
							}
						}
					});
		}catch (Exception e ) {
			e.printStackTrace();
			listener.onUpdatedAlert(alert, false);
		}
	}
	
	protected boolean canUseCache() {
		return false;
	}
	
	String getAlertAPI(){
		return NetUtil.API_URL + NetUtil.ALERTS;
	}
	
	String getAlertAPI(AlertDM alert){
		return getAlertAPI() + "/" + alert.id;
	}
	
	String getGroupsURL(){
		return NetUtil.API_URL + "groups";
	}
	
	String getAlertsByGroupId(int groupId) {
		return getAlertAPI() + "?group=" + groupId;
	}
	
	public interface AlertReceiveListener {
		void onReceivedAlert(List<AlertDM> alerts);
	}
	public interface  AlertUpdateListener{
		void onUpdatedAlert(AlertDM alert, boolean status);
	}
}
