package com.cirp.mfisheries.alerts.model;

import android.arch.persistence.room.Entity;

@Entity(primaryKeys = {"alertId", "state"})
public class Like {

    private int alertId;
    private boolean state;

    public Like(int alertId, boolean state) {
        this.alertId = alertId;
        this.state = state;
    }

    public int getAlertId() {
        return alertId;
    }

    public void setAlertId(int alertId) {
        this.alertId = alertId;
    }

    public boolean isState() {
        return state;
    }

    public void setState(boolean state) {
        this.state = state;
    }
}
