package com.cirp.mfisheries.alerts;

import android.content.Context;
import android.util.Log;
import android.widget.Toast;

import com.cirp.mfisheries.alerts.model.GroupDM;
import com.cirp.mfisheries.core.DatabaseHandler;
import com.cirp.mfisheries.util.BaseUtils;
import com.cirp.mfisheries.util.NetUtil;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;
import com.squareup.moshi.JsonAdapter;
import com.squareup.moshi.Moshi;
import com.squareup.moshi.Types;

import java.lang.reflect.Type;
import java.util.List;

public class GroupUtils extends BaseUtils {
	
	private final String TAG = "GroupUtils";
	
	
	public GroupUtils(Context context) {
		super(context);
	}
	
	public void retrieve(final OnRecordsReceiveListener listener, String countryid){
	
	}
	
	public void retrieveByCountry(final OnRecordsReceiveListener listener, final String countryid){
		this.retrieve(listener, countryid);
	}
	
	public void retrieveByUserID(final OnRecordsReceiveListener listener, final String userid){
		final String url = NetUtil.API_URL + "user/" + userid + "/groups";
		Log.d(TAG, "Attempting to retrieve user groups from: " + url);
		if (NetUtil.isOnline(context)) {
			Ion.with(context)
					.load(url)
					.setHeader(NetUtil.API_KEY_PARAM, NetUtil.API_KEY)
					.asJsonObject()
					.setCallback(new FutureCallback<JsonObject>() {
						@Override
						public void onCompleted(Exception e, JsonObject result) {
							if (e == null && result != null) {
								Log.d(TAG, "Retrieved: " + result.toString());
								if (result.get("status").getAsInt() == 200) {
									try {
										JsonArray array = result.getAsJsonArray("data");
										Moshi moshi = new Moshi.Builder().build();
										Type listOfGroupDMType = Types.newParameterizedType(List.class, GroupDM.class);
										JsonAdapter<List<GroupDM>> jsonAdapter = moshi.adapter(listOfGroupDMType);
										List<GroupDM> groups = jsonAdapter.fromJson(array.toString());
										// Cache Records in Database
										cache(groups);
										// Display records to the user
										listener.onRecordsReceived(groups);
										return;
									} catch (Exception ex) {
										Log.d(TAG, "Failure to parse group: " + ex.getMessage());
										ex.printStackTrace();
									}
								}
							} else {
								if (e != null) e.printStackTrace();
								Log.d(TAG, "Unable to retrieve groups from server. Attempting to use local records");
							}
							// Did not receive groups from Server so we retrieve from the cache
							getCache(new OnRecordsReceiveListener() {
								@Override
								public void onRecordsReceived(List<GroupDM> records) {
									listener.onRecordsReceived(records);
								}
							}, userid);
							
						}
					});
		}else{
			Toast.makeText(context, "Application is offline. Using cached groups.", Toast.LENGTH_SHORT).show();
			getCache(new OnRecordsReceiveListener() {
				@Override
				public void onRecordsReceived(List<GroupDM> records) {
					listener.onRecordsReceived(records);
				}
			}, userid);
		}
	}
	
	void cache(final List<GroupDM> groups){
		// TODO attempt to retrieve from cache
		(new Thread(){
			public void run(){
				//adding to the database
				final DatabaseHandler database = new DatabaseHandler(context, DatabaseHandler.GROUP_DB_HELPER);
				try {
					database.open();
					for (GroupDM group : groups) {
						Log.d("GroupDirection", group.oneWay + "");
						database.groupDBHelper.addGroup(group);
					}
				} catch (Exception exc) {
					Log.d("Alert Group", "Error adding groups in " + exc.getLocalizedMessage());
				}finally {
					database.close();
				}
			}
		}).start();
		
	}
	
	void getCache(final OnRecordsReceiveListener listener, final String recid){
		Log.d(TAG, "Retrieving records from local cache");
		(new Thread(){
			public void run(){
				final DatabaseHandler database = new DatabaseHandler(context, 6);
				try {
					database.open();
					List<GroupDM> groupsFromDB = database.groupDBHelper.getAllGroups();
					Log.d(TAG, String.format("Retrieved %s groups from cache", groupsFromDB.size()));
					listener.onRecordsReceived(groupsFromDB);
					return;
				} catch (Exception e) {
					e.printStackTrace();
				} finally {
					database.close();
				}
				listener.onRecordsReceived(null);
			}
		}).start();
	}
	
	
	
	public interface OnRecordsReceiveListener{
		void onRecordsReceived(List<GroupDM> records);
	}
	
	public interface OnRecordUpdateListener{
		void onRecordUpdated(GroupDM groupDM, boolean status);
	}
}
