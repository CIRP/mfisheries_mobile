package com.cirp.mfisheries.weather.models;

import android.support.annotation.NonNull;

import com.cirp.mfisheries.R;

import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;

/**
 * Created by kersc on 7/20/2017.
 */

public class CurrentReadings implements List<CurrentReadings> {
    public String reading;
    public String icon;
    public String head;
    public String threshold = "nothing";


    public String tide ="a";
    public String times ="a";
    public String feet= "a";



    public CurrentReadings(String tide, String times,String feet,String place,String data) {
        this.tide = tide;
        this.times = times;
        this.feet = feet;
    }

    public CurrentReadings(String reading, String icon, String head, String threshold) {
        this.reading = reading;
        this.icon = icon;
        this.head = head;
        this.threshold = threshold;
    }

    public CurrentReadings() {

    }

    public String getThreshold() {
        return threshold;
    }

    public void setThreshold(String threshold) {
        this.threshold = threshold;
    }

    public CurrentReadings(String reading, String icon, String head) {
        this.reading = reading;
        this.icon = icon;
        this.head = head;
    }

    public String getHead() {
        return head;
    }

    public void setHead(String head) {
        this.head = head;
    }

    public String grabTextIcon() { return icon; }



    public static int getIcon(String code) {
        switch (code) {
            case "red":
                return R.color.customred;
            case "yellow":
                return R.color.yellowDark;

            default:
                return 1;
    }
    }


//    public static int getImage(String code) {
////        switch (code) {
////            case "pressure":
////                return R.drawable.pressure;
////            case "temperature":
////                return R.drawable.temperature;
////            case "humidity":
////                return R.drawable.humidity;
////            case "rel. humidity":
////                return R.drawable.humidity;
////            case "wind":
////                return R.drawable.wind;
////            case "sunrise":
////                return R.drawable.sunrise;
////            case "sunset":
////                return R.drawable.sunset;
////            case "visibility":
////                return R.drawable.visibility;
////            case "cloudypercent":
////                return R.drawable.cloudpercent;
////
////            default:
////                return 1;
////        }
////    }
    public static int getImage(String code)
    {
        String lowerCode = code.toLowerCase();
        if(lowerCode.contains("pressure"))
        {
            return R.drawable.pressure;
        }
        if(lowerCode.contains("temperature") || lowerCode.contains("temp"))
        {
            return R.drawable.temperature;
        }
        if(lowerCode.contains("humidity"))
        {
            return R.drawable.humidity;
        }
        if(lowerCode.contains("rel. humidity"))
        {
            return R.drawable.humidity;
        }
        if(lowerCode.contains("wind") || lowerCode.contains("winds"))
        {
            return R.drawable.wind;
        }
        if(lowerCode.contains("sunrise"))
        {
            return R.drawable.sunrise;
        }
        if(lowerCode.contains("sunset"))
        {
            return R.drawable.sunset;
        }
        if(lowerCode.contains("visibility"))
        {
            return R.drawable.visibility;
        }
        if(lowerCode.contains("cloudypercent"))
        {
            return R.drawable.cloudpercent;
        }
        else
        {
            return 1;
        }
    }


    public void setIcon(String icon) {
        this.icon = icon;
    }

    public String getReading() {
        return reading;
    }

    public void setReading(String reading) {
        this.reading = reading;
    }


    @Override
    public int size() {
        return 0;
    }

    @Override
    public boolean isEmpty() {
        return false;
    }

    @Override
    public boolean contains(Object o) {
        return false;
    }

    @NonNull
    @Override
    public Iterator<CurrentReadings> iterator() {
        return null;
    }

    @NonNull
    @Override
    public Object[] toArray() {
        return new Object[0];
    }

    @NonNull
    @Override
    public <T> T[] toArray(@NonNull T[] a) {
        return null;
    }

    @Override
    public boolean add(CurrentReadings currentReadings) {
        return false;
    }

    @Override
    public boolean remove(Object o) {
        return false;
    }

    @Override
    public boolean containsAll(@NonNull Collection<?> c) {
        return false;
    }

    @Override
    public boolean addAll(@NonNull Collection<? extends CurrentReadings> c) {
        return false;
    }

    @Override
    public boolean addAll(int index, @NonNull Collection<? extends CurrentReadings> c) {
        return false;
    }

    @Override
    public boolean removeAll(@NonNull Collection<?> c) {
        return false;
    }

    @Override
    public boolean retainAll(@NonNull Collection<?> c) {
        return false;
    }

    @Override
    public void clear() {

    }

    @Override
    public CurrentReadings get(int index) {
        return null;
    }

    @Override
    public CurrentReadings set(int index, CurrentReadings element) {
        return null;
    }

    @Override
    public void add(int index, CurrentReadings element) {

    }

    @Override
    public CurrentReadings remove(int index) {
        return null;
    }

    @Override
    public int indexOf(Object o) {
        return 0;
    }

    @Override
    public int lastIndexOf(Object o) {
        return 0;
    }

    @Override
    public ListIterator<CurrentReadings> listIterator() {
        return null;
    }

    @NonNull
    @Override
    public ListIterator<CurrentReadings> listIterator(int index) {
        return null;
    }

    @NonNull
    @Override
    public List<CurrentReadings> subList(int fromIndex, int toIndex) {
        return null;
    }
}
