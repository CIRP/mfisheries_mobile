package com.cirp.mfisheries.weather.models;

import android.content.Context;

import com.cirp.mfisheries.R;

public class CurrentWeather {
	public Coord coord;
	public Sys sys;
	public Weather weather[];
	public Main main;
	public Wind wind;
	public Clouds clouds;
	public long dt;
	public long id;
	public String name;
	public long cod;
	

	public static class Coord {
		public double lon;
		public double lat;
	}

	public static class Sys {
		public String country;
		public long sunrise;
		public long sunset;
	}

	public static class Weather {
		public long id;
		public String main;
		public String description;
		public String icon;
	}

	public static class Main {
		public double temp;
		public long humidity;
		public double pressure;
		public double temp_min;
		public double temp_max;
	}

	public static class Wind {
		public double speed;
		public double deg;
	}

	public static class Clouds {
		public long all;
	}

	public static int getIcon(String iconCode, Context context) {
		switch (iconCode) {
			case "01d":
				return R.drawable.sunny;
			case "01n":
				return R.drawable.sunny;
			case "02d":
				return R.drawable.mostly_cloudy;
			case "02n":
				return R.drawable.cloudy_night;
			case "03d":
				return R.drawable.cloudy;
			case "03n":
				return R.drawable.cloudy_night;
			case "04d":
				return R.drawable.cloudy;
			case "04n":
				return R.drawable.cloudy_night;
			case "09d":
				return R.drawable.slight_drizzle;
			case "09n":
				return R.drawable.drizzle_night;
			case "10d":
				return R.drawable.drizzle;
			case "10n":
				return R.drawable.drizzle_night;
			case "11d":
				return R.drawable.thunderstorms;
			case "11n":
				return R.drawable.thunderstorms_night;
			case "13d":
				return R.drawable.snow;
			case "13n":
				return R.drawable.snow;
			case "50d":
				return R.drawable.haze;
			case "50n":
				return R.drawable.cloudy;
			default:
				return R.drawable.sunny;
		}
	}
}
