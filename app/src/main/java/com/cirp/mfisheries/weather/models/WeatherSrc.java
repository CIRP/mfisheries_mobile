package com.cirp.mfisheries.weather.models;

public class WeatherSrc {
	public final String id;
	public final String countryid;
	public final String country;
	public final String infotype;
	public final String name;
	public final String lastexecuted;
	
	public WeatherSrc(String id, String countryid, String country, String infotype, String name, String lastexecuted, String url) {
		this.id = id;
		this.countryid = countryid;
		this.country = country;
		this.infotype = infotype;
		this.name = name;
		this.lastexecuted = lastexecuted;
		this.url = url;
	}
	
	public final String url;

}
