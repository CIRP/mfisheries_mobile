package com.cirp.mfisheries.weather.models;

import java.util.List;
import java.util.Map;

public class WeatherReading {
	public String id;
	public String country;
	public String countryid;
	public String sourceid;
	public List<Map<String, String>>readings;
	
}
