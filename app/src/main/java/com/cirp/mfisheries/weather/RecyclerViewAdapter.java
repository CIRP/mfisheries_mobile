package com.cirp.mfisheries.weather;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.cirp.mfisheries.R;
import com.cirp.mfisheries.weather.models.CurrentWeather;
import com.cirp.mfisheries.weather.models.Forecast;

import java.text.SimpleDateFormat;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Locale;

@SuppressWarnings("WeakerAccess")
public class RecyclerViewAdapter extends RecyclerView.Adapter<RecyclerViewAdapter.ViewHolder>{
    private final List<Forecast.Condition> conditions;
    Context context;

    public RecyclerViewAdapter(Context context, List<Forecast.Condition> conditions ){
        this.conditions = conditions;
        this.context = context;
    }

    @SuppressWarnings("WeakerAccess")
    public static class ViewHolder extends RecyclerView.ViewHolder{
        public ImageView iconView;
        public TextView date;
        public TextView temp;
        public TextView condition;


        public ViewHolder(View v){

            super(v);
            iconView = itemView.findViewById(R.id.forecast_icon);
            date = itemView.findViewById(R.id.forecast_date);
            temp = itemView.findViewById(R.id.forecast_temp);
            condition = itemView.findViewById(R.id.forecast_condition);
        }
    }

    @NonNull
    @Override
    public RecyclerViewAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType){
        View forecastLayoutView = LayoutInflater.from(context).inflate(R.layout.layout_forecast, parent, false);
        return new ViewHolder(forecastLayoutView);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position){
        Forecast.Condition condition = conditions.get(position);
        Date date = new Date(condition.dt * 1000);
        SimpleDateFormat format = new SimpleDateFormat("EEE", Locale.ENGLISH);
        holder.date.setText(format.format(date));
        holder.temp.setText(String.format(Locale.US,"%d - %d °C", Math.round(condition.temp.min), Math.round(condition.temp.max)));
        holder.condition.setText(condition.weather[0].main);
        Glide.with(context)
                .load(CurrentWeather.getIcon(condition.weather[0].icon, context))
                .into(holder.iconView);
    }

    @Override
    public int getItemCount(){

        return conditions.size();
    }
}