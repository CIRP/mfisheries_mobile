package com.cirp.mfisheries.weather;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.cirp.mfisheries.R;
import com.cirp.mfisheries.weather.models.Tides;

import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Locale;

public class TidesAdapter extends BaseAdapter{
    Context context;
    List <Tides> weather;

    public TidesAdapter(Context context, List<Tides> weather) {
        this.context = context;
        this.weather = weather;
    }


    @Override
    public int getCount(){return weather.size();}

    @Override
    public Object getItem(int position){return weather.get(position);}

    @Override
    public long getItemId(int position){return weather.indexOf(getItem(position));}

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View v = View.inflate(context,R.layout.tides_fragment,null);


        TextView feet = v.findViewById(R.id.height);
        TextView tide = v.findViewById(R.id.tide);
        TextView time = v.findViewById(R.id.time);
        ImageView image = v.findViewById(R.id.tideImage);


        Tides entry = weather.get(position);
            feet.setText(entry.getFeet());
            tide.setText(entry.getTide());
            SimpleDateFormat format = new SimpleDateFormat("EEE h:mm a", Locale.ENGLISH);

            time.setText(entry.getTimes());
            if(entry.tide.equals("Low Tide"))
            image.setImageResource(R.drawable.down_arrow);
            else
                image.setImageResource(R.drawable.up_arrow);
        return v;
    }



}
