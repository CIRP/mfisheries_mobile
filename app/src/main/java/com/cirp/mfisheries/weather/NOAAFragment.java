package com.cirp.mfisheries.weather;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;

import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.cirp.mfisheries.R;
import com.cirp.mfisheries.util.GlideApp;
import com.cirp.mfisheries.util.NetUtil;
import com.cirp.mfisheries.weather.marine.MarineActivity;
import com.cirp.mfisheries.weather.marine.MarineUtil;
import com.cirp.mfisheries.weather.models.Marine;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link NOAAFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link NOAAFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class NOAAFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String TAG = "NOAAFragment";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;
    private Context context;
    private List<Marine> marines;
    private MarineUtil marineUtil;

    // The View components used for the user
    private CheckBox check1, check2, check3, check4,check5,check6;
    private ImageView image1, image2, image3, image4, baseImage, image5, image6, image7, image8,leftArrow,rightArrow;
    private Button button1, button2, button3, buttonBack, buttonForward;
    ArrayList<String> first = new ArrayList<>();
    ArrayList<String> second = new ArrayList<>();
    ArrayList<String> third = new ArrayList<>();
    ArrayList<String> extras = new ArrayList<>();
    private Integer position = 1;

    public NOAAFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @return A new instance of fragment NOAAFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static NOAAFragment newInstance(Context context, String param1) {
        NOAAFragment fragment = new NOAAFragment();
        fragment.context = context;
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
        }
        marines = new ArrayList<>();
        marineUtil =  new MarineUtil(getActivity()); //context

//        Intent intent = new Intent(getContext(),MarineActivity.class);
//        startActivity(intent);
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view =  inflater.inflate(R.layout.fragment_noaa, container, false);

        // Assign the Views that will handle the images
        baseImage = view.findViewById(R.id.baseImg);
        image1 = view.findViewById(R.id.windImage);
        image2 = view.findViewById(R.id.barbsImage);
        image3 = view.findViewById(R.id.contourImage);
        image4 = view.findViewById(R.id.featuresImage);
        image5 = view.findViewById(R.id.waveHeightImage);
        image6 = view.findViewById(R.id.hazardImage);
        image7 = view.findViewById(R.id.sigWaveImage);
        image8 = view.findViewById(R.id.windWave);
        leftArrow = view.findViewById(R.id.imageView7);
        rightArrow = view.findViewById(R.id.imageView8);

        //buttonBack = view.findViewById(R.id.button6);
        //buttonForward = view.findViewById(R.id.button7);



        // Assign the controls
        check1 = view.findViewById(R.id.checkBox2);
        check2 = view.findViewById(R.id.checkBox4);
        check3 = view.findViewById(R.id.checkBox6);
        check4 = view.findViewById(R.id.checkBox8);
        check5 = view.findViewById(R.id.checkBox5);
        check6 = view.findViewById(R.id.checkBox7);

        // Load the data from the API
        retrieveAndDisplayData();
        // Configure the controls of the checkboxes
        configureControls();

        return view;
    }

    private void configureControls() {

        check1.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener()
        {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked)
            {
                if ( isChecked )
                {
                    check5.setChecked(false);
                    check6.setChecked(false);
                    //image1.setImageResource(R.drawable.arrow); //glide get windimage

                    image1.setVisibility(View.VISIBLE);

                }
                if (!isChecked)
                {
                    image1.setVisibility(View.GONE);
                }

            }
        });
        check2.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener()
        {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked)
            {
                if ( isChecked )
                {
                    image2.setVisibility(View.VISIBLE);
                }
                if (!isChecked)
                {
                    image2.setVisibility(View.GONE);
                }

            }
        });
        check3.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener()
        {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked)
            {
                if ( isChecked )
                {
                    image3.setVisibility(View.VISIBLE);
                }
                if (!isChecked)
                {
                    image3.setVisibility(View.GONE);
                }

            }
        });
        check4.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener()
        {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked)
            {
                if ( isChecked )
                {
                    image4.setVisibility(View.VISIBLE);
                }
                if (!isChecked)
                {
                    image4.setVisibility(View.GONE);
                }

            }
        });
        check5.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener()
        {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked)
            {
                if ( isChecked )
                {
                    check1.setChecked(false);
                    check6.setChecked(false);
                    //image1.setImageResource(R.drawable.arrow); //glide get waveheightimage
                    image5.setVisibility(View.VISIBLE);

                }
                if (!isChecked)
                {
                    image5.setVisibility(View.GONE);
                }

            }
        });
        check6.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener()
        {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked)
            {
                if ( isChecked )
                {
                    check1.setChecked(false);
                    check5.setChecked(false);
                    //image1.setImageResource(R.drawable.arrow);//Glide get hazards
                    image6.setVisibility(View.VISIBLE);

                }
                if (!isChecked)
                {
                    image6.setVisibility(View.GONE);
                }
            }
        });

        leftArrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Code here executes on main thread after user presses button
                final String url = NetUtil.SITE_URL;
                int currentHour =  Calendar.getInstance().get(Calendar.HOUR_OF_DAY);
                if(position ==2)
                {
                    position =1;
                    String one = url + first.get(0);
                    String two = url + first.get(1);
                    String three = url + first.get(2);
                    String four = url + first.get(3);
                    String five = url + first.get(4);
                    String six = url + first.get(5);

                    GlideApp.with(context).load(six).diskCacheStrategy(DiskCacheStrategy.NONE).skipMemoryCache(true).into(image1);
                    GlideApp.with(context).load(five).diskCacheStrategy(DiskCacheStrategy.NONE).skipMemoryCache(true).into(image2);
                    GlideApp.with(context).load(three).diskCacheStrategy(DiskCacheStrategy.NONE).skipMemoryCache(true).into(image3);
                    GlideApp.with(context).load(one).diskCacheStrategy(DiskCacheStrategy.NONE).skipMemoryCache(true).into(image4);
                    GlideApp.with(context).load(four).diskCacheStrategy(DiskCacheStrategy.NONE).skipMemoryCache(true).into(image5);
                    GlideApp.with(context).load(two).diskCacheStrategy(DiskCacheStrategy.NONE).skipMemoryCache(true).into(image6);
                }
                else if(position ==3)
                {
                    position =2;
                    String one = url + second.get(0);
                    String two = url + second.get(1);
                    String three = url + second.get(2);
                    String four = url + second.get(3);
                    String five = url + second.get(4);
                    String six = url + second.get(5);

                    GlideApp.with(context).load(six).diskCacheStrategy(DiskCacheStrategy.NONE).skipMemoryCache(true).into(image1);
                    GlideApp.with(context).load(five).diskCacheStrategy(DiskCacheStrategy.NONE).skipMemoryCache(true).into(image2);
                    GlideApp.with(context).load(three).diskCacheStrategy(DiskCacheStrategy.NONE).skipMemoryCache(true).into(image3);
                    GlideApp.with(context).load(one).diskCacheStrategy(DiskCacheStrategy.NONE).skipMemoryCache(true).into(image4);
                    GlideApp.with(context).load(four).diskCacheStrategy(DiskCacheStrategy.NONE).skipMemoryCache(true).into(image5);
                    GlideApp.with(context).load(two).diskCacheStrategy(DiskCacheStrategy.NONE).skipMemoryCache(true).into(image6);
                }

            }
        });
        rightArrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Code here executes on main thread after user presses button
                final String url = NetUtil.SITE_URL;
                int currentHour =  Calendar.getInstance().get(Calendar.HOUR_OF_DAY);
                if(position == 1)
                {
                    position =2;
                    String one = url + second.get(0);
                    String two = url + second.get(1);
                    String three = url + second.get(2);
                    String four = url + second.get(3);
                    String five = url + second.get(4);
                    String six = url + second.get(5);

                    GlideApp.with(context).load(six).diskCacheStrategy(DiskCacheStrategy.NONE).skipMemoryCache(true).into(image1);
                    GlideApp.with(context).load(five).diskCacheStrategy(DiskCacheStrategy.NONE).skipMemoryCache(true).into(image2);
                    GlideApp.with(context).load(three).diskCacheStrategy(DiskCacheStrategy.NONE).skipMemoryCache(true).into(image3);
                    GlideApp.with(context).load(one).diskCacheStrategy(DiskCacheStrategy.NONE).skipMemoryCache(true).into(image4);
                    GlideApp.with(context).load(four).diskCacheStrategy(DiskCacheStrategy.NONE).skipMemoryCache(true).into(image5);
                    GlideApp.with(context).load(two).diskCacheStrategy(DiskCacheStrategy.NONE).skipMemoryCache(true).into(image6);
                }
                else if(position ==2)
                {
                    position =3;
                    String one = url + third.get(0);
                    String two = url + third.get(1);
                    String three = url + third.get(2);
                    String four = url + third.get(3);
                    String five = url + third.get(4);
                    String six = url + third.get(5);

                    GlideApp.with(context).load(six).diskCacheStrategy(DiskCacheStrategy.NONE).skipMemoryCache(true).into(image1);
                    GlideApp.with(context).load(five).diskCacheStrategy(DiskCacheStrategy.NONE).skipMemoryCache(true).into(image2);
                    GlideApp.with(context).load(three).diskCacheStrategy(DiskCacheStrategy.NONE).skipMemoryCache(true).into(image3);
                    GlideApp.with(context).load(one).diskCacheStrategy(DiskCacheStrategy.NONE).skipMemoryCache(true).into(image4);
                    GlideApp.with(context).load(four).diskCacheStrategy(DiskCacheStrategy.NONE).skipMemoryCache(true).into(image5);
                    GlideApp.with(context).load(two).diskCacheStrategy(DiskCacheStrategy.NONE).skipMemoryCache(true).into(image6);
                }

            }
        });

    }

    private void retrieveAndDisplayData() {
        marineUtil.retrieveMarineList(new MarineUtil.MarineSourceListener() {
            @Override
            public void onReceiveMarineList(@Nullable List<Marine> marineList) {
                if(marineList != null)
                {
                    Log.d(TAG, "Successfully retrieved marine list");


                    Log.d(TAG,"myMarine1: "+marineList);
                    marines = marineList;
                    Log.d(TAG,"thisMarine: "+marines);
                    int currentHour =  Calendar.getInstance().get(Calendar.HOUR_OF_DAY);
                    for(int i = 0; i < marines.size(); i += 1)
                    {
                        Log.d(TAG,"Marinez: "+marines.get(i).getPath());
                        String path = marines.get(i).getPath();
                        if(path.toLowerCase().contains("0"))
                        {
                            Log.d(TAG, "Adding " + path + " to first");
                            first.add(path);
                        }
                        else if(path.toLowerCase().contains("12"))
                        {
                            Log.d(TAG, "Adding " + path + " to second");
                            second.add(path);
                        }
                        else if(path.toLowerCase().contains("24"))
                        {
                            Log.d(TAG, "Adding " + path + " to third");
                            third.add(path);
                        }
                        else
                        {
                            Log.d(TAG, "Adding " + path + " to extra");
                            extras.add(path);
                        }
                    }


                    final String url = NetUtil.SITE_URL;
                    //Separate the URL and the request for debugging
                    String first_url = "";
                    String second_url = "";
                    String third_url ="";
                    String fourth_url ="";
                    String fifth_url ="";
                    String sixth_url ="";


                    //May not be necessary
//                    Collections.sort(first,String.CASE_INSENSITIVE_ORDER);
//                    Collections.sort(second,String.CASE_INSENSITIVE_ORDER);
//                    Collections.sort(third,String.CASE_INSENSITIVE_ORDER);

                    //To display the images relevant to the current time of the user
                    if(currentHour >0 && currentHour < 8)
                    {
                        position = 1;
                        Log.d("First","Entered first");
                        first_url = url + first.get(0);
                        second_url = url + first.get(1);
                        third_url = url + first.get(2);
                        fourth_url = url + first.get(3);
                        fifth_url = url + first.get(4);
                        sixth_url = url + first.get(5);
                    }
                    else if(currentHour >=8 && currentHour < 20)
                    {
                        position = 2;
                        Log.d("Second","Entered second");
                        first_url = url + second.get(0);
                        second_url = url + second.get(1);
                        third_url = url + second.get(2);
                        fourth_url = url + second.get(3);
                        fifth_url = url + second.get(4);
                        sixth_url = url + second.get(5);
                    }
                    else
                    {
                        position = 3;
                        Log.d("Third","Entered third");
                        first_url = url + third.get(0);
                        second_url = url + third.get(1);
                        third_url = url + third.get(2);
                        fourth_url = url + third.get(3);
                        fifth_url = url + third.get(4);
                        sixth_url = url + third.get(5);
                    }




                    for(int i=0;i<extras.size();i++)
                    {
                        if(extras.get(i).contains("CARIB_blank"))
                        {
                            String baseImgUrl = url + extras.get(i);
                            GlideApp.with(context).load(baseImgUrl).into(baseImage);
                        }
                        if(extras.get(i).contains("sigWaves"))
                        {
                            String sigWaveUrl = url + extras.get(i);
                            GlideApp.with(context).load(sigWaveUrl).diskCacheStrategy(DiskCacheStrategy.NONE).skipMemoryCache(true).into(image7);
                        }
                        if(extras.get(i).contains("windWave"))
                        {
                            String windWaveUrl = url + extras.get(i);
                            GlideApp.with(context).load(windWaveUrl).diskCacheStrategy(DiskCacheStrategy.NONE).skipMemoryCache(true).into(image8);
                        }
                    }

                    Log.d(TAG, String.format("1st %s, 2nd %s, 3rd %s, 4th %s, 5th %s, 6th %s", first_url, second_url, third_url, fourth_url, fifth_url, sixth_url));

                    // request images and load into respective views
                    GlideApp.with(context).load(sixth_url).diskCacheStrategy(DiskCacheStrategy.NONE).skipMemoryCache(true).into(image1);
                    GlideApp.with(context).load(fifth_url).diskCacheStrategy(DiskCacheStrategy.NONE).skipMemoryCache(true).into(image2);
                    GlideApp.with(context).load(third_url).diskCacheStrategy(DiskCacheStrategy.NONE).skipMemoryCache(true).into(image3);
                    GlideApp.with(context).load(first_url).diskCacheStrategy(DiskCacheStrategy.NONE).skipMemoryCache(true).into(image4);
                    GlideApp.with(context).load(fourth_url).diskCacheStrategy(DiskCacheStrategy.NONE).skipMemoryCache(true).into(image5);
                    GlideApp.with(context).load(second_url).diskCacheStrategy(DiskCacheStrategy.NONE).skipMemoryCache(true).into(image6);

                    // Load an initial image and set control to reflect loaded option
                    //displayDefaultMap();

                }else{
                    Log.d(TAG, "Unable to successfully retrieve marine list");
                }
            }
        });
    }

    /**
     * Provides an initial map for users to appreciate that information is loaded
     */
    private void displayDefaultMap() {
        // Hide the placeholder
        baseImage.setVisibility(View.INVISIBLE);
        // Display map
        image4.setVisibility(View.VISIBLE);
        // Enable control
        check4.setChecked(true);
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
