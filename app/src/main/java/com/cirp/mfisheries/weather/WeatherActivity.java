package com.cirp.mfisheries.weather;

import android.Manifest;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.location.Location;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.cirp.mfisheries.R;
import com.cirp.mfisheries.core.Country;
import com.cirp.mfisheries.core.CountryAdapter;
import com.cirp.mfisheries.core.location.LocationActivity;
import com.cirp.mfisheries.util.CountryUtil;
import com.cirp.mfisheries.util.PrefsUtil;
import com.cirp.mfisheries.util.adapters.ViewPagerAdapter;

import java.util.ArrayList;
import java.util.List;


public class WeatherActivity extends LocationActivity implements DisplayCountryUpdatable {
	
	private final String TAG = "WeatherActivity";
	private String countryid;
	private String countryName;
	private TextView cNameView;
	private ArrayList<LocationUpdatable> updatables;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		super.onStart();
		module = new Weather(this);
		module.onInstalled();
		permissions = new String[]{Manifest.permission.ACCESS_FINE_LOCATION};
		requestPermissions();
		
		// Used to display Current Country to user
		cNameView = findViewById(R.id.countryname);
		
		displayCountrySelection(PrefsUtil.getCountry(this));
		
		// Stores all classes that need to be updated when country changes
		updatables = new ArrayList<>();
		
		setupViewPager();
	}

	@Override
	public void onPermissionGranted(String permission) {
		super.onPermissionGranted(permission);
//		setupViewPager();
	}

	@Override
	public void onLocationChanged(Location location) {
		super.onLocationChanged(location);
		// Only need current location, so we can stop location listener as soon as we retrieve the first location
		stopLocationUpdates();
	}

	//sets up the fragments for each page of the screen
	private void setupViewPager() {
		Log.d(TAG, "Setting up View pager");
		final ViewPager viewPager = findViewById(R.id.tabanim_viewpager);
		//gets current filepath from either Cache of GPS
		if (mCurrentLocation == null) {
			//if current filepath not available, gets last known filepath
			mCurrentLocation = new Location("Weather");
			
			// Retrieve Current Location
			SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(this);
			mCurrentLocation.setLatitude(preferences.getFloat("lat", 0));
			mCurrentLocation.setLongitude(preferences.getFloat("lng", 0));
		}

		ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());

		//adds fragments to viewpager adapter
		CurrentWeatherFragment weatherFragment = CurrentWeatherFragment.newInstance(this, mCurrentLocation.getLatitude(), mCurrentLocation.getLongitude(), (Weather) module);
		TidesFragment tidesFragment = TidesFragment.newInstance(this,(Weather)module);
		METFragment metFragment = METFragment.newInstance(this, (Weather) module);
        //RecommendAppsFragment suggestedFragment = RecommendAppsFragment.newInstance(this,(Weather) module);
        NOAAFragment noaaFragment = NOAAFragment.newInstance(this, "Param2");


		adapter.addFrag(weatherFragment, "Open Weather");
		adapter.addFrag(metFragment, "MET Office");
		//adapter.addFrag(suggestedFragment,"Suggested Apps");
//		adapter.addFrag(noaaFragment, "NOAA Marine");
		adapter.addFrag(tidesFragment,"Tides Forecast");

		Log.i(TAG, "Added the fragments to the adapter");

		//sets adapter to viewpager
		viewPager.setAdapter(adapter);
		TabLayout tabLayout = findViewById(R.id.tabanim_tabs);
		tabLayout.setupWithViewPager(viewPager);
		Log.d(TAG, "View Pager added to the tab layout");
		
		// Set fragments to be notified when location changes
		updatables.add(weatherFragment);
		updatables.add(tidesFragment);
		updatables.add(metFragment);
		
		// set updates to change UI when fragment location changes
		weatherFragment.setCountryUpdates(this);

		//opens the external tide app when the last fragment is selected
		viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
			public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {}
			public void onPageSelected(int position) { }
			public void onPageScrollStateChanged(int state) { }
		});
	}
	
	public void chooseCountry(View view){
		CountryUtil.getInstance(this).retrieveCountries(new CountryUtil.CountryRetrievedListener() {
			@Override
			public void processCountries(List<Country> countries) {
				if (countries != null){
					displayCountries(countries);
				}else{
					Toast.makeText(WeatherActivity.this, "Unable to retrieve countries", Toast.LENGTH_SHORT).show();
				}
			}
		});
	}
	
	private void displayCountries(final List<Country> countries){
		final CountryAdapter arrayAdapter = new CountryAdapter(this, countries);
		
		new AlertDialog.Builder(this)
				.setTitle(getResources().getString(R.string.dialog_country))
				.setCancelable(false)
				.setAdapter(arrayAdapter,
						new DialogInterface.OnClickListener() {
							@Override
							public void onClick(DialogInterface dialog, int which) {
								Country selectedCountry = countries.get(which);
								countryid = selectedCountry.getId();
								countryName = selectedCountry.getName();
								
								notifyCountryChanged(selectedCountry);
								displayCountrySelection(countryName);
							}
						})
				.show();
	}
	
	private void notifyCountryChanged(Country selectedCountry) {
		boolean result = true;
		if (updatables != null){
			Log.d(TAG, "Number of updatable resources: " + updatables.size());
			int i = 0;
			for(LocationUpdatable updatable : updatables){
				Log.d(TAG, "Processing updatable: " + ++i);
				updatable.updateLocation(selectedCountry);
			}
		}
		Log.d(TAG, "Country changed produced: " + result);
	}
	
	private void displayCountrySelection(String country){
		String msg = String.format("Displaying Weather for: %s", country);
		cNameView.setText(msg);
	}

	@Override
	public int getLayoutResourceId() {
		return R.layout.activity_weather;
	}

	@Override
	public int getColor(){
		return R.color.blue;
	}

	@Override
	public int getColorDark(){
		return R.color.blueDark;
	}
	
	@Override
	public void updateCountry(String country) {
		Log.d(TAG, "A fragment request the country source be udpated to" + country);
		displayCountrySelection(country);
	}
	
	public interface LocationUpdatable{
		boolean updateLocation(Country country);
	}
	
	
	
	
}
