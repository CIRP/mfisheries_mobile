package com.cirp.mfisheries.weather;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.location.Location;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AbsListView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.cirp.mfisheries.R;
import com.cirp.mfisheries.core.Country;
import com.cirp.mfisheries.core.CountryAdapter;
import com.cirp.mfisheries.core.CountryLoc;
import com.cirp.mfisheries.core.module.ModuleActivity;
import com.cirp.mfisheries.util.CountryUtil;
import com.cirp.mfisheries.util.NetUtil;
import com.cirp.mfisheries.util.PrefsUtil;
import com.cirp.mfisheries.weather.models.CurrentReadings;
import com.cirp.mfisheries.weather.models.CurrentWeather;
import com.cirp.mfisheries.weather.models.WeatherSrc;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;


import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;


/*
This page gives the user more details on the current weather. It also displays values that were triggered based on the thresholds
configured by colour coding them. Yellow for warning , red for emergency
Similar to the Tides, when configuring the weather source on the web service you must set infotype as Weather
in order for the android system to detect it as a valid weather source.
 */
public class SourceActivity extends ModuleActivity {
    private static final String TAG = "WeatherSourceActivity";
    public String openweather = "";
    public Location mCurrentLocation;
    public ArrayAdapter<String> adapterSource;
    public String share = "";
    public String sourceid;
    public String save ="";
    public Context context;
    public List<String> sourcelist;
    public String selectedSource;
    public Country currCountry;
    TextView cNameView;
    TextView sourceName;
    Button changeCountryBtn;
    private ListView listView;
    private SwipeRefreshLayout swipe_view;
    private CurrentAdapter adapter;
    private SwipeRefreshLayout swLayout;
    private JSONArray orderJSON;
    private List<String> myList;

    private int numRetryThresholds = 0;
    private WeatherUtil weatherUtil;
    private int numTries;
    private double lat;
    private double lon;
    
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_weather_source, menu);
        return true;
    }
    
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case R.id.action_share:
                Intent shares =new Intent(Intent.ACTION_SEND);
                shares.setType("text/plain");
                shares.putExtra(Intent.EXTRA_SUBJECT,"Today's Weather Condition");
                shares.putExtra(Intent.EXTRA_TEXT,share);
                startActivity(Intent.createChooser(shares,"Share Via"));
                return true;
            case R.id.changeSource:
                View v = new View(SourceActivity.this);
                selectSource(v);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

//    public class keyValue{
//        String key;
//        String value;
//
//        public keyValue(String key, String value)
//        {
//            this.key = key;
//            this.value = value;
//        }
//    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        context = getApplicationContext();
        module = new Weather(this);
        weatherUtil = new WeatherUtil(context);
        sourcelist = new ArrayList<>();
        numRetryThresholds = 0;

        // Setup UI Components
        setUpSwipeRefresh();
        // Adapter For the Source list
        adapterSource = new ArrayAdapter<>(context,R.layout.list_item,R.id.sourceitem,sourcelist); // TODO Remove As an alert dialog would be sufficient
        // Retrieve needed UI Components
        sourceName = findViewById(R.id.sourceName);
        listView = findViewById(R.id.details_list);
        swipe_view = findViewById(R.id.swipe_source);
        cNameView = findViewById(R.id.cNameView);
        changeCountryBtn = findViewById(R.id.changeCountry);
        
        // Setup Initial Sources
        openweather = getResources().getString(R.string.openweather);
        selectedSource = openweather;
        // If a notification is received it displays the source which it was sent from
        if(getIntent().getExtras() != null && getIntent().hasExtra("selectedSource")){
            Log.d(TAG,"Passed source is: "+getIntent().getExtras().getString("selectedSource"));
            selectedSource = getIntent().getExtras().getString("selectedSource");
        }

        Log.d(TAG, "Selected Source: "+selectedSource);
        
        setInitialLocation();

        // Initialize Country based on user privileges
        if(getIntent().hasExtra("selectedCountry")){
            if (getIntent().getExtras() != null){
                final String country = getIntent().getExtras().getString("selectedCountry");
                CountryUtil.getInstance(context).retrieveCountries(new CountryUtil.CountryRetrievedListener() {
                    @Override
                    public void processCountries(List<Country> countries) {
                        if (countries != null){
                            for (Country val : countries){
                                if(val.getName().equalsIgnoreCase(country)){
                                    currCountry = val;
                                    updateLocation(currCountry); // Request the sources and initiates the retrieval
                                    break;
                                }
                            }
                        }
                        // If country is still undefined then use default (repeated to reduce method count)
                        if (currCountry == null) {
                            currCountry = new Country(PrefsUtil.getCountryId(context), PrefsUtil.getCountry(context), PrefsUtil.getPath(context));
                            updateLocation(currCountry); // Request the sources and initiates the retrieval
                        }
                    }
                });
            }
        }else{
            currCountry = new Country(PrefsUtil.getCountryId(context), PrefsUtil.getCountry(context), PrefsUtil.getPath(context));
            setInitialLocation();
            notifyCountryChanged(currCountry); // Request the sources and initiates the retrieval
        }
    }
    
    @Override
    public int getLayoutResourceId() {
        return R.layout.activity_source;
    }
    
    private void setUpSwipeRefresh(){
        swLayout = findViewById(R.id.swipe_source);
        swLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                sourcelist.clear();
                getCountryData(selectedSource);
            }
        });
    }

    private void setInitialLocation(){
        mCurrentLocation = new Location("Weather");
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(this);
        mCurrentLocation.setLatitude(preferences.getFloat("lat", 0));
        mCurrentLocation.setLongitude(preferences.getFloat("lng", 0));

        
        lat = mCurrentLocation.getLatitude();
        lon = mCurrentLocation.getLongitude();

        Log.d(TAG,"Latitude: "+lat);
        Log.d(TAG,"Longitude: "+lon);
    }
    
    public void getSourceList() {
        String url = NetUtil.API_URL + "weathersources" + "?countryid=" + currCountry.getId();
        Ion.with(this)
                .load(url)
                .setHeader(NetUtil.API_KEY_PARAM, NetUtil.API_KEY)
                .asJsonArray()
                .setCallback(new FutureCallback<JsonArray>() {
                    @Override
                    public void onCompleted(Exception e, JsonArray result) {
                        try {
                            for (int i = 0; i < result.size(); i++) {
                                String infotype = result.get(i).getAsJsonObject().get("infotype").toString().replace("\"", "");
                                String source = result.get(i).getAsJsonObject().get("name").toString().replace("\"", "");
                                if (infotype.equals("Weather")) {
                                    sourcelist.add(source);
                                }
                            }
                            if (sourcelist.size() == 0) {
                                changeCountryBtn.setVisibility(View.GONE);
                            } else
                                changeCountryBtn.setVisibility(View.VISIBLE);
                        } catch (Exception ex) {
                            Log.d(TAG, "Failed to retrieve information from web service");
                        }
                    }
                });
        
    }
    
    public void getCountryData(final String selectedSource){
        share = "";
        if (swLayout != null){
            swLayout.post(new Runnable() {
                @Override
                public void run() {
                    swLayout.setRefreshing(true);
                }
            });
        }
        
        // returns for specific country
        final DateFormat dateFormat = new SimpleDateFormat( "MMMM-dd-yyyy", Locale.US);
        final Date date = new Date();
        // retrieve the weather source for the country selected
        if(currCountry == null){
            currCountry = new Country(PrefsUtil.getCountryId(context), PrefsUtil.getCountry(context), PrefsUtil.getPath(context));
            Toast.makeText(context,"No country selected",Toast.LENGTH_SHORT);

        }
        weatherUtil.retrieveWeatherSource(currCountry.getId(), new WeatherUtil.WeatherSourceListener() {
            public void onReceiveSourceList(@Nullable List<WeatherSrc> weatherSources) {
                if (weatherSources != null){
                    for (WeatherSrc src : weatherSources){
                        if (src.infotype.equalsIgnoreCase("weather")) {
                            // If the selected source is one of the data provided by system
                            if (src.name.toLowerCase().contains(selectedSource.toLowerCase())) {
                                share = String.format("Weather report for %s on %s\n Source:%s \n", currCountry.getName(), dateFormat.format(date), src.name);
                                // Update the UI to display the current source
                                sourceName.setText(String.format("Source: %s", src.name));
                                // Retrieves the weather extractor
                                sourceid = src.id;
                                numTries = 0; // Reset num Tries before attempting to retrieve info
                                getWeatherExtractor(sourceid);
                            } else {
                                // We add the source name to the list of sources
                                sourcelist.add(src.name);
                            }
                        }
                    }
                }
                // We the selected source is the Open Weather API
                if (selectedSource.equals(getResources().getString(R.string.openweather))){
                    if (mCurrentLocation == null)setInitialLocation();
                    weatherUtil.getAPIWeather(lat, lon, new WeatherUtil.APIWeatherReadingListener() {
                        public void onReceiveCurrentWeather(@Nullable CurrentWeather weather) {
                            if (weather != null){
                                listView.post(new Runnable() {
                                    @Override
                                    public void run() {
                                        share = String.format("Weather report for %s on %s\n Source:%s \n", currCountry.getName(), dateFormat.format(date), openweather);
                                        cNameView.setText(currCountry.getName());
                                        sourceName.setText(openweather);
                                    }
                                });
                                displayData(weather);
                            }
                        }
                    });
                }else{
                    // Update the Source list to include the open weather as a choice
                    sourcelist.add(openweather);
                }
                
                
                listView.post(new Runnable() {
                    @Override
                    public void run() {
                        adapterSource.notifyDataSetChanged();
                        swLayout.setRefreshing(false);
                    }
                });
            }
        });
    }
    
    public void getWeatherExtractor(final String selectedSourceId){
        numTries = numTries + 1;
        final String url = NetUtil.API_URL + "entry/weather/"+ selectedSourceId;
        Log.d(TAG, "Requesting Information from: " + url);
        Ion.with(this)
                .load(url)
                .setHeader(NetUtil.API_KEY_PARAM, NetUtil.API_KEY)
                .asJsonObject()
                .setCallback(new FutureCallback<JsonObject>() {
                    @Override
                    public void onCompleted(Exception e, JsonObject result) {
                        Log.d(TAG,NetUtil.API_URL + "entry/weather/"+ selectedSourceId);
                        Log.d(TAG,"Raw response for weather entry:" + result);
                        if (e == null) {
                            try {
                                JSONObject objectData = new JSONObject(result.toString());
                                JSONObject readings = new JSONObject(result.get("readings").toString());
                                Log.d(TAG, "About to retrieve Thresholds");
                                getThresholds(objectData, readings, selectedSourceId);
                                Log.d(TAG, result.toString());
                            } catch (Exception ex) {
                                Log.d(TAG, "Failed to retrieve information from web service. Trying again");
                                Log.d(TAG, ex.toString() + "failed here");
                                getWeatherExtractor(selectedSourceId);
                            }
                        }else{
                            Toast.makeText(context, "Unable to Retrieve weather details at this time, try again later", Toast.LENGTH_SHORT).show();
                            e.printStackTrace();
                        }
                    }
                });
    }
    
    private void cacheWeather(JsonObject thresholds, JSONObject readings){
        SharedPreferences sharedPreferences = getSharedPreferences("weather", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString("thresholds", thresholds.toString());
        editor.putString("readings",readings.toString());
        editor.apply();
    }

    // TODO - Weather - Review implementation
    protected void loadCache() {
        SharedPreferences sharedPreferences = getSharedPreferences("weather", Context.MODE_PRIVATE);
        String data = sharedPreferences.getString("thresholds", "");
        Log.d(TAG,"cached data");
        JsonParser parser;
        JsonElement element;
        JsonObject list = new JsonObject();
        int count = 0;
        int maxTries = 3;
        while(true) {
            try {
                parser = new JsonParser();
                element = parser.parse(data);
                list = element.getAsJsonObject();
                Log.d(TAG, list.toString());
                break;
            } catch (Exception e) {
                if(++count == maxTries) {
                    Toast.makeText(this, "Unable to retrieve cached data ", Toast.LENGTH_SHORT).show();
                    e.printStackTrace();
                    break;
                }
            }
        }
        JSONObject threshold = null;
        try {
            threshold = new JSONObject(list.toString());
        } catch (JSONException e) {
            e.printStackTrace();

        }
        String data2 = sharedPreferences.getString("readings", "");
        parser = new JsonParser();
        element = parser.parse(data2);
        list = element.getAsJsonObject();
        JSONObject readings = null;
        try {
            readings = new JSONObject(list.toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        try {
            displayThresholds(threshold,readings);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
    
    public void getThresholds(final JSONObject data, final JSONObject readings, final String sourceid){
        // Web Service use the sourceid id to send entries values only related to the source selected
        final String thresholdUrl = NetUtil.API_URL + "weather/thresholds/"+sourceid;
        Log.d(TAG,"Retrieving Thresholds from: " + thresholdUrl);
        
        Ion.with(this)
                .load(thresholdUrl)
                .setHeader(NetUtil.API_KEY_PARAM, NetUtil.API_KEY)
                .asJsonObject()
                .setCallback(new FutureCallback<JsonObject>() {
                    @Override
                    public void onCompleted(Exception e, JsonObject result) {
                        if (e == null) {
                            try {
                                Log.d(TAG, "Threshold results: " + result.toString()); // TODO Change all the System.out to Log.debug
                                JSONObject objectData = new JSONObject(result.toString());
                                Log.d(TAG, "About to try to display readings");
                                displayThresholds(objectData, readings);
        
                            } catch (Exception ex) {
                                Log.d(TAG, "Unable to retrieve Thresholds. Retrying.");
                                ex.printStackTrace();
                                if (numRetryThresholds++ < 3) {
                                    getThresholds(data, readings, sourceid);
                                } else {
                                    Toast.makeText(getApplicationContext(), "Unable to receive Warnings. View More details for additional information.", Toast.LENGTH_LONG).show();
                                }
                            }
                            cacheWeather(result, readings);
                        }else{
                            Log.d(TAG, "Unable to receive threshold values");
                            Toast.makeText(context, "Unable to retrieve weather at this time. Try Again later", Toast.LENGTH_SHORT).show();
                        }
                    }
                });
    }

    public void displayData(CurrentWeather data){
        List<CurrentReadings> readingsList;
        readingsList = new ArrayList<>();
        SimpleDateFormat format = new SimpleDateFormat("hh:mm a", Locale.US);
        readingsList.add(new CurrentReadings(String.valueOf(Math.round(data.main.temp)) + " \u00B0" + "C", "temperature", "Temperature"));
        readingsList.add(new CurrentReadings( data.clouds.all + "%", "cloudypercent", getString(R.string.percent_cloudy)));
        readingsList.add(new CurrentReadings(format.format(new Date(data.sys.sunset * 1000)), "sunset", getString(R.string.sunset)));
        readingsList.add(new CurrentReadings(format.format(new Date(data.sys.sunrise * 1000)), "sunrise",getString(R.string.sunrise)));
        readingsList.add(new CurrentReadings(String.valueOf(data.main.humidity) + "%" , "humidity", "Humidity"));
        readingsList.add(new CurrentReadings(String.valueOf(Math.round(data.main.pressure)) +" hPa", "pressure", "Pressure"));
        adapter = new CurrentAdapter(context, readingsList);
        
        listView.post(new Runnable() {
            @Override
            public void run() {
                listView.setOnScrollListener(new AbsListView.OnScrollListener() {
                    public void onScrollStateChanged(AbsListView view, int scrollState) {}
                    public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
                        int topRowVerticalPosition = (listView == null || listView.getChildCount() == 0) ? 0 : listView.getChildAt(0).getTop();
                        swipe_view.setEnabled(firstVisibleItem == 0 && topRowVerticalPosition >= 0);
                    }
                });
            }
        });
        
        
        share += "Temperature: " + String.valueOf(Math.round(data.main.temp)) + " \u00B0" + "C \n\n";
        share += "Percentage Cloudy " + data.clouds.all + "% \n";
        share += "Humidity: " + String.valueOf(data.main.humidity) + "%\n";
        share += "Pressure: " + String.valueOf(Math.round(data.main.pressure)) +" hPa\n";
        share += "Sunset: " + format.format(new Date(data.sys.sunset * 1000)) + "\n";
        share += "Sunrise: " + format.format(new Date(data.sys.sunrise * 1000));
        
        listView.post(new Runnable() {
            @Override
            public void run() {
                listView.setAdapter(adapter);
                adapter.notifyDataSetChanged();
            }
        });
    }

    //Display all readings from extractor and display thresholds.
    public void displayThresholds(JSONObject threshold, JSONObject fields) throws JSONException {
        Log.d(TAG,"Trying to display readings");
        if(fields.length() <=1)
        {
            Toast.makeText(context,"The MET office may be unavailable at this time",Toast.LENGTH_SHORT).show();
        }



        Log.d(TAG, "Fields Before: " + fields);
        List<CurrentReadings> extrasList = new ArrayList<>();
        List<String> extrasToRemove = new ArrayList<>();

        int hasOrder = 0;
        orderJSON = new JSONArray();
        if(!fields.has("order"))
        {
            hasOrder=1;
        }
        else
        {
            if (fields.get("order") instanceof JSONArray) {
                orderJSON = (JSONArray) fields.get("order");
            }
            Log.d(TAG, "orderJSON: " + orderJSON);
            myList = new ArrayList<String>();
            //creating an ArrayList of the order field from the JSON data
            for (int i = 0; i < orderJSON.length(); i++) {
                myList.add(orderJSON.getString(i));
            }
            Log.d(TAG, "myList: " + myList);

            Iterator<String> myKeys = fields.keys();
            int found = 0;
            JSONObject extraJSON = new JSONObject();
            //finding the extra fields that are not a part of the defined order
            while (myKeys.hasNext()) {
                found = 0;
                String myKey = myKeys.next();
                for (int i = 0; i < myList.size(); i++) {
                    if (myKey.equals(myList.get(i))) {
                        found = 1;
                    }
                }
                if (found == 0) {
                    extrasToRemove.add(myKey);
                    extraJSON.put(myKey, String.valueOf(fields.get(myKey)));
                }


            }
            //ensuring order and source id are not a part of the displayed readings
            fields.remove("sourceid");
            fields.remove("order");

            extraJSON.remove("sourceid");
            extraJSON.remove("order");
            Log.d(TAG, "Extra JSON: " + extraJSON);
            Log.d(TAG, "Extras to Remove: " + extrasToRemove);
            //removing the unsorted extra fields from the readings
            for (int i = 0; i < extrasToRemove.size(); i++) {
                fields.remove(extrasToRemove.get(i));
            }
            Iterator<String> extraIter = extraJSON.keys();
            //creating a list of CurrentReadings objects based on the extra fields
            while (extraIter.hasNext()) {
                String extraKey = extraIter.next();
                String extraKeyTitle = extraKey.substring(0, 1).toUpperCase() + extraKey.substring(1);
                extrasList.add(new CurrentReadings(String.valueOf(extraJSON.get(extraKey)), extraKey, extraKeyTitle));
            }
            Log.d(TAG, "Fields After: " + fields);

            //Sorting the extra fields alphabetically
            Collections.sort(extrasList, new Comparator<CurrentReadings>() {
                @Override
                public int compare(CurrentReadings lhs, CurrentReadings rhs) {
                    return (lhs.getHead().compareTo(rhs.getHead()));
                }
            });
//        for(int i=0; i< extrasList.size(); i++)
////        {
////            Log.d(TAG,"List Extras After Sort: " + extrasList.get(i).getHead());
////        }
        }








        List<CurrentReadings> readingsList;
        readingsList = new ArrayList<>();

        JSONObject warnings = null;
        JSONObject emergency = null;
        try {
            JSONObject thresh = new JSONObject(threshold.get("thresholds").toString());
            warnings = new JSONObject(thresh.get("warnings").toString());
            emergency = new JSONObject(thresh.get("emergency").toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        
        Log.d(TAG, "Warnings: " + warnings);
        Log.d(TAG, "Emergencies:" + emergency);
        Log.d(TAG, "Thresholds:" + threshold);
        Log.d(TAG, "Fields:" + fields);


        Iterator<String> iter = fields.keys();
        while (iter.hasNext()) {
            String key = iter.next();
            String keys = key.substring(0, 1).toUpperCase() + key.substring(1);
            

            if (key.toLowerCase().contains("warning")) {

                    if (!String.valueOf(fields.get(key)).toLowerCase().contains("none")) {
                        readingsList.add(new CurrentReadings(String.valueOf(fields.get(key)), key, keys, "yellow"));
                    }
                    else
                        readingsList.add(new CurrentReadings(String.valueOf(fields.get(key)), key, keys));


            } else {

                if (key.toLowerCase().equals("temperature")) {
//                    convert from fahrenheit to celsius
                        double strip = Double.parseDouble(String.valueOf(fields.get(key)).replaceAll("[^\\d.]", ""));
//                      Assuming that no temperature in celcius is more than 50
                        if (strip > 50.0) {
                            Log.d(TAG,"Degrees celcius" + strip);
                            strip = (strip - 32.0) * (5.0 / 9.0);
                            fields.put("temperature", (int) Math.ceil(strip) + " \u00B0" + "C");
                            Log.d(TAG,"Degrees " + fields.get("temperature"));
                        } else {
                            fields.put("temperature", (int) Math.ceil(strip) + " \u00B0" + "C");
                        }
                }

                Log.d(TAG,key + "keys are here");
                try {
                    if (!(key.equals("sourceid"))) {
                        int state = 0;
                        if (emergency != null && emergency.has(key)) {
                            if (weatherUtil.isNumeric(emergency.get(key).toString())){
                                double emergencys = Double.parseDouble(String.valueOf(emergency.get(key)));
                                double strip = Double.parseDouble(String.valueOf(fields.get(key)).replaceAll("[^\\d.]", ""));
                                if (emergencys <= strip) {
                                    share = String.format("%s%s", share, keys + ":" + String.valueOf(fields.get(key) + "\n\n"));
                                    readingsList.add(new CurrentReadings(String.valueOf(fields.get(key)), key, keys, "red"));
                                    state = 1;
                                }
                            }
                        }

                        if (warnings != null && warnings.has(key) && state == 0) {
                            if (weatherUtil.isNumeric(warnings.get(key).toString())) {
                                double warning = Double.parseDouble(String.valueOf(warnings.get(key)));
                                double strip = Double.parseDouble(String.valueOf(fields.get(key)).replaceAll("[^\\d.]", ""));
                                if (warning <= strip) {
                                    readingsList.add(new CurrentReadings(String.valueOf(fields.get(key)), key, keys, "yellow"));
                                    share = String.format("%s%s", share, keys + ":" + String.valueOf(fields.get(key)) + "\n\n");
        
                                    state = 1;
                                }
                            }
                        }

                        if (state == 0) {
                            Log.d(TAG,"Currently here " + String.valueOf(fields.get(key)));
                            try {
                                readingsList.add(new CurrentReadings(String.valueOf(fields.get(key)), key, keys));
                                share += keys + ":" + String.valueOf(fields.get(key)) + "\n\n";
                            } catch (Exception e) {
                                e.printStackTrace();
                            }

                        }
                    }


                } catch (JSONException e) {
                    Log.d(TAG,"stopped working here");
                    Log.d(TAG,e.toString() + "message");

                }
            }
            Log.d(TAG,"Just before the adapter");
            Log.d(TAG,"This is readings: "+ readingsList);
            //adapter = new CurrentAdapter(context, readingsList);
            Log.d(TAG,"Just before the list set");
            listView.setOnScrollListener(new AbsListView.OnScrollListener() {
                @Override
                public void onScrollStateChanged(AbsListView view, int scrollState) {

                }

                @Override
                public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
                    int topRowVerticalPosition = (listView == null || listView.getChildCount() == 0) ? 0 : listView.getChildAt(0).getTop();
                    swipe_view.setEnabled(firstVisibleItem == 0 && topRowVerticalPosition >= 0);
                }
            });

            //listView.setAdapter(adapter);
            Log.d(TAG,"Adapter has been set");
        }
        //Sorting the readingsList based on the defined order from the extractor
        if(hasOrder==0) {
            Collections.sort(readingsList, new Comparator<CurrentReadings>() {
                @Override
                public int compare(CurrentReadings lhs, CurrentReadings rhs) {
                    if (myList.indexOf(String.valueOf(lhs.grabTextIcon())) < myList.indexOf(String.valueOf(rhs.grabTextIcon()))) {
                        return -1;
                    }
                    if (myList.indexOf(String.valueOf(lhs.grabTextIcon())) > myList.indexOf(String.valueOf(rhs.grabTextIcon()))) {
                        return 1;
                    } else {
                        return 0;
                    }
                }
            });
            //Adding the alphabetically ordered extra fields to the end of the order sorted list of fields
            readingsList.addAll(extrasList);

//            for (int i = 0; i < readingsList.size(); i++) {
//                Log.d(TAG, "List Final Keys: " + readingsList.get(i).getHead());
//            }
//            for (int i = 0; i < extrasList.size(); i++) {
//                Log.d(TAG, "List Final ExtraKeys: " + extrasList.get(i).getHead());
//            }
        }
        //Setting adapter with correctly ordered fields
        adapter = new CurrentAdapter(context, readingsList);
        listView.setAdapter(adapter);
//        for(int i=0; i< readingsList.size(); i++)
//        {
//            Log.d(TAG,"List Keys: " + readingsList.get(i).getHead());
//        }

    }

    public void selectSource(View v) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setCancelable(true);
        builder.setPositiveButton("OK",null);

        builder.setTitle("Select Source");
        Log.d(TAG,sourcelist + "");
        builder.setAdapter(adapterSource,
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        selectedSource = sourcelist.get(which);
                        sourcelist.clear();
                        getCountryData(selectedSource);
                    }
                })
                .show();
    }
    
    private void displayCountrySelection(String country){
        try {
            if(country == null){
                String msg = "Displaying Weather Details for: Unknown";
                cNameView.setText(msg);
            }else{
                String msg = String.format("Displaying Weather Details for: %s", country);
                cNameView.setText(msg);
            }
        }catch (Exception e){
            e.printStackTrace();
        }
    }
    
    public void chooseCountry(View view){
        CountryUtil.getInstance(this).retrieveCountries(new CountryUtil.CountryRetrievedListener() {
            @Override
            public void processCountries(List<Country> countries) {
                if (countries != null){
                    displayCountries(countries);
                }else{
                    Toast.makeText(SourceActivity.this, "Unable to retrieve countries", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }
    
    private void displayCountries(final List<Country> countries){
        final CountryAdapter arrayAdapter = new CountryAdapter(this, countries);
        
        new AlertDialog.Builder(this)
                .setTitle("Select Country to View Weather")
                .setAdapter(arrayAdapter,
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                currCountry = countries.get(which);
                                // Select the source to default back to open weather when changing countries
                                selectedSource = openweather;
                                // Update location by retrieve information for country selected
                                updateLocation(currCountry);
                            }
                        })
                .show();
    }

    public void updateLocation(final Country country) {
        currCountry = country;
        displayCountrySelection(country.getName());
        sourcelist.clear();

        // make request in separate thread to ease up processing for remaining update locations
        (new Thread(){
            public void run() {
                Log.d(TAG, "Attempting to change Country to " + currCountry);
                CountryUtil.getInstance(context).retrieveCountryLocations(new CountryUtil.CountryLocRetrievedListener() {
                    @Override
                    public void processCountryLocations(List<CountryLoc> countryLocs) {
                        Log.d(TAG, String.valueOf(countryLocs));
                        if (countryLocs != null){
                            for (CountryLoc loc : countryLocs){
                                if (loc.country.equalsIgnoreCase(currCountry.getName())){
                                    lat = Double.parseDouble(loc.latitude);
                                    lon = Double.parseDouble(loc.longitude);

                                    Log.d(TAG, String.format("Retrieved (%s, %s) from server for country %s", lat,lon, country.getName()));
                                    if (mCurrentLocation == null)setInitialLocation();
                                    mCurrentLocation.setLatitude(lat);
                                    mCurrentLocation.setLongitude(lon);
                                    break;
                                }
                            }
                        }
                        // After retrieving the current GPS location based on country selected, retrieve data for source selected
                        getCountryData(selectedSource);
                    }
                });
            }
        }).start();
    }
    
    private void notifyCountryChanged(Country selectedCountry) {
        displayCountrySelection(selectedCountry.getName());
        if (sourcelist != null) sourcelist.clear();
        if (NetUtil.isOnline(this)) {
            updateLocation(selectedCountry);
            getCountryData(selectedSource);
        } else {
            Toast.makeText(this, "Device Offline using cached data ", Toast.LENGTH_SHORT).show();
            loadCache();
        }
    }
}
