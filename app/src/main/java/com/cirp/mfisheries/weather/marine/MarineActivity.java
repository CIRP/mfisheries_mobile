package com.cirp.mfisheries.weather.marine;

import android.content.Context;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.cirp.mfisheries.R;


import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.Toast;

import java.util.Calendar;

import com.cirp.mfisheries.util.GlideApp;
import com.cirp.mfisheries.util.NetUtil;
import com.cirp.mfisheries.weather.models.Marine;
import com.google.gson.JsonArray;

import java.util.ArrayList;
import java.util.List;

public class MarineActivity extends AppCompatActivity {
    private static final String TAG = "MarineActivity";
    private JsonArray marineList;
    private Context context;

    private List<Marine> marines;
    private MarineUtil marineUtil;
    private SwipeRefreshLayout swLayout;

    // The View components used for the user
    private CheckBox check1, check2, check3, check4,check5,check6;
    private ImageView image1, image2, image3, image4, baseImage, image5, image6, image7, image8,leftArrow,rightArrow;
    private Button button1, button2, button3, buttonBack, buttonForward;
    ArrayList<String> first = new ArrayList<>();
    ArrayList<String> second = new ArrayList<>();
    ArrayList<String> third = new ArrayList<>();
    ArrayList<String> extras = new ArrayList<>();
    private Integer position = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_marine);
        context = this;

        marines = new ArrayList<>();
        try
        {
            marineUtil =  new MarineUtil(this);
        }
        catch(Exception e)
        {
            e.printStackTrace();
            Log.e(TAG,"Something went wrong in marineUtil");
        }

        setUpSwipeRefresh();



        // Assign the Views that will handle the images
        baseImage = findViewById(R.id.baseImg);
        image1 = findViewById(R.id.windImage);
        image2 = findViewById(R.id.barbsImage);
        image3 = findViewById(R.id.contourImage);
        image4 = findViewById(R.id.featuresImage);
        image5 = findViewById(R.id.waveHeightImage);
        image6 = findViewById(R.id.hazardImage);
        image7 = findViewById(R.id.sigWaveImage);
        image8 = findViewById(R.id.windWave);
        leftArrow = findViewById(R.id.imageView7);
        rightArrow = findViewById(R.id.imageView8);

        //buttonBack = view.findViewById(R.id.button6);
        //buttonForward = view.findViewById(R.id.button7);



        // Assign the controls
        check1 = findViewById(R.id.checkBox2);
        check2 = findViewById(R.id.checkBox4);
        check3 = findViewById(R.id.checkBox6);
        check4 = findViewById(R.id.checkBox8);
        check5 = findViewById(R.id.checkBox5);
        check6 = findViewById(R.id.checkBox7);

        // Load the data from the API
        // Configure the controls of the checkboxes
        configureControls();
    }

    private void setUpSwipeRefresh(){
        swLayout = findViewById(R.id.swipe_marine);
        swLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                onResume();
            }
        });
    }

    private void configureControls() {

        check1.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener()
        {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked)
            {
                if ( isChecked )
                {
                    check5.setChecked(false);
                    check6.setChecked(false);
                    //image1.setImageResource(R.drawable.arrow); //glide get windimage

                    image1.setVisibility(View.VISIBLE);

                }
                if (!isChecked)
                {
                    image1.setVisibility(View.GONE);
                }

            }
        });
        check2.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener()
        {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked)
            {
                if ( isChecked )
                {
                    image2.setVisibility(View.VISIBLE);
                }
                if (!isChecked)
                {
                    image2.setVisibility(View.GONE);
                }

            }
        });
        check3.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener()
        {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked)
            {
                if ( isChecked )
                {
                    image3.setVisibility(View.VISIBLE);
                }
                if (!isChecked)
                {
                    image3.setVisibility(View.GONE);
                }

            }
        });
        check4.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener()
        {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked)
            {
                if ( isChecked )
                {
                    image4.setVisibility(View.VISIBLE);
                }
                if (!isChecked)
                {
                    image4.setVisibility(View.GONE);
                }

            }
        });
        check5.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener()
        {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked)
            {
                if ( isChecked )
                {
                    check1.setChecked(false);
                    check6.setChecked(false);
                    //image1.setImageResource(R.drawable.arrow); //glide get waveheightimage
                    image5.setVisibility(View.VISIBLE);

                }
                if (!isChecked)
                {
                    image5.setVisibility(View.GONE);
                }

            }
        });
        check6.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener()
        {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked)
            {
                if ( isChecked )
                {
                    check1.setChecked(false);
                    check5.setChecked(false);
                    //image1.setImageResource(R.drawable.arrow);//Glide get hazards
                    image6.setVisibility(View.VISIBLE);

                }
                if (!isChecked)
                {
                    image6.setVisibility(View.GONE);
                }
            }
        });

        leftArrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Code here executes on main thread after user presses button
                final String url = NetUtil.SITE_URL;
                boolean connectionState = NetUtil.isOnline(context);
                if(!connectionState)
                {
                    Toast.makeText(context,"Cannot load while offline",Toast.LENGTH_SHORT).show();
                }
                int currentHour =  Calendar.getInstance().get(Calendar.HOUR_OF_DAY);
                try
                {
                    if(position ==2)
                    {
                        position =1;
                        String one = url + first.get(0);
                        String two = url + first.get(1);
                        String three = url + first.get(2);
                        String four = url + first.get(3);
                        String five = url + first.get(4);
                        String six = url + first.get(5);

                        GlideApp.with(context).load(six).diskCacheStrategy(DiskCacheStrategy.NONE).skipMemoryCache(true).into(image1);
                        GlideApp.with(context).load(five).diskCacheStrategy(DiskCacheStrategy.NONE).skipMemoryCache(true).into(image2);
                        GlideApp.with(context).load(three).diskCacheStrategy(DiskCacheStrategy.NONE).skipMemoryCache(true).into(image3);
                        GlideApp.with(context).load(one).diskCacheStrategy(DiskCacheStrategy.NONE).skipMemoryCache(true).into(image4);
                        GlideApp.with(context).load(four).diskCacheStrategy(DiskCacheStrategy.NONE).skipMemoryCache(true).into(image5);
                        GlideApp.with(context).load(two).diskCacheStrategy(DiskCacheStrategy.NONE).skipMemoryCache(true).into(image6);
                    }
                    else if(position ==3)
                    {
                        position =2;
                        String one = url + second.get(0);
                        String two = url + second.get(1);
                        String three = url + second.get(2);
                        String four = url + second.get(3);
                        String five = url + second.get(4);
                        String six = url + second.get(5);

                        GlideApp.with(context).load(six).diskCacheStrategy(DiskCacheStrategy.NONE).skipMemoryCache(true).into(image1);
                        GlideApp.with(context).load(five).diskCacheStrategy(DiskCacheStrategy.NONE).skipMemoryCache(true).into(image2);
                        GlideApp.with(context).load(three).diskCacheStrategy(DiskCacheStrategy.NONE).skipMemoryCache(true).into(image3);
                        GlideApp.with(context).load(one).diskCacheStrategy(DiskCacheStrategy.NONE).skipMemoryCache(true).into(image4);
                        GlideApp.with(context).load(four).diskCacheStrategy(DiskCacheStrategy.NONE).skipMemoryCache(true).into(image5);
                        GlideApp.with(context).load(two).diskCacheStrategy(DiskCacheStrategy.NONE).skipMemoryCache(true).into(image6);
                    }
                }
                catch(Exception e)
                {
                    Log.e(TAG,e.getMessage());
                    Toast.makeText(context,"Could not load images",Toast.LENGTH_SHORT).show();
                }


            }
        });
        rightArrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Code here executes on main thread after user presses button
                final String url = NetUtil.SITE_URL;
                boolean connectionState = NetUtil.isOnline(context);
                if(!connectionState)
                {
                    Toast.makeText(context,"Cannot load while offline",Toast.LENGTH_SHORT).show();
                }
                int currentHour =  Calendar.getInstance().get(Calendar.HOUR_OF_DAY);
                try
                {
                    if(position == 1)
                    {
                        position =2;
                        String one = url + second.get(0);
                        String two = url + second.get(1);
                        String three = url + second.get(2);
                        String four = url + second.get(3);
                        String five = url + second.get(4);
                        String six = url + second.get(5);

                        GlideApp.with(context).load(six).diskCacheStrategy(DiskCacheStrategy.NONE).skipMemoryCache(true).into(image1);
                        GlideApp.with(context).load(five).diskCacheStrategy(DiskCacheStrategy.NONE).skipMemoryCache(true).into(image2);
                        GlideApp.with(context).load(three).diskCacheStrategy(DiskCacheStrategy.NONE).skipMemoryCache(true).into(image3);
                        GlideApp.with(context).load(one).diskCacheStrategy(DiskCacheStrategy.NONE).skipMemoryCache(true).into(image4);
                        GlideApp.with(context).load(four).diskCacheStrategy(DiskCacheStrategy.NONE).skipMemoryCache(true).into(image5);
                        GlideApp.with(context).load(two).diskCacheStrategy(DiskCacheStrategy.NONE).skipMemoryCache(true).into(image6);
                    }
                    else if(position ==2)
                    {
                        position =3;
                        String one = url + third.get(0);
                        String two = url + third.get(1);
                        String three = url + third.get(2);
                        String four = url + third.get(3);
                        String five = url + third.get(4);
                        String six = url + third.get(5);

                        GlideApp.with(context).load(six).diskCacheStrategy(DiskCacheStrategy.NONE).skipMemoryCache(true).into(image1);
                        GlideApp.with(context).load(five).diskCacheStrategy(DiskCacheStrategy.NONE).skipMemoryCache(true).into(image2);
                        GlideApp.with(context).load(three).diskCacheStrategy(DiskCacheStrategy.NONE).skipMemoryCache(true).into(image3);
                        GlideApp.with(context).load(one).diskCacheStrategy(DiskCacheStrategy.NONE).skipMemoryCache(true).into(image4);
                        GlideApp.with(context).load(four).diskCacheStrategy(DiskCacheStrategy.NONE).skipMemoryCache(true).into(image5);
                        GlideApp.with(context).load(two).diskCacheStrategy(DiskCacheStrategy.NONE).skipMemoryCache(true).into(image6);
                    }
                }
                catch(Exception e)
                {
                    Log.e(TAG,e.getMessage());
                    Toast.makeText(context,"Could not load images",Toast.LENGTH_SHORT).show();
                }


            }
        });

    }

    @Override
    protected void onResume(){
        super.onResume();
        boolean status = NetUtil.isOnline(context);
        if(!status)
        {
            Toast.makeText(context,"Images will not load offline",Toast.LENGTH_LONG).show();
            if (swLayout.isRefreshing()) {
                swLayout.setRefreshing(false);
            }
        }
        // The previous error was due to the lack of UI because the activity is created before
        // We attempt to delay the retrieval of information to update the view
        marineUtil.retrieveMarineList(new MarineUtil.MarineSourceListener() {
            @Override
            public void onReceiveMarineList(@Nullable List<Marine> marineList) {
                if(marineList != null)
                {
                    if (swLayout.isRefreshing()) {
                        swLayout.setRefreshing(false);
                    }
                    Log.d(TAG, "Successfully retrieved marine list");
                    Log.d(TAG,"myMarine1: "+marineList);
                    marines = marineList;
                    Log.d(TAG,"thisMarine: "+marines);
                    int currentHour =  Calendar.getInstance().get(Calendar.HOUR_OF_DAY);
                    for(int i = 0; i < marines.size(); i += 1)
                    {
                        Log.d(TAG,"Marinez: "+marines.get(i).getPath());
                        String path = marines.get(i).getPath();
                        if(path.toLowerCase().contains("0"))
                        {
                            Log.d(TAG, "Adding " + path + " to first");
                            first.add(path);
                        }
                        else if(path.toLowerCase().contains("12"))
                        {
                            Log.d(TAG, "Adding " + path + " to second");
                            second.add(path);
                        }
                        else if(path.toLowerCase().contains("24"))
                        {
                            Log.d(TAG, "Adding " + path + " to third");
                            third.add(path);
                        }
                        else
                        {
                            Log.d(TAG, "Adding " + path + " to extra");
                            extras.add(path);
                        }
                    }


                    final String url = NetUtil.SITE_URL;
                    //Separate the URL and the request for debugging
                    String first_url = "";
                    String second_url = "";
                    String third_url ="";
                    String fourth_url ="";
                    String fifth_url ="";
                    String sixth_url ="";

                    //May not be necessary
//                    Collections.sort(first,String.CASE_INSENSITIVE_ORDER);
//                    Collections.sort(second,String.CASE_INSENSITIVE_ORDER);
//                    Collections.sort(third,String.CASE_INSENSITIVE_ORDER);

                    //To display the images relevant to the current time of the user
                    if(currentHour >0 && currentHour < 8)
                    {
                        position = 1;
                        Log.d("First","Entered first");
                        first_url = url + first.get(0);
                        second_url = url + first.get(1);
                        third_url = url + first.get(2);
                        fourth_url = url + first.get(3);
                        fifth_url = url + first.get(4);
                        sixth_url = url + first.get(5);
                    }
                    else if(currentHour >=8 && currentHour < 20)
                    {
                        position = 2;
                        Log.d("Second","Entered second");
                        first_url = url + second.get(0);
                        second_url = url + second.get(1);
                        third_url = url + second.get(2);
                        fourth_url = url + second.get(3);
                        fifth_url = url + second.get(4);
                        sixth_url = url + second.get(5);
                    }
                    else
                    {
                        position = 3;
                        Log.d("Third","Entered third");
                        first_url = url + third.get(0);
                        second_url = url + third.get(1);
                        third_url = url + third.get(2);
                        fourth_url = url + third.get(3);
                        fifth_url = url + third.get(4);
                        sixth_url = url + third.get(5);
                    }

                    for(int i=0;i<extras.size();i++)
                    {
                        if(extras.get(i).contains("CARIB_blank"))
                        {
                            String baseImgUrl = url + extras.get(i);
                            GlideApp.with(context).load(baseImgUrl).into(baseImage);
                        }
                        if(extras.get(i).contains("sigWaves"))
                        {
                            String sigWaveUrl = url + extras.get(i);
                            GlideApp.with(context).load(sigWaveUrl).diskCacheStrategy(DiskCacheStrategy.NONE).skipMemoryCache(true).into(image7);
                        }
                        if(extras.get(i).contains("windWave"))
                        {
                            String windWaveUrl = url + extras.get(i);
                            GlideApp.with(context).load(windWaveUrl).diskCacheStrategy(DiskCacheStrategy.NONE).skipMemoryCache(true).into(image8);
                        }
                    }

                    Log.d(TAG, String.format("1st %s, 2nd %s, 3rd %s, 4th %s, 5th %s, 6th %s", first_url, second_url, third_url, fourth_url, fifth_url, sixth_url));
                    boolean isOnline = NetUtil.isOnline(context);
                    if(!isOnline)
                    {
                        Toast.makeText(context,"Cannot load images while offline",Toast.LENGTH_SHORT).show();
                    }

                    // request images and load into respective views
                    try
                    {
                        GlideApp.with(context).load(sixth_url).diskCacheStrategy(DiskCacheStrategy.NONE).skipMemoryCache(true).into(image1);
                        GlideApp.with(context).load(fifth_url).diskCacheStrategy(DiskCacheStrategy.NONE).skipMemoryCache(true).into(image2);
                        GlideApp.with(context).load(third_url).diskCacheStrategy(DiskCacheStrategy.NONE).skipMemoryCache(true).into(image3);
                        GlideApp.with(context).load(first_url).diskCacheStrategy(DiskCacheStrategy.NONE).skipMemoryCache(true).into(image4);
                        GlideApp.with(context).load(fourth_url).diskCacheStrategy(DiskCacheStrategy.NONE).skipMemoryCache(true).into(image5);
                        GlideApp.with(context).load(second_url).diskCacheStrategy(DiskCacheStrategy.NONE).skipMemoryCache(true).into(image6);
                    }
                    catch(Exception e)
                    {
                        Toast.makeText(context,"Could not load images",Toast.LENGTH_SHORT).show();
                        Log.e(TAG, e.getMessage());
                    }


                    // Load an initial image and set control to reflect loaded option
                    //displayDefaultMap();

                }else{
                    Log.d(TAG, "Unable to successfully retrieve marine list");
                    Toast.makeText(context,"Could not load images",Toast.LENGTH_SHORT).show();
                    if (swLayout.isRefreshing()) {
                        swLayout.setRefreshing(false);
                    }
                }
            }
        });
    }
}
