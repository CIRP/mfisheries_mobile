package com.cirp.mfisheries.weather;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.cirp.mfisheries.R;
import com.cirp.mfisheries.weather.models.CurrentWeather;
import com.cirp.mfisheries.weather.models.Forecast;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;

public class ForecastAdapter extends ArrayAdapter<Forecast.Condition> {
	private final Context context;
	private final List<Forecast.Condition> conditions;

	static class ViewHolder {
		public ImageView iconView;
		public TextView date;
		public TextView temp;
		public TextView condition;
	}

	public ForecastAdapter(Context context, List<Forecast.Condition> conditions) {
		super(context, R.layout.layout_forecast, conditions);
		this.context = context;
		this.conditions = conditions;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		View rowView = convertView;
		if (rowView == null) {
			LayoutInflater inflater = (LayoutInflater) context
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			rowView = inflater.inflate(R.layout.layout_forecast, parent, false);

			ViewHolder viewHolder = new ViewHolder();
			viewHolder.iconView = rowView.findViewById(R.id.forecast_icon);
			viewHolder.date = rowView.findViewById(R.id.forecast_date);
			viewHolder.temp = rowView.findViewById(R.id.forecast_temp);
			viewHolder.condition = rowView.findViewById(R.id.forecast_condition);
			rowView.setTag(viewHolder);
		}
		Forecast.Condition condition = conditions.get(position);

		ViewHolder holder = (ViewHolder) rowView.getTag();
		Date date = new Date(condition.dt * 1000);
		SimpleDateFormat format = new SimpleDateFormat("EEE", Locale.ENGLISH);
		holder.date.setText(format.format(date));
		holder.temp.setText(Math.round(condition.temp.min) + " - " + Math.round(condition.temp.max) + " \u00B0" + "C");
		holder.condition.setText(condition.weather[0].main);
		Glide.with(context)
				.load(CurrentWeather.getIcon(condition.weather[0].icon, context)).into(holder.iconView);
		return rowView;
	}

}