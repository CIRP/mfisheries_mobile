package com.cirp.mfisheries.weather;

import android.content.Context;
import android.graphics.Typeface;
import android.support.v4.content.ContextCompat;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.cirp.mfisheries.R;
import com.cirp.mfisheries.weather.models.CurrentReadings;

import java.util.List;

public class CurrentAdapter extends BaseAdapter{
    Context context;
    List <CurrentReadings> weather;

    public CurrentAdapter(Context context, List<CurrentReadings> weather) {
        this.context = context;
        this.weather = weather;
    }

    static class ViewHolder {
        public ImageView iconView;
        public TextView readings;
        public TextView heading;
        public RelativeLayout threshold;
    }


    @Override
    public int getCount(){return weather.size();}

    @Override
    public Object getItem(int position){return weather.get(position);}

    @Override
    public long getItemId(int position){return weather.indexOf(getItem(position));}

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View v = View.inflate(context,R.layout.layout_source,null);

        ViewHolder viewHolder = new ViewHolder();
        viewHolder.iconView = v.findViewById(R.id.icon_image);
        viewHolder.readings = v.findViewById(R.id.reading);
        viewHolder.heading = v.findViewById(R.id.heading);
        viewHolder.threshold = v.findViewById(R.id.card);

        TextView feet = v.findViewById(R.id.feet);
        TextView tide = v.findViewById(R.id.highlow);
        TextView time = v.findViewById(R.id.date);
        ImageView image = v.findViewById(R.id.tideimage);
        v.setTag(viewHolder);

        CurrentReadings entry = weather.get(position);
        if(!entry.times.equals("a") && !entry.tide.equals("a") && !entry.feet.equals("a")){
            feet.setText("0.94ft");
            tide.setText("High");
            time.setText("7:10pm");
            image.setImageResource(R.drawable.hightide);

        }
        else {
            viewHolder.heading.setText(entry.head);
            viewHolder.readings.setText(entry.reading);
            if(CurrentReadings.getImage(entry.icon)==1){
                viewHolder.iconView.setVisibility(View.GONE);
            }
            else
            viewHolder.iconView.setImageResource(CurrentReadings.getImage(entry.icon));
            if (!entry.threshold.equals("nothing")) {
                viewHolder.threshold.setBackgroundColor(ContextCompat.getColor(context, CurrentReadings.getIcon(entry.threshold)));
                viewHolder.heading.setTypeface(null, Typeface.BOLD);
                viewHolder.readings.setTypeface(null, Typeface.BOLD);
            }
        }
        return v;
    }



}
