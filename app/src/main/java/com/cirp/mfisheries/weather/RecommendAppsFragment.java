package com.cirp.mfisheries.weather;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.cirp.mfisheries.R;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.InputStream;

public class RecommendAppsFragment extends Fragment {
    
    private static final String TAG = "AppRecommendFrag";
    private static Context mContext;
    public static RecommendAppsFragment newInstance(Context context, Weather wModule) {
        mContext = context;
        return new RecommendAppsFragment();
    }

    public RecommendAppsFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_apps, container, false);
        ListView list = view.findViewById(R.id.apps_list_view);
    
        try {
            JSONArray apps = retrieveRecommendApps();
            String [] names = new String[apps.length()];
            String [] icons = new String[apps.length()];
            String [] uri = new String[apps.length()];
            String [] uritype = new String[apps.length()];
            String [] type = new String[apps.length()];
            
            for (int i = 0; i < apps.length(); i++) {
                JSONObject app = apps.getJSONObject(i);
                names[i] = app.getString("name");
                icons[i] = app.getString("icon");
                uri[i] = app.getString("uri");
                uritype[i] = app.getString("uri_type");
                type[i] = app.getString("type");
            }
            
            AppsAdapter adapter = new AppsAdapter(inflater,names,icons,uri);
            list.setAdapter(adapter);
            
        }catch(Exception e){
            e.printStackTrace();
        }
        
        
        return  view;
    }
    
    public JSONArray retrieveRecommendApps(){
        String jsonStr;
        try {
            InputStream inputStream = getResources().openRawResource(R.raw.recommend_apps);
            int size = inputStream.available();
            byte[] buffer = new byte[size];
            inputStream.read(buffer);
            inputStream.close();
            jsonStr = new String(buffer, "UTF-8");
            
            return new JSONArray(jsonStr);
        }catch (Exception e){
            e.printStackTrace();
            Log.d(TAG, "Unable to read JSON from Raw File");
        }
        return null;
    }


    @SuppressWarnings("WeakerAccess")
    class AppsAdapter extends BaseAdapter{
        private LayoutInflater inflater;
        private String[] names;
        private String[] app_uri;
        private String[] icons;
        public AppsAdapter(LayoutInflater inflater,String[] names,String []icons,String[] uri){
            this.names = names;
            this.app_uri =uri;
            this.icons = icons;
            this.inflater = inflater;
        }

        public int getImage(String code){
            switch (code) {
                case "tide1":
                    return R.drawable.tide1;
                case "tide2":
                    return R.drawable.tide2;
                case "moon1":
                    return R.drawable.moon1;
                case "moon2":
                    return R.drawable.moon2;
                case "thunderstorms":
                    return R.drawable.thunderstorms;
            }
            return 0; //Not sure why. If this returns 1 instead of 0 the application crashes when loading Tides and Suggested apps
            //Update. Application was crashing because Tide Forecast icon resource is does not exist. returning 1 causes crash due to 0x1 is not a resource generated in our R files.
        }
        @Override
        public int getCount() {
            return names.length;
        }

        @Override
        public Object getItem(int i) {
            return null;
        }

        @Override
        public long getItemId(int i) {
            return 0;
        }

        @Override
        public View getView(final int i, View view, ViewGroup viewGroup) {
            view = inflater.inflate(R.layout.recommended_apps,null);

            ImageView image = view.findViewById(R.id.app_icon);
            TextView appname = view.findViewById(R.id.app_name);
            Button install = view.findViewById(R.id.app_install);

            image.setImageResource(getImage(icons[i]));
            appname.setText(names[i]);
            install.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View view) {
                    Uri uri = Uri.parse(app_uri[i]);
                    Intent intent = new Intent(Intent.ACTION_VIEW, uri);
                    mContext.startActivity(intent);
                }
            });
            return view;
        }
    }


}