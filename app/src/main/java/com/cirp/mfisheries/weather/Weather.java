package com.cirp.mfisheries.weather;

import android.content.Context;
import android.util.Log;

import com.cirp.mfisheries.R;
import com.cirp.mfisheries.core.module.Module;
import com.cirp.mfisheries.util.PrefsUtil;
import com.google.firebase.messaging.FirebaseMessaging;

public class Weather extends Module {

	public Weather(Context context) {
		super(context);
	}

	@Override
	protected Module setModuleId() {
		this.moduleId = "Weather";
		return this;
	}

	@Override
	protected Module setModuleName() {
		this.name = "Weather";
		return this;
	}

	@Override
	protected Module setIsDisplayed() {
		this.displayed = true;
		return this;
	}

	@Override
	protected Module setImageResource() {
		this.imageResource = R.drawable.icon_marine;
		return this;
	}

	@Override
	protected Module setNeedsRegistration() {
		this.needsRegistration = false;
		return this;
	}

	@Override
	protected Module setActivityClass() {
		this.activityClass = WeatherActivity.class;
		return this;
	}

	@Override
	public void onInstalled(){
		try {
			final String country = PrefsUtil.getCountryId(context);
			if (country.length() > 0) {
				final String topic = String.format("weather_%s", country);
				Log.d(moduleId, "Weather Installed. Attempting to subscribe to" + topic);
				FirebaseMessaging.getInstance().subscribeToTopic(topic);
			} else {
				Log.d(moduleId, "Unable to subscribe to weather notifications");
			}
		}catch (Exception e){e.printStackTrace();}
	}

}
