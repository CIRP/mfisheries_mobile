package com.cirp.mfisheries.weather;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.cirp.mfisheries.R;
import com.cirp.mfisheries.core.Country;
import com.cirp.mfisheries.util.NetUtil;
import com.cirp.mfisheries.util.PrefsUtil;
import com.cirp.mfisheries.weather.models.Tides;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
For the tides only the sources with infotype = Oceanic will be considered. Therefore when configuring the source
you must set it up appropriately
 */
public class TidesFragment extends Fragment implements WeatherActivity.LocationUpdatable {
    private static final String TAG = "TidesFrag";
    private Context mContext;
    public ListView tideList;
    public List<Tides> tidal;
    public TidesAdapter adapter;
    private SwipeRefreshLayout swLayout;
    public int numtries = 0;
    private Weather module;
    private String countryId;
    
    
    public static TidesFragment newInstance(Context context, Weather wModule) {
        Log.d(TAG, "Creating a nee Instance of the Tide Fragment");
        TidesFragment fragment = new TidesFragment();
        fragment.mContext = context;
        fragment.module = wModule;
        
        return fragment;
    }

    public TidesFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.tides_layout, container, false);
        tideList = view.findViewById(R.id.tides_view);
        if (mContext == null) mContext = getContext();
        countryId = PrefsUtil.getCountryId(mContext);
        
        setUpSwipeRefresh(view);
        fetchData();
        return view;
    }
    
    public void fetchData(){
        // TODO Make request conform to cache strategy
        try {
            if (NetUtil.isOnline(mContext)) {
                Log.d(TAG, "Device is online, retrieving from server");
                getSourceID();
            }else{
                Log.d(TAG, "Attempting to load from cache");
                loadCache();
            }
        }
        catch (Exception e){
            e.printStackTrace();
        }
    }


    public void getSourceID() {
        swLayout.setRefreshing(true);
        if (tidal != null) tidal.clear();
        if (adapter != null) adapter.notifyDataSetChanged();
        String url = NetUtil.API_URL + "weathersources"+"?countryid=" + countryId;
        Log.d(TAG, "Attempting to get weather sources from: " + url);
        Ion.with(this)
                .load(url)
                .asJsonArray()
                .setCallback(new FutureCallback<JsonArray>() {
                    @Override
                    public void onCompleted(Exception e, JsonArray result) {
                        if (swLayout!= null)swLayout.setRefreshing(false);
                        
                        try {
                            if (e == null) {
                                for (int i = 0; i < result.size(); i++) {
                                    String infotype = result.get(i).getAsJsonObject().get("infotype").toString().replace("\"", "");
                                    Log.d(TAG, "Retrieve Info Type as: " + infotype);
                                    if (infotype.equals("Oceanic")) {
                                        String sourceid = result.get(i).getAsJsonObject().get("id").toString();
                                        getTide(sourceid);
                                        return;
                                    }
                                }
                            }else{
                                throw new Exception(e);
                            }
                        } catch (Exception exc) {
                            Log.d(TAG, "Unable to retrieve data for tides");
                            exc.printStackTrace();
                        }
                        // If we reach this point we have not received a ocenaic
//                        Snackbar.make(tideList, "No tide data available for selected country.", Snackbar.LENGTH_SHORT).show();
                    }
                });
    }

    private void setUpSwipeRefresh( View view){
        swLayout = view.findViewById(R.id.swipe_tide);
        swLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                fetchData();
            }
        });
    }

    public void getTide(final String sourceID) {
        final String url = NetUtil.API_URL + "entry/weather/"+ sourceID;
        Log.d(TAG, "Attempting to Retrieve Tide information from: " + url);
        Ion.with(this)
                .load(url)
                .setHeader(NetUtil.API_KEY_PARAM, NetUtil.API_KEY)
                .asJsonObject()
                .setCallback(new FutureCallback<JsonObject>() {
                    @Override
                    public void onCompleted(Exception e, JsonObject result) {
                        if (e == null){
                            try {
                                tidal = new ArrayList<>();
                                JSONObject readings = new JSONObject(result.get("readings").toString());
                                Log.d(TAG,"Readings:"+readings.toString());
                                JSONArray tideArray= (new JSONArray(readings.get("tides").toString()));
                                Log.d(TAG,"Tide Array:"+tideArray.toString());
                                for (int i=0;i<tideArray.length();i++){
                                    JSONObject tide = (JSONObject) tideArray.get(i);
                                    tidal.add(new Tides(tide.get("event").toString(),tide.get("date")+" "+tide.get("time").toString(),tide.get("level_imperial").toString()));
                                }
        
                                adapter  = new TidesAdapter(mContext,tidal);
                                tideList.setAdapter(adapter);
                                Log.d(TAG, "Retrieved Readings as: " + readings.toString());
                                cacheTides(result);
                                Log.d(TAG,"Cache set");

                            }
                            catch(Exception ex){
                                Log.d(TAG, "Failed to retrieve information from web service.");
                                ex.printStackTrace();
                                if(numtries <5) {
                                    Log.d(TAG, "Trying again");
                                    numtries++;
                                    getTide(sourceID);
                                }
                            }
                        }
                    }
                });
    }

    private void cacheTides(JsonObject result){
        SharedPreferences sharedPreferences =  getActivity().getSharedPreferences("tide", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString("tidal", result.toString());
        editor.apply();
    }

    protected void loadCache() {
        if (swLayout!= null)swLayout.setRefreshing(false);
        SharedPreferences sharedPreferences = getActivity().getSharedPreferences("tide", Context.MODE_PRIVATE);
        if (sharedPreferences.contains("tidal")){
            String data = sharedPreferences.getString("tidal", "");
            JsonParser parser = new JsonParser();
            JsonElement element = parser.parse(data);
            JsonObject list = element.getAsJsonObject();
    
            JSONObject tides = null;
            try {
                tides = new JSONObject(list.toString());
            } catch (JSONException e) {
                e.printStackTrace();
        
            }
    
    
            JSONObject readings = null;
            JSONArray tideArray = null;
            try {
                readings = new JSONObject(tides.get("readings").toString());
                tideArray= (new JSONArray(readings.get("readings").toString()));
                tidal = new ArrayList<>();
        
                for (int i=0;i<tideArray.length();i++){
                    JSONObject tide = (JSONObject) tideArray.get(i);
                    tidal.add(new Tides(tide.get("type").toString(),tide.get("dt").toString(),tide.get("height").toString()));
            
                    adapter  = new TidesAdapter(mContext,tidal);
                    tideList.setAdapter(adapter);
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }else{
            Log.d(TAG, "No Data in Cache");
        }
    }
	
	
	@Override
	public boolean updateLocation(Country country) {
        Log.d(TAG, "Attempting to change country to " + country.getName());
		countryId = country.getId();
		fetchData();
        return true;
	}
}