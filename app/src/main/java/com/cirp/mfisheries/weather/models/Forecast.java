package com.cirp.mfisheries.weather.models;

import java.util.List;

public class Forecast {
	public City city;
	public String cod;
	public double message;
	public long cnt;
	public List<Condition> list;

	public static class City {
		public long id;
		public String name;
		public Coord coord;
		public String country;
		public long population;

		public static class Coord {
			public double lon;
			public double lat;
		}
	}

	public static class Condition {
		public long dt;
		public Temp temp;
		public double pressure;
		public long humidity;
		public Weather weather[];
		public double speed;
		public long deg;
		public long clouds;
		public double rain;

		public static class Temp {
			public double day;
			public double min;
			public double max;
			public double night;
			public double eve;
			public double morn;

		}

		public static class Weather {
			public long id;
			public String main;
			public String description;
			public String icon;

		}
	}
}