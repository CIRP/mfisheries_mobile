package com.cirp.mfisheries.weather.marine;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;

import com.cirp.mfisheries.util.NetUtil;
import com.cirp.mfisheries.weather.models.Marine;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;
import com.squareup.moshi.JsonAdapter;
import com.squareup.moshi.Moshi;
import com.squareup.moshi.Types;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

public class MarineUtil {
    private static final String TAG = "MarineUtil";
    private final Context context;
    private List<Marine> marineList;

    public MarineUtil(@NonNull Context context) {
        this.context = context;
        Log.d(TAG, "Created the marine utilities");
    }

    private List<Marine> convertToMarineList(JsonArray list){
        if(list == null) return null;
        try{
            // Attempt to convert from JSON straight to the models
            Moshi moshi = new Moshi.Builder().build();
            Type listOfMarineType = Types.newParameterizedType(List.class, Marine.class);
            JsonAdapter<List<Marine>> jsonAdapter = moshi.adapter(listOfMarineType);
            List<Marine> marineList = new ArrayList<>();
            List<Marine> json = jsonAdapter.fromJson(list.toString());
            // If we successfully convert the JSON data, we cache and return to requesting client
            if (json != null) {
                marineList.addAll(json);
                cacheReadings(marineList);
                return marineList;
            }
        }catch (Exception e){
            e.printStackTrace();
            Log.e(TAG, "Unable to convert from JSON: " + e.getMessage());
        }
        // If we get to this point, then we were unsuccessful in retrieving or converting data
        return null;
    }

    private void cacheReadings(List<Marine> marineList) {

    }

    public void retrieveMarineList(@NonNull final MarineSourceListener listener){
        marineList = new ArrayList<>();
        final String url = NetUtil.API_URL + "marine";
        Log.d(TAG, "Attempting to retrieve marine info from: " + url);
        try
        {
            Ion.with(context)
                    .load(url)
                    .setHeader(NetUtil.API_KEY_PARAM, NetUtil.API_KEY)
                    .asJsonArray()
                    .setCallback(new FutureCallback<JsonArray>() {
                        @Override
                        public void onCompleted(Exception e, JsonArray result) {
                            try{
                                if (e != null)throw new Exception(e);
                                marineList = convertToMarineList(result);
                                listener.onReceiveMarineList(marineList);
                            }catch (Exception ex){
                                ex.printStackTrace();
                                Log.e(TAG, ex.getMessage());
                            }
                        }
                    });
        }
        catch(Exception e)
        {
            e.printStackTrace();
            Log.e(TAG, e.getMessage());
        }

    }

    public interface MarineSourceListener {
        void onReceiveMarineList(@Nullable List<Marine> marineList);
    }
}
