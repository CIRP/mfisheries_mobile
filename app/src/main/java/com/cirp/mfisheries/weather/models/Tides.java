package com.cirp.mfisheries.weather.models;

import java.util.Date;

/**
 * Created by kersc on 8/9/2017.
 */

public class Tides {
    public String tide ="a";
    public String times ="a";
    public String feet= "a";

    public Tides(String tide, String times, String feet) {
        this.tide = tide;
        this.times = times;
        this.feet = feet;
    }

    public String getTide() {
        return tide;
    }

    public void setTide(String tide) {
        this.tide = tide;
    }

    public String getTimes() {
//        long epoch = Long.parseLong(times);
//        Date expiry = new Date( epoch * 1000 );
        return times;
    }

    public void setTimes(String times) {
        this.times = times;
    }

    public String getFeet() {
        return feet;
    }

    public void setFeet(String feet) {
        this.feet = feet;
    }
}
