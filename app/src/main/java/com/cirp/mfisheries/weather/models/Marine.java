package com.cirp.mfisheries.weather.models;

public class Marine {
    private String name;
    private String description;
    private String timecreated;
    private String path;
    private String url;
    private String id;
    public Marine(String name, String description, String timecreated, String path, String url, String id)
    {
        this.name = name;
        this.description = description;
        this.timecreated =timecreated;
        this.path = path;
        this.url = url;
        this.id = id;
    }

    public String getPath(){
        return this.path;
    }
}
