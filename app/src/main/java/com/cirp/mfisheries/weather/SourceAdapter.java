package com.cirp.mfisheries.weather;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.List;

/**
 * Created by kersc on 7/27/2017.
 */

public class SourceAdapter extends BaseAdapter {
    private final Context mContext;
    private final List<String> sources;

    public SourceAdapter(Context context, List<String> source) {
        this.mContext = context;
        this.sources = source;
    }


    @Override
    public int getCount() {
        return 0;
    }

    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        TextView rowView = (TextView) inflater.inflate(android.R.layout.simple_list_item_1, parent, false);

        String source = sources.get(position);
        final String sourceName = (source.charAt(0) + "").toUpperCase() + source.substring(1);

        rowView.setText(sourceName);

        return rowView;

    }
}