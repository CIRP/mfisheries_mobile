package com.cirp.mfisheries.weather;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.cirp.mfisheries.R;
import com.cirp.mfisheries.core.Country;
import com.cirp.mfisheries.core.CountryLoc;
import com.cirp.mfisheries.core.module.Module;
import com.cirp.mfisheries.util.CountryUtil;
import com.cirp.mfisheries.util.NetUtil;
import com.cirp.mfisheries.util.PrefsUtil;
import com.cirp.mfisheries.weather.models.CurrentReadings;
import com.cirp.mfisheries.weather.models.CurrentWeather;
import com.cirp.mfisheries.weather.models.Forecast;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;

/**
 * This is the weather home screen page. The data is retrieved from OpenWeather API and displayed. The met office data configured for the
 * country is also displayed if there are warnings or emergencies triggered by the thresholds.
 * When reading the code you shall see api and api2. Two were used in order to reduce the api calls to a single api
 */
public class CurrentWeatherFragment extends Fragment implements WeatherActivity.LocationUpdatable {
	private static final String TAG = "CurrentWeather";
	//	private List<CurrentReadings> readingsList;
	public List<JsonObject> sourceList;
	public TextView details;
	public CardView card;
	RecyclerView.Adapter forecastListAdapter;
	RecyclerView.LayoutManager recyclerViewLayoutManager;
	RecyclerView forecastList;
	DisplayCountryUpdatable countryUpdatable;
	TextView source;
	LinearLayout linear;
	private List<Forecast.Condition> conditions;
	private double lat;
	private double lng;
	private String currCountry;
	private Context mContext;
	private Module module;
	private View currentView;
	private RecyclerView myRecyclerView;
	
	private int numTriesExtract = 1;
	private WeatherUtil weatherUtil;
	
	
	public CurrentWeatherFragment() {
		// Required empty public constructor
	}
	
	public static CurrentWeatherFragment newInstance(Context context, double latitude, double longitude, Weather wModule) {
		CurrentWeatherFragment fragment = new CurrentWeatherFragment();
		fragment.mContext = context;
		fragment.module = wModule;
		fragment.setLocation(latitude, longitude);
		
		if (fragment.lat == 0 && fragment.lng == 0) {
			fragment.resetLocation();
		}
		return fragment;
	}
	
	public void setLocation(double latitude, double longitude) {
		this.lat = latitude;
		this.lng = longitude;
		// Save to Cache
//		PrefsUtil.setCurrentLatitude(mContext, latitude);
//		PrefsUtil.setCurrentLongitude(mContext, longitude);
	}
	
	@Override
	public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.fragment_current_weather, container, false);
		currentView = view;
		
		if (mContext == null) mContext = getContext();
		currCountry = PrefsUtil.getCountry(mContext);
		
		if (weatherUtil == null) weatherUtil = new WeatherUtil(mContext);
		
		sourceList = new ArrayList<>();
		conditions = new ArrayList<>();
		
		source = view.findViewById(R.id.source_openweather);
		linear = view.findViewById(R.id.linear);
		details = view.findViewById(R.id.more);
		card = view.findViewById(R.id.card_view);
		
		// Setup a horizontal recycler view to display the forecasts
		forecastList = currentView.findViewById(R.id.forecastList);
		recyclerViewLayoutManager = new LinearLayoutManager(mContext);
		LinearLayoutManager layoutManager = new LinearLayoutManager(mContext);
		layoutManager.setOrientation(LinearLayoutManager.HORIZONTAL);
		forecastList.setLayoutManager(layoutManager);
		forecastListAdapter = new RecyclerViewAdapter(mContext, conditions);
		forecastList.setAdapter(forecastListAdapter);
		
		// Set the selection of the text "more details" to open the details for the current source
		details.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent sourceIntent = new Intent(mContext, SourceActivity.class);
				sourceIntent.putExtra("selectedCountry", currCountry);
				startActivity(sourceIntent);
			}
		});
		// Set the selection of the card to open the details for the current source
		card.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent sourceIntent = new Intent(mContext, SourceActivity.class);
				sourceIntent.putExtra("selectedCountry", currCountry);
				startActivity(sourceIntent);
			}
		});
		
		// Notify the user of the source of the information displayed (currently set to Open Weather on launch)
		updateDisplayedSource(null);
		// Load all weather related
		loadWeatherData();
		
		return view;
	}
	
	// TODO - Weather - Reimplement logic for handling cached and offline requests
	
	/**
	 * Load Weather Data values to display on the main screen of the weather module
	 * <p>
	 * 1a. if we have a GPS location, we make a request to the API with current location
	 * 1b. If we have no GPS, we use the local country GPS from countryutil.retrieveCountryLocation
	 * until the GPS locs and updates the location
	 * NB: The retrieval of the data will be based on the caching strategy in the util
	 * 2. If offline we use the cache from the last known location
	 */
	private void loadWeatherData() {
		// Will handle extractors later
		if (NetUtil.isOnline(mContext)) getWeatherExtractor();
		// The utilities for get weather and get forecast will handle offline
		// If we have a GPS location
		if (lat != 0.0 && lng != 0.0) {
			Log.d(TAG, String.format("Retrieving weather and forecast for (%s,%s) retrieved from current location", lat, lng));
			getWeather(lat, lng);
			getForecast(lat, lng);
		} else {
			Log.d(TAG, "GPS has no location, we attempt to use system country location");
			// We do not have the GPS, can we use the location of the registered country
			String countryId = PrefsUtil.getCountryId(mContext);
			int id = Integer.parseInt(countryId);
			if (countryId != null && !countryId.equalsIgnoreCase("-1")) {
				CountryUtil.getInstance(mContext).retrieveCountryLocation(id, new CountryUtil.CountryLocRetrievedListener() {
					@Override
					public void processCountryLocations(List<CountryLoc> countryLocs) {
						if (countryLocs != null && countryLocs.size() > 0) { // we have a valid location
							CountryLoc loc = countryLocs.get(0);
							double tempLat = Double.parseDouble(loc.latitude);
							double tempLng = Double.parseDouble(loc.longitude);
							Log.d(TAG, String.format("Retrieving weather and forecast for (%s,%s) retrieved from the country location records", tempLat, tempLng));
							getWeather(tempLat, tempLng);
							getForecast(tempLat, tempLng);
						} else { // we got no location (either because we have an invalid country id or we are offline and location not cached)
							Log.d(TAG, "Failed to load GPS and Unable to retrieve locations.");
							Toast.makeText(mContext, "Unable to location for accessing weather", Toast.LENGTH_SHORT).show();
						}
					}
				});
			} else {
				Toast.makeText(mContext, "No Country ID specified. Ensure You are logged in", Toast.LENGTH_SHORT).show();
				if (countryId == null) Log.d(TAG, "Country ID not set");
				else if (!countryId.equalsIgnoreCase("-1"))
					Log.d(TAG, "Country id set to -1");
				
			}
		}
	}
	
	private void updateDisplayedSource(String country) {
		String sourceStr = "Source: Open Weather";
		source.setText(sourceStr);
	}
	
	public void getWeather(final double lat, final double lng) {
		weatherUtil.getAPIWeather(lat, lng, new WeatherUtil.APIWeatherReadingListener() {
			@Override
			public void onReceiveCurrentWeather(@Nullable CurrentWeather weather) {
				if (weather != null) {
					Log.d(TAG, "Attempting to display weather data:" + weather);
					if (weather.sys != null)
						updateDisplayedLocation(weather.sys.country);
					displayData(weather);
				} else {
					Snackbar.make(currentView, "Unable to Retrieve Weather Information", Snackbar.LENGTH_SHORT)
							.setAction("Retry", new View.OnClickListener() {
								@Override
								public void onClick(View v) {
									getWeather(lat, lng);
								}
							}).show();
				}
			}
		});
	}
	
	private void updateDisplayedLocation(String code) {
		String country = CountryUtil.getInstance(mContext).getCountryFromCode(code);
		Log.d(TAG, String.format("Code %s generated %s", code, country));
		
		updateDisplayedSource(country);
		if (countryUpdatable != null) countryUpdatable.updateCountry(country);
	}
	
	public void resetLocation() {
		SharedPreferences pref = PreferenceManager.getDefaultSharedPreferences(mContext);
		lat = pref.getFloat("lat", 0);
		lng = pref.getFloat("lng", 0);
	}
	
	
	//displays current weather data in view
	public void displayData(CurrentWeather data) {
		ProgressBar progressBar = currentView.findViewById(R.id.weather_progress);
		progressBar.setVisibility(View.GONE);
		
		TextView temp = currentView.findViewById(R.id.weather_temp);
		temp.setText(String.format("%s °C", String.valueOf(Math.round(data.main.temp))));
		
		TextView condition = currentView.findViewById(R.id.weather_condition);
		condition.setText(data.weather[0].main);
		
		ImageView imageView = currentView.findViewById(R.id.weather_icon);
		Log.d(TAG, "Weather data icon: " + data.weather[0].icon);
		Glide.with(this)
				.load(CurrentWeather.getIcon(data.weather[0].icon, mContext))
				.into(imageView);
		
		TextView cloudy = currentView.findViewById(R.id.weather_cloudy);
		cloudy.setText(String.format(Locale.US, "%s: %d%%", getString(R.string.percent_cloudy), data.clouds.all));
	}
	
	public void getForecast(final double lat, final double lng) {
		weatherUtil.getAPIForecast(lat, lng, new WeatherUtil.APIForecastReadingListener() {
			@Override
			public void onReceiveForecastWeather(@Nullable Forecast forecast) {
				if (forecast != null) {
					conditions.clear();
					conditions.addAll(forecast.list);
					forecastListAdapter.notifyDataSetChanged();
				} else {
					Snackbar.make(currentView, "Unable to Retrieve Forecast", Snackbar.LENGTH_SHORT)
							.setAction("Retry", new View.OnClickListener() {
								@Override
								public void onClick(View v) {
									getForecast(lat, lng);
								}
							}).show();
				}
			}
		});
	}
	
	
	public void getWeatherExtractor() {
		final String url = NetUtil.API_URL + "entry/weather";
		Log.d(TAG, "Attempting to Retrieve weather from: " + url);
		
		Ion.with(this)
				.load(url)
				.setHeader(NetUtil.API_KEY_PARAM, NetUtil.API_KEY)
				.asJsonArray()
				.setCallback(new FutureCallback<JsonArray>() {
					@Override
					public void onCompleted(Exception e, JsonArray result) {
						int status = 0;
						try {
							if (e != null) {
								throw e;
							}
							
							for (int i = 0; i < result.size(); i++) {
								//Based on country selected it checks if met office is a source in order to do comparison with the threshold
								String country = result.get(i).getAsJsonObject().get("country").toString().replace("\"", "");
								String sourceID = result.get(i).getAsJsonObject().get("sourceid").toString().replace("\"", "");
								String source = result.get(i).getAsJsonObject().get("source").toString().replace("\"", "");
								sourceList.add(result.get(i).getAsJsonObject());
								Log.d(TAG, "Source listing: " + sourceList.toString());
								
								if (country.equalsIgnoreCase(currCountry) && source.toLowerCase().contains("met office")) {
									status = 1;
									JsonObject data = result.get(i).getAsJsonObject();
									JSONObject objectData = new JSONObject(data.toString());
									
									JSONObject readings = new JSONObject(result.get(i).getAsJsonObject().get("readings").toString());
									getThresholds(objectData, readings);
									weatherUtil.checkReadings();
									Log.d(TAG, data.toString());
									
									break; // wont be more than one met office for a given country
								}
							}
							
							//If no met office is found it checks for other sources
							if (status == 0) {
								for (int i = 0; i < result.size(); i++) {
									
									String country = result.get(i).getAsJsonObject().get("country").toString().replace("\"", "");
									String source = result.get(i).getAsJsonObject().get("source").toString().replace("\"", "");
									if (country.equalsIgnoreCase(currCountry)) {
										
										JsonObject data = result.get(i).getAsJsonObject();
										JSONObject objectData = new JSONObject(data.toString());
										JSONObject readings = new JSONObject(result.get(i).getAsJsonObject().get("readings").toString());
										getThresholds(objectData, readings);
										Log.d(TAG, data.toString());
										
									}
								}
							}
							
							
						} catch (Exception ex) {
//                            Toast.makeText(getApplicationContext(), ex.getMessage(), Toast.LENGTH_LONG).show();
//                            trying again
							ex.printStackTrace();
							if (numTriesExtract++ < 3)
								getWeatherExtractor();
						}
					}
				});
	}
	
	
	public void getThresholds(final JSONObject data, final JSONObject readings) {
		
		Ion.with(this)
				.load(NetUtil.API_URL + "weather/thresholds")
				.setHeader(NetUtil.API_KEY_PARAM, NetUtil.API_KEY)
				.asJsonArray()
				.setCallback(new FutureCallback<JsonArray>() {
					@Override
					public void onCompleted(Exception e, JsonArray result) {
						try {
							if (e != null) {
								throw e;
							}
							for (int i = 0; i < result.size(); i++) {
								String sourceid = result.get(i).getAsJsonObject().get("weathersourceid").toString();
								if (sourceid.equals(String.valueOf(data.get("sourceid")))) {
									JsonObject thresholds = result.get(i).getAsJsonObject();
									JSONObject objectData = new JSONObject(thresholds.toString());
									Log.d(TAG, readings.toString());
									display(objectData, readings);
								}
							}
						} catch (Exception ex) {
							Log.e(TAG, "Unable to process thresholds");
//							ex.printStackTrace();
//							Toast.makeText(mContext, "Try Again", Toast.LENGTH_LONG).show();
						}
					}
				});
	}
	
	
	public void display(JSONObject threshold, JSONObject fields) throws JSONException {
		final JSONObject finalThreshold = threshold;
		ImageView signal = currentView.findViewById(R.id.warning_signal);
		ImageView icon1 = currentView.findViewById(R.id.warn1);
		ImageView icon2 = currentView.findViewById(R.id.warn2);
		TextView fishing = currentView.findViewById(R.id.fish);
		ArrayList<ImageView> icons = new ArrayList<>();
//		Add icon to array list to be able to display only the last 2 warnings or emergency icons on the screen
		icons.add(icon1);
		icons.add(icon2);
		CurrentReadings iconname = new CurrentReadings();
		
		JSONObject warnings = null;
		JSONObject emergency = null;
		Glide.with(mContext).clear(currentView.findViewById(R.id.warning_signal));
		Glide.with(mContext).clear(currentView.findViewById(R.id.warn1));
		Glide.with(mContext).clear(currentView.findViewById(R.id.warn2));
		Glide.with(mContext).clear(currentView.findViewById(R.id.fish));
		int i = 0;
		
		
		try {
			JSONObject thresh = new JSONObject(threshold.get("thresholds").toString());
			warnings = new JSONObject(thresh.get("warnings").toString());
			emergency = new JSONObject(thresh.get("emergency").toString());
		} catch (JSONException e) {
			e.printStackTrace();
		}
		Iterator<String> iter = fields.keys();
		while (iter.hasNext()) {
			String key = iter.next();
			if (key.toLowerCase().contains("warning")) {
				if (!String.valueOf(fields.get(key)).toLowerCase().contains("none")) {
					Glide.with(mContext)
							.load(R.drawable.sign_warning).into(signal);
					signal.setClickable(true);
					signal.setOnClickListener(new View.OnClickListener() {
						@Override
						public void onClick(View v) {
							Intent sourceIntent = new Intent(mContext, SourceActivity.class);
							Log.d(TAG, currCountry);
							int sourceid;
							String Source;
							sourceIntent.putExtra("selectedCountry", currCountry);
							try {
								Log.d(TAG, "Threshhold" + finalThreshold.toString());
								sourceid = (int) finalThreshold.get("weathersourceid");
								Log.d(TAG, String.valueOf(sourceid));
								for (int i = 0; i < sourceList.size(); i++) {
									if (sourceList.get(i).get("sourceid").getAsString() == String.valueOf(sourceid)) {
										Source = sourceList.get(i).get("source").getAsString();
										sourceIntent.putExtra("selectedSource", Source);
									}
								}
							} catch (JSONException e) {
								e.printStackTrace();
							}
							startActivity(sourceIntent);
						}
					});
					if (!key.equals(!String.valueOf(fields.get(key)).toLowerCase().contains("none"))) {
						Glide.with(mContext)
								.load(CurrentReadings.getImage(key.toLowerCase())).into(icons.get(i % 2));
						i++;
					}
					Log.d(TAG, "heres" + icons.get(i % 2).toString() + i);
				}
				
			}
		}
		String saved = "";
//Displays the emergency before warning
		iter = warnings.keys();
		while (iter.hasNext()) {
			
			String key = iter.next();
			double warning = 0;
			double strip = 0;

//Check for warning threshold
			
			if (key.toLowerCase().equals("temperature")) {
				try {
//                    convert from fahrenheit to celcius
					strip = Double.parseDouble(String.valueOf(fields.get(key)).replaceAll("[^\\d.]", ""));
					if (strip > 50.0) {
						Log.d(TAG, "Degrees celcius" + strip);
						strip = (strip - 32.0) * (5.0 / 9.0);
						
						fields.put("temperature", (int) strip + " \u00B0" + "C");
						Log.d(TAG, "Degrees " + fields.get("temperature"));
						
					}
				} catch (JSONException e) {
					Log.e(TAG, "error while processing thresholds");
//					e.printStackTrace();
				}
				
			}
//Check for warning threshold
			try {
				warning = Double.parseDouble(String.valueOf(warnings.get(key)));
				strip = Double.parseDouble(String.valueOf(fields.get(key)).replaceAll("[^\\d.]", ""));
				Log.d(TAG, strip + "");
				//Strip variable removes all non numeric data and converts to double to compare with threshold
				if (warning <= strip && !key.equals(saved)) {
					//Places warning sign on screen
					Glide.with(mContext)
							.load(R.drawable.sign_warning).into(signal);
					signal.setClickable(true);
					signal.setOnClickListener(new View.OnClickListener() {
						@Override
						public void onClick(View v) {
							Intent sourceIntent = new Intent(mContext, SourceActivity.class);
							Log.d(TAG, currCountry);
							int sourceid;
							String Source;
							sourceIntent.putExtra("selectedCountry", currCountry);
							try {
								Log.d(TAG, "Threshhold" + finalThreshold.toString());
								sourceid = (int) finalThreshold.get("weathersourceid");
								Log.d(TAG, String.valueOf(sourceid));
								for (int i = 0; i < sourceList.size(); i++) {
									if (sourceList.get(i).get("sourceid").getAsString() == String.valueOf(sourceid)) {
										Source = sourceList.get(i).get("source").getAsString();
										sourceIntent.putExtra("selectedSource", Source);
									}
								}
							} catch (JSONException e) {
								e.printStackTrace();
							}
							startActivity(sourceIntent);
						}
					});
					//Places the icon of threhold value
					Glide.with(mContext)
							.load(CurrentReadings.getImage(key.toLowerCase())).into(icons.get(i % 2));
					Log.d(TAG, "here" + icons.get(i % 2).toString() + i);
					i++;
					saved = key;
				}
			} catch (JSONException e) {
				Log.e(TAG, "Error while processing thresholds");
//				e.printStackTrace();
			}
			
			
		}
		
		iter = emergency.keys();
		while (iter.hasNext()) {
			
			String key = iter.next();
			double emergencys = 0;
			double strip = 0;
			//Check for emergency
			try {
				emergencys = Double.parseDouble(String.valueOf(emergency.get(key)));
				strip = Double.parseDouble(String.valueOf(fields.get(key)).replaceAll("[^\\d.]", ""));
				if (emergencys <= strip) {
					//Strip variable removes all non numeric data and converts to double to compare with threshold
					Glide.with(mContext).load(R.drawable.sign_emergency).into(signal);
					signal.setClickable(true);
					signal.setOnClickListener(new View.OnClickListener() {
						@Override
						public void onClick(View v) {
							Intent sourceIntent = new Intent(mContext, SourceActivity.class);
							Log.d(TAG, currCountry);
							int sourceid;
							String Source;
							sourceIntent.putExtra("selectedCountry", currCountry);
							try {
								Log.d(TAG, "Threshhold" + finalThreshold.toString());
								sourceid = (int) finalThreshold.get("weathersourceid");
								Log.d(TAG, String.valueOf(sourceid));
								for (int i = 0; i < sourceList.size(); i++) {
									if (sourceList.get(i).get("sourceid").getAsString() == String.valueOf(sourceid)) {
										Source = sourceList.get(i).get("source").getAsString();
										sourceIntent.putExtra("selectedSource", Source);
									}
								}
							} catch (JSONException e) {
								Log.e(TAG, "Problem occurred while processing thresholds");
//								e.printStackTrace();
							}
							startActivity(sourceIntent);
						}
					});
					//Places warning sign on screen
					// the if statement checks if the icon was placed already for a warning so that it only appears once
					if (!key.equals(saved)) {
						Glide.with(mContext)
								.load(CurrentReadings.getImage(key.toLowerCase())).into(icons.get(i % 2));
						saved = key;
						i++;
					}
					Log.d(TAG, "heres" + icons.get(i % 2).toString() + i);
					
				}
			} catch (JSONException e) {
				Log.e(TAG, "Problem occurred while processing thresholds");
//				e.printStackTrace();
			}
		}
		//If threshold was triggered then it gives a caution message
		
		if (i == 0) {
			fishing.setText(R.string.good_day_fish);
		} else
			fishing.setText(R.string.bad_day_fish);
	}
	
	public void setCountryUpdates(DisplayCountryUpdatable countryUpdates) {
		this.countryUpdatable = countryUpdates;
	}
	
	@Override
	public boolean updateLocation(final Country country) {
		currCountry = country.getName();
		
		// make request in separate thread to ease up processing for remaining update locations
		(new Thread() {
			public void run() {
				Log.d(TAG, "Attempting to change Country to " + currCountry);
				CountryUtil.getInstance(mContext).retrieveCountryLocations(new CountryUtil.CountryLocRetrievedListener() {
					@Override
					public void processCountryLocations(List<CountryLoc> countryLocs) {
						Log.d(TAG, String.valueOf(countryLocs));
						if (countryLocs != null) {
							for (CountryLoc loc : countryLocs) {
								if (loc.country.equalsIgnoreCase(country.getName())) {
									double latitude = Double.parseDouble(loc.latitude);
									Log.d(TAG, "New Lat: " + latitude);
									double longitude = Double.parseDouble(loc.longitude);
									Log.d(TAG, "New longitude: " + longitude);
									
									Log.d(TAG, String.format("Retrieved (%s, %s) from server for country %s", latitude, longitude, country.getName()));
									lat = latitude;
									lng = longitude;
									if (currentView != null)
										currentView.post(new Runnable() {
											@Override
											public void run() {
												loadWeatherData();
											}
										});
									break;
								}
							}
						}
					}
				});
			}
		}).start();
		
		
		return false;
	}
}




