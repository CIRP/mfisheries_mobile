package com.cirp.mfisheries.weather;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.location.Location;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.cirp.mfisheries.R;
import com.cirp.mfisheries.core.Country;
import com.cirp.mfisheries.core.CountryAdapter;
import com.cirp.mfisheries.core.CountryLoc;
import com.cirp.mfisheries.core.module.ModuleActivity;
import com.cirp.mfisheries.util.CountryUtil;
import com.cirp.mfisheries.util.NetUtil;
import com.cirp.mfisheries.util.PrefsUtil;
import com.cirp.mfisheries.weather.models.CurrentReadings;
import com.cirp.mfisheries.weather.models.CurrentWeather;
import com.cirp.mfisheries.weather.models.WeatherSrc;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * to handle interaction events.
 * Use the {@link METFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class METFragment extends Fragment implements WeatherActivity.LocationUpdatable{

    public static final String TAG = "METFrag";
    private Context mContext;
    private Weather weather;
    private ListView listView;
    private SwipeRefreshLayout swipe_view;
    private CurrentAdapter adapter;
    public ArrayAdapter<String> adapterSource;

    private SwipeRefreshLayout swLayout;
    private View currentView;
    private Country currCountry;
    private String countryName;

    private int numRetryThresholds = 0;
    public String sourceid;
    private WeatherUtil weatherUtil;
    private int numTries;
    private String selectedSource = "met";

    public String openweather = "";
    public Location mCurrentLocation;

    public String share = "";

    public String save ="";
    public Context context;
    public List<String> sourcelist;
    TextView cNameView;
    TextView sourceName;
    Button changeCountryBtn;


    private JSONArray orderJSON;
    private List<String> myList;




    private double lat;
    private double lon;






    public METFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     */
    // TODO: Rename and change types and number of parameters
    public static METFragment newInstance(Context context, Weather wModule) {
        Log.d(TAG,"Creating new instance of MET Fragment");
        METFragment fragment = new METFragment();
        fragment.mContext = context;
        fragment.weather = wModule;
        return fragment;
    }

//    @Override
//    public void onCreate(Bundle savedInstanceState) {
//        super.onCreate(savedInstanceState);
//
//    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_met, container, false);

        currentView = view;

//        setUpSwipeRefresh();
        sourceName = currentView.findViewById(R.id.sourceName);
        listView = currentView.findViewById(R.id.details_list);
        swipe_view = currentView.findViewById(R.id.swipe_source);

        if (mContext == null) mContext = getContext();

        if(weatherUtil == null) weatherUtil = new WeatherUtil(mContext);

        countryName = PrefsUtil.getCountry(mContext);
        currCountry = new Country(PrefsUtil.getCountryId(mContext), PrefsUtil.getCountry(mContext), PrefsUtil.getPath(mContext));
        getCountryData(selectedSource);


//        CountryUtil.getInstance(mContext).retrieveCountries(new CountryUtil.CountryRetrievedListener() {
//            @Override
//            public void processCountries(List<Country> countries) {
//                if (countries != null){
//                    for (Country val : countries){
//                        if(val.getName().equalsIgnoreCase(countryName)){
//                            currCountry = val;
//                            break;
//                        }
//                    }
//                }
//                // If country is still undefined then use default (repeated to reduce method count)
//                if (currCountry == null) {
//                    currCountry = new Country(PrefsUtil.getCountryId(mContext), PrefsUtil.getCountry(mContext), PrefsUtil.getPath(mContext));
//                    Log.d(TAG, "Current Country is:" + currCountry);
//                }
//                getCountryData(selectedSource);
//            }
//        });


        return view;
    }

    private void setUpSwipeRefresh(){
        swLayout = currentView.findViewById(R.id.swipe_source);
        swLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                getCountryData(selectedSource);
            }
        });
    }

    @Override
    public boolean updateLocation(Country country) {
        Log.d(TAG, "Attempting to change country to "+ country.getName());
        currCountry = country;
        getCountryData(selectedSource);
        return true;
    }

    public void getCountryData(final String selectedSource) {
        if (weatherUtil == null) weatherUtil = new WeatherUtil(mContext);
        if (swLayout != null) {
            swLayout.post(new Runnable() {
                @Override
                public void run() {
                    swLayout.setRefreshing(true);
                }
            });
        }

//        final DateFormat dateFormat = new SimpleDateFormat("MMMM-dd-yyyy", Locale.US);
//        final Date date = new Date();
        // retrieve the weather source for the country selected
        try {
            weatherUtil.retrieveWeatherSource(currCountry.getId(), new WeatherUtil.WeatherSourceListener() {
                @Override
                public void onReceiveSourceList(@Nullable List<WeatherSrc> weatherSources) {
                    if (weatherSources != null) {
                        for (WeatherSrc src : weatherSources) {
                            if (src.infotype.equalsIgnoreCase("weather")) {
                                // If the selected source is one of the data provided by system
                                if (src.name.toLowerCase().contains(selectedSource.toLowerCase())) {
                                    // Update the UI to display the current source
                                    sourceName.setText(String.format("Source: %s", src.name));
                                    // Retrieves the weather extractor
                                    sourceid = src.id;
                                    numTries = 0; // Reset num Tries before attempting to retrieve info
                                    getWeatherExtractor(sourceid);
                                }
                            }
                        }
                    }
                }
            });
        } catch (Exception e){
            Log.d(TAG, "Unable to retrieve Thresholds. Retrying.");
            e.printStackTrace();
            if (weatherUtil == null) {
                weatherUtil = new WeatherUtil(mContext);
                getCountryData(selectedSource);
            }

        }
    }

    public void getWeatherExtractor(final String selectedSourceId){
        numTries = numTries + 1;
        final String url = NetUtil.API_URL + "entry/weather/"+ selectedSourceId;
        Log.d(TAG, "Requesting Information from: " + url);
        Ion.with(this)
                .load(url)
                .setHeader(NetUtil.API_KEY_PARAM, NetUtil.API_KEY)
                .asJsonObject()
                .setCallback(new FutureCallback<JsonObject>() {
                    @Override
                    public void onCompleted(Exception e, JsonObject result) {
                        Log.d(TAG,NetUtil.API_URL + "entry/weather/"+ selectedSourceId);
                        Log.d(TAG,"Raw response for weather entry:" + result);
                        if (e == null) {
                            try {
                                JSONObject objectData = new JSONObject(result.toString());
                                JSONObject readings = new JSONObject(result.get("readings").toString());
//                                Log.d(TAG, "About to retrieve Thresholds");
                                getThresholds(objectData, readings, selectedSourceId);
                                Log.d(TAG, result.toString());
                            } catch (Exception ex) {
                                Log.d(TAG, "Failed to retrieve information from web service. Trying again");
                                Log.d(TAG, ex.toString() + "failed here");
                                getWeatherExtractor(selectedSourceId);
                            }
                        }else{
                            Toast.makeText(mContext, "Unable to Retrieve weather details at this time, try again later", Toast.LENGTH_SHORT).show();
                            e.printStackTrace();
                        }
                    }
                });
    }

    public void getThresholds(final JSONObject data, final JSONObject readings, final String sourceid){
        // Web Service use the sourceid id to send entries values only related to the source selected
        final String thresholdUrl = NetUtil.API_URL + "weather/thresholds/"+sourceid;
        Log.d(TAG,"Retrieving Thresholds from: " + thresholdUrl);

        Ion.with(this)
                .load(thresholdUrl)
                .setHeader(NetUtil.API_KEY_PARAM, NetUtil.API_KEY)
                .asJsonObject()
                .setCallback(new FutureCallback<JsonObject>() {
                    @Override
                    public void onCompleted(Exception e, JsonObject result) {
                        if (e == null) {
                            try {
                                Log.d(TAG, "Threshold results: " + result.toString()); // TODO Change all the System.out to Log.debug
                                JSONObject objectData = new JSONObject(result.toString());
                                Log.d(TAG, "About to try to display readings");
                                displayThresholds(objectData, readings);

                            } catch (Exception ex) {
                                Log.d(TAG, "Unable to retrieve Thresholds. Retrying.");
                                ex.printStackTrace();
                                if (numRetryThresholds++ < 3) {
                                    getThresholds(data, readings, sourceid);
                                } else {
                                    Toast.makeText(mContext.getApplicationContext(), "Unable to receive Warnings. View More details for additional information.", Toast.LENGTH_LONG).show();
                                }
                            }
                            cacheWeather(result, readings);
                        }else{
                            Log.d(TAG, "Unable to receive threshold values");
                            Toast.makeText(mContext, "Unable to retrieve weather at this time. Try Again later", Toast.LENGTH_SHORT).show();
                        }
                    }
                });
    }

    private void cacheWeather(JsonObject thresholds, JSONObject readings){
        SharedPreferences sharedPreferences = mContext.getSharedPreferences("weather", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString("thresholds", thresholds.toString());
        editor.putString("readings",readings.toString());
        editor.apply();
    }

    public void displayData(CurrentWeather data){
        List<CurrentReadings> readingsList;
        readingsList = new ArrayList<>();
        SimpleDateFormat format = new SimpleDateFormat("hh:mm a", Locale.US);
        readingsList.add(new CurrentReadings(String.valueOf(Math.round(data.main.temp)) + " \u00B0" + "C", "temperature", "Temperature"));
        readingsList.add(new CurrentReadings( data.clouds.all + "%", "cloudypercent", getString(R.string.percent_cloudy)));
        readingsList.add(new CurrentReadings(format.format(new Date(data.sys.sunset * 1000)), "sunset", getString(R.string.sunset)));
        readingsList.add(new CurrentReadings(format.format(new Date(data.sys.sunrise * 1000)), "sunrise",getString(R.string.sunrise)));
        readingsList.add(new CurrentReadings(String.valueOf(data.main.humidity) + "%" , "humidity", "Humidity"));
        readingsList.add(new CurrentReadings(String.valueOf(Math.round(data.main.pressure)) +" hPa", "pressure", "Pressure"));
        adapter = new CurrentAdapter(mContext, readingsList);

        listView.post(new Runnable() {
            @Override
            public void run() {
                listView.setOnScrollListener(new AbsListView.OnScrollListener() {
                    public void onScrollStateChanged(AbsListView view, int scrollState) {}
                    public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
                        int topRowVerticalPosition = (listView == null || listView.getChildCount() == 0) ? 0 : listView.getChildAt(0).getTop();
                        swipe_view.setEnabled(firstVisibleItem == 0 && topRowVerticalPosition >= 0);
                    }
                });
            }
        });


//        share += "Temperature: " + String.valueOf(Math.round(data.main.temp)) + " \u00B0" + "C \n\n";
//        share += "Percentage Cloudy " + data.clouds.all + "% \n";
//        share += "Humidity: " + String.valueOf(data.main.humidity) + "%\n";
//        share += "Pressure: " + String.valueOf(Math.round(data.main.pressure)) +" hPa\n";
//        share += "Sunset: " + format.format(new Date(data.sys.sunset * 1000)) + "\n";
//        share += "Sunrise: " + format.format(new Date(data.sys.sunrise * 1000));

        listView.post(new Runnable() {
            @Override
            public void run() {
                listView.setAdapter(adapter);
                adapter.notifyDataSetChanged();
            }
        });
    }

    //Display all readings from extractor and display thresholds.
    public void displayThresholds(JSONObject threshold, JSONObject fields) throws JSONException {
        Log.d(TAG,"Trying to display readings");
        if(fields.length() <=1)
        {
            Toast.makeText(mContext,"The MET office may be unavailable at this time",Toast.LENGTH_SHORT).show();
        }
        Log.d(TAG, "Fields Before: " + fields);
        List<CurrentReadings> extrasList = new ArrayList<>();
        List<String> extrasToRemove = new ArrayList<>();

        int hasOrder = 0;
        orderJSON = new JSONArray();
        if(!fields.has("order"))
        {
            hasOrder=1;
        }
        else
        {
            if (fields.get("order") instanceof JSONArray) {
                orderJSON = (JSONArray) fields.get("order");
            }
            Log.d(TAG, "orderJSON: " + orderJSON);
            myList = new ArrayList<String>();
            //creating an ArrayList of the order field from the JSON data
            for (int i = 0; i < orderJSON.length(); i++) {
                myList.add(orderJSON.getString(i));
            }
            Log.d(TAG, "myList: " + myList);

            Iterator<String> myKeys = fields.keys();
            int found = 0;
            JSONObject extraJSON = new JSONObject();
            //finding the extra fields that are not a part of the defined order
            while (myKeys.hasNext()) {
                found = 0;
                String myKey = myKeys.next();
                for (int i = 0; i < myList.size(); i++) {
                    if (myKey.equals(myList.get(i))) {
                        found = 1;
                    }
                }
                if (found == 0) {
                    extrasToRemove.add(myKey);
                    extraJSON.put(myKey, String.valueOf(fields.get(myKey)));
                }
            }

            //ensuring order and source id are not a part of the displayed readings
            fields.remove("sourceid");
            fields.remove("order");
            extraJSON.remove("sourceid");
            extraJSON.remove("order");
            Log.d(TAG, "Extra JSON: " + extraJSON);
            Log.d(TAG, "Extras to Remove: " + extrasToRemove);

            //removing the unsorted extra fields from the readings
            for (int i = 0; i < extrasToRemove.size(); i++) {
                fields.remove(extrasToRemove.get(i));
            }
            Iterator<String> extraIter = extraJSON.keys();

            //creating a list of CurrentReadings objects based on the extra fields
            while (extraIter.hasNext()) {
                String extraKey = extraIter.next();
                String extraKeyTitle = extraKey.substring(0, 1).toUpperCase() + extraKey.substring(1);
                extrasList.add(new CurrentReadings(String.valueOf(extraJSON.get(extraKey)), extraKey, extraKeyTitle));
            }
            Log.d(TAG, "Fields After: " + fields);

            //Sorting the extra fields alphabetically
            Collections.sort(extrasList, new Comparator<CurrentReadings>() {
                @Override
                public int compare(CurrentReadings lhs, CurrentReadings rhs) {
                    return (lhs.getHead().compareTo(rhs.getHead()));
                }
            });

        }

        List<CurrentReadings> readingsList;
        readingsList = new ArrayList<>();

        JSONObject warnings = null;
        JSONObject emergency = null;
        try {
            JSONObject thresh = new JSONObject(threshold.get("thresholds").toString());
            warnings = new JSONObject(thresh.get("warnings").toString());
            emergency = new JSONObject(thresh.get("emergency").toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }

        Log.d(TAG, "Warnings: " + warnings);
        Log.d(TAG, "Emergencies:" + emergency);
        Log.d(TAG, "Thresholds:" + threshold);
        Log.d(TAG, "Fields:" + fields);

        Iterator<String> iter = fields.keys();
        while (iter.hasNext()) {
            String key = iter.next();
            String keys = key.substring(0, 1).toUpperCase() + key.substring(1);


            if (key.toLowerCase().contains("warning")) {
                if (!String.valueOf(fields.get(key)).toLowerCase().contains("none")) {
                    readingsList.add(new CurrentReadings(String.valueOf(fields.get(key)), key, keys, "yellow"));
                }
                else
                    readingsList.add(new CurrentReadings(String.valueOf(fields.get(key)), key, keys));
            } else {
                if (key.toLowerCase().equals("temperature")) {
//                    convert from fahrenheit to celsius
                    double strip = Double.parseDouble(String.valueOf(fields.get(key)).replaceAll("[^\\d.]", ""));
//                      Assuming that no temperature in celcius is more than 50
                    if (strip > 50.0) {
                        Log.d(TAG,"Degrees celcius" + strip);
                        strip = (strip - 32.0) * (5.0 / 9.0);
                        fields.put("temperature", (int) Math.ceil(strip) + " \u00B0" + "C");
                        Log.d(TAG,"Degrees " + fields.get("temperature"));
                    } else {
                        fields.put("temperature", (int) Math.ceil(strip) + " \u00B0" + "C");
                    }
                }
                Log.d(TAG,key + "keys are here");
                try {
                    if (!(key.equals("sourceid"))) {
                        int state = 0;
                        if (emergency != null && emergency.has(key)) {
                            if (weatherUtil.isNumeric(emergency.get(key).toString())){
                                double emergencys = Double.parseDouble(String.valueOf(emergency.get(key)));
                                double strip = Double.parseDouble(String.valueOf(fields.get(key)).replaceAll("[^\\d.]", ""));
                                if (emergencys <= strip) {
                                    share = String.format("%s%s", share, keys + ":" + String.valueOf(fields.get(key) + "\n\n"));
                                    readingsList.add(new CurrentReadings(String.valueOf(fields.get(key)), key, keys, "red"));
                                    state = 1;
                                }
                            }
                        }

                        if (warnings != null && warnings.has(key) && state == 0) {
                            if (weatherUtil.isNumeric(warnings.get(key).toString())) {
                                double warning = Double.parseDouble(String.valueOf(warnings.get(key)));
                                double strip = Double.parseDouble(String.valueOf(fields.get(key)).replaceAll("[^\\d.]", ""));
                                if (warning <= strip) {
                                    readingsList.add(new CurrentReadings(String.valueOf(fields.get(key)), key, keys, "yellow"));
                                    share = String.format("%s%s", share, keys + ":" + String.valueOf(fields.get(key)) + "\n\n");

                                    state = 1;
                                }
                            }
                        }

                        if (state == 0) {
                            Log.d(TAG,"Currently here " + String.valueOf(fields.get(key)));
                            try {
                                readingsList.add(new CurrentReadings(String.valueOf(fields.get(key)), key, keys));
                                share += keys + ":" + String.valueOf(fields.get(key)) + "\n\n";
                            } catch (Exception e) {
                                e.printStackTrace();
                            }

                        }
                    }


                } catch (JSONException e) {
                    Log.d(TAG,"stopped working here");
                    Log.d(TAG,e.toString() + "message");

                }
            }
            Log.d(TAG,"Just before the adapter");
            Log.d(TAG,"This is readings: "+ readingsList);
            //adapter = new CurrentAdapter(context, readingsList);
            Log.d(TAG,"Just before the list set");
            listView.setOnScrollListener(new AbsListView.OnScrollListener() {
                @Override
                public void onScrollStateChanged(AbsListView view, int scrollState) {

                }

                @Override
                public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
                    int topRowVerticalPosition = (listView == null || listView.getChildCount() == 0) ? 0 : listView.getChildAt(0).getTop();
                    swipe_view.setEnabled(firstVisibleItem == 0 && topRowVerticalPosition >= 0);
                }
            });

            //listView.setAdapter(adapter);
            Log.d(TAG,"Adapter has been set");
        }
        //Sorting the readingsList based on the defined order from the extractor
        if(hasOrder==0) {
            Collections.sort(readingsList, new Comparator<CurrentReadings>() {
                @Override
                public int compare(CurrentReadings lhs, CurrentReadings rhs) {
                    if (myList.indexOf(String.valueOf(lhs.grabTextIcon())) < myList.indexOf(String.valueOf(rhs.grabTextIcon()))) {
                        return -1;
                    }
                    if (myList.indexOf(String.valueOf(lhs.grabTextIcon())) > myList.indexOf(String.valueOf(rhs.grabTextIcon()))) {
                        return 1;
                    } else {
                        return 0;
                    }
                }
            });
            //Adding the alphabetically ordered extra fields to the end of the order sorted list of fields
            readingsList.addAll(extrasList);

        }
        //Setting adapter with correctly ordered fields
        adapter = new CurrentAdapter(mContext, readingsList);
        listView.setAdapter(adapter);

    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
}
