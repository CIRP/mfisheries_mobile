package com.cirp.mfisheries.weather;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;

import com.cirp.mfisheries.R;
import com.cirp.mfisheries.util.CrashReporter;
import com.cirp.mfisheries.util.NetUtil;
import com.cirp.mfisheries.util.NotifyUtil;
import com.cirp.mfisheries.util.PrefsUtil;
import com.cirp.mfisheries.weather.models.CurrentWeather;
import com.cirp.mfisheries.weather.models.Forecast;
import com.cirp.mfisheries.weather.models.WeatherReading;
import com.cirp.mfisheries.weather.models.WeatherSrc;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;
import com.squareup.moshi.JsonAdapter;
import com.squareup.moshi.Moshi;
import com.squareup.moshi.Types;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;

@SuppressWarnings({"UnusedReturnValue", "WeakerAccess"})
public class WeatherUtil {
	private static final String TAG = "WeatherUtil";
	private static final long MAX_HRS = 5;
	final Context context;
	String readLastCountryID = "-1";
	List<WeatherReading> readingList;
	private String noInternetMsg;
	private String errorMsg;
	private String srcLastCountryID = "-1";
	private List<WeatherSrc> sourceList;
	private SharedPreferences preferences;
	
	
	WeatherUtil(Context context) {
		this.context = context;
		errorMsg = "Unable to retrieve data.";
		noInternetMsg = " Phone not connected. Using Cache values.";
		init();
	}
	
	private void init() {
		sourceList = new ArrayList<>();
		readingList = new ArrayList<>();
		preferences = PreferenceManager.getDefaultSharedPreferences(context);
	}
	
	public Set<String> getCacheKeyList() {
		String jsonStr = preferences.getString("weather_keys", null);
		Set<String> lists = new HashSet<>();
		try {
			if (jsonStr != null) {
				// Convert String from shared preference to JSON
				JsonParser parser = new JsonParser();
				JsonElement element = parser.parse(jsonStr);
				JsonArray result = element.getAsJsonArray();
				
				Moshi moshi = new Moshi.Builder().build();
				Type listMyData = Types.newParameterizedType(Set.class, String.class);
				JsonAdapter<Set<String>> adapter = moshi.adapter(listMyData);
				lists = adapter.fromJson(result.toString());
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return lists;
	}
	
	private boolean addKeyToCacheList(String key) {
		Set<String> lists = getCacheKeyList();
		lists.add(key);
		String jsonStr = new Gson().toJson(lists);
		return preferences.edit().putString("weather_keys", jsonStr).commit();
	}
	
	public String getNoInternetMsg(){
		return errorMsg + noInternetMsg;
	}
	
	// ******************* Weather Source **************************
	
	String getWeatherSrcAPI(){
		return NetUtil.API_URL + "weathersources";
	}
	
	void retrieveWeatherSource(@NonNull final String countryid, @NonNull final WeatherSourceListener listener){
		// If we are online,
		//      check if we have last time updated
		//          if we have time, check if max hours exceeded
		//              if not exceeded we use cache else, request from online
		//          if we have no time, request from online
		//
		// If we are offline,
		//      check if we have last time for current location
		//          we have time use cache
		//          we have no time, we use the last location key
		//      If we have no location key,
		//          gracefully fail (i.e. notify user not data available)
		final String key = String.format(Locale.US, "weather-src-%s", countryid);
		final Calendar lastTime = getAPILastTime(key);
		boolean useCache = true;
		if (lastTime != null){
			long diff = Calendar.getInstance().getTimeInMillis() - lastTime.getTimeInMillis();
			long hrs = diff / (60 * 60 * 1000);
			if (hrs > MAX_HRS){
				if (NetUtil.isOnline(context)){
					Log.d(TAG, "Using api for current weather. Time limit exceeded and phone is online");
					useCache = false;
				}
			}
		}else useCache = false; // if last time is null we never received a cache
		
		if (useCache) { // We have information in the cache of sources we can use
			Log.d(TAG, "Using cache for current weather. Time is within the Cached limit");
			sourceList = this.retrieveCachedSources(countryid);
			listener.onReceiveSourceList(sourceList);
			return;
		}
		
		// Either we have no info or the TTL for cache expired so retrieve online
		final String url = getWeatherSrcAPI() + "?countryid=" + countryid;
		Log.d(TAG, "Attempting to weather sources from: " + url);
		Ion.with(context)
				.load(url)
				.setHeader(NetUtil.API_KEY_PARAM, NetUtil.API_KEY)
				.asJsonArray()
				.setCallback(new FutureCallback<JsonArray>() {
					@Override
					public void onCompleted(Exception e, JsonArray result) {
						try {
							Log.d(TAG, "Callback in retrieve weather source executed successfully");
							sourceList = new ArrayList<>();
							srcLastCountryID = countryid;
							// If request was made with no errors (convert json, cache response notify requesting client)
							if (e == null && result != null){
								Log.d(TAG, "Received sources as :" + result.toString());
								Moshi moshi = new Moshi.Builder().build();
								Type listOfCategoryType = Types.newParameterizedType(List.class,WeatherSrc.class);
								JsonAdapter<List<WeatherSrc>> jsonAdapter = moshi.adapter(listOfCategoryType);
								List<WeatherSrc> categories = jsonAdapter.fromJson(result.toString());
								if (categories!= null) {
									Log.d(TAG, "Received " + categories.size() + " sources");
									sourceList.addAll(categories);
									cacheSources(countryid, sourceList);
								}else throw new Exception("Failed to convert Weather Source to representation");
							}else if (e != null) throw new Exception(e);
							// If we at this point, both the retrieval and processing of response was successfull
							listener.onReceiveSourceList(sourceList);
						} catch (Exception ex) {
							ex.printStackTrace();
							Log.d(TAG, "Failed to retrieve information from web service. Attempting to use cache");
							if (lastTime != null) { // If request Failed and we have data in cache, use that
								sourceList = retrieveCachedSources(countryid);
								listener.onReceiveSourceList(sourceList);
							} else { // We made our best effort but no data to be provided
								Log.d(TAG, "No cached sources available for selected country");
								listener.onReceiveSourceList(null);
							}
						}
					}
				});

	}
	
	boolean cacheSources(final String countryid, final List<WeatherSrc> sources){
		String json = new Gson().toJson(sources);
		if (PrefsUtil.setWeatherSource(context, countryid,  json)){
			Log.d(TAG, "Successfully saved Cached sources: " + sources.size());
			final String key = String.format(Locale.US, "weather-src-%s", countryid);
			return this.updateAPICacheTime(key);
		}else{
			Log.d(TAG, "Unable to add sources to cache");
			CrashReporter.getInstance(context).log(Log.ERROR,TAG, "Unable to add sources to cache");
		}
		return false;
	}
	
	List<WeatherSrc> retrieveCachedSources(final String countryid){
		List<WeatherSrc> sources = new ArrayList<>();
		String cachedStr = PrefsUtil.getWeatherSource(context, countryid);
		if (cachedStr != null){
			try{
				Moshi moshi = new Moshi.Builder().build();
				Type listOfCategoryType = Types.newParameterizedType(List.class,WeatherSrc.class);
				JsonAdapter<List<WeatherSrc>> jsonAdapter = moshi.adapter(listOfCategoryType);
				JsonElement element = (new JsonParser()).parse(cachedStr);
				List<WeatherSrc> jsonList = jsonAdapter.fromJson(element.getAsJsonArray().toString());
				if (jsonList != null)sources.addAll(jsonList);
			}catch (Exception e){
				e.printStackTrace();
				Log.d(TAG, "Unable to convert weather source from cache");
			}
		}else Log.d(TAG, "No weather sources in cache");
		return sources;
	}
	
	// ************** Open Weather Operations ******************
	// Weather
	public void getAPIWeather(final double lat, final double lng, @NonNull  final APIWeatherReadingListener listener){
		
		// If we are online,
		//      check if we have last time updated
		//          if we have time, check if max hours exceeded
		//              if not exceeded we use cache else, request from online
		//          if we have no time, request from online
		//
		// If we are offline,
		//      check if we have last time for current location
		//          we have time use cache
		//          we have no time, we use the last location key
		//      If we have no location key,
		//          gracefully fail (i.e. notify user not data available)
		
		final String key = String.format(Locale.US, "current:%.2f:%.2f", lat, lng);
		Log.d(TAG, "Key: " + key);
		Calendar lastTime = getAPILastTime(key);
		
		if (NetUtil.isOnline(context)){ // if we are online
			if (lastTime != null){
				long diff = Calendar.getInstance().getTimeInMillis() - lastTime.getTimeInMillis();
				long hrs = diff / (60 * 60 * 1000);
				if (hrs < MAX_HRS){
					Log.d(TAG, "Using cache for current weather. Time is within the Cached limit");
					this.getAPIWeatherFromCache(lat, lng, listener);
					return;
				}
				Log.d(TAG, "Using api for current weather. Time limit exceeded");
			}
			// We request the data from the API because either we have no previous record or hours exceeded
			final String url = context.getString(R.string.weather_api_current, String.valueOf(lat), String.valueOf(lng), context.getString(R.string.weather_appid));
			Log.d(TAG, "Attempting to retrieve weather from: " + url);
			Ion.with(context)
					.load(url)
					.setHeader(NetUtil.API_KEY_PARAM, NetUtil.API_KEY)
					.asJsonObject()
					.setCallback(new FutureCallback<JsonObject>() {
						@Override
						public void onCompleted(Exception e, JsonObject result) {
							try {
								if (e == null) {
									//parses json
									Moshi moshi = new Moshi.Builder().build();
									JsonAdapter<CurrentWeather> jsonAdapter = moshi.adapter(CurrentWeather.class);
									CurrentWeather data = jsonAdapter.fromJson(result.toString());
									
									if (data != null) {
										listener.onReceiveCurrentWeather(data);
										if (cacheAPIWeather(lat, lng, data))
											Log.d(TAG, "Successfully Update Weather API Cache");
									}else {
										Log.d(TAG, "Received null from the server call, falling back to cache");
										getAPIWeatherFromCache(lat, lng, listener);
									}
								}else{
									Log.d(TAG, "Unable to retrieve current weather data");
									// TODO - Weather - How should cache be used here
									// listener.onReceiveCurrentWeather(null);
									getAPIWeatherFromCache(lat, lng, listener);
									e.printStackTrace();
								}
							} catch (Exception exc) {
								exc.printStackTrace();
								//listener.onReceiveCurrentWeather(null);
								// TODO - Weather - How should cache be used here
								getAPIWeatherFromCache(lat, lng, listener);
							}
						}
					});
		}else{ // We are offline, implement cache strategy
			if (lastTime != null){
				Log.d(TAG, "We have a cached weather report for this location");
				this.getAPIWeatherFromCache(lat, lng, listener);
			}else{ // no previous record for this general location
				// Do we have a past record
				Log.d(TAG, "We have no previous cached record of your current location");
				String lastKey = getLastAPIWeatherKey();
				if (lastKey != null && lastKey.length() > 1  && lastKey.split(":").length > 2){
					Log.d(TAG, "But we have a cached record from: " + lastKey);
					String [] compo = lastKey.split(":");
					double latTemp = Double.parseDouble(compo[1]);
					double lngTemp = Double.parseDouble(compo[2]);
					getAPIWeatherFromCache(latTemp, lngTemp, listener);
				}else{
					Log.d(TAG, "No current  weather record available");
					listener.onReceiveCurrentWeather(null);
				}
			}
		}
	}
	
	private CurrentWeather getAPIWeatherFromCache(final double lat, final double lng, @Nullable final APIWeatherReadingListener listener){
		final String key = String.format(Locale.US, "current:%.2f:%.2f", lat, lng);
		Log.d(TAG, "Attempting to retrieve data for Weather API for " + key);
		try {
			SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
			String jsonStr = preferences.getString(key, null);
			if (jsonStr != null) {
				// Convert String from shared preference to JSON
				JsonParser parser = new JsonParser();
				JsonElement element = parser.parse(jsonStr);
				JsonObject result = element.getAsJsonObject();
				// Convert JSON to CurrentWeather object
				Moshi moshi = new Moshi.Builder().build();
				JsonAdapter<CurrentWeather> jsonAdapter = moshi.adapter(CurrentWeather.class);
				CurrentWeather data = jsonAdapter.fromJson(result.toString());
				Log.d(TAG, "Received current weather from cache as: " + data);
				if (listener != null)listener.onReceiveCurrentWeather(data);
				return data;
			}
		}catch (Exception e){
			Log.d(TAG, "Failed to convert JSON data from cache for Current Weather API");
			e.printStackTrace();
		}
		Log.d(TAG, "Unable to retrieve current data from cache");
		if (listener != null)listener.onReceiveCurrentWeather(null);
		return null;
	}
	
	private boolean cacheAPIWeather(double lat, double lng, @NonNull CurrentWeather weather) {
		final String key = String.format(Locale.US, "current:%.2f:%.2f", lat, lng);
		Log.d(TAG, "Key: " + key);
		final String str = new Gson().toJson(weather);
		if ( preferences.edit().putString(key, str).commit()){
			updateLastAPIWeatherKey(key);
			return updateAPICacheTime(key);
		}
		return false;
	}
	
	private void updateLastAPIWeatherKey(String key){
		addKeyToCacheList(key);
		preferences.edit().putString("weather_api_key", key).apply();
	}
	
	private String getLastAPIWeatherKey(){
		return preferences.getString("weather_api_key", null);
	}

	// Used for both Current and Forecast because key contains the differentiators
	private boolean updateAPICacheTime(@NonNull String key){
		Calendar currTime = Calendar.getInstance();
		key += "-time";
		addKeyToCacheList(key);
		return preferences.edit().putLong(key, currTime.getTimeInMillis()).commit();
	}
	
	// Used for both Current and Forecast because key contains the differentiators
	private Calendar getAPILastTime(@NonNull String key){
		key += "-time";
		long time = preferences.getLong(key, 0);
		if (time != 0){
			Calendar cal =  Calendar.getInstance();
			cal.setTimeInMillis(time);
			return cal;
		}
		return null;
	}
	
	// Forecasts
	public void getAPIForecast(final double lat, final double lng, @NonNull final APIForecastReadingListener listener){
		// If we are online,
		//      check if we have last time updated
		//          if we have time, check if max hours exceeded
		//              if not exceeded we use cache else, request from online
		//          if we have no time, request from online
		//
		// If we are offline,
		//      check if we have last time for current location
		//          we have time use cache
		//          we have no time, we use the last location key
		//      If we have no location key,
		//          gracefully fail (i.e. notify user not data available)
		final String key = String.format(Locale.US, "forecast%.2f-%.2f", lat, lng);
		Log.d(TAG, "Key: " + key);
		Calendar lastTime = getAPILastTime(key);
		
		if (NetUtil.isOnline(context)){ // if we are online
			if (lastTime != null){
				long diff = Calendar.getInstance().getTimeInMillis() - lastTime.getTimeInMillis();
				long hrs = diff / (60 * 60 * 1000);
				if (hrs < MAX_HRS){
					Log.d(TAG, "Using cache for forecast. Time is within the Cached limit");
					this.getAPIForecastFromCache(lat, lng, listener);
					return;
				}
				Log.d(TAG, "Using api for forecast. Time limit exceeded");
			}
			final String url = context.getString(R.string.weather_api_forecast, String.valueOf(lat), String.valueOf(lng), String.valueOf(7), context.getString(R.string.forecast_api));
			Log.d(TAG, "Requesting Forecast from: " + url);
			
			Ion.with(context)
					.load(url)
					.setHeader(NetUtil.API_KEY_PARAM, NetUtil.API_KEY)
					.asJsonObject()
					.setCallback(new FutureCallback<JsonObject>() {
						@Override
						public void onCompleted(Exception e, JsonObject result) {
							try {
								if (e == null) {
									//parses json
									Moshi moshi = new Moshi.Builder().build();
									JsonAdapter<Forecast> jsonAdapter = moshi.adapter(Forecast.class);
									Forecast data = jsonAdapter.fromJson(result.toString());
									if (data != null) {
										listener.onReceiveForecastWeather(data);
										if (cacheAPIForecast(lat, lng, data))
											Log.d(TAG, "Successfully updated Cache for API Forecasts");
									}
									else {
										Log.d(TAG, "Received null from the server call, falling back to cache");
										getAPIForecastFromCache(lat, lng, listener);
									}
								}else{
									Log.d(TAG, "Unable to retrieve current weather");
									getAPIForecastFromCache(lat, lng, listener);
									// TODO - Weather - How should cache be used here
								}
								
							} catch (Exception exc) {
								exc.printStackTrace();
								getAPIForecastFromCache(lat, lng, listener);
								// TODO - Weather - How should cache be used here
							}
							
						}
					});
		}else{ // We are offline, implement cache strategy
			if (lastTime != null){
				Log.d(TAG, "We have a cached weather report for this location");
				this.getAPIForecastFromCache(lat, lng, listener);
			}else{ // no previous record for this general location
				// Do we have a past record
				Log.d(TAG, "We have no previous cached record of your current location");
				String lastKey = getLastAPIForecastKey();
				if (lastKey != null && lastKey.length() > 1 && lastKey.split(":").length > 2){
					Log.d(TAG, "But we have a cached record from: " + lastKey);
					String [] compo = lastKey.split(":");
					double latTemp = Double.parseDouble(compo[1]);
					double lngTemp = Double.parseDouble(compo[2]);
					getAPIForecastFromCache(latTemp, lngTemp, listener);
				}else{
					Log.d(TAG, "No current  weather record available");
					listener.onReceiveForecastWeather(null);
				}
				
			}
		}
		
	}
	
	private Forecast getAPIForecastFromCache(final double lat, final double lng,  @Nullable final APIForecastReadingListener listener){
		final String key = String.format(Locale.US, "forecast:%.2f:%.2f", lat, lng);
		try {
			SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
			String jsonStr = preferences.getString(key, null);
			if (jsonStr != null) {
				// Convert String from shared preference to JSON
				JsonParser parser = new JsonParser();
				JsonElement element = parser.parse(jsonStr);
				JsonObject result = element.getAsJsonObject();
				// Convert JSON to Forecast object
				Moshi moshi = new Moshi.Builder().build();
				JsonAdapter<Forecast> jsonAdapter = moshi.adapter(Forecast.class);
				Forecast data = jsonAdapter.fromJson(result.toString());
				if (listener != null)listener.onReceiveForecastWeather(data);
				return data;
			}
		}catch (Exception e){
			e.printStackTrace();
		}
		Log.d(TAG, "Unable to retrieve forecast data from cache");
		if (listener != null)listener.onReceiveForecastWeather(null);
		return null;
	}
	
	private boolean cacheAPIForecast(double lat, double lng, @NonNull Forecast forecast) {
		final String key = String.format(Locale.US, "forecast:%.2f:%.2f", lat, lng);
		final String str = new Gson().toJson(forecast);
		if (preferences.edit().putString(key, str).commit()){
			updateLastAPIForecastKey(key);
			return updateAPICacheTime(key);
		}
		return false;
	}
	
	private void updateLastAPIForecastKey(String key){
		addKeyToCacheList(key);
		preferences.edit().putString("forecast_api_key", key).apply();
	}
	
	private String getLastAPIForecastKey(){
		return preferences.getString("forecast_api_key", null);
	}

	String getWeatherReadingAPI(){
		return NetUtil.API_URL + "entry/weather";
	}

	public void checkReadings(){
		Log.d(TAG, "Attempting to Run Weather readings");
		retrieveCountryWeatherReading(PrefsUtil.getCountryId(context), new WeatherReadingListener() {
			@Override
			public void onReceiveReadingList(@Nullable List<WeatherReading> list) {
				Log.d(TAG, "Weather readings received from server");
			}
		});

	}

		@SuppressWarnings("SameParameterValue")
	public void retrieveCountryWeatherReading(final String countryid, final WeatherReadingListener listener){
		if (readingList.size() < 1 || !countryid.equals(readLastCountryID)){
			final String url = getWeatherReadingAPI() + "?countryid=" + countryid;
			Log.d(TAG, "Attempting to receive information from: " + url);
			Ion.with(context)
					.load(url)
					.setHeader(NetUtil.API_KEY_PARAM, NetUtil.API_KEY)
					.asJsonArray()
					.setCallback(new FutureCallback<JsonArray>() {
						@Override
						public void onCompleted(Exception e, JsonArray result) {
							try {
								Log.d(TAG, "Callback in retrieve reading by country executed successfully");
								readingList = new ArrayList<>();
								readLastCountryID = countryid;
								if (e == null && result != null){
									Moshi moshi = new Moshi.Builder().build();
									Type listOfCategoryType = Types.newParameterizedType(List.class,WeatherReading.class);
									JsonAdapter<List<WeatherReading>> jsonAdapter = moshi.adapter(listOfCategoryType);
									List<WeatherReading> categories = jsonAdapter.fromJson(result.toString());
									if (categories!= null) {
										readingList.addAll(categories);
										checkForWarning(countryid, readingList);
										cacheReadings(readingList);
									}else throw new Exception("Failed to convert Weather Readings to representation");
								}else if (e != null) throw new Exception(e);
								
								listener.onReceiveReadingList(readingList);
							} catch (Exception ex) {
								ex.printStackTrace();
								Log.d(TAG, "Failed to retrieve information from web service.");
								listener.onReceiveReadingList(null);
							}
						}
					});
		}
		else{
			Log.d(TAG, "Request made recently. Resend Previous data");
			listener.onReceiveReadingList(readingList);
		}
	}
	
	private boolean cacheReadings(List<WeatherReading> readingList) {
		String json = new Gson().toJson(readingList);
		if (PrefsUtil.setWeatherReadings(context, json)){
			Log.d(TAG, "Successfully saved Cached sources: " + readingList.size());
			return PrefsUtil.setWeatherReadingsCacheTime(context, Calendar.getInstance());
		}else{
			Log.d(TAG, "Unable to add sources to cache");
			CrashReporter.getInstance(context).log(Log.ERROR,TAG, "Unable to add sources to cache");
		}
		return false;
	}


	public void checkForWarning(final String countryid, final List<WeatherReading> readingList){
		Log.d(TAG, "Checking for warning in sources: " + readingList.size());
		boolean warning = false;
		for (int i = 0; i< readingList.size(); i++){
			String country = readingList.get(i).countryid.replace("\"", "");
			if(country.equalsIgnoreCase(countryid)){
				List<Map<String, String>> readings = readingList.get(i).readings;
				for (int j = 0; j < readings.size(); j++){
					Map<String, String> map = readings.get(j);
					for(Map.Entry<String, String> entry : map.entrySet()){
						if(entry.getValue().contains("Warning") || entry.getValue().contains("Emergency") || entry.getValue().contains("caution")){
							warning = true;
							Log.d(TAG,"Warning found in"+entry.getKey());
							String message = PrefsUtil.getFirstName(context)+" "+ PrefsUtil.getLastName(context)+"Reminder to check your weather forecast";
							Log.d(TAG, message);
							NotifyUtil notifyUtil = new NotifyUtil(context);
							notifyUtil.weathernotif(message);
							return;
						}
					}
				}
			}
		}
	}
	
	// ************** Weather Readings/Entries ******************
	
	String getWeatherThresholdAPI(){
		return NetUtil.API_URL + "weather/thresholds";
	}
	
	// Utilities
	public boolean isNumeric(String str) {
		for (char c : str.toCharArray()) {
			if (!Character.isDigit(c)) return false;
		}
		return true;
	}
	
	interface WeatherSourceListener {
		void onReceiveSourceList(@Nullable List<WeatherSrc> weatherSources);
	}
	
	public interface APIWeatherReadingListener {
		void onReceiveCurrentWeather(@Nullable CurrentWeather weather);
	}
	
	// ************** Weather Thresholds ******************
	
	public interface APIForecastReadingListener {
		void onReceiveForecastWeather(@Nullable Forecast forecast);
	}
	
	
	public interface WeatherReadingListener {
		void onReceiveReadingList(@Nullable List<WeatherReading> list);
	}
	
	// ************** Tides ******************
	
//	weatherUtil.retrieveWeatherSource(currCountry, new WeatherUtil.MarineSourceListener() {
//		@Override
//		public void onReceiveSourceList(@Nullable List<String> weatherSources) {
//			if (weatherSources != null){
//				// reset sources list
//				sourceList.clear();
//				sourceList.addAll(weatherSources);
//				sourceList.add(openweather);
//
//				for(String source: weatherSources){
//					if (source.toLowerCase().contains(selectedSource.toLowerCase())){
//						sourceName.setText(source);
//						adapterSource.notifyDataSetChanged();
//
//						getWeatherExtractor(sourceid);
//						break;
//					}
//				}
//			}
//		}
//	});
}
