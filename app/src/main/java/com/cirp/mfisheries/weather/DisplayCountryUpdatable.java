package com.cirp.mfisheries.weather;


public interface DisplayCountryUpdatable{
	void updateCountry(String country);
}
