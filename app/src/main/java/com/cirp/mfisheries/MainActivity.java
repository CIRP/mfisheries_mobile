package com.cirp.mfisheries;

import android.Manifest;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.cirp.mfisheries.core.BaseActivity;
import com.cirp.mfisheries.core.Country;
import com.cirp.mfisheries.core.CountryAdapter;
import com.cirp.mfisheries.core.CountryLoc;
import com.cirp.mfisheries.core.NetworkChangeReceiver;
import com.cirp.mfisheries.core.config.ConfigUtil;
import com.cirp.mfisheries.core.download.InstallModuleService;
import com.cirp.mfisheries.core.location.LocationService;
import com.cirp.mfisheries.core.module.Module;
import com.cirp.mfisheries.core.module.ModuleFactory;
import com.cirp.mfisheries.core.module.ModuleRecyclerAdapter;
import com.cirp.mfisheries.core.onboarding.OnboardingActivity;
import com.cirp.mfisheries.core.register.RegisterActivity;
import com.cirp.mfisheries.core.register.RegisterHelper;
import com.cirp.mfisheries.core.register.WebViewActivity;
import com.cirp.mfisheries.sms.addAlertNumber.AddAlertNumberActivity;
import com.cirp.mfisheries.util.CountryUtil;
import com.cirp.mfisheries.util.CrashReporter;
import com.cirp.mfisheries.util.ExperimentUtil;
import com.cirp.mfisheries.util.FileUtil;
import com.cirp.mfisheries.util.ModuleUtil;
import com.cirp.mfisheries.util.NetUtil;
import com.cirp.mfisheries.util.PrefsUtil;
import com.cirp.mfisheries.util.StartupUtil;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;


public class MainActivity extends BaseActivity {
	
	private static final String TAG = "MainActivity";
	public ProgressDialog downloadProgress;
	// Broadcast Receivers
	public BroadcastReceiver registerBroadcastHandler;
	public BroadcastReceiver networkChangeReceiver;
	public BroadcastReceiver installReceiver;
	private boolean NEEDS_REGISTER;
	private boolean SIGNED_IN;
	private SharedPreferences preferences;
	private String country;
	private String countryId;
	private ArrayList<String> selectedModules;
	private boolean[] selected;
	private boolean isInitiated;
	private CountryUtil countryUtil;
	private StartupUtil startupUtil;

	private boolean modulesAreConfigurable = false;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		// Setup toolbar for application
		final Toolbar toolbar = findViewById(R.id.toolbar);
		if (toolbar != null) {
			setSupportActionBar(toolbar);
			customToolbar(toolbar);
		}
		//
		// Retrieve Utility for country-related requests
		countryUtil = CountryUtil.getInstance(this);
        // Retrieve Utility for startup-related requests
        startupUtil = StartupUtil.getInstance(this);
		//Retrieve SharedPreferences object
		preferences = PreferenceManager.getDefaultSharedPreferences(MainActivity.this);
		isInitiated = false;
		// Ensure that the application is up-to-date
		startupUtil.checkVersion();
		
		initiateListeners();
		initializeFirebase();
		retrieveBackgroundData();
		setGreeting();
		
		// Load all the permissions needed for critical components
		permissions = new String[]{
//				Manifest.permission.SEND_SMS, // Added the SMS and Call Phone here because when alerts trigger would be too late
				Manifest.permission.CALL_PHONE,
				Manifest.permission.WRITE_EXTERNAL_STORAGE,
				Manifest.permission.ACCESS_FINE_LOCATION,
		};
		requestPermissions();
	}
	
	private void customToolbar(Toolbar toolbar) {
		//noinspection ConstantConditions
		if (BuildConfig.FLAVOR.equals("fewer")) {
			toolbar.setBackgroundColor(ContextCompat.getColor(this, R.color.red));
		}
	}
	
	/**
	 * A utility method to retrieve information that does not require user intervention
	 */
	public void retrieveBackgroundData(){
		// Retrieve Country Locations for offline use
		CountryUtil.getInstance(this).retrieveCountryLocations(new CountryUtil.CountryLocRetrievedListener() {
			public void processCountryLocations(List<CountryLoc> countryLocs) {
				if (countryLocs != null) Log.d(TAG, "Retrieved locations: " + countryLocs);
				else Log.d(TAG, "Unable to retrieve country locations");
			}
		});
	}
	
	/**
	 * A utility method to ensure that FireBase is available for the system's use
	 */
	public void initializeFirebase(){
		PrefsUtil.setToken(this, FirebaseInstanceId.getInstance().getToken());
	}

	/**
	 * Runs after the user selects approve for the permissions requested by the BaseActivity class.
	 * It will attempt to load data after the
	 * @param permission
	 */
	@Override
	public void onPermissionGranted(final String permission) {
		super.onPermissionGranted(permission);
		if (!isInitiated) {
			isInitiated = true;
			// Retrieve the selected modules from the local file system
			this.selectedModules = new ArrayList<>(Arrays.asList(getResources().getStringArray(R.array.mfish_modules)));
			Log.d(TAG, "Currently has " + this.selectedModules.size() + " selected modules");
			loadData(this.selectedModules);
		}
	}
	
	public void loadData(final ArrayList<String> modules) {
		// Check if Application Running for the First Time
		if (startupUtil.isFirstLaunch()) {
			Log.d(TAG, "The mFisheries was opened for the first time");
			// Make the request to the user to select the countries
			startupUtil.requestCountry(new StartupUtil.CountrySelectionHandler() {
				public void handleCountrySelection(Country selectedCountry) {
					// Configure the country data for the MainActivity from the selected country
					country = selectedCountry.getName();
					countryId = selectedCountry.getId();
					// Make the request for the user to select the what modules are needed
					if (modulesAreConfigurable) {
						loadModules(selectedCountry.getName(), selectedCountry.getId());
					}else{ // No need to retrieve online, simply sort, display and download resources
						NEEDS_REGISTER = ModuleUtil.needsRegistration(MainActivity.this, selectedModules);
						sortModules(selectedModules, ModuleUtil.MODULE_ORDER);
						saveSelectedModules(selectedModules);
						downloadModulesResources(selectedModules);
					}
				}
			});
		} else { //load already selected modules
			Log.d(TAG, "The mFisheries App was opened previously");
			//get selected country from prefs
			country = PrefsUtil.getCountry(this);
			countryId = PrefsUtil.getCountryId(this);
			// Get if user previously registered
			final boolean isRegistered = PrefsUtil.getIsRegistered(this);
			
			Log.d(TAG, String.format("Number of Modules Selected: %d", modules.size()));
			Log.v(TAG, String.format("Loading data for country: %s(%s)", country, countryId));
			Log.d(TAG, String.format("Is user registered: %s", isRegistered));
			
			if (isDownloadNeeded(modules)) downloadModulesResources(modules);
			else displayModules(modules);

			if (PrefsUtil.hasUserId(this) && PrefsUtil.getUserId(this) != -999) {
				Log.d(TAG, "User ID was set");
				SIGNED_IN = true;
				startLocationServices();
				if (!isRegistered) { // has id but profile not completed
					// Relaunch web view activity
					Intent webViewIntent = new Intent(this, WebViewActivity.class);
					startActivity(webViewIntent);
				} else {
					Log.d(TAG, "User id set but the system is not marked as registered");
				}
			}
			else {
				Log.d(TAG, "User ID was not set");
			}
		}
		
		setGreeting();
		track();
	}
	
	/**
	 * checks whether each selected module has finished downloading successfully
	 * @param modules The list of modules that were selected by user
	 * @return True if download is needed, false if all modules installed
	 */
	public boolean isDownloadNeeded(List<String> modules) {
		for (String moduleName : modules) {
			final Module module = ModuleFactory.getInstance(this).getModule(moduleName);
			final String filePath = FileUtil.getFileUri("/" + module.getId()).toString();
			boolean hasFiles = true; // by default set to true to accommodate resources that do not need files
			if (module.hasDownload() || module.getDataLoc() != null) // IF module has resources to download, check that directory exists
				hasFiles = FileUtil.directoryExists(filePath); // TODO Check if files are what is expected (i.e. if user deleted files)
				
			Log.d(TAG, String.format("DownloadNeeded for:%s Installation Status:%s, hasFiles:%s ", module.getId(),preferences.getBoolean(module.getId(), false),  hasFiles));
			// If the module fails to pass check, immediately signal that download is required
			if (!preferences.getBoolean(module.getId(), false) ||  !hasFiles)
				return true;
			// else continue processing the next module
		}
		return false;
	}
	
	/**
	 * Retrieve the listing of available countries within mFisheries
	 */
	public void loadCountries() {
		if (!NetUtil.isOnline(this)) {
			NetUtil.retryDialog(this, new NetUtil.OnRetryClicked() {
				@Override
				public void retry() {
					loadCountries();
				}
			});
			return;
		}
		final ProgressDialog progress = new ProgressDialog(this);
		progress.setMessage(getString(R.string.loading_countries));
		progress.setProgressStyle(ProgressDialog.STYLE_SPINNER);
		progress.setIndeterminate(true);
		progress.setCancelable(false);
		progress.show();
		countryUtil.retrieveCountries(new CountryUtil.CountryRetrievedListener() {
			@Override
			public void processCountries(List<Country> countries) {
				if (countries != null) {
					selectCountry(countries);
				} else {
					Toast.makeText(MainActivity.this, "Unable to Retrieve Countries", Toast.LENGTH_SHORT).show();
					NetUtil.retryDialog(MainActivity.this, new NetUtil.OnRetryClicked() {
						@Override
						public void retry() {
							loadCountries();
						}
					});
				}
				progress.dismiss();
			}
		});
	}
	
	/**
	 * Presents the Dialog for users to select which country they belong
	 * @param countries A listing of the countries to be displayed to user
	 */
	public void selectCountry(final List<Country> countries) {
		//show country dialog
		for (Country c : countries) Log.d(TAG, c.toString());
		final CountryAdapter arrayAdapter = new CountryAdapter(this, countries);
		Log.d(TAG, "Attempting to display country list:" + countries.size());
		
		new AlertDialog
				.Builder(this)
				.setTitle(getResources().getString(R.string.dialog_country))
				.setCancelable(false)
				.setAdapter(arrayAdapter,
						new DialogInterface.OnClickListener() {
							@Override
							public void onClick(DialogInterface dialog, int which) {
								Country selectedCountry = countries.get(which);
								country = selectedCountry.getName();
								Log.d(TAG, String.format(
										"Country: %s, Path: %s, ID: %s",
										selectedCountry.getName(),
										selectedCountry.getPath(),
										selectedCountry.getId()
								));
								countryUtil.saveCountryData(selectedCountry);
								//load available modules for country
								MainActivity.this.countryId = selectedCountry.getId();
//								loadModules(country, MainActivity.this.countryId);
								sortModules(selectedModules, ModuleUtil.MODULE_ORDER);
								saveSelectedModules(selectedModules);
								downloadModulesResources(selectedModules);
							}
						})
				.show();
	}
	
	public void loadModules(final String country, final String countryId) {
		Log.d(TAG, "Load Module Executed");
		if (country != null && countryId != null) {
			if (!NetUtil.isOnline(this)) {
				NetUtil.retryDialog(this, new NetUtil.OnRetryClicked() {
					@Override
					public void retry() {
						loadModules(country, countryId);
					}
				});
				return;
			}

			final ProgressDialog progress = new ProgressDialog(this);
			progress.setMessage(getString(R.string.loading_modules));
			progress.setProgressStyle(ProgressDialog.STYLE_SPINNER);
			progress.setIndeterminate(true);
			progress.setCancelable(true);
			progress.show();
			// Build the API to make the request for the available country modules
			final String url = NetUtil.API_URL + NetUtil.GET_MODULES_PATH + "?countryid=" + countryId;
			Log.i(TAG, String.format("Country ID selected: %s Requested the Path: %s", countryId, url));
			try {
				Ion.with(this)
						.load(url)
						.setHeader(NetUtil.API_KEY_PARAM, NetUtil.API_KEY)
						.asJsonArray()
						.setCallback(new FutureCallback<JsonArray>() {
							@Override
							public void onCompleted(Exception e, JsonArray result) {
								try {
									// If an error occurred when attempting to retrieve the data
									if (e != null) throw new Exception(e);
									else {
										String[] modules = new String[result.size()];
										for (int i = 0; i < result.size(); i++) {
											JsonObject object = result.get(i).getAsJsonObject();
											modules[i] = object.get("module").getAsString();
										}
										// If we successfully parse the data, then display interface for user to select
										selectModules(modules);
									}
								} catch (Exception ine) {
									ine.printStackTrace();
									CrashReporter.getInstance(getBaseContext())
											.log("Unable to process the response from requesting the modules")
											.report(ine);
									// Prompt the user if they would like to redo the request
									makeLoadModuleRequest(country, countryId);
								}
								progress.dismiss();
							}
						});
			} catch (Exception e) {
				e.printStackTrace();
				progress.dismiss();
				CrashReporter.getInstance(getBaseContext())
						.log("Unable to make request for modules")
						.report(e);
				// Prompt the user if they would like to redo the request
				makeLoadModuleRequest(country, countryId);

			}
		}else{
			Log.d(TAG, "Country was not set. Requesting countries");
			loadCountries();
		}
		
		
	}

	public void makeLoadModuleRequest(final String country, final String countryId){
		new AlertDialog.Builder(this)
				.setTitle("Load Modules")
				.setMessage("Would you like to try reloading the modules?")
				.setIcon(android.R.drawable.ic_dialog_alert)
				.setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int whichButton) {
						loadModules(country, countryId);
					}})
				.setNegativeButton(android.R.string.no, null).show();
	}
	
	public void selectModules(final String[] modules) {
		Log.d(TAG, "Selecting Modules");
		selected = new boolean[modules.length];
		
		//loads previously selected modules and sets them to display as checked in list
		selectedModules = this.getSelectedModules();
		for (int i = 0; i < modules.length; i++) {
			selected[i] = selectedModules.contains(modules[i]);
		}
		
		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		builder.setCancelable(false)
				.setTitle(getResources().getString(R.string.dialog_modules))
				.setMultiChoiceItems(modules, selected,
						new DialogInterface.OnMultiChoiceClickListener() {
							@Override
							public void onClick(DialogInterface dialog, int which, boolean isChecked) {
								//keep track of selected modules
								selected[which] = isChecked;
								String module = modules[which];
								if (isChecked) {
									selectedModules.add(module);
								} else if (selectedModules.contains(module)) {
									selectedModules.remove(module);
								}
							}
						})
				.setPositiveButton(R.string.finish, new DialogInterface.OnClickListener() {
					
					@Override
					public void onClick(DialogInterface dialogInterface, int which) {
						NEEDS_REGISTER = ModuleUtil.needsRegistration(MainActivity.this, selectedModules);
						//download modules
						Log.d(TAG, "User selected: " + selectedModules.toString());
						if (selectedModules.size() == 0) {
							saveSelectedModules(selectedModules);
							displayModules(selectedModules);
							return;
						}
						
						sortModules(selectedModules, ModuleUtil.MODULE_ORDER);
						saveSelectedModules(selectedModules);
						downloadModulesResources(selectedModules);
					}
				})
				.show();
	}
	
	public void displayModules(List<String> modules) {
		Log.d(TAG, "Displaying modules " + modules.toString());
		PrefsUtil.setFirstOpen(this, false);
		if (NEEDS_REGISTER && !SIGNED_IN) {
			Log.d(TAG, "Application Requires signin");
			startLocationServices();
			signIn(null);
		}
		
		if (modules.size() % 2 != 0 || modules.size() == 0)
			modules.add(ModuleUtil.PLACEHOLDER_TILE);
		
		RecyclerView gridView = this.findViewById(R.id.gridView);
		gridView.setHasFixedSize(true);
		RecyclerView.LayoutManager layoutManager = new GridLayoutManager(this, 2);
		gridView.setLayoutManager(layoutManager);
		ModuleRecyclerAdapter adapter = new ModuleRecyclerAdapter(this, modules);
		adapter.setPlaceholderListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				loadModules(country, countryId);
			}
		});
		gridView.setAdapter(adapter);
		adapter.notifyDataSetChanged();
	}
	
	private void sortModules(ArrayList<String> selected, String[] allModules) {
		// Adds the Modules in the Order Specified by the Selected Country
		ArrayList<String> moduleOrder = new ArrayList<>(Arrays.asList(allModules));
		ArrayList<String> list = new ArrayList<>();
		int size = moduleOrder.size();
		for (int i = 0; i < size; i++) {
			if (selected.contains(moduleOrder.get(i))) {
				list.add(moduleOrder.get(i));
			}
		}
		selectedModules = list;
	}
	
	public boolean saveSelectedModules(List<String> modules) {
		Log.d(TAG, modules.toString());
		return FileUtil.saveModules(modules);
	}
	
	@SuppressWarnings("unchecked")
	public ArrayList<String> getSelectedModules() {
		return (ArrayList<String>) FileUtil.loadSavedModules();
	}
	
	/**
	 * Utilizes the Install Module Service to handle service request in a synchronized and sequential process
	 * for downloading and extracted resources that have additional information.
	 * @param modules a list of module ids to be processed
	 */
	public void downloadModulesResources(ArrayList<String>modules){
		Log.d(TAG, "Download Module Resources was launched");
		// Start the notification for the user to be aware of the operations in progress
		downloadProgress = new ProgressDialog(this);
		downloadProgress.setMessage(getString(R.string.download_alert));
		downloadProgress.setProgressStyle(ProgressDialog.STYLE_SPINNER);
		downloadProgress.setIndeterminate(true);
		downloadProgress.setCancelable(false);
		downloadProgress.show();
		
		// Make request to the install module
		InstallModuleService.requestInstallModules(this, modules);
		
		// Check with the register listeners to view what is happening
	}
	
	private void startLocationServices() {
		LocationService.startLocationServices(getApplicationContext());
	}
	
	public void signOut(View v) {
		FirebaseAuth.getInstance().signOut();
		if (PrefsUtil.removeUser(this)) {
			Toast.makeText(this, "Successfully logged out.", Toast.LENGTH_SHORT).show();
		} else {
			Toast.makeText(this, "Unable to successfully log out at this time. Try again later.", Toast.LENGTH_SHORT).show();
		}
		setGreeting();
	}
	
	public void signIn(View view) {
		Log.d(TAG, "Signing In Process Request");
		Intent signIn = new Intent(MainActivity.this, RegisterActivity.class);
		startActivityForResult(signIn, RegisterActivity.SIGN_IN_REQUEST);
//		startActivity(signIn);
	}
	
	public void setGreeting() {
		final String username = PrefsUtil.getFirstName(this);
		final TextView greeting = findViewById(R.id.greetingText);
		final Button button = findViewById(R.id.register);
		greeting.setText(String.format("%s%s", getString(R.string.welcome), username));
		if (username.equalsIgnoreCase("guest")) {
			button.setText(R.string.register);
			button.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					signIn(v);
				}
			});
		} else {
			button.setText(R.string.logout);
			button.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					signOut(v);
				}
			});
		}
	}
	
	private void track() {
		try {
			////Analytics
			// Obtain the shared Tracker instance.
			App application = (App) getApplication();
			Tracker appTracker = application.getDefaultTracker();
			if (appTracker != null) {
				appTracker.setSessionTimeout(300); //starts a new session after user has placed the app in background for 300seconds
				appTracker.setClientId(PrefsUtil.getCountryId(getApplicationContext()) + "-" + ((Integer) PrefsUtil.getUserId(getApplicationContext())).toString());
				Log.d("mF-ClientID", PrefsUtil.getCountryId(getApplicationContext()) + "-" + ((Integer) PrefsUtil.getUserId(getApplicationContext())).toString());
				
				// This hit will be sent with the User ID value and be visible in
				// User-ID-enabled views (profiles).
				appTracker.send(new HitBuilders.EventBuilder()
						.setCategory("General Usage")
						.setAction("mFisheries, App Opened")
						.build());
			}
			
		} catch (Exception e) {
			Log.d("mFisheries-Analytics", "Tracker exception - " + e.getLocalizedMessage());
		}
	}
	
	@Override
	public void onResume() {
		super.onResume();
		setGreeting();
		
		if (!preferences.getBoolean("loc_service_stopped", true) && PrefsUtil.hasCountry(this)){
			startLocationServices();
		}
		if (PrefsUtil.hasUserId(this)) {
			SIGNED_IN = true;
		}
		
		registerListeners();
	}
	
	@Override
	protected void onPause() {
		unregisterListeners();
		super.onPause();
	}
	
	@Override
	protected void onStop() {
		if (downloadProgress != null && downloadProgress.isShowing())
			downloadProgress.dismiss();
		super.onStop();
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.menu_main, menu);
		return true;
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			Intent settings = new Intent(this, SettingsActivity.class);
			startActivity(settings);
		}
//		if (id == R.id.action_add) {
//			loadModules(country, countryId + "");
//		}
		if (id==R.id.action_sendSms){
			startActivity(new Intent(this, AddAlertNumberActivity.class));
		}
//		if (id==R.id.action_testBroadcast){
//			//TODO: Remove this after broadcasting works
//			new android.support.v7.app.AlertDialog.Builder(this)
//					.setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
//						@Override
//						public void onClick(DialogInterface dialogInterface, int i) {
//							Ion.with(MainActivity.this)
//									.load("POST", NetUtil.API_URL + NetUtil.TEST_BROADCAST)
//									.setHeader(NetUtil.API_KEY_PARAM, NetUtil.API_KEY)
//									.setMultipartParameter("message","Broadcast Message Test")
//									.setMultipartParameter("simCountry",PrefsUtil.getCountryId(MainActivity.this))
//									.asJsonObject()
//									.setCallback(new FutureCallback<JsonObject>() {
//										@Override
//										public void onCompleted(Exception e, JsonObject result) {
//											if (e==null &&result!=null){
//												Toast.makeText(MainActivity.this,"I should receive a text soon",Toast.LENGTH_SHORT).show();
//											}
//										}
//									});
//						}
//					})
//					.setNegativeButton(android.R.string.no,null)
//					.setMessage("Would you like to test broadcast?")
//					.setTitle("Add number")
//					.show();
//
//		}
		
		return super.onOptionsItemSelected(item);
	}
	
	/**
	 * The method will instantiate all the BroadcastReceivers and assign them to the respective attributes of the activity
	 */
	public void initiateListeners(){
		// Will Launch the Register Activity when authentication is successful
		registerBroadcastHandler = new BroadcastReceiver() {
			@Override
			public void onReceive(Context context, Intent intent) {
//				Intent webViewIntent = new Intent(MainActivity.this, RegisterActivity.class);
//				MainActivity.this.startActivity(webViewIntent);
				Log.d(TAG, "Received Request for Registration");
			}
		};
		networkChangeReceiver = new NetworkChangeReceiver();
		
		// Receiver for handling ability to download and extract additional module resources
		installReceiver = new BroadcastReceiver() {
			@Override
			public void onReceive(Context context, Intent intent) {
				Log.d(TAG, "Received Broadcast from Install Module Service");
				String action = intent.getAction();
				if (action != null) {
					// An individual module installation is completed
					switch (action) {
						case InstallModuleService.INTENT_MODULE_COMPLETE: {
							// Extract module processed and build string
							final String moduleId = intent.getStringExtra(InstallModuleService.STATUS_INSTALL_INFO);
							final String message = String.format("Completed Installing %s", moduleId);
							Log.d(TAG, message);
							
							// Set preferences to let the system know installation was successful
							if (preferences.edit().putBoolean(moduleId, true).commit())
								Log.d(TAG, "Completed setting installation to true");
							
							// Update the user that installation was completed
							runOnUiThread(new Runnable() {
								public void run() {
									if (downloadProgress != null && downloadProgress.isShowing())
										downloadProgress.setMessage(message);
								}
							});
							break;
						}
						// An individual module installation process was initiated
						case InstallModuleService.INTENT_MODULE_START: {
							final String message = String.format("Started download for %s", intent.getStringExtra(InstallModuleService.STATUS_INSTALL_INFO));
							Log.d(TAG, message);
							runOnUiThread(new Runnable() {
								public void run() {
									if (downloadProgress != null && downloadProgress.isShowing())
										downloadProgress.setMessage(message);
								}
							});
							break;
						}
						// All the modules were processed
						case InstallModuleService.INTENT_MODULES_COMPLETE:
							Log.d(TAG, "All Modules were completely installed");
							if (downloadProgress != null && downloadProgress.isShowing())
								downloadProgress.dismiss();
							Log.d(TAG, "Selected Modules have: " + selectedModules);
							displayModules(selectedModules);
							break;
						default:
							Log.d(TAG, "Invalid action received");
							break;
					}
				}
			}
		};
		
		// Register Listener to react if the SOS module requires registration
		registerListeners();
	}
	
	/**
	 * Used to assign the existing listeners
	 */
	public void registerListeners() {
		// If the receiver is null (such as it cleared by memory) then re-instantiate
		if (installReceiver == null)initiateListeners();
		
		Log.d(TAG, "Registering Receivers");
		// Currently the only listener is to trigger the registration process after sign in is completed
		if (registerBroadcastHandler != null)registerReceiver(registerBroadcastHandler, new IntentFilter(RegisterActivity.BROADCAST_ID));
		if (networkChangeReceiver != null) registerReceiver(networkChangeReceiver, new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION));
		
		// Register Receivers to keep track of the installation of module additional resources
		if (installReceiver != null) registerReceiver(installReceiver, new IntentFilter(InstallModuleService.INTENT_MODULES_COMPLETE));
		if (installReceiver != null) registerReceiver(installReceiver, new IntentFilter(InstallModuleService.INTENT_MODULE_COMPLETE));
		if (installReceiver != null) registerReceiver(installReceiver, new IntentFilter(InstallModuleService.INTENT_MODULE_START));
	}
	
	public void unregisterListeners() {
		Log.d(TAG, "Unregister Receivers");
		if (registerBroadcastHandler != null) unregisterReceiver(registerBroadcastHandler);
		if (networkChangeReceiver != null) unregisterReceiver(networkChangeReceiver);
		if (installReceiver != null)unregisterReceiver(installReceiver);
	}
	
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data){
		RegisterHelper helper = new RegisterHelper(this);
		helper.onActivityResult(this, requestCode, resultCode, data);
	}
	
	
}
