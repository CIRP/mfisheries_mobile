package com.cirp.mfisheries;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.cirp.mfisheries.core.BaseActivity;
import com.cirp.mfisheries.core.Country;
import com.cirp.mfisheries.core.CountryAdapter;
import com.cirp.mfisheries.util.CountryUtil;
import com.cirp.mfisheries.util.NetUtil;
import com.cirp.mfisheries.util.PrefsUtil;

import java.util.List;

public class SettingsActivity extends BaseActivity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_settings);
		Toolbar toolbar = findViewById(R.id.toolbar);
		if (toolbar != null) setSupportActionBar(toolbar);
		
		if (getSupportActionBar()!=null)getSupportActionBar().setDisplayHomeAsUpEnabled(true);

		if (PrefsUtil.hasSOSExtraPhone(this)) {
			EditText extra = findViewById(R.id.sosExtra);
			extra.setText(PrefsUtil.getSOSExtraPhone(this));
		}
		
		// Load Text for Country
		updateDisplayLocation();
	}
	
	public void updateDisplayLocation(){
		TextView view = findViewById(R.id.txtCountry);
		String country = PrefsUtil.getCountry(this);
		view.setText(String.format("Current Country is: %s", country));
	}

	public void saveToPrefs(View v) {
		EditText extra = findViewById(R.id.sosExtra);
		String num = extra.getText().toString();

		Log.d("SOS SETTINGS:", "Number " + num);
		int l = num.length();
		if (num.equals("")) {
			Toast.makeText(SettingsActivity.this, "Please enter a valid number", Toast.LENGTH_SHORT).show();
		} else if (l != 10) {
			Log.d("SOS SETTINGS:", "Length = " + l);
			Toast.makeText(SettingsActivity.this, "Please enter a valid number", Toast.LENGTH_SHORT).show();
		} else if (num.matches("[0-9]+")) {
			PrefsUtil.setSOSExtraPhone(this, num);
			Toast.makeText(SettingsActivity.this, "Additional SOS number has been set", Toast.LENGTH_SHORT).show();
		} else {
			Toast.makeText(SettingsActivity.this, "Please enter a valid number", Toast.LENGTH_SHORT).show();
		}
	}
	
	public void changeCountry(final View v){
		final CountryUtil countryUtil = CountryUtil.getInstance(this);
		if (!NetUtil.isOnline(this)) {
			NetUtil.retryDialog(this, new NetUtil.OnRetryClicked() {
				@Override
				public void retry() {
					changeCountry(v);
				}
			});
			return;
		}
		final ProgressDialog progress = new ProgressDialog(this);
		progress.setMessage(getString(R.string.loading_countries));
		progress.setProgressStyle(ProgressDialog.STYLE_SPINNER);
		progress.setIndeterminate(true);
		progress.setCancelable(false);
		progress.show();

		countryUtil.retrieveCountries(new CountryUtil.CountryRetrievedListener() {
			@Override
			public void processCountries(final List<Country> countries) {
				if (countries != null){
					final CountryAdapter arrayAdapter = new CountryAdapter(SettingsActivity.this, countries);
					new AlertDialog.Builder(SettingsActivity.this)
							.setTitle(getResources().getString(R.string.dialog_country))
							.setCancelable(false)
							.setAdapter(arrayAdapter,
									new DialogInterface.OnClickListener() {
										
										@Override
										public void onClick(DialogInterface dialog, int which) {
											Country selectedCountry = countries.get(which);
											countryUtil.saveCountryData(selectedCountry);
											updateDisplayLocation();
											String country = PrefsUtil.getCountry(SettingsActivity.this);
											Toast.makeText(SettingsActivity.this, "Country set to "+country, Toast.LENGTH_SHORT).show();
										}
									})
							.show();
				}else{
					Toast.makeText(SettingsActivity.this, "Unable to Retrieve Countries", Toast.LENGTH_SHORT).show();
					NetUtil.retryDialog(SettingsActivity.this, new NetUtil.OnRetryClicked() {
						@Override
						public void retry() {
							changeCountry(v);
						}
					});
				}
				progress.dismiss();
			}
		});
	}
}
