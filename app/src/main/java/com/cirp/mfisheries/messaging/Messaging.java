package com.cirp.mfisheries.messaging;

import android.content.Context;

import com.cirp.mfisheries.R;
import com.cirp.mfisheries.core.module.Module;

public class Messaging extends Module{
    
    public Messaging(){
        initialize();
    }

    public Messaging(Context context){
        super(context);
    }

    @Override
    protected Module setModuleId() {
        this.moduleId = "Messaging";
        return this;
    }

    @Override
    protected Module setModuleName() {
        this.name = "Messaging";
        return this;
    }

    @Override
    protected Module setIsDisplayed() {
        this.displayed = true;
        return this;
    }

    @Override
    protected Module setImageResource() {
        this.imageResource = R.drawable.icon_messaging;
        return this;
    }

    @Override
    protected Module setNeedsRegistration() {
        this.needsRegistration = true;
        return this;
    }

    @Override
    protected Module setActivityClass() {
        this.activityClass = MessagingActivity.class;
        return this;
    }
}
