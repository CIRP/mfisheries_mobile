package com.cirp.mfisheries.messaging;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.SearchView;
import android.view.View;
import android.widget.TextView;

import com.cirp.mfisheries.R;
import com.cirp.mfisheries.core.module.ModuleActivity;
import com.cirp.mfisheries.util.PrefsUtil;

public class ViewUsersActivity extends ModuleActivity implements SearchView.OnQueryTextListener {

    private UsersFragment usersFragment;
    private FloatingActionButton newGroupFab;
    private static String type = UsersFragment.TYPE_ALL;
    private String countryid;
    private String countryName;
    private TextView cNameView;

    public static void startActivity(Context context) {
        Intent intent = new Intent(context, UserListingActivity.class);
        context.startActivity(intent);
    }

    public static void startActivity(Context context, int flags) {
        Intent intent = new Intent(context, UserListingActivity.class);
        intent.setFlags(flags);
        context.startActivity(intent);
    }

    public static void startActivity(Context context, String type){
        Intent intent = new Intent(context, UserListingActivity.class);
        intent.putExtra("type", type);
        context.startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        bindViews();
        init();
    }

    @Override
    public int getLayoutResourceId() {
        return R.layout.activity_user_listing;
    }

    private void bindViews() {
        newGroupFab = findViewById(R.id.new_group_fab);
        newGroupFab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                getMoreGroupData();
            }
        });

        countryid = PrefsUtil.getCountryId(this);
        countryName = PrefsUtil.getCountry(this);
        cNameView = findViewById(R.id.countryname);
        cNameView.setText(R.string.all_countries);
    }

    private void init() {
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        if(getIntent().hasExtra("type")){
            String type = getIntent().getExtras().getString("type");
            usersFragment = UsersFragment.newInstance(type);
        }
        else{
//            newGroupFab.setVisibility(View.GONE);
            usersFragment = UsersFragment.newInstance(UsersFragment.TYPE_ALL);
        }

        transaction.replace(R.id.users_container, usersFragment);
        transaction.commit();

    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        return false;
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        return false;
    }
}
