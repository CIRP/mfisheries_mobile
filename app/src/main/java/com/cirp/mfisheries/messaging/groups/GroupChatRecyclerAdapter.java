package com.cirp.mfisheries.messaging.groups;

import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.cirp.mfisheries.R;
import com.google.firebase.auth.FirebaseAuth;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Gerard Rique on 8/10/17.
 */

public class GroupChatRecyclerAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder>{

    private List<GroupChatMessage> myMessages;
    private String currentUserID;

    private static final int VIEW_TYPE_ME = 1;
    private static final int VIEW_TYPE_OTHER = 2;

    public GroupChatRecyclerAdapter(List<GroupChatMessage> myMessages) {
        this.myMessages = myMessages;
        currentUserID = FirebaseAuth.getInstance().getCurrentUser().getUid();
    }

    public GroupChatRecyclerAdapter() {
        this.myMessages = new ArrayList<>();
        currentUserID = FirebaseAuth.getInstance().getCurrentUser().getUid();
    }

    public void add(GroupChatMessage groupChatMessage){
        myMessages.add(groupChatMessage);
        notifyItemInserted(myMessages.size() - 1);
    }


    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        RecyclerView.ViewHolder viewHolder = null;
        switch (viewType){
            case VIEW_TYPE_ME:
                View viewChatMine = layoutInflater.inflate(R.layout.group_chat_message_mine, parent, false);
                viewHolder = new MyChatViewHolder(viewChatMine);
                break;
            case VIEW_TYPE_OTHER:
                View viewChatOther = layoutInflater.inflate(R.layout.group_chat_message_theirs, parent, false);
                viewHolder = new OtherChatViewHolder(viewChatOther);
                break;
        }
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {

        GroupChatMessage groupChatMessage = myMessages.get(position);

        if(TextUtils.equals(myMessages.get(position).getSenderUid(), currentUserID)){

            MyChatViewHolder myChatViewHolder = (MyChatViewHolder)holder;

            myChatViewHolder.username.setText("You");
            myChatViewHolder.messageContent.setText(groupChatMessage.getMessage());
        }
        else{

            OtherChatViewHolder otherChatViewHolder = (OtherChatViewHolder)holder;

            otherChatViewHolder.username.setText(groupChatMessage.getSender());
            otherChatViewHolder.messageContent.setText(groupChatMessage.getMessage());
        }
    }

    @Override
    public int getItemCount() {
        if(myMessages != null){
            return myMessages.size();
        }
        return 0;
    }

    @Override
    public int getItemViewType(int position) {
        if(TextUtils.equals(myMessages.get(position).getSenderUid(), FirebaseAuth.getInstance().getCurrentUser().getUid())){
            return VIEW_TYPE_ME;
        }
        else {
            return VIEW_TYPE_OTHER;
        }

    }

    private static class MyChatViewHolder extends RecyclerView.ViewHolder{

        private TextView messageContent, username;

        public MyChatViewHolder(View itemView) {
            super(itemView);
            messageContent = itemView.findViewById(R.id.group_chat_message_mine_content);
            username = itemView.findViewById(R.id.group_chat_message_mine_username);
        }
    }

    private static class OtherChatViewHolder extends RecyclerView.ViewHolder{

        private TextView messageContent, username;

        public OtherChatViewHolder(View itemView) {
            super(itemView);
            messageContent = itemView.findViewById(R.id.group_chat_message_theirs_content);
            username = itemView.findViewById(R.id.group_chat_message_theirs_username);
        }
    }
}
