package com.cirp.mfisheries.messaging;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.TextView;
import android.widget.Toast;

import com.cirp.mfisheries.App;
import com.cirp.mfisheries.R;
import com.cirp.mfisheries.core.Country;
import com.cirp.mfisheries.messaging.groups.MembersRecyclerAdapter;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * This fragment will be used to retrieve a list of all users from FireBase and display this list to the user.
 * There are different scenarios in which a list of users may be required to be displayed to the user which may require different list item designs and  different events to be triggered when a the user selects an item from the list.
 * Different recycler adapters were therefore designed to accommodate these various functionality and design requirements.
 */
public class UsersFragment extends Fragment implements ItemClickSupport.OnItemClickListener, SwipeRefreshLayout.OnRefreshListener {

    public static final String ARG_TYPE = "type";
    public static final String TYPE_CHATS = "type_chats";
    public static final String TYPE_ALL = "type_all";
    public static final String TYPE_CREATE_GROUP = "type_create_group";

    private static final String TAG = "UsersFragment";

    private SwipeRefreshLayout mSwipeRefreshLayout;
    private RecyclerView mRecyclerViewAllUserListing;
    private TextView emptyListMessage;
    private RecyclerView.Adapter myAdapter;
    private List<User> myUsers;
    private DatabaseReference fbReference;
    
    public static UsersFragment newInstance(String type) {
        Bundle args = new Bundle();
        args.putString(ARG_TYPE, type);
        UsersFragment fragment = new UsersFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View fragmentView = inflater.inflate(R.layout.fragment_users, container, false);
        bindViews(fragmentView);
        return fragmentView;
    }

    private void bindViews(View view) {
        mSwipeRefreshLayout = view.findViewById(R.id.swipe_fresh_layout);
        mRecyclerViewAllUserListing = view.findViewById(R.id.recycler_view_all_user_listing);
        emptyListMessage = view.findViewById(R.id.users_fragemt_empty_list);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        init();
    }

    private void init() {
        fbReference = App.getFBDatabase().getReference();
        mSwipeRefreshLayout.setOnRefreshListener(this);
        ItemClickSupport.addTo(mRecyclerViewAllUserListing).setOnItemClickListener(this);
    
        getUsers();
    }

    @Override
    public void onRefresh() {
        getUsers();
    }

    private void getUsers() {
        mSwipeRefreshLayout.setRefreshing(true);
        
        fbReference
                .child(MSGConstants.ARG_USERS)
                .orderByChild("firstName")
                .addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        Log.d(TAG, "Received Messages from FB");
                        final List<User> users = new ArrayList<>();
                        try {
                            Iterator<DataSnapshot> dataSnapshots = dataSnapshot.getChildren().iterator();
                            //noinspection ConstantConditions
                            if (dataSnapshots != null) {
                                while (dataSnapshots.hasNext()) {
                                    DataSnapshot dataSnapshotChild = dataSnapshots.next();
                                    //noinspection ConstantConditions
                                    if (dataSnapshot != null) {
                                        User user = dataSnapshotChild.getValue(User.class);
                                        if (user != null) {
                                            if (!TextUtils.equals(user.uid, FirebaseAuth.getInstance().getCurrentUser().getUid())) {
                                                users.add(user);
                                            }
                                        } else {
                                            Log.d(TAG, "Received a Null value when attempting to retrieve user");
                                        }
                                    }
                                }
                            }
                        }catch(Exception e){
                            e.printStackTrace();
                        }
                        displayUsers(users);
                    }
            
                    @Override
                    public void onCancelled(DatabaseError databaseError) {
                        mSwipeRefreshLayout.setRefreshing(false);
                        Toast.makeText(getContext(), "Unable to retrieve Messages from Server", Toast.LENGTH_SHORT).show();
                    }
                });
    }

    @Override
    public void onItemClicked(RecyclerView recyclerView, int position, View v) {
        if(TextUtils.equals(getArguments().getString(ARG_TYPE), TYPE_ALL)){
            User user = ((UserListingRecyclerAdapter)myAdapter).getUser(position);
            String receiverName = user.firstName + " " + user.lastName;
            ChatActivity.startActivity(getActivity(), user.uid, receiverName, user.firebaseToken);
        }
        else{
            CheckBox myCheckBox = v.findViewById(R.id.select_group_member_check);
            myCheckBox.toggle();
        }
    }
    
    public void displayUsers(List<User> users) {
        myUsers = users;
        mSwipeRefreshLayout.setRefreshing(false);
        
        if(TextUtils.equals(getArguments().getString(ARG_TYPE), TYPE_ALL)){
            myAdapter = new UserListingRecyclerAdapter(users);
        }
        else{
            myAdapter = new MembersRecyclerAdapter(users);
        }

        mRecyclerViewAllUserListing.setAdapter(myAdapter);
        myAdapter.notifyDataSetChanged();
    }

    public void filterUsers(String s){
        String text = s.toLowerCase();
        List<User> filteredList = new ArrayList<>();
        for(User user: myUsers){
            String name = user.firstName.toLowerCase() + " " + user.lastName.toLowerCase();
            if(name.contains(text)) {
                filteredList.add(user);
                Log.d(TAG, String.valueOf(user));
            }
        }

        Log.d(TAG, "User list: "+filteredList);


        if(filteredList.isEmpty()){
            emptyListMessage.setVisibility(View.VISIBLE);
            mRecyclerViewAllUserListing.setVisibility(View.GONE);
        }
        else{
            emptyListMessage.setVisibility(View.GONE);
            mRecyclerViewAllUserListing.setVisibility(View.VISIBLE);
            if(TextUtils.equals(getArguments().getString(ARG_TYPE), TYPE_ALL)){
                UserListingRecyclerAdapter mUserListingRecyclerAdapter = (UserListingRecyclerAdapter)myAdapter;
                mUserListingRecyclerAdapter.setFilter(filteredList);

            }
            else{
                MembersRecyclerAdapter membersRecyclerAdapter = (MembersRecyclerAdapter)myAdapter;
                membersRecyclerAdapter.setFilter(filteredList);
            }
        }
    }
    
    public void filterByCountry(String s,final List<Country> countries){
        final String text = s.toLowerCase();
        Log.d(TAG, "Text is: "+text);
        Country countryFilter = new Country();
        String countryID = "";
        List<User> filteredList = new ArrayList<>();
        if(TextUtils.equals(s, "All Countries")){
            filteredList = myUsers;
        }else{
            if (countries != null){
                for (Country country : countries) {
                    Log.d(TAG, "Country is: "+country);
                    if(country.getName().toLowerCase().equalsIgnoreCase(text)){
                        countryFilter = country;
                        countryID = country.getId();
                    }
                }
            }else{
                Toast.makeText(getContext(), "Unable to retrieve countries", Toast.LENGTH_SHORT).show();
            }
            Log.d(TAG, "Country ID is: "+countryID);
            if (myUsers != null) {
                for (User user : myUsers) {
                    if (user.getCountryId() != null) {
                        Log.d(TAG, "User: "+user);
                        Log.d(TAG, "User Country: "+user.getCountryId().toLowerCase());
                        if (TextUtils.equals(user.getCountryId().toLowerCase(), countryID)){
                            filteredList.add(user);
                            Log.d(TAG, "Users: "+filteredList);
                        }
                    }
                }
            }
        }

        try {
            if (filteredList == null){
                Toast.makeText(getContext(),"No Users found",Toast.LENGTH_SHORT).show();
            }
            else if(TextUtils.equals(getArguments().getString(ARG_TYPE), TYPE_ALL)){
                UserListingRecyclerAdapter mUserListingRecyclerAdapter = (UserListingRecyclerAdapter)myAdapter;
                mUserListingRecyclerAdapter.setFilter(filteredList);
            }
            else{
                MembersRecyclerAdapter membersRecyclerAdapter = (MembersRecyclerAdapter)myAdapter;
                membersRecyclerAdapter.setFilter(filteredList);
            }
        }catch (Exception e){
            e.printStackTrace();
        }

    }
    
    public List<User> getGroupMembers() {
        MembersRecyclerAdapter membersRecyclerAdapter = (MembersRecyclerAdapter)myAdapter;
        return membersRecyclerAdapter.getGroupMembers();
    }
}
