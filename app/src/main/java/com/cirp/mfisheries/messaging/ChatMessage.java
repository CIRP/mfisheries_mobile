package com.cirp.mfisheries.messaging;

public class ChatMessage {

    public String sender;
    public String reciever;
    public String senderUid;
    public String receiverUid;
    public String message;
    public long timestamp;

    public ChatMessage() {
    }

    public ChatMessage(String sender, String reciever, String senderUid, String receiverUid, String message, long timestamp) {
        this.sender = sender;
        this.reciever = reciever;
        this.senderUid = senderUid;
        this.receiverUid = receiverUid;
        this.message = message;
        this.timestamp = timestamp;
    }
}
