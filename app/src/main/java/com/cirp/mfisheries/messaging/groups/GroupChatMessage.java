package com.cirp.mfisheries.messaging.groups;

/**
 * Created by dcitdeveloper on 8/4/17.
 */

public class GroupChatMessage {
    private String senderUid;
    private String sender;
    private String message;
    private long timestamp;

    public GroupChatMessage() {
    }

    public GroupChatMessage(String senderUid, String sender, String message, long timestamp) {
        this.senderUid = senderUid;
        this.sender = sender;
        this.message = message;
        this.timestamp = timestamp;
    }

    public String getSenderUid() {
        return senderUid;
    }

    public void setSenderUid(String senderUid) {
        this.senderUid = senderUid;
    }

    public String getSender() {
        return sender;
    }

    public void setSender(String sender) {
        this.sender = sender;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public long getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(long timestamp) {
        this.timestamp = timestamp;
    }
}
