package com.cirp.mfisheries.messaging;

import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.cirp.mfisheries.R;
import com.cirp.mfisheries.util.CountryUtil;

import java.util.ArrayList;
import java.util.List;


public class UserListingRecyclerAdapter extends RecyclerView.Adapter<UserListingRecyclerAdapter.ViewHolder> {
    
    private static final String TAG = "UserListingAdapter";
    private List<User> mUsers;
    private List<User> groupMembers;
    private CountryUtil countryUtil;

    public UserListingRecyclerAdapter(List<User> users) {
        if (users.size() > 0)
            Log.d(TAG, String.format("Recycler Adapter received %s users", users.get(0).email));
        this.mUsers = users;
    }

    public void add(User user) {
        mUsers.add(user);
        notifyItemInserted(mUsers.size() - 1);
    }

    public void addGroupMember(User user){
        groupMembers.add(user);
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_all_user_listing, parent, false);
        countryUtil = CountryUtil.getInstance(parent.getContext());
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        User user = mUsers.get(position);
        String name = user.firstName + " " + user.lastName;
        holder.layout.setSelected(false);
        if(user.countryId != null){
            if (countryUtil != null)
                holder.flagIcon.setImageResource(countryUtil.getCountryFlagResourceID(user.countryId));
            else Log.d(TAG, "CountryUtil was null");
        }
        if (user.email != null && user.firstName !=null) {
            holder.txtUsername.setText(name);
        }
    }

    @Override
    public int getItemCount() {
        if (mUsers != null) {
            return mUsers.size();
        }
        return 0;
    }

    public User getUser(int position) {
        return mUsers.get(position);
    }
    
    public void setFilter(List<User> users) {
        mUsers = new ArrayList<>();
        mUsers.addAll(users);
        notifyDataSetChanged();
    }

    static class ViewHolder extends RecyclerView.ViewHolder {
        private TextView txtUserAlphabet, txtUsername;
        private RelativeLayout layout;
        private ImageView flagIcon;

        ViewHolder(View itemView) {
            super(itemView);
            txtUserAlphabet = itemView.findViewById(R.id.text_view_user_list_item);
            txtUsername = itemView.findViewById(R.id.text_view_user_list_username);
            layout = itemView.findViewById(R.id.item_all_user_listing_layout);
            flagIcon = itemView.findViewById(R.id.user_listing_country_icon);
        }
    }
}
