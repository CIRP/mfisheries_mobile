package com.cirp.mfisheries.messaging;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.cirp.mfisheries.R;
import com.cirp.mfisheries.messaging.groups.GroupChat;
import com.cirp.mfisheries.util.CountryUtil;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;


@SuppressWarnings("WeakerAccess")
public class ChatListingRecyclerAdapter extends RecyclerView.Adapter<ChatListingRecyclerAdapter.ViewHolder> {
    
    private final Context context;
    //The last messages hash map will be used to map user unique id's to their corresponding last messages to be displayed in the chat listing activity.
    HashMap<String, String> lastMessages;
    HashMap<String, String> usernames;
	private List<Object> myUsers;

    public ChatListingRecyclerAdapter(Context context) {
        this.context = context;
        myUsers = new ArrayList<>();
        lastMessages = new HashMap<>();
    }

    public void addChat(User user){
        if(!exists(user))
            myUsers.add(user);

        notifyItemInserted(myUsers.size() - 1);
    }

    public void addChat(GroupChat groupChat){
        if(!groupExists(groupChat)){
            myUsers.add(groupChat);
            notifyItemInserted(myUsers.size() - 1);
        }
    }

    public void updateLastMessage(String userID, String lastMessage){
        lastMessages.put(userID, lastMessage);
    }

    public void removeChat(int position){
        myUsers.remove(position);
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_chat_all_listing, parent, false);
        return new ViewHolder(view);
    }

    private boolean exists(User myUser){
        for(Object current: myUsers){
            if(current instanceof User){
                User currentUser = (User)current;
                if(TextUtils.equals(currentUser.uid, myUser.uid))
                    return true;
            }
        }
        return false;
    }

    private boolean groupExists(GroupChat groupChat){
        for(Object current: myUsers){
            if(current instanceof GroupChat){
                GroupChat currentGroupChat = (GroupChat)current;
                if(TextUtils.equals(currentGroupChat.getGroupID(), groupChat.getGroupID()))
                    return true;
            }
        }
        return false;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        Object current = myUsers.get(position);
        if(current != null && current instanceof User){
            User currentUser = (User)current;
            String username = currentUser.firstName + " " + currentUser.lastName;
            String lastMessage = lastMessages.get(currentUser.uid);

            holder.username.setText(username);
            holder.lastMessageText.setText(lastMessage);
            
            setCountryFlag(currentUser, holder);
    
            // Control visibility for the group
            holder.userIcon.setVisibility(View.GONE);
            holder.flagIcon.setVisibility(View.VISIBLE);
    
            // Ensure that we change what the username is pointed relative-to
            final RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) holder.username.getLayoutParams();
            params.addRule(RelativeLayout.END_OF, R.id.user_listing_country_icon);
            holder.username.setLayoutParams(params);
            
        }
        else if(current != null && current instanceof GroupChat){
            GroupChat currentGroupChat = (GroupChat)current;
	        String iconLetter;
	        if (currentGroupChat.getGroupName() == null) {
		        iconLetter = "-";
	        } else {
	            if(currentGroupChat.getGroupName().length() <= 1){
	                iconLetter = currentGroupChat.getGroupName();
                }else{
                    iconLetter = currentGroupChat.getGroupName().substring(0, 1);
                }
	        }
            String lastMessage = lastMessages.get(currentGroupChat.getGroupID());

            holder.userIcon.setText(iconLetter);
            holder.username.setText(currentGroupChat.getGroupName());
            holder.lastMessageText.setText(lastMessage);
            
            // Control visibility for the group
            holder.userIcon.setVisibility(View.VISIBLE);
            holder.flagIcon.setVisibility(View.GONE);
    
            // Ensure that we change what the username is pointed relative-to
            final RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) holder.username.getLayoutParams();
            params.addRule(RelativeLayout.END_OF, R.id.text_view_chat_listing_icon);
            holder.username.setLayoutParams(params);
            
        }
        
    }
    
    private void setCountryFlag(User user, ViewHolder holder){
        if(user.countryId != null){
            holder.flagIcon.setImageResource(CountryUtil.getInstance(context).getCountryFlagResourceID(user.countryId));
        }
    }


    @Override
    public int getItemCount() {
        if(myUsers != null){
            return myUsers.size();
        }
        return 0;
    }

    public Object getUser(int position){
        if (isValidPos(position)){
            return myUsers.get(position);
        }else{
            return  null;
        }
    }

    private boolean isValidPos(int position){
        return position >= 0 && position < myUsers.size();
    }

    static class ViewHolder extends RecyclerView.ViewHolder{
        private ImageView flagIcon;
        private TextView username;
        private TextView lastMessageText;
        private TextView userIcon;

        public ViewHolder(View itemView) {
            super(itemView);
            userIcon = itemView.findViewById(R.id.text_view_chat_listing_icon);
            flagIcon = itemView.findViewById(R.id.user_listing_country_icon);
            username = itemView.findViewById(R.id.chat_list_username);
            lastMessageText = itemView.findViewById(R.id.chat_list_last_message);
        }
    }
}
