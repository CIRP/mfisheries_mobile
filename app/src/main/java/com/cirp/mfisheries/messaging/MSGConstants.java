package com.cirp.mfisheries.messaging;

public class MSGConstants {
    public static final String ARG_USERS = "users";
    public static final String ARG_RECEIVER = "receiver";
    public static final String ARG_RECEIVER_UID = "receiver_uid";
    public static final String ARG_CHAT_ROOMS = "chat_rooms";
    public static final String ARG_FIREBASE_TOKEN = "firebaseToken";
    public static final String FIREBASE_STORAGE_PATH = "image/";
    public static final String FIREBASE_DATABASE_PATH = "image";
    public static final String ARG_FRIENDS = "friends";
    public static final String ARG_UID = "uid";
    public static final String ARG_TYPE = "type";
    public static final String ARG_ONE_TO_ONE = "one_to_one_chat";
    public static final String ARG_GROUP_CHAT = "group_chat";
    public static final String ARG_GROUP_ID = "group_id";
    public static final String ARG_GROUP_NAME = "group_name";
    
    
}
