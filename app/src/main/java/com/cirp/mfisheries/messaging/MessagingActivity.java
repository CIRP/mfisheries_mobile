package com.cirp.mfisheries.messaging;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.cirp.mfisheries.App;
import com.cirp.mfisheries.R;
import com.cirp.mfisheries.core.module.ModuleActivity;
import com.cirp.mfisheries.core.register.RegisterActivity;
import com.cirp.mfisheries.messaging.groups.GroupChat;
import com.cirp.mfisheries.util.NetUtil;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

public class MessagingActivity extends ModuleActivity {

    FirebaseUser currentUser;
    private SwipeRefreshLayout refreshChatHistoryList;
    private View emptyView;
    private RecyclerView chatHistoryList;
    private ChatListingRecyclerAdapter chatHistoryAdapter;
    private final String TAG = "MessagingListing";
    
    private FirebaseDatabase firebaseDatabase;
    private DatabaseReference messageDBReference;
    private DatabaseReference activeChatReference;
    private DatabaseReference userDBRefereence;
    
    @Override
    public int getLayoutResourceId() {
        return R.layout.activity_messaging;
    }
    
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        bindViews();
    }
    
    @Override
    public void onResume(){
        super.onResume();
        init();
    }
    
    /**
     * Retrieve the UI components in the XML for programmatic manipulation
     */
    private void bindViews(){
        // Set up advance animations
        setUpFABAnimation();
        
        // refresh to indicate to user data is loading
        refreshChatHistoryList = findViewById(R.id.fragment_chat_list_swipe_refresh_layout);
        refreshChatHistoryList.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                getChats();
            }
        });
        
        emptyView = findViewById(R.id.empty_chat_view);
        
        // Setup RecyclerView for Listings
        chatHistoryList = findViewById(R.id.fragment_chat_list_recycler_view);
        chatHistoryAdapter = new ChatListingRecyclerAdapter(getBaseContext());
        chatHistoryList.setAdapter(chatHistoryAdapter);
        
        handleTouchEvents();
    }
    
    private void setUpFABAnimation(){
        // Fab to start new chat
        FloatingActionButton startChatIcon = findViewById(R.id.fab);
        startChatIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(!NetUtil.isOnline(getApplicationContext())){
                    Log.d(TAG,"No internet connection when sending message");
                    Toast.makeText(MessagingActivity.this, "No internet connection please try again later.", Toast.LENGTH_LONG).show();
                }else{
                    UserListingActivity.startActivity(getBaseContext());
                }
            }
        });
    }
    
    /**
     * Setup components needed
     */
    private void init(){
        currentUser = FirebaseAuth.getInstance().getCurrentUser();
        firebaseDatabase = App.getFBDatabase();
        // Ensure that User is successfully connected to FireBase
        if (currentUser == null){
            (new AlertDialog.Builder(this))
                    .setTitle("Sign In Required")
                    .setIcon(android.R.drawable.ic_dialog_alert)
                    .setMessage("Registration is required to use Messaging. Would you Like to Sign in now?")
                    .setNegativeButton("No", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            dialogInterface.dismiss();
                            finish();
                        }
                    })
                    .setPositiveButton("Sign In", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            signIn();
                        }
                    })
                    .show();
        }else{
            messageDBReference = firebaseDatabase.getReference().child("messaging");
            activeChatReference = messageDBReference.child("active_chats");
            userDBRefereence = firebaseDatabase.getReference().child("users");
            getChats();
        }
    }
    
    public void signIn() {
        Log.d(TAG, "Signing In Process Request");
        // TODO Evaluate the Option to use Launch Activity for Result (and remove finish from authentication check in init method)
        Intent signIn = new Intent(this, RegisterActivity.class);
        startActivity(signIn);
    }
    
    /**
     * Handle the click and long press operations on the chat history records
     */
    private void handleTouchEvents() {
        // Click to Launch the chat with contact
        ItemClickSupport.addTo(chatHistoryList).setOnItemClickListener(new ItemClickSupport.OnItemClickListener() {
            @Override
            public void onItemClicked(RecyclerView recyclerView, int position, View v) {
                Object currentChat = chatHistoryAdapter.getUser(position);
                if(currentChat instanceof User){
                    User currentUser = (User)currentChat;
                    String receiverName = currentUser.firstName + " " + currentUser.lastName;
                    ChatActivity.startActivity(MessagingActivity.this, currentUser.uid, receiverName,  currentUser.firebaseToken);
                }
                else if(currentChat instanceof GroupChat){
                    GroupChat groupChat = (GroupChat)currentChat;
                    ChatActivity.startActivity(MessagingActivity.this, groupChat.getGroupID(), groupChat.getGroupName());
                }
            }
        });
        
        // Long Click to launch additional options for the chat item
        ItemClickSupport.addTo(chatHistoryList).setOnItemLongClickListener(new ItemClickSupport.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClicked(RecyclerView recyclerView, int position, View v) {
                final Object currentChat = chatHistoryAdapter.getUser(position);
                if(currentChat instanceof User){
                    User myUser = (User)currentChat;
                    launchRemoveChatDialog(myUser.uid, position, "single_chat");
                } else if(currentChat instanceof GroupChat){
                    GroupChat myGroupChat = (GroupChat)currentChat;
                    launchRemoveChatDialog(myGroupChat.getGroupID(), position, "group_chat");
                }
                return true;
            }
        });
    }
    
    
    /**
     * Retrieves the active messages for the current user from FireBase
     */
    private void getChats(){
        if (refreshChatHistoryList != null)refreshChatHistoryList.setRefreshing(true);
        Log.d(TAG, String.format("User %s has an id %s", currentUser.getDisplayName(),currentUser.getUid()));
        
        //We must first check if the user has any ongoing chats with other users.
        activeChatReference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                refreshChatHistoryList.post(new Runnable() {
                    @Override
                    public void run() {
                        refreshChatHistoryList.setRefreshing(false);
                    }
                });
                //If the user has no ongoing chats, we hide the recycler view and display a text view indicating that they have no ongoing chats.
                if(!dataSnapshot.hasChild(currentUser.getUid())){
                    emptyView.setVisibility(View.VISIBLE);
                    chatHistoryList.setVisibility(View.GONE);
                }
                else{
                    Log.d(TAG, "Chats exists display history list");
                    emptyView.setVisibility(View.GONE);
                    chatHistoryList.setVisibility(View.VISIBLE);
                }
            }
            
            @Override
            public void onCancelled(DatabaseError databaseError) {
                refreshChatHistoryList.post(new Runnable() {
                    @Override
                    public void run() {
                        refreshChatHistoryList.setRefreshing(false);
                    }
                });
            }
        });
        
        //We can retrieve a list of active chats of the user under the following heading.
        // .orderByChild("") for ordering listing
        activeChatReference.child(currentUser.getUid()).addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                final String chat_room = dataSnapshot.getValue(String.class);//We can gain the chat room ID
                final String contactID = dataSnapshot.getKey();             //We get the receiver of this current chat.
    
                userDBRefereence.child(contactID).addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        Log.d(TAG, String.format("Retrieved user: %s with key: %s and they exist: %s", contactID, dataSnapshot.getKey(), dataSnapshot.exists()));
                        if(dataSnapshot.exists()){
                            final User myUser = dataSnapshot.getValue(User.class);
                            updateUI(myUser);
                            getLastMessage(myUser, chat_room);
                        }
                        else getGroupData(contactID);
                    }
                    
                    @Override
                    public void onCancelled(DatabaseError databaseError) {
                        Log.e(TAG, "Found Unique ID with no corresponding user");
                    }
                });
                Log.d(TAG, "Chat Room: " + chat_room);
            }
            
            public void onChildChanged(DataSnapshot dataSnapshot, String s) { }
            public void onChildRemoved(DataSnapshot dataSnapshot) { }
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {  }
            public void onCancelled(DatabaseError databaseError) { }
        });
        
    }
    
    /**
     * Retrieves the group data related to the group whose id is passed in as a parameter
     * @param groupID The FireBase ID for the desired group
     */
    private void getGroupData(final String groupID){
        messageDBReference.child("groups").child(groupID).child("group_name").addListenerForSingleValueEvent(new ValueEventListener() {
            public void onCancelled(DatabaseError databaseError) { }
            public void onDataChange(DataSnapshot dataSnapshot) {
                if(dataSnapshot.exists()){
                    String groupName = dataSnapshot.getValue(String.class);
                    GroupChat groupChat = new GroupChat(groupName, groupID);
                    updateUI(groupChat);
                    updateLastMessage(groupChat.getGroupID(), "");
                    getLastGroupMessage(groupChat);
                }
            }
        });
    }
    
    /**
     * Retrieve the last post made within the group to display on the group listing
     * @param groupChat The group to get the last message from
     */
    private void getLastGroupMessage(final GroupChat groupChat){
        DatabaseReference databaseReference = firebaseDatabase.getReference();
        
        databaseReference.child("chat_rooms").child(groupChat.getGroupID()).limitToLast(1).addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                ChatMessage lastMessage = dataSnapshot.getValue(ChatMessage.class);
                if(lastMessage != null){
                    updateLastMessage(groupChat.getGroupID(), lastMessage.message);
                }
            }
            
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {  }
            public void onChildRemoved(DataSnapshot dataSnapshot) {  }
            public void onChildMoved(DataSnapshot dataSnapshot, String s) { }
            public void onCancelled(DatabaseError databaseError) {  }
        });
    }
    
    /**
     * Retrieves the last message sent between individual contacts to be displayed in the recent
     * @param contact
     * @param chat_room
     */
    private void getLastMessage(final User contact, String chat_room){
        firebaseDatabase.getReference().child("chat_rooms").child(chat_room).limitToLast(1).addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                ChatMessage lastMessage = dataSnapshot.getValue(ChatMessage.class);
                if (lastMessage != null)
                    updateLastMessage(contact.uid, lastMessage.message);
            }
            public void onChildChanged(DataSnapshot dataSnapshot, String s) { }
            public void onChildRemoved(DataSnapshot dataSnapshot) { }
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {  }
            public void onCancelled(DatabaseError databaseError) {  }
        });
    }
    
    private void updateLastMessage(String userID, String lastMessage){
        refreshChatHistoryList.post(new Runnable() {
            @Override
            public void run() {
                refreshChatHistoryList.setRefreshing(false);
            }
        });
        chatHistoryAdapter.updateLastMessage(userID, lastMessage);
        chatHistoryAdapter.notifyDataSetChanged();
    }
    
    private void updateUI(GroupChat groupChat){
        refreshChatHistoryList.post(new Runnable() {
            @Override
            public void run() {
                refreshChatHistoryList.setRefreshing(false);
            }
        });
        chatHistoryAdapter.addChat(groupChat);
        chatHistoryAdapter.notifyDataSetChanged();
    }
    
    private void updateUI(User myUser){
        refreshChatHistoryList.post(new Runnable() {
            @Override
            public void run() {
                refreshChatHistoryList.setRefreshing(false);
            }
        });
        chatHistoryAdapter.addChat(myUser);
        chatHistoryAdapter.notifyDataSetChanged();
    }
    
    private void launchRemoveChatDialog(final String userChatToRemove, final int position, final String type){
        String message = "Are you sure you want to remove this ";
        String title = "Remove ";
        if (type.equalsIgnoreCase("group_chat")){
            message += "Group";
            title += "Group";
        }else{
            message += "Chat";
            title += "Chat";
        }
        (new AlertDialog.Builder(this))
                .setTitle(title)
                .setMessage(message)
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        if (type.equalsIgnoreCase("group_chat")){
                            removeUserFromGroup(userChatToRemove);
                        }
                        removeChat(userChatToRemove);
                        // Updating the display is located here to facilitate operations for both group and individual chats
                        chatHistoryAdapter.removeChat(position);
                        chatHistoryAdapter.notifyDataSetChanged();
                    }
                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                    }
                })
                .create()
                .show();
    }
    
    private void removeUserFromGroup(String groupId){
        DatabaseReference myReference = firebaseDatabase.getReference();
        
        myReference.child("messaging").child("groups").child(groupId).child("group_members").child(currentUser.getUid()).removeValue(new DatabaseReference.CompletionListener() {
            @Override
            public void onComplete(DatabaseError databaseError, DatabaseReference databaseReference) {
                Log.d(TAG, "Successfully Removed User from Group");
            }
        });
    }
    
    private void removeChat(String contactUid){
        DatabaseReference myReference = firebaseDatabase.getReference();
        String currentUserId = FirebaseAuth.getInstance().getCurrentUser().getUid();
        
        activeChatReference.child(currentUserId).child(contactUid).removeValue(new DatabaseReference.CompletionListener() {
            @Override
            public void onComplete(DatabaseError databaseError, DatabaseReference databaseReference) {
                Toast.makeText(MessagingActivity.this, "Chat Successfully Removed", Toast.LENGTH_LONG).show();
            }
        });
    }
    

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_chat_list, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id){
            case R.id.option_create_group: //Allow the user to select group members from the list users.
//                UserListingActivity.startActivity(MessagingActivity.this, UsersFragment.TYPE_CREATE_GROUP);
                
//                UserListingActivity.startActivity(getBaseContext(), UsersFragment.TYPE_CREATE_GROUP);
                if(!NetUtil.isOnline(getApplicationContext())){
                    Log.d(TAG,"No internet connection when sending message");
                    Toast.makeText(MessagingActivity.this, "No internet connection please try again later.", Toast.LENGTH_LONG).show();
                }else{
                    UserListingActivity.startActivity(getApplicationContext(), UsersFragment.TYPE_CREATE_GROUP);
                }
        }
        return super.onOptionsItemSelected(item);
    }
    @Override
    public int getColor(){
        return R.color.yellow;
    }
    
    @Override
    public int getColorDark(){
        return R.color.yellowDark;
    }
    
    public static void startActivity(Context context){
        Intent intent = new Intent(context, MessagingActivity.class);
        context.startActivity(intent);
    }
    
    public static void startActivity(Context context, int flags){
        Intent intent = new Intent(context, MessagingActivity.class);
        intent.setFlags(flags);
        context.startActivity(intent);
    }
}
