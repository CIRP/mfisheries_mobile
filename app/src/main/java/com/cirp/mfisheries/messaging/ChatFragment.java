package com.cirp.mfisheries.messaging;

import android.app.ProgressDialog;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.cirp.mfisheries.App;
import com.cirp.mfisheries.R;
import com.cirp.mfisheries.messaging.groups.GroupChatMessage;
import com.cirp.mfisheries.messaging.groups.GroupChatRecyclerAdapter;
import com.cirp.mfisheries.util.NetUtil;
import com.cirp.mfisheries.util.PrefsUtil;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.ValueEventListener;
import com.google.gson.JsonObject;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.io.File;
import java.util.ArrayList;

import id.zelory.compressor.Compressor;

import static android.app.Activity.RESULT_OK;
import static com.cirp.mfisheries.util.MediaUtils.COMPRESS_QUALITY;

public class ChatFragment extends Fragment implements ChatContract.View {
    
    public static final int REQUEST_CODE = 1234;
    public static final int PICK_IMAGE = 100;
    private static String TAG = "ChatFragment";
    private RecyclerView mRecyclerViewChat;
    private EditText mETxtMessage;
    private ImageButton mySendButton;
    private ImageButton attachImageButton;
    private RecyclerView.Adapter myAdapter;
    private ChatPresenter mChatPresenter;

    
    public static Fragment newInstance(String receiver, String receiverUid, String firebaseToken) {
        Bundle args = new Bundle();
        args.putString(MSGConstants.ARG_TYPE, MSGConstants.ARG_ONE_TO_ONE);
        args.putString(MSGConstants.ARG_RECEIVER, receiver);
        args.putString(MSGConstants.ARG_RECEIVER_UID, receiverUid);
        args.putString(MSGConstants.ARG_FIREBASE_TOKEN, firebaseToken);
        Fragment fragment = new ChatFragment();
        fragment.setArguments(args);
        return fragment;
    }

    public static Fragment newInstance(String groupID){
        Bundle args = new Bundle();
        args.putString(MSGConstants.ARG_TYPE, MSGConstants.ARG_GROUP_CHAT);
        args.putString("group_id", groupID);
        Fragment fragment = new ChatFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        super.onStop();
        EventBus.getDefault().unregister(this);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        final View fragmentView = inflater.inflate(R.layout.fragment_chat, container, false);
        bindView(fragmentView);

        mySendButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(TextUtils.equals(getArguments().getString(MSGConstants.ARG_TYPE), MSGConstants.ARG_ONE_TO_ONE))
                    sendMessage();
                else if(TextUtils.equals(getArguments().getString(MSGConstants.ARG_TYPE), MSGConstants.ARG_GROUP_CHAT))
                    sendGroupMessage();
            }
        });

//        attachImageButton.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                browseImages(fragmentView);
//            }
//        });

        // Setup Listener to stop user from sending empty text by disabling send button
        mETxtMessage.addTextChangedListener(new TextWatcher() {
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {}
            public void afterTextChanged(Editable editable) { }
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (mETxtMessage.getText().length() > 1)mySendButton.setEnabled(true);
                else mySendButton.setEnabled(false);
            }
        });

        mETxtMessage.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int i, KeyEvent keyEvent) {
                if(i == EditorInfo.IME_ACTION_SEND){
                    if(TextUtils.equals(getArguments().getString(MSGConstants.ARG_TYPE), MSGConstants.ARG_ONE_TO_ONE))
                        sendMessage();
                    else if(TextUtils.equals(getArguments().getString(MSGConstants.ARG_TYPE), MSGConstants.ARG_GROUP_CHAT))
                        sendGroupMessage();
                    return true;
                }
                return false;
            }
        });

        return fragmentView;
    }

    private void bindView(View view){
        mRecyclerViewChat =  view.findViewById(R.id.recycler_view_chat);
//        attachImageButton = view.findViewById(R.id.my_attach_image_button);
        mySendButton =  view.findViewById(R.id.my_send_button);
        mySendButton.setEnabled(false);
        mETxtMessage = view.findViewById(R.id.edit_text_message);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        init();
    }

    private void init(){
        ProgressDialog mProgressDialog = new ProgressDialog(getActivity());
        mProgressDialog.setTitle("Loading");
        mProgressDialog.setMessage("Please Wait");
        mProgressDialog.setIndeterminate(true);

        if(TextUtils.equals(getArguments().getString(MSGConstants.ARG_TYPE) , MSGConstants.ARG_ONE_TO_ONE)){
            mChatPresenter = new ChatPresenter(this);
            mChatPresenter.getMessage(FirebaseAuth.getInstance().getCurrentUser().getUid(),
                    getArguments().getString(MSGConstants.ARG_RECEIVER_UID));
        }
        else if(TextUtils.equals(getArguments().getString(MSGConstants.ARG_TYPE), MSGConstants.ARG_GROUP_CHAT)){
            getGroupChatMessage(getArguments().getString("group_id"));
        }
    }

    public void browseImages(View view){
        Intent photoPickerIntent = new Intent(Intent.ACTION_PICK);
        File pictureDirectory = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES);
        String pictureDirectoryPath = pictureDirectory.getPath();
        Uri data = Uri.parse(pictureDirectoryPath);

        photoPickerIntent.setDataAndType(data, "image/*");

        startActivityForResult(photoPickerIntent, PICK_IMAGE);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        super.onActivityResult(requestCode, resultCode, data);

        if(requestCode == ChatFragment.PICK_IMAGE && resultCode == RESULT_OK){
            Uri imageUri = data.getData();
            
            if (imageUri != null && getContext() != null) {
                Log.e(TAG, "Attempting to send Image");
    
                //Get the path of the selected image to be uploaded.
                Cursor cursor;
                cursor = getContext().getContentResolver().query(imageUri, null, null, null, null);
                if (cursor != null) {
                    cursor.moveToFirst();
                    int index = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
                    final String filePath = cursor.getString(index);
                    cursor.close();
    
                    String fileType = "image/*";
                    File myFile = new File(filePath);
    
                    FutureCallback<JsonObject> callback = new FutureCallback<JsonObject>() {
                        @Override
                        public void onCompleted(Exception e, JsonObject result) {
                            if (e == null && result != null) {
                                try {
                                    Log.e(TAG, "Image Post Result: " + result.toString());
                                    int status = result.get("status").getAsInt();
                                    if (status == ChatActivity.SUCCESS_STATUS) {
                                        Toast.makeText(getContext(), "Image Successfully Posted", Toast.LENGTH_LONG).show();
                                        String imageUrl = NetUtil.MESSAGING_IMAGE_PATH + result.get("filename").getAsString();
                                        Log.d(TAG, imageUrl);
                                        sendImage(imageUrl);
                                    }
                                } catch (Exception exc) {
                                    Toast.makeText(getContext(), "Inner Exception: " + exc.getMessage(), Toast.LENGTH_LONG).show();
                                }
                            } else if (e != null) {
                                Toast.makeText(getContext(), filePath + "\nOuter Exception: " + e.getMessage(), Toast.LENGTH_LONG).show();
                            }
                        }
                    };
                    uploadImage(myFile, fileType, callback);
                }
            }
            //getTest();

        }
    }

    private void uploadImage(File imageFile, String fileType, FutureCallback<JsonObject> callback){
        try{
            if (getContext() != null && imageFile != null) {
                Log.d(TAG, "Attempting to compress image");
                imageFile = new Compressor(getContext()).setQuality(COMPRESS_QUALITY).compressToFile(imageFile);
            }
        }catch (Exception e){ Log.e(TAG, "Unable to compress images"); e.printStackTrace(); }
        
        Ion.with(this)
                .load(NetUtil.API_URL + "add/messagingimage")
                .setHeader(NetUtil.API_KEY_PARAM, NetUtil.API_KEY)
                .setMultipartFile("file", fileType, imageFile)
                .setMultipartParameter("userid", String.valueOf(PrefsUtil.getUserId(this.getContext())))
                .asJsonObject()
                .setCallback(callback);
    }

    
    private void sendMessage(){
        String message = mETxtMessage.getText().toString();
        if(!NetUtil.isOnline(getContext())){
            Log.d(TAG,"No internet connection when sending message");
            Toast.makeText(getActivity(), "No internet connection please try again later.", Toast.LENGTH_LONG).show();
        }
        else if (message.length() > 1) {
            String receiver = getArguments().getString(MSGConstants.ARG_RECEIVER);
            String receiverUid = getArguments().getString(MSGConstants.ARG_RECEIVER_UID);
            String sender = PrefsUtil.getFirstName(getContext()) + " " + PrefsUtil.getLastName(getContext());
            String senderUid = FirebaseAuth.getInstance().getCurrentUser().getUid();
            String receiverFirebaseToken = getArguments().getString(MSGConstants.ARG_FIREBASE_TOKEN);
            ChatMessage chatMessage = new ChatMessage(sender, receiver, senderUid, receiverUid, message, System.currentTimeMillis());
            mChatPresenter
                    .sendMessage(getActivity().getApplicationContext(),
                    chatMessage,
                    receiverFirebaseToken);
        }
    }

    private void sendImage(String imageUrl){
        String receiver = getArguments().getString(MSGConstants.ARG_RECEIVER);
        String receiverUid = getArguments().getString(MSGConstants.ARG_RECEIVER_UID);
        //String sender = FirebaseAuth.getInstance().getCurrentUser().getEmail();
        String sender = PrefsUtil.getFirstName(getContext()) + " " + PrefsUtil.getLastName(getContext());
        String senderUid = FirebaseAuth.getInstance().getCurrentUser().getUid();
        String receiverFirebaseToken = getArguments().getString(MSGConstants.ARG_FIREBASE_TOKEN);

        ChatImage chatImage = new ChatImage(sender, receiver, senderUid, receiverUid, "Image", System.currentTimeMillis(), imageUrl);
        mChatPresenter.sendImageToFirebase(getActivity().getApplicationContext(), chatImage, receiverFirebaseToken);
    }

    private void sendGroupMessage(){
        String username = PrefsUtil.getFirstName(getContext()) + PrefsUtil.getLastName(getContext());
        String message = mETxtMessage.getText().toString();
        if (message.length() > 1) {
            String senderUid = FirebaseAuth.getInstance().getCurrentUser().getUid();
            GroupChatMessage chatMessage = new GroupChatMessage(senderUid, username, message, System.currentTimeMillis());
            sendGroupChatMessage(chatMessage);
        }
    }

    private void sendGroupChatMessage(final GroupChatMessage groupChatMessage){

        final String groupID = getArguments().getString("group_id");

        final DatabaseReference databaseReference = App.getFBDatabase().getReference();

        databaseReference.child(MSGConstants.ARG_CHAT_ROOMS).getRef().addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                //If the chat already exists under chat rooms, we just add the new message to the chat room.
                if(dataSnapshot.hasChild(groupID)){
                    databaseReference
                            .child(MSGConstants.ARG_CHAT_ROOMS)
                            .child(groupID)
                            .child(String.valueOf(groupChatMessage.getTimestamp()))
                            .setValue(groupChatMessage);
                }
                else{
                    databaseReference
                            .child(MSGConstants.ARG_CHAT_ROOMS)
                            .child(groupID)
                            .child(String.valueOf(groupChatMessage.getTimestamp()))
                            .setValue(groupChatMessage);
                    getGroupChatMessage(groupID);
                }
                onSendMessageSuccess();
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                onSendMessageFailure("Unable to Send Message to Group");
            }
        });
    }


    private void getGroupChatMessage(String groupID){
        final DatabaseReference databaseReference = App.getFBDatabase().getReference();

        databaseReference.child(MSGConstants.ARG_CHAT_ROOMS).child(groupID).addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                GroupChatMessage groupChatMessage = dataSnapshot.getValue(GroupChatMessage.class);
                //ChatMessage chatMessage = dataSnapshot.getValue(ChatMessage.class);
                if (myAdapter == null) {
                    myAdapter = new GroupChatRecyclerAdapter();
                    mRecyclerViewChat.setAdapter(myAdapter);
                }
                GroupChatRecyclerAdapter myGroupChatRecyclerAdapter = (GroupChatRecyclerAdapter)myAdapter;
                myGroupChatRecyclerAdapter.add(groupChatMessage);
                mRecyclerViewChat.smoothScrollToPosition(myGroupChatRecyclerAdapter.getItemCount() - 1);
            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) { }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {  }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) { }

            @Override
            public void onCancelled(DatabaseError databaseError) { }
        });
    }

    @Override
    public void onSendMessageSuccess() {
        mETxtMessage.setText("");
    }

    @Override
    public void onSendMessageFailure(String message) {
        Toast.makeText(getActivity(), message, Toast.LENGTH_LONG).show();
    }

    @Override
    public void onGetMessagesSuccess(ChatMessage chatMessage) {
        if (myAdapter == null) {
            myAdapter = new ChatRecyclerAdapter(new ArrayList<ChatMessage>());
            mRecyclerViewChat.setAdapter(myAdapter);
        }
        ChatRecyclerAdapter myChatRecyclerAdapter = (ChatRecyclerAdapter)myAdapter;
        myChatRecyclerAdapter.add(chatMessage);
        mRecyclerViewChat.smoothScrollToPosition(myChatRecyclerAdapter.getItemCount() - 1);
    }

    @Override
    public void onGetMessagesFailure(String message) {
        Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();
    }

    @Subscribe
    public void onPushNotificationEvent(PushNotificationEvent pushNotificationEvent){
        if (myAdapter == null || myAdapter.getItemCount() == 0) {
            mChatPresenter.getMessage(FirebaseAuth.getInstance().getCurrentUser().getUid(),
                    pushNotificationEvent.getUid());
        }
    }

}
