package com.cirp.mfisheries.messaging;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

import com.cirp.mfisheries.App;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.ValueEventListener;

/**
 * Created by Gerard Rique on 7/13/17.
 */

public class ChatInteractor implements ChatContract.Interactor {
    private static final String TAG = "ChatInteractor";

    private ChatContract.OnSendMessageListener mOnSendMessageListener;
    private ChatContract.OnGetMessagesListener mOnGetMessagesListener;

    public ChatInteractor(ChatContract.OnSendMessageListener onSendMessageListener) {
        this.mOnSendMessageListener = onSendMessageListener;
    }

    public ChatInteractor(ChatContract.OnGetMessagesListener onGetMessagesListener) {
        this.mOnGetMessagesListener = onGetMessagesListener;
    }

    public ChatInteractor(ChatContract.OnSendMessageListener onSendMessageListener,
                          ChatContract.OnGetMessagesListener onGetMessagesListener) {
        this.mOnSendMessageListener = onSendMessageListener;
        this.mOnGetMessagesListener = onGetMessagesListener;
    }

    public void sendImageToFirebaseUser(final Context context, final ChatImage chatImage, final String recieverFirebaseToken){
        final String room_type_1 = chatImage.senderUid + "_" + chatImage.receiverUid;
        final String room_type_2 = chatImage.receiverUid + "_" + chatImage.senderUid;

        final DatabaseReference databaseReference = App.getFBDatabase().getReference();

        databaseReference.child(MSGConstants.ARG_CHAT_ROOMS).getRef().addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if(dataSnapshot.hasChild(room_type_1)){
                    databaseReference.child(MSGConstants.ARG_CHAT_ROOMS).child(room_type_1).child(String.valueOf(chatImage.timestamp)).setValue(chatImage);
                    addToFirebaseChatListing(room_type_1, chatImage.senderUid, chatImage.receiverUid);
                }
                else if(dataSnapshot.hasChild(room_type_2)){
                    databaseReference.child(MSGConstants.ARG_CHAT_ROOMS).child(room_type_2).child(String.valueOf(chatImage.timestamp)).setValue(chatImage);
                    addToFirebaseChatListing(room_type_2, chatImage.senderUid, chatImage.receiverUid);
                }
                else{
                    databaseReference.child(MSGConstants.ARG_CHAT_ROOMS).child(room_type_1).child(String.valueOf(chatImage.timestamp)).setValue(chatImage);
                    getMessageFromFirebaseUser(chatImage.senderUid, chatImage.receiverUid);
                    addToFirebaseChatListing(room_type_1, chatImage.senderUid, chatImage.receiverUid);
                }
                mOnSendMessageListener.onSendMessageSuccess();
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    @Override
    public void sendMessageToFirebaseUser(final Context context, final ChatMessage chatMessage, final String receiverFirebaseToken) {
        final String room_type_1 = chatMessage.senderUid + "_" + chatMessage.receiverUid;
        final String room_type_2 = chatMessage.receiverUid + "_" + chatMessage.senderUid;

        final DatabaseReference databaseReference = App.getFBDatabase().getReference();

        databaseReference.child(MSGConstants.ARG_CHAT_ROOMS).getRef().addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if (dataSnapshot.hasChild(room_type_1)) {
                    Log.e(TAG, "sendMessageToFirebaseUser: " + room_type_1 + " exists");
                    databaseReference.child(MSGConstants.ARG_CHAT_ROOMS).child(room_type_1).child(String.valueOf(chatMessage.timestamp)).setValue(chatMessage);
                    addToFirebaseChatListing(room_type_1, chatMessage.senderUid, chatMessage.receiverUid);
                } else if (dataSnapshot.hasChild(room_type_2)) {
                    Log.e(TAG, "sendMessageToFirebaseUser: " + room_type_2 + " exists");
                    databaseReference.child(MSGConstants.ARG_CHAT_ROOMS).child(room_type_2).child(String.valueOf(chatMessage.timestamp)).setValue(chatMessage);
                    addToFirebaseChatListing(room_type_2, chatMessage.senderUid, chatMessage.receiverUid);
                } else {
                    Log.e(TAG, "sendMessageToFirebaseUser: success");
                    databaseReference.child(MSGConstants.ARG_CHAT_ROOMS).child(room_type_1).child(String.valueOf(chatMessage.timestamp)).setValue(chatMessage);
                    getMessageFromFirebaseUser(chatMessage.senderUid, chatMessage.receiverUid);
                    addToFirebaseChatListing(room_type_1, chatMessage.senderUid, chatMessage.receiverUid);
                }
                mOnSendMessageListener.onSendMessageSuccess();
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                mOnSendMessageListener.onSendMessageFailure("Unable to send message: " + databaseError.getMessage());
            }
        });
    }

    @Override
    public void getMessageFromFirebaseUser(String senderUid, String receiverUid) {
        final String room_type_1 = senderUid + "_" + receiverUid;
        final String room_type_2 = receiverUid + "_" + senderUid;

        final DatabaseReference databaseReference = App.getFBDatabase().getReference();

        databaseReference.child(MSGConstants.ARG_CHAT_ROOMS).getRef().addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if (dataSnapshot.hasChild(room_type_1)) {
                    Log.e(TAG, "getMessageFromFirebaseUser: " + room_type_1 + " exists");
                    App.getFBDatabase()
                            .getReference()
                            .child(MSGConstants.ARG_CHAT_ROOMS)
                            .child(room_type_1).addChildEventListener(new ChildEventListener() {
                        @Override
                        public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                            if(dataSnapshot.hasChild("imageURL")){
                                ChatImage chatImage = dataSnapshot.getValue(ChatImage.class);
                                mOnGetMessagesListener.onGetMessagesSuccess(chatImage);
                            }
                            else{
                                ChatMessage chatMessage = dataSnapshot.getValue(ChatMessage.class);
                                mOnGetMessagesListener.onGetMessagesSuccess(chatMessage);
                            }
                        }

                        @Override
                        public void onChildChanged(DataSnapshot dataSnapshot, String s) {

                        }

                        @Override
                        public void onChildRemoved(DataSnapshot dataSnapshot) {

                        }

                        @Override
                        public void onChildMoved(DataSnapshot dataSnapshot, String s) {

                        }

                        @Override
                        public void onCancelled(DatabaseError databaseError) {
                            mOnGetMessagesListener.onGetMessagesFailure("Unable to get message: " + databaseError.getMessage());
                        }
                    });
                } else if (dataSnapshot.hasChild(room_type_2)) {
                    Log.e(TAG, "getMessageFromFirebaseUser: " + room_type_2 + " exists");
                    App.getFBDatabase()
                            .getReference()
                            .child(MSGConstants.ARG_CHAT_ROOMS)
                            .child(room_type_2).addChildEventListener(new ChildEventListener() {
                        @Override
                        public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                            if(dataSnapshot.hasChild("imageURL")){
                                ChatImage chatImage = dataSnapshot.getValue(ChatImage.class);
                                mOnGetMessagesListener.onGetMessagesSuccess(chatImage);
                            }
                            else{
                                ChatMessage chatMessage = dataSnapshot.getValue(ChatMessage.class);
                                mOnGetMessagesListener.onGetMessagesSuccess(chatMessage);
                            }
                        }

                        @Override
                        public void onChildChanged(DataSnapshot dataSnapshot, String s) {

                        }

                        @Override
                        public void onChildRemoved(DataSnapshot dataSnapshot) {

                        }

                        @Override
                        public void onChildMoved(DataSnapshot dataSnapshot, String s) {

                        }

                        @Override
                        public void onCancelled(DatabaseError databaseError) {
                            mOnGetMessagesListener.onGetMessagesFailure("Unable to get message: " + databaseError.getMessage());
                        }
                    });
                } else {
                    Log.e(TAG, "getMessageFromFirebaseUser: no such room available");
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                mOnGetMessagesListener.onGetMessagesFailure("Unable to get message: " + databaseError.getMessage());
            }
        });
    }

    private void addToFirebaseChatListing(final String roomType, final String senderUid, final String receiverUid){
        Log.e(TAG, "Creating active chat record on Firebase");
        final DatabaseReference databaseReference = App.getFBDatabase().getReference();

        databaseReference.child("messaging").child("active_chats").child(senderUid).child(receiverUid).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if(!dataSnapshot.exists()){
                    databaseReference.child("messaging").child("active_chats").child(senderUid).child(receiverUid).setValue(roomType);
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
        databaseReference.child("messaging").child("active_chats").child(receiverUid).child(senderUid).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if(!dataSnapshot.exists()){
                    databaseReference.child("messaging").child("active_chats").child(receiverUid).child(senderUid).setValue(roomType);
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
        //databaseReference.child("messaging").child("active_chats").child(senderUid).child(receiverUid).setValue(roomType);
        //databaseReference.child("messaging").child("active_chats").child(receiverUid).child(senderUid).setValue(roomType);
    }

    private void addToLocalChatListing(String roomType, String senderUid, String receiverUid, Context context){
        Log.e(TAG, "Creating active chat record in shared preferences");
        SharedPreferences.Editor myEditor;
        SharedPreferences sharedPreferences = context.getSharedPreferences("MessagingPreferences", Context.MODE_PRIVATE);
        myEditor = sharedPreferences.edit();
        myEditor.putString(receiverUid, roomType);
        myEditor.apply();
    }
}