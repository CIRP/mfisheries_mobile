package com.cirp.mfisheries.messaging;

import com.google.gson.JsonObject;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

@SuppressWarnings("WeakerAccess")
public class User implements Serializable {
	final public String uid;
	final public String email;
	final public String firstName;
	final public String lastName;
	final public String firebaseToken;
	final public String countryId;
	final public String googleId;
	
	public User() {
		this.uid = "";
		this.email = "";
		this.firstName = "";
		this.lastName = "";
		this.firebaseToken = "";
		this.countryId = "";
		this.googleId = "";
	}
	
	public User(String uid, String email, String firstName, String lastName, String firebaseToken, String countryId) {
		this(uid, email, firstName, lastName, firebaseToken, countryId, "");
	}
	
	public User(String uid, String email, String firstName, String lastName, String firebaseToken, String countryId, String googleId) {
		this.uid = uid;
		this.email = email;
		this.firstName = firstName;
		this.lastName = lastName;
		this.firebaseToken = firebaseToken;
		this.countryId = countryId;
		this.googleId = googleId;
	}
	
	public String getUid() {
		return uid;
	}
	
	public String getEmail() {
		return email;
	}
	
	public String getFirstName() {
		return firstName;
	}
	
	public String getLastName() {
		return lastName;
	}
	
	public String getFirebaseToken() {
		return firebaseToken;
	}
	
	public String getCountryId() {
		return countryId;
	}
	
	public String getGoogleId() {
		return googleId;
	}
	
	public Map<String, Object> toMap() {
		HashMap<String, Object> result = new HashMap<>();
		result.put("uid", uid);
		result.put("email", email);
		result.put("firstName", firstName);
		result.put("lastName", lastName);
		result.put("firebaseToken", firebaseToken);
		result.put("countryId", countryId);
		result.put("googleId", googleId);
		return result;
	}
	
	public JsonObject toJson() {
		JsonObject result = new JsonObject();
		result.addProperty("uid", uid);
		result.addProperty("email", email);
		result.addProperty("fname", firstName);
		result.addProperty("lname", lastName);
		result.addProperty("firebaseToken", firebaseToken);
		result.addProperty("countryId", countryId);
		result.addProperty("googleId", googleId);
		// Needed for registration
		result.addProperty("username", email);
		result.addProperty("password", googleId);
		result.addProperty("countryid", countryId);
		
		return result;
	}
}
