package com.cirp.mfisheries.messaging;

/**
 * Created by Gerard Rique on 7/23/17.
 */

public class ChatImage extends ChatMessage{

    public String imageURL;

    public ChatImage() {

    }

    public ChatImage(String sender, String reciever, String senderUid, String receiverUid, String message, long timestamp, String imageURL) {
        super(sender, reciever, senderUid, receiverUid, message, timestamp);
        this.imageURL = imageURL;
    }

    public String getImageURL() {
        return imageURL;
    }

    public void setImageURL(String imageURL) {
        this.imageURL = imageURL;
    }
}
