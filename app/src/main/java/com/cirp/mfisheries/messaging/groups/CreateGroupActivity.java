package com.cirp.mfisheries.messaging.groups;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.cirp.mfisheries.App;
import com.cirp.mfisheries.R;
import com.cirp.mfisheries.core.module.ModuleActivity;
import com.cirp.mfisheries.messaging.MessagingActivity;
import com.cirp.mfisheries.messaging.User;
import com.cirp.mfisheries.messaging.UserListingRecyclerAdapter;
import com.cirp.mfisheries.util.PrefsUtil;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Gerard Rique on 8/1/17.
 */

public class CreateGroupActivity extends ModuleActivity {

    private static List<User> myGroupMembers;

    private EditText groupNameEditText;
    private FloatingActionButton createGroupFab;
    private RecyclerView memberListingRecyclerView;
    private UserListingRecyclerAdapter myAdapter;
    private TextView numParticipants;

    FirebaseUser firebaseUser = FirebaseAuth.getInstance().getCurrentUser();

    private static String TAG = "CreateGroupActivity";

    Toolbar myToolbar;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        bindViews();
        try {
            Bundle b = new Bundle();
            b = getIntent().getExtras();
            myGroupMembers = (List<User>) b.getSerializable("users");
            init();
        }catch (Exception e){
            e.printStackTrace();
            Toast.makeText(this, "Cannot make a group. Please try again", Toast.LENGTH_LONG).show();
            finish();
        }
    }
    
    @Override
    public int getLayoutResourceId() {
        return R.layout.activity_create_group;
    }
    
    private void bindViews(){
        myToolbar = findViewById(R.id.create_group_toolbar);
        groupNameEditText = findViewById(R.id.group_name_edit_text);
        createGroupFab = findViewById(R.id.upload_group_data);
        memberListingRecyclerView = findViewById(R.id.create_group_members_listing);
        numParticipants = findViewById(R.id.create_group_number_participants);
    }

    private void createGroupOnFirebase(){

        String currentUserName = PrefsUtil.getFirstName(this) + " " + PrefsUtil.getLastName(this);
        Toast.makeText(this, "Creating Group with " + myGroupMembers.size() + " members", Toast.LENGTH_LONG).show();
        String myID = firebaseUser.getUid();
        String groupName = groupNameEditText.getText().toString();

        DatabaseReference myReference = App.getFBDatabase().getReference().child("messaging");
        String newGroupID = myReference.push().getKey();

        Map<String, Object> groupData = new HashMap<>();
        myReference.child("active_chats").child(myID).child(newGroupID).setValue(newGroupID);

        groupData.put("/groups/" + newGroupID + "/group_name/", groupName);

        //Loop through all group members and add their names to the group data map sp that all group members can be listed on the firebase server.

        for(User currentUser: myGroupMembers){
            myReference.child("active_chats").child(currentUser.uid).child(newGroupID).setValue(newGroupID);
            String username = currentUser.firstName + " " + currentUser.lastName;
            groupData.put("/groups/" + newGroupID + "/group_members/" + currentUser.uid, username);
        }

        //Add the current user(the user creating the group) to the list of members
        groupData.put("/groups/" + newGroupID + "/group_members/" + firebaseUser.getUid(), currentUserName);

        Task<Void> res = myReference.updateChildren(groupData);
        res.addOnSuccessListener(new OnSuccessListener<Void>() {
            @Override
            public void onSuccess(Void aVoid) {
                Toast.makeText(CreateGroupActivity.this, "Successfully added Group data", Toast.LENGTH_LONG).show();
                myGroupMembers = new ArrayList<>();
                MessagingActivity.startActivity(CreateGroupActivity.this);
            }
        });
    }

    private void init(){
        if((myGroupMembers != null) && (myGroupMembers.size() >= 2)){
            try {
                String numParticipantsString = myGroupMembers.size() + " participants";
                Log.d(TAG, numParticipantsString);
                numParticipants.setText(numParticipantsString);
                myAdapter = new UserListingRecyclerAdapter(myGroupMembers);
                memberListingRecyclerView.setAdapter(myAdapter);
                setSupportActionBar(myToolbar);
                createGroupFab.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        createGroupOnFirebase();
                    }
                });
            }catch (Exception e){
                e.printStackTrace();
                finish();
            }
        }else{
            Toast.makeText(this, "Cannot make a group. Please try again", Toast.LENGTH_LONG).show();
            finish();
        }

    }
}
