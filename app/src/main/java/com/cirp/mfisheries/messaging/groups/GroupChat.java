package com.cirp.mfisheries.messaging.groups;

import com.cirp.mfisheries.messaging.User;

import java.util.HashMap;

/**
 * Created by Gerard Rique on 8/3/17.
 */

public class GroupChat {

    private String groupName;
    private String groupID;
    private HashMap<String, User> users;

    public GroupChat() {
    }

    public GroupChat(String groupName, String groupID) {
        this.groupName = groupName;
        this.groupID = groupID;
        users = new HashMap<>();
    }

    public String getGroupName() {
        return groupName;
    }

    public String getGroupID() {
        return groupID;
    }

    public User getUser(String uid){
        return users.get(uid);
    }
    public void adduser(User myUser){
        users.put(myUser.uid, myUser);
    }
}
