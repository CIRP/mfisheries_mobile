package com.cirp.mfisheries.messaging;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.SearchView;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.cirp.mfisheries.R;
import com.cirp.mfisheries.core.Country;
import com.cirp.mfisheries.core.CountryAdapter;
import com.cirp.mfisheries.core.module.ModuleActivity;
import com.cirp.mfisheries.messaging.groups.CreateGroupActivity;
import com.cirp.mfisheries.util.CountryUtil;
import com.cirp.mfisheries.util.PrefsUtil;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class UserListingActivity extends ModuleActivity implements SearchView.OnQueryTextListener{
	
	private static String type = UsersFragment.TYPE_ALL;
	private UsersFragment usersFragment;
    private FloatingActionButton newGroupFab;
    private String countryid;
    private String countryName;
    private TextView cNameView;
    
    public static void startActivity(Context context) {
        Intent intent = new Intent(context, UserListingActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        context.startActivity(intent);
    }

    public static void startActivity(Context context, int flags) {
        Intent intent = new Intent(context, UserListingActivity.class);
        intent.setFlags(flags);
        context.startActivity(intent);
    }

    public static void startActivity(Context context, String type){
        Intent intent = new Intent(context, UserListingActivity.class);
        intent.putExtra("type", type);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        context.startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        bindViews();
        init();
    }
    
    @Override
    public int getLayoutResourceId() {
        return R.layout.activity_user_listing;
    }
    
    private void bindViews() {
        newGroupFab = findViewById(R.id.new_group_fab);
        newGroupFab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getMoreGroupData();
            }
        });
    
        countryid = PrefsUtil.getCountryId(this);
        countryName = PrefsUtil.getCountry(this);
        cNameView = findViewById(R.id.countryname);
        cNameView.setText(R.string.all_countries);
    }

    //The get more group data will launch the create group activity which will allow the user the enter the name of the group they would like to create.
    private void getMoreGroupData(){
        if (usersFragment.getGroupMembers().size() >= 2) {
            List<User> users = new ArrayList<User>();
            users = usersFragment.getGroupMembers();
            Intent intent = new Intent(UserListingActivity.this, CreateGroupActivity.class);
            intent.putExtra("users", (Serializable) users);
            startActivity(intent);
//            CreateGroupActivity.startActivity(UserListingActivity.this, usersFragment.getGroupMembers());
        }else{
            (new AlertDialog.Builder(this))
                    .setTitle("Create Group")
                    .setMessage("You must select at least two members to create a group")
                    .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            finish();
                        }
                    })
                    .setNeutralButton("Continue", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            dialogInterface.dismiss();
                        }
                    })
                    .create()
                    .show();
        }
    }

    private void init() {
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        if(getIntent().hasExtra("type")){
            String type = "";
            if (getIntent().getExtras() != null) {
                type = getIntent().getExtras().getString("type");
                usersFragment = UsersFragment.newInstance(type);
            }
        }
        else{
            newGroupFab.setVisibility(View.GONE);
            usersFragment = UsersFragment.newInstance(UsersFragment.TYPE_ALL);
        }
        transaction.replace(R.id.users_container, usersFragment);
        transaction.commit();
    }

    @Override
    public boolean onCreateOptionsMenu(final Menu menu) {
        getMenuInflater().inflate(R.menu.messaging_menu, menu);
        MenuItem menuItem = menu.findItem(R.id.action_search);
        SearchView searchView = (SearchView) menuItem.getActionView();
        searchView.setOnQueryTextListener(this);
        return true;
    }
    
    public void chooseCountry(View view){
        final Country allCountry = new Country();
        allCountry.setName("All Countries");
        CountryUtil.getInstance(this).retrieveCountries(new CountryUtil.CountryRetrievedListener() {
            @Override
            public void processCountries(List<Country> countries) {
                if (countries != null){
                    countries.add(0, allCountry);
                    displayCountrySelection(countries);
                }else{
                    Toast.makeText(UserListingActivity.this, "Unable to retrieve countries", Toast.LENGTH_SHORT).show();
                }
            }
        });
        
    }
    
    private void displayCountrySelection(final List<Country> countries){
        final CountryAdapter arrayAdapter = new CountryAdapter(this, countries);
        
        new android.support.v7.app.AlertDialog.Builder(this)
                .setTitle(getResources().getString(R.string.dialog_country))
                .setCancelable(false)
                .setAdapter(arrayAdapter,
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                Country selectedCountry = countries.get(which);
                                countryid = selectedCountry.getId();
                                countryName = selectedCountry.getName();
                                cNameView.setText(countryName);
                                usersFragment.filterByCountry(countryName, countries);
                            }
                        })
                .show();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        return super.onOptionsItemSelected(item);
    }


    @Override
    public boolean onQueryTextSubmit(String query) {
        return false;
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        usersFragment.filterUsers(newText);
        return true;
    }
    
    @Override
    public int getColor(){
        return R.color.yellow;
    }
    
    @Override
    public int getColorDark(){
        return R.color.yellowDark;
    }
}
