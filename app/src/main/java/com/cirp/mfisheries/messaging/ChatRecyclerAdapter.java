package com.cirp.mfisheries.messaging;

import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.cirp.mfisheries.R;
import com.google.firebase.auth.FirebaseAuth;
import com.squareup.picasso.Picasso;

import java.util.List;


public class ChatRecyclerAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private static final int VIEW_TYPE_ME = 1;
    private static final int VIEW_TYPE_OTHER = 2;

    private List<ChatMessage> mChatMessages;

    ChatRecyclerAdapter(List<ChatMessage> chatMessages) {
        mChatMessages = chatMessages;
    }

    public void add(ChatMessage chatMessage) {
        mChatMessages.add(chatMessage);
        notifyItemInserted(mChatMessages.size() - 1);
    }

    @Override
    public long getItemId(int position) {
        return mChatMessages.get(position).timestamp;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        RecyclerView.ViewHolder viewHolder = null;
        switch (viewType) {
            case VIEW_TYPE_ME:
                View viewChatMine = layoutInflater.inflate(R.layout.item_chat_mine, parent, false);
                viewHolder = new MyChatViewHolder(viewChatMine);
                break;
            case VIEW_TYPE_OTHER:
                View viewChatOther = layoutInflater.inflate(R.layout.item_chat_theirs, parent, false);
                viewHolder = new OtherChatViewHolder(viewChatOther);
                break;
        }
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if (TextUtils.equals(mChatMessages.get(position).senderUid, FirebaseAuth.getInstance().getCurrentUser().getUid())) {
            configureMyChatViewHolder((MyChatViewHolder) holder, position);
        } else {
            configureOtherChatViewHolder((OtherChatViewHolder) holder, position);
        }
    }

    private void configureMyChatViewHolder(MyChatViewHolder myChatViewHolder, int position) {
        Object chatMessage = mChatMessages.get(position);

        if(chatMessage instanceof ChatImage){
            ChatImage myImage = (ChatImage)chatMessage;
            if (myChatViewHolder.myImageView != null) {
                Picasso.with(myChatViewHolder.myImageView.getContext()).load(myImage.getImageURL()).fit().into(myChatViewHolder.myImageView);
                myChatViewHolder.myImageView.setVisibility(View.VISIBLE);
                myChatViewHolder.txtChatMessage.setVisibility(View.GONE);
            }

        }
        else{
            ChatMessage myMessage = (ChatMessage)chatMessage;
            String alphabet = myMessage.sender.substring(0, 1);

            myChatViewHolder.txtChatMessage.setText(myMessage.message);
            myChatViewHolder.txtUserAlphabet.setText(alphabet);
        }
    }

    private void configureOtherChatViewHolder(OtherChatViewHolder otherChatViewHolder, int position) {
        Object chatMessage = mChatMessages.get(position);

        if(chatMessage instanceof ChatImage){
            ChatImage myImage = (ChatImage)chatMessage;
            if (otherChatViewHolder.myImageView != null) {
                Picasso.with(otherChatViewHolder.myImageView.getContext()).load(myImage.getImageURL()).fit().into(otherChatViewHolder.myImageView);
                otherChatViewHolder.myImageView.setVisibility(View.VISIBLE);
                otherChatViewHolder.txtChatMessage.setVisibility(View.GONE);
            }
        }
        else{
            ChatMessage myMessage = (ChatMessage)chatMessage;
            String alphabet = myMessage.sender.substring(0, 1);

            otherChatViewHolder.txtChatMessage.setText(myMessage.message);
            otherChatViewHolder.txtUserAlphabet.setText(alphabet);
        }
    }

    @Override
    public int getItemCount() {
        if (mChatMessages != null) {
            return mChatMessages.size();
        }
        return 0;
    }

    @Override
    public int getItemViewType(int position) {
        if (FirebaseAuth.getInstance().getCurrentUser() != null) {
            if (TextUtils.equals(mChatMessages.get(position).senderUid,
                    FirebaseAuth.getInstance().getCurrentUser().getUid())) {
                return VIEW_TYPE_ME;
            } else {
                return VIEW_TYPE_OTHER;
            }
        }
        return VIEW_TYPE_OTHER;
    }

    private static class MyChatViewHolder extends RecyclerView.ViewHolder {
        private TextView txtChatMessage, txtUserAlphabet;
        private ImageView myImageView;

        MyChatViewHolder(View itemView) {
            super(itemView);
            txtChatMessage = itemView.findViewById(R.id.text_view_chat_mine);
            txtUserAlphabet = itemView.findViewById(R.id.text_view_user_list_item);
            myImageView = itemView.findViewById(R.id.sent_image);

            txtChatMessage.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View view) {
                    return false;
                }
            });
        }
    }

    private static class OtherChatViewHolder extends RecyclerView.ViewHolder {
        private TextView txtChatMessage, txtUserAlphabet;
        private ImageView myImageView;

        OtherChatViewHolder(View itemView) {
            super(itemView);
            txtChatMessage = itemView.findViewById(R.id.text_view_chat_mine);
            txtUserAlphabet = itemView.findViewById(R.id.text_view_user_list_item);
            myImageView = itemView.findViewById(R.id.received_image);
        }
    }
}
