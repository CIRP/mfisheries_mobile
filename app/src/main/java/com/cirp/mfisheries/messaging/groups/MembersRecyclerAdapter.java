package com.cirp.mfisheries.messaging.groups;

import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.cirp.mfisheries.R;
import com.cirp.mfisheries.messaging.User;
import com.cirp.mfisheries.util.CountryUtil;

import java.util.ArrayList;
import java.util.List;

public class MembersRecyclerAdapter extends RecyclerView.Adapter<MembersRecyclerAdapter.ViewHolder> {

    private List<User> myUsers;
    private List<User> groupMembers;

    private static String TAG = "MembersRecyclerAdapter";
    private CountryUtil countryUtil;
    
    public MembersRecyclerAdapter(List<User> myUsers) {

        this.myUsers = myUsers;
        this.groupMembers = new ArrayList<>();
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.select_group_member_listing, parent, false);
        countryUtil = CountryUtil.getInstance(parent.getContext());
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        final User user = myUsers.get(position);
        
        if(user != null){
            final String name = user.firstName + " " + user.lastName;
            String letter = user.email.substring(0, 1);
            holder.username.setText(name);
            if(user.countryId != null){
                if (countryUtil != null)
                    holder.flagIcon.setImageResource(countryUtil.getCountryFlagResourceID(user.countryId));
                else Log.d(TAG, "CountryUtil was null");
            }else{
                holder.iconLetter.setText(letter);
                holder.iconLetter.setVisibility(View.VISIBLE);
                holder.flagIcon.setVisibility(View.GONE);
    
                // Ensure that we change what the username is pointed relative-to
                final RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) holder.username.getLayoutParams();
                params.addRule(RelativeLayout.END_OF, R.id.select_group_member_icon);
                params.addRule(RelativeLayout.ALIGN_BASELINE, R.id.select_group_member_icon);
                params.addRule(RelativeLayout.ALIGN_BOTTOM, R.id.select_group_member_icon);
                
                holder.username.setLayoutParams(params);
                
            }
            holder.checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                    updateGroupList(user);
                }
            });

        }
    }

    public void updateGroupList(User user){
        String name = user.firstName + " " + user.lastName;
        if(groupMembers.contains(user)){
            Log.e(TAG, name + " has been removed from the group");
            groupMembers.remove(user);
        }
        else{
            Log.e(TAG, name + " has been added to the group");
            groupMembers.add(user);
        }
    }

    public void updateGroupList(int position){
        User user = myUsers.get(position);
        updateGroupList(user);
    }

    public User getUser(int position) {
        return myUsers.get(position);
    }

    @Override
    public int getItemCount() {
        if(myUsers != null)
            return myUsers.size();
        return 0;
    }

    public void setFilter(List<User> users){
        myUsers = new ArrayList<>();
        myUsers.addAll(users);
        notifyDataSetChanged();
    }

    public List<User> getGroupMembers() {
        return groupMembers;
    }

    static class ViewHolder extends RecyclerView.ViewHolder{
        private TextView username, iconLetter;
        private CheckBox checkBox;
        private ImageView flagIcon;
        public ViewHolder(View itemView) {
            super(itemView);
            username = itemView.findViewById(R.id.select_group_member_username);
            iconLetter = itemView.findViewById(R.id.select_group_member_icon);
            checkBox = itemView.findViewById(R.id.select_group_member_check);
            flagIcon = itemView.findViewById(R.id.select_group_country_icon);
        }
    }
}
