package com.cirp.mfisheries.messaging;

import android.content.Context;

public class ChatPresenter implements ChatContract.Presenter, ChatContract.OnSendMessageListener,
        ChatContract.OnGetMessagesListener {
    private ChatContract.View mView;
    private ChatInteractor mChatInteractor;

    public ChatPresenter(ChatContract.View view) {
        this.mView = view;
        mChatInteractor = new ChatInteractor(this, this);
    }

    @Override
    public void sendMessage(Context context, ChatMessage chatMessage, String receiverFirebaseToken) {
        mChatInteractor.sendMessageToFirebaseUser(context, chatMessage, receiverFirebaseToken);
    }

    public void sendImageToFirebase(Context context, ChatImage chatImage, String recieverFirebaseToken){
        mChatInteractor.sendImageToFirebaseUser(context, chatImage, recieverFirebaseToken);
    }

    @Override
    public void getMessage(String senderUid, String receiverUid) {
        mChatInteractor.getMessageFromFirebaseUser(senderUid, receiverUid);
    }

    @Override
    public void onSendMessageSuccess() {
        mView.onSendMessageSuccess();
    }

    @Override
    public void onSendMessageFailure(String message) {
        mView.onSendMessageFailure(message);
    }

    @Override
    public void onGetMessagesSuccess(ChatMessage chatMessage) {
        mView.onGetMessagesSuccess(chatMessage);
    }

    @Override
    public void onGetMessagesFailure(String message) {
        mView.onGetMessagesFailure(message);
    }
}