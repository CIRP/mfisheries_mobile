package com.cirp.mfisheries.messaging;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import com.cirp.mfisheries.R;
import com.cirp.mfisheries.core.module.ModuleActivity;
import com.cirp.mfisheries.util.CrashReporter;

public class ChatActivity extends ModuleActivity{

    private Toolbar mToolbar;

    public static final int SUCCESS_STATUS = 200;

    private Uri imageUri;

    private static String TAG = "ChatActivity";

    public static void startActivity(Context context, String receiverUid, String receiver, String firebaseToken){
        Intent intent = new Intent(context, ChatActivity.class);
        intent.putExtra(MSGConstants.ARG_TYPE, MSGConstants.ARG_ONE_TO_ONE);
        intent.putExtra(MSGConstants.ARG_RECEIVER, receiver);
        intent.putExtra(MSGConstants.ARG_RECEIVER_UID, receiverUid);
        intent.putExtra(MSGConstants.ARG_FIREBASE_TOKEN, firebaseToken);
        context.startActivity(intent);
    }

    public static void startActivity(Context context, String groupID, String groupName){
        Intent intent = new Intent(context, ChatActivity.class);
        intent.putExtra(MSGConstants.ARG_TYPE, MSGConstants.ARG_GROUP_CHAT);
        intent.putExtra(MSGConstants.ARG_GROUP_ID, groupID);
        intent.putExtra(MSGConstants.ARG_GROUP_NAME, groupName);
        context.startActivity(intent);
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        bindViews();
        init();
    }
    
    @Override
    public int getLayoutResourceId() {
        return R.layout.activity_chat;
    }
    
    
    private void bindViews(){
        mToolbar = findViewById(R.id.toolbar);
    }
    
    /**
     * Sets up which fragment should be used based on parameters passed in the intent
     */
    private void init(){

        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        Fragment fragment = null;
        // If Individual Chat
        if(getIntent().getExtras() != null){
            if(TextUtils.equals(getIntent().getExtras().getString(MSGConstants.ARG_TYPE), MSGConstants.ARG_ONE_TO_ONE)){
                mToolbar.setTitle(getIntent().getExtras().getString(MSGConstants.ARG_RECEIVER));

                fragment = ChatFragment.newInstance(getIntent().getExtras().getString(MSGConstants.ARG_RECEIVER),
                        getIntent().getExtras().getString(MSGConstants.ARG_RECEIVER_UID),
                        getIntent().getExtras().getString(MSGConstants.ARG_FIREBASE_TOKEN));
            }
            // If Group Chat
            else if(TextUtils.equals(getIntent().getExtras().getString(MSGConstants.ARG_TYPE), MSGConstants.ARG_GROUP_CHAT)){
                mToolbar.setTitle(getIntent().getExtras().getString("group_name"));
                fragment = ChatFragment.newInstance(getIntent().getExtras().getString("group_id"));
            }
            if (fragment != null) {
                fragmentTransaction.replace(R.id.frame_layout_content_chat, fragment, ChatFragment.class.getSimpleName());
                fragmentTransaction.commit();
            }else{
                Toast.makeText(this, "Invalid Request Received. Report Submitted", Toast.LENGTH_SHORT).show();
                CrashReporter.getInstance(getBaseContext()).log(Log.ERROR, TAG, "Invalid option passed to ChatActivity");
            }
        }else{
            Log.d(TAG, "Intent returned as null");
            ChatActivity.this.finish();
        }

    }

    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_group_chat, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.option_view_users) {
            Intent viewUsers = new Intent(this,ViewUsersActivity.class);
            startActivity(viewUsers);
        }
        return super.onOptionsItemSelected(item);
    }
    
    @Override
    public int getColor(){
        return R.color.yellow;
    }
    
    @Override
    public int getColorDark(){
        return R.color.yellowDark;
    }
}
