package com.cirp.mfisheries.tracking;

import android.location.Location;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;

import com.cirp.mfisheries.R;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;

public class TrackingActivity extends AppCompatActivity {

    private GoogleMap googleMap;
    private CameraPosition mCameraPosition;
    private static final String TAG = "TrackingActivity";


    private GoogleApiClient mGoogleApiClient;
    private static final int DEFAULT_ZOOM = 15;
    private static final int PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION = 1;
//    private final Location mCurrentLocation;
    private boolean mLocationPermissionGranted;

    // The geographical location where the device is currently located. That is, the last-known
    // location retrieved by the Fused Location Provider.
    private android.location.Location mLastKnownLocation;

    // Keys for storing activity state.
    private static final String KEY_CAMERA_POSITION = "camera_position";
    private static final String KEY_LOCATION = "location";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tracking);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

//        if (savedInstanceState != null) {
//            mLastKnownLocation = savedInstanceState.getParcelable(KEY_LOCATION);
//            mCameraPosition = savedInstanceState.getParcelable(KEY_CAMERA_POSITION);
//            mCurrentLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
//            if (mRequestingLocationUpdates) {
//                startLocationUpdates();
//            }
//        } else {
//            Log.d(TAG, "Saved Instance was not null");
//        }


        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    public void onMapReady(GoogleMap googleMap){
        this.googleMap = googleMap;
        this.googleMap.getUiSettings().setZoomControlsEnabled(false);
        this.googleMap.getUiSettings().setTiltGesturesEnabled(false);
        this.googleMap.getUiSettings().setCompassEnabled(false);
        this.googleMap.getUiSettings().setRotateGesturesEnabled(false);
//        this.googleMap.setOnMapLongClickListener(this);
//        this.googleMap.setOnInfoWindowClickListener(this);
//        this.googleMap.setInfoWindowAdapter(this);
        LatLng zoom_position = new LatLng(10.934606, -61.178558);
        this.googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(zoom_position, 7));
//        if (mCurrentLocation != null) {
//            updateUserPosition(mCurrentLocation);
//        }
    }

}
