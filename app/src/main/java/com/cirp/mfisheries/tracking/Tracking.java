package com.cirp.mfisheries.tracking;

import android.content.Context;

import com.cirp.mfisheries.R;
import com.cirp.mfisheries.core.module.Module;
import com.cirp.mfisheries.util.NetUtil;
import com.cirp.mfisheries.util.PrefsUtil;

public class Tracking extends Module {

    public Tracking(Context context){super(context);}

    @Override
    protected Module setModuleId() {
        this.moduleId = "Tracking";
        return this;
    }

    @Override
    protected Module setModuleName() {
        this.name = "Tracking";
        return this;
    }

    @Override
    protected Module setIsDisplayed() {
        this.displayed = true;
        return this;
    }

    @Override
    protected Module setImageResource() {
        this.imageResource = R.drawable.icon_tracking;
        return this;
    }

    @Override
    protected Module setNeedsRegistration() {
        this.needsRegistration = false;
        return this;
    }

    @Override
    protected Module setActivityClass() {
        this.activityClass = TrackingActivity.class;
        return this;
    }
}
