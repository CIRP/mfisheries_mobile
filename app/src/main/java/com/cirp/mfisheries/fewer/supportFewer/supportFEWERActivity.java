package com.cirp.mfisheries.fewer.supportFewer;

import android.os.Bundle;
import android.text.method.LinkMovementMethod;
import android.widget.TextView;

import com.cirp.mfisheries.R;
import com.cirp.mfisheries.core.module.ModuleActivity;


public class supportFEWERActivity extends ModuleActivity{

    @Override
    protected  void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        TextView url = findViewById(R.id.urlText);
        url.setMovementMethod(LinkMovementMethod.getInstance());

        TextView M_url = findViewById(R.id.manual_urlText);
        M_url.setMovementMethod(LinkMovementMethod.getInstance());
    }

    @Override
    public int getLayoutResourceId() {
        return R.layout.activity_support_fewer;
    }
}
