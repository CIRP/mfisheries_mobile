package com.cirp.mfisheries.fewer.emergProc;

import android.content.Context;

import com.cirp.mfisheries.R;
import com.cirp.mfisheries.core.module.Module;
import com.cirp.mfisheries.util.NetUtil;
import com.cirp.mfisheries.util.PrefsUtil;

public class EmergencyProcedures extends Module {

    public EmergencyProcedures() {
        super();
    }

    public EmergencyProcedures(Context context) {
        super(context);
    }
    
    @Override
    public Module initialize() {
        if (this.context != null && !PrefsUtil.getPath(this.context).equals("")) // ensure country path is set
            this.dataUrl = NetUtil.SITE_URL + PrefsUtil.getPath(this.context) + "/EmergencyProcedures.zip";
        return super.initialize();
    }
    
    @Override
    protected Module setModuleId() {
        this.moduleId = "EmergencyProcedures";
        return this;
    }

    @Override
    protected Module setModuleName() {
        this.name = "Emergency Procedures";
        return this;
    }

    @Override
    protected Module setIsDisplayed() {
        this.displayed = true;
        return this;
    }

    @Override
    protected Module setImageResource() {
        this.imageResource = R.drawable.icon_emg_proc;
        return this;
    }

    @Override
    protected Module setNeedsRegistration() {
        this.needsRegistration = true;
        return this;
    }

    @Override
    protected Module setActivityClass() {
        this.activityClass = EmergencyProceduresActivity.class;
        return this;
    }
}
