package com.cirp.mfisheries.fewer.dmgReport;

import android.content.Context;
import android.util.Log;
import android.widget.Toast;

import com.cirp.mfisheries.fewer.dmgReport.model.DMCategory;
import com.cirp.mfisheries.fewer.dmgReport.model.Post;
import com.cirp.mfisheries.util.CrashReporter;
import com.cirp.mfisheries.util.NetUtil;
import com.cirp.mfisheries.util.PrefsUtil;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;
import com.squareup.moshi.JsonAdapter;
import com.squareup.moshi.Moshi;
import com.squareup.moshi.Types;

import java.io.IOException;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

@SuppressWarnings("WeakerAccess")
public class DMReportUtil {

    private static final String TAG = "DMReportUtil";
    private static final long MAX_DAYS = 5;
    final Context context;
    private String noInternetMsg;
    String errorMsg;

    public DMReportUtil(Context context){
        this.context = context;
        errorMsg = "Unable to retrieve Damage Reports";
        noInternetMsg = errorMsg + " Phone not connected. Using Cache values.";
    }

    protected void makeRequest(final String url, final PostListener listener){
        if (NetUtil.isOnline(context)) {
            Log.d(TAG, "Retrieving information for reports at:" + url);
            Ion.with(context)
                    .load(url)
                    .setHeader(NetUtil.API_KEY_PARAM, NetUtil.API_KEY)
                    .asJsonArray()
                    .setCallback(new FutureCallback<JsonArray>() {
                        @Override
                        public void onCompleted(Exception e, JsonArray array) {
                            if (array != null) {
                                try {
                                    Moshi moshi = new Moshi.Builder().build();
                                    Type listOfPostType = Types.newParameterizedType(List.class, Post.class);
                                    JsonAdapter<List<Post>> jsonAdapter = moshi.adapter(listOfPostType);
                                    List<Post> posts = new ArrayList<>();
                                    List<Post> json = jsonAdapter.fromJson(array.toString());
                                    if (json != null) {
                                        posts.addAll(json);
                                        cacheReports(posts);
                                        listener.onReceivedPost(posts);
                                    }
                                } catch (Exception exc) {
                                    exc.printStackTrace();
                                    Toast.makeText(context, errorMsg, Toast.LENGTH_SHORT).show();
                                }
                            } else {
                                Toast.makeText(context, errorMsg, Toast.LENGTH_SHORT).show();
                            }
                        }
                    });
            return;
        }else{
            if (canUseCache()) {
                Toast.makeText(context, noInternetMsg, Toast.LENGTH_SHORT).show();
                List<Post> posts = retrieveReportsFromCache();
                listener.onReceivedPost(posts);
                return;
            }else{
                noInternetMsg = errorMsg + " Phone not connected. Unable to retrieve Post at this time.";
            }
        }
        listener.onReceivedPost(null);
    }

    public void retrievePersonalPost(final PostListener listener) {
        if (!canUseCache()){
            final int userid = PrefsUtil.getUserId(context);
            String url = NetUtil.API_URL +
                    DamageReport.DMReport_URL +
                    "?userid=" +
                    userid +
                    "&type=all";
            makeRequest(url, listener);
        }
    }

    public void retrieve(final PostListener listener){
        if (!canUseCache()){
            final String url = NetUtil.API_URL + DamageReport.DMReport_URL;
            makeRequest(url, listener);
        }
    }
    
    public void retrieve(String countryid, final PostListener listener){
        final String url = NetUtil.API_URL + DamageReport.DMReport_URL + "?countryid="+countryid;
        makeRequest(url, listener);
    }
	
	public void cacheReports(List<Post> posts){
		String json = new Gson().toJson(posts);
		if (PrefsUtil.setReports(context, json)){
			Log.d(TAG, String.format("Successfully Cached %s DM Reports", posts.size()));
			PrefsUtil.setDMReportCacheTime(context, Calendar.getInstance());
		}else{
			Log.d(TAG, "Unable to add DM Reports to cache");
			CrashReporter.getInstance(context).log(Log.ERROR,TAG, "Unable to add DM Reports to cache");
		}
	}
	
	public List<Post> retrieveReportsFromCache(){
		List<Post> posts = new ArrayList<>();
		String cachedPosts = PrefsUtil.getReports(context);
		try {
			if (cachedPosts != null) {
				JsonParser parser = new JsonParser();
				JsonElement element = parser.parse(cachedPosts);
				Moshi moshi = new Moshi.Builder().build();
				Type listOfPostType = Types.newParameterizedType(List.class, Post.class);
				JsonAdapter<List<Post>> jsonAdapter = moshi.adapter(listOfPostType);
				List<Post> json = jsonAdapter.fromJson(element.getAsJsonArray().toString());
				if (json != null) {
					posts.addAll(json);
				}
			}
		}catch (IOException e){
			e.printStackTrace();
			Toast.makeText(context, "Unable to retrieve DM Reports from cache", Toast.LENGTH_SHORT).show();
			CrashReporter.getInstance(context).log("Retrieve DM Reports").report(e);
		}
		Log.d(TAG, String.format("Retrieved %s reports from cache.", posts.size()));
		return posts;
	}
	
    // Handle Categories
    
    
    public void retrieveCategories(final CategoryListener listener){
        retrieveCategories(listener, true);
    }

    public void retrieveCategories(final CategoryListener listener, final boolean useAll) {
        if (!canUseCache()){
            final String url = NetUtil.API_URL + DamageReport.DMReportCategories_URL + "?countryid="+PrefsUtil.getCountryId(context);
            final String errorMsg = "Unable to retrieve Report Categories";
            if (NetUtil.isOnline(context)) {
	            Log.d(TAG, "Requesting Categories from: " + url);
                Ion.with(context)
                    .load(url)
                    .setHeader(NetUtil.API_KEY_PARAM, NetUtil.API_KEY)
                    .asJsonArray()
                    .setCallback(new FutureCallback<JsonArray>() {
                        @Override
                        public void onCompleted(Exception e, JsonArray array) {
                        if (array != null) {
                            try {
                                Moshi moshi = new Moshi.Builder().build();
                                Type listOfCategoryType = Types.newParameterizedType(List.class, DMCategory.class);
                                JsonAdapter<List<DMCategory>> jsonAdapter = moshi.adapter(listOfCategoryType);
                                List<DMCategory> categories = new ArrayList<>();
                                if (useAll)categories.add(new DMCategory(0, "All", 0));
                                List<DMCategory> json = jsonAdapter.fromJson(array.toString());
                                if (json != null) {
                                    categories.addAll(json);
                                    cacheCategories(categories);
                                    listener.onReceivedCategories(categories);
                                }
                            } catch (Exception exc) {
                                exc.printStackTrace();
                                Toast.makeText(context, errorMsg, Toast.LENGTH_SHORT).show();
                            }
                        }else{
                            Toast.makeText(context, errorMsg, Toast.LENGTH_SHORT).show();
                        }
                        }
                    });
            }else{ // Offline so we attempt to use cache if it exists
                List<DMCategory> categories = retrieveCategoriesFromCache();
                listener.onReceivedCategories(categories);
            }
        }else{ // Attempt to use cache
            List<DMCategory> categories = retrieveCategoriesFromCache();
            listener.onReceivedCategories(categories);
        }
        
    }
    
    private List<DMCategory> retrieveCategoriesFromCache() {
        List<DMCategory> categories = new ArrayList<>();
        String cachedPosts = PrefsUtil.getCategories(context);
        try {
            if (cachedPosts != null) {
                JsonParser parser = new JsonParser();
                JsonElement element = parser.parse(cachedPosts);
                Moshi moshi = new Moshi.Builder().build();
                Type listType = Types.newParameterizedType(List.class, DMCategory.class);
                JsonAdapter<List<DMCategory>> jsonAdapter = moshi.adapter(listType);
                List<DMCategory> json = jsonAdapter.fromJson(element.getAsJsonArray().toString());
                if (json != null) {
                    categories.addAll(json);
                }
            }
        }catch (IOException e){
            e.printStackTrace();
            CrashReporter.getInstance(context).report(e);
            Toast.makeText(context, "Unable to retrieve DM Categories from cache", Toast.LENGTH_SHORT).show();
        }
        Log.d(TAG, String.format("Retrieved %s categories from cache", categories.size()));
        return categories;
    }
    
    public boolean canUseCache(){
        // If we are on WiFi, use online
        if (NetUtil.isOnline(context) && NetUtil.isWiFi(context))return false;
        if (PrefsUtil.getReports(context) != null && PrefsUtil.getCategories(context) != null){ // we have data in the cache
            Calendar currTime = Calendar.getInstance();
            Calendar preTime = PrefsUtil.getDMReportCacheTime(context);
            if (preTime != null) {
                long diff = currTime.getTimeInMillis() - preTime.getTimeInMillis();
                long days = diff / (24 * 60 * 60 * 1000);
	            return days < MAX_DAYS;
            }
        }
        return false;
    }
    
    public void cacheCategories(List<DMCategory> categories){
        String json = new Gson().toJson(categories);
        if (PrefsUtil.setCategories(context, json)){
            Log.d(TAG, "Successfully Cached DM Categories");
            PrefsUtil.setDMCategoryCacheTime(context, Calendar.getInstance());
        }else{
            Toast.makeText(context, "Unable to add DM Reports to cache", Toast.LENGTH_SHORT).show();
	        CrashReporter.getInstance(context).log(Log.ERROR,TAG, "Unable to add DM Categories to cache");
        }
    }
    
    
    

    public interface CategoryListener{
        void onReceivedCategories(List<DMCategory> categories);
    }

    public interface PostListener{
        void onReceivedPost(List<Post> posts);
    }
}
