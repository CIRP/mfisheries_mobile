package com.cirp.mfisheries.fewer.abtFewer;

import android.os.Bundle;

import com.cirp.mfisheries.R;
import com.cirp.mfisheries.core.module.ModuleActivity;

public class AboutFEWERActivity extends ModuleActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public int getLayoutResourceId() {
        return R.layout.activity_about_fewer;
    }
}
