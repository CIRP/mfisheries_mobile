package com.cirp.mfisheries.fewer.dmgReport.model;

import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.cirp.mfisheries.R;
import com.cirp.mfisheries.fewer.dmgReport.fragments.PostListingFragment;
import com.cirp.mfisheries.util.GlideApp;
import com.cirp.mfisheries.util.NetUtil;

import java.util.List;

public class PostListAdapter extends RecyclerView.Adapter<PostListAdapter.PostViewHolder>{

    private static final String TAG = "PostListAdapter";
    private final Context context;
    private final LayoutInflater inflater;
    private final List<Post> data;
    private final PostListingFragment listingFragment;
	
	public PostListAdapter(@NonNull PostListingFragment listingFragment, @NonNull List<Post> posts) {
		this.context = listingFragment.getContext();
        this.listingFragment = listingFragment;
        inflater = LayoutInflater.from(context);
        data = posts;
    }

    @Override
    public PostViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.item_dm_report, parent, false);
        return new PostViewHolder(view);
    }
    
    private void controlClickOperations(final PostViewHolder holder, final Post current){
     
    	holder.icon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                listingFragment.displayPost(current);
            }
        });
    
        holder.icon.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {
                return listingFragment.requestRemovePost(current);
            }
        });
    }
    

    @Override
    public void onBindViewHolder(final PostViewHolder holder, int position) {
        final Post current = data.get(position);
        controlClickOperations(holder, current);
        holder.nameView.setText(current.name);
        holder.userView.setText(current.fullname);
	
	    if (current.filetype == null) {
		    holder.icon.setImageResource(R.drawable.ic_content_paste_black);
	    } else if (current.filetype.contains("text")) {
            holder.icon.setImageResource(R.drawable.ic_content_paste_black);
        }else if (current.filetype.contains("image")){
            displayImage(holder, current);
        }else if (current.filetype.contains("video")) {
            displayImage(holder, current);
        }else if (current.filetype.contains("audio")) {
            holder.icon.setImageResource(R.drawable.ic_play_circle_outline_black_36dp);
        }else{
            holder.icon.setImageResource(R.drawable.ic_content_paste_black);
        }
    }
    
    private void displayImage(final PostViewHolder holder, final Post current){
        final String imageURL = NetUtil.SITE_URL + current.filepath;
        Log.d(TAG, "Requesting File at: " + imageURL);
	    GlideApp.with(context)
                .load(imageURL)
                .placeholder(R.drawable.loading)
                .fallback(new ColorDrawable(Color.GRAY))
                .centerCrop()
                .into(holder.icon);
    }

    @Override
    public int getItemCount() {
        return data.size();
    }
    
    
    class PostViewHolder extends RecyclerView.ViewHolder{

        TextView nameView;
        TextView userView;
        ImageView icon;

        PostViewHolder(View view){
            super(view);
            icon =  view.findViewById(R.id.icon);
            nameView = view.findViewById(R.id.name);
            userView = view.findViewById(R.id.user);
        }
    }
}
