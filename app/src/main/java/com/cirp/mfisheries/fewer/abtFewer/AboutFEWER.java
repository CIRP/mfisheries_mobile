package com.cirp.mfisheries.fewer.abtFewer;

import android.content.Context;

import com.cirp.mfisheries.R;
import com.cirp.mfisheries.core.module.Module;

public class AboutFEWER extends Module {

    public AboutFEWER() {
        super();
    }

    public AboutFEWER(Context context) {
        super(context);
    }

    @Override
    protected Module setModuleId() {
        this.moduleId = "AboutFEWER";
        return this;
    }

    @Override
    protected Module setModuleName() {
        this.name = "About FEWER";
        return this;
    }

    @Override
    protected Module setIsDisplayed() {
        this.displayed = true;
        return this;
    }

    @Override
    protected Module setImageResource() {
        this.imageResource = R.drawable.icon_fewer_about;
        return this;
    }

    @Override
    protected Module setNeedsRegistration() {
        this.needsRegistration = true;
        return this;
    }

    @Override
    protected Module setActivityClass() {
        this.activityClass = AboutFEWERActivity.class;
        return this;
    }
}
