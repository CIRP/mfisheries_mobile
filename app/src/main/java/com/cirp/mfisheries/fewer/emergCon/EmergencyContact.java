package com.cirp.mfisheries.fewer.emergCon;

import android.content.Context;

import com.cirp.mfisheries.R;
import com.cirp.mfisheries.core.module.Module;

public class EmergencyContact extends Module {

    public EmergencyContact(){
        super();
    }

    public EmergencyContact(Context context){
        super(context);
    }

    @Override
    protected Module setModuleId() {
        this.moduleId = "EmergencyContacts";
        return this;
    }

    @Override
    protected Module setModuleName() {
        this.name = "Emergency Contacts";
        return this;
    }

    @Override
    protected Module setIsDisplayed() {
        this.displayed = true;
        return this;
    }

    @Override
    protected Module setImageResource() {
        this.imageResource = R.drawable.icon_emergency_contacts;
        return this;
    }

    @Override
    protected Module setNeedsRegistration() {
        this.needsRegistration = false;
        return this;
    }

    @Override
    protected Module setActivityClass() {
        this.activityClass = EmergencyContactActivity.class;
        return this;
    }
}
