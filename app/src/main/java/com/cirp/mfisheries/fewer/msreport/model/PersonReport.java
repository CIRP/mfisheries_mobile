package com.cirp.mfisheries.fewer.msreport.model;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.HashMap;
import java.util.Map;

public class PersonReport implements Parcelable {
	public String id;
	public String title;
	public String description;
	public String additional;
	public String contact;
	public String filepath;
	public String latitude;
	public String longitude;
	public String userid;
	public String countryid;
	public String isFound;
	public String aDate;
	public String timecreated;
	public String fullname;
	public String user;
	public String country;
	
	public PersonReport (){
	
	}
	
	protected PersonReport(Parcel in) {
		id = in.readString();
		title = in.readString();
		description = in.readString();
		additional = in.readString();
		contact = in.readString();
		filepath = in.readString();
		latitude = in.readString();
		longitude = in.readString();
		userid = in.readString();
		countryid = in.readString();
		isFound = in.readString();
//		aDate = in.readString();
		timecreated = in.readString();
		fullname = in.readString();
		user = in.readString();
		country = in.readString();
	}
	
	@SuppressWarnings("unused")
	public static final Parcelable.Creator<PersonReport> CREATOR = new Parcelable.Creator<PersonReport>() {
		@Override
		public PersonReport createFromParcel(Parcel in) {
			return new PersonReport(in);
		}
		@Override
		public PersonReport[] newArray(int size) {
			return new PersonReport[size];
		}
	};
	
	public Map<String, String> getValues() {
		HashMap<String, String> map = new HashMap<>();
		map.put("title", title);
		map.put("description", description);
		map.put("additional", additional);
		map.put("contact", contact);
		map.put("filepath", filepath);
		map.put("latitude", latitude);
		map.put("longitude", longitude);
		map.put("userid", userid);
		map.put("countryid", countryid);
		map.put("isFound", String.valueOf(isFound));
		map.put("timecreated", timecreated);
		
		return map;
	}
	
	public String toString() {
		return String.format("%s:%s for %s of %s", title, description, fullname, country);
	}
	
	public boolean isFound(){
		return isFound.equals("1");
	}
	
	@Override
	public int describeContents() {
		return 0;
	}
	
	@Override
	public void writeToParcel(Parcel dest, int flags) {
		dest.writeString(id);
		dest.writeString(title);
		dest.writeString(description);
		dest.writeString(additional);
		dest.writeString(contact);
		dest.writeString(filepath);
		dest.writeString(latitude);
		dest.writeString(longitude);
		dest.writeString(userid);
		dest.writeString(countryid);
		dest.writeString(isFound);
//		dest.writeString(aDate);
		dest.writeString(timecreated);
		dest.writeString(fullname);
		dest.writeString(user);
		dest.writeString(country);
	}
	
	
}