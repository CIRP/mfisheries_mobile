package com.cirp.mfisheries.fewer.emergCon;

import android.databinding.BaseObservable;
import android.databinding.Bindable;
import android.support.annotation.NonNull;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import java.util.ArrayList;


public class Contact extends BaseObservable implements Comparable<Contact>{

    public static final String ORGANIZATION = "organization";
    public static final String PERSONAL = "personal_phone_contact";
//    private static final String TAG = "ContactModel";
    
    @Bindable
    private String id;
    
    @Bindable
    private String name;

    //The type field can either be set to "organisation" or "personal_phone_contact".
    @Bindable
    private String type;

    @Bindable
    private String phone;

    @Bindable
    private String email;

    @Bindable
    private String country;
    
    @Bindable
    private String countryid;

    @Bindable
    private String additional;

    private ArrayList<String> details;

    public Contact(String name, String type, String phone, String email, String country, String additional, String id, String countryid) {
        this.id = id;
        this.name = name;
        this.type = type;
        this.phone = phone;
        this.email = email;
        this.country = country;
        this.countryid = countryid;
        this.additional = additional;
        this.details = new ArrayList<>();
        this.convertAdditionalDetails();
    }

    private void convertAdditionalDetails(){
//        Log.d(TAG, String.format("Attempting to convert %s to array list of details", this.additional));
        if (this.additional != null && this.additional.length() > 3){
            JsonParser jsonParser = new JsonParser();
            JsonArray jsonArray = (JsonArray) jsonParser.parse(this.additional);
//            Log.d(TAG, String.format("Received %s additional details", jsonArray.size()));

            for (int i = 0 ; i < jsonArray.size(); i++){
                JsonObject rec = jsonArray.get(i).getAsJsonObject();
                details.add(rec.get("key").getAsString());
                details.add(rec.get("value").getAsString());
            }
//            Log.d(TAG, String.format("After processing details object has %s entries", details.size()));
        }
    }

    public String getName() {
        return name;
    }

    public String getType() {
        return type;
    }

    public String getPhone() {
        return phone;
    }

    public String getEmail() {
        return email;
    }

    public String getCountry() {
        return country;
    }

    public String getAdditional() {
        return additional;
    }
    
    public String getId() {
        return id;
    }
    
    public String getCountryid() {
        return countryid;
    }
    
    public ArrayList<String> getDetails(){ return details; }

    @Override
    public int compareTo(@NonNull Contact contact){
        int val = 0;
        if (name != null && contact.name != null)val += name.compareTo(contact.name);
        if (phone != null && contact.phone != null)val += phone.compareTo(contact.phone);
        if (email != null && contact.email != null)val += email.compareTo(contact.email);
        return val;
    }
    
    @Override
    public String toString(){
        return String.format("Name: %s Phone:%s Email: %s Country: %s Type:%s", name, phone, email, country, type);
    }
}
