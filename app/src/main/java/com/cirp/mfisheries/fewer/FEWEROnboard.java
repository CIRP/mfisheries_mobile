package com.cirp.mfisheries.fewer;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.cirp.mfisheries.R;
import com.cirp.mfisheries.fewer.weather.FEWERSlideAdapter;


import android.graphics.Color;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.RelativeLayout;

public class FEWEROnboard extends AppCompatActivity {
    private ViewPager viewpager;
    private FEWERSlideAdapter myadapter;
    private RelativeLayout myLayout;
    private int pos;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_onboarding);
        pos=0;
        myLayout = (RelativeLayout) findViewById(R.id.myrelative);
        viewpager = (ViewPager) findViewById(R.id.viewpager);
        myadapter = new FEWERSlideAdapter(this);
        viewpager.setAdapter(myadapter);
        Button closeButton = (Button)findViewById(R.id.button2);
        Button backButton = (Button)findViewById(R.id.button3);
        closeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(pos==3) {
                    finish();
                }
                else{
                    if(pos==0)
                    {
                        viewpager.setCurrentItem(1);
                    }
                    else if(pos==1){
                        viewpager.setCurrentItem(2);
                    }
                    else
                    {
                        viewpager.setCurrentItem(3);
                    }
                }

            }
        });

        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(pos==1)
                {
                    viewpager.setCurrentItem(0);
                }
                if(pos==2)
                {
                    viewpager.setCurrentItem(1);
                }
                if(pos==3)
                {
                    viewpager.setCurrentItem(2);
                }

            }
        });

        viewpager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                pos=position;
                Button nextButton = (Button) findViewById(R.id.button2);
                Button backButton = (Button)findViewById(R.id.button3);
                String text = (String)nextButton.getText();
                if(position==0){
                    backButton.setVisibility(View.GONE);
                    //myLayout.setBackgroundColor(Color.parseColor("#01BCD4"));
                }
                if(position==1){
                    backButton.setVisibility(View.VISIBLE);
                    //myLayout.setBackgroundColor(Color.parseColor("#EE5555"));
                }
                if(position==2){
                    //nextButton.setText("Finish");
                    backButton.setVisibility(View.VISIBLE);
                    //yLayout.setBackgroundColor(Color.parseColor("#6E3159"));
                }
                if(position==3)
                {
                    nextButton.setText("Finish");
                    backButton.setVisibility(View.VISIBLE);
                }
                else if(position!=3 && text=="Finish" )
                {
                    nextButton.setText("Next");
                }

            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
    }
}