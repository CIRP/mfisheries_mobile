package com.cirp.mfisheries.fewer.emergCon;

import android.content.Context;
import android.util.Log;
import android.widget.Toast;

import com.cirp.mfisheries.util.NetUtil;
import com.cirp.mfisheries.util.PrefsUtil;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;


@SuppressWarnings("WeakerAccess")
public class EmgContactUtil {
	
	private static final String TAG = "EmgContactUtil";
	private final int MAX_DAYS = 5;
	private Context context;
	
	public EmgContactUtil(Context context) {
		this.context = context;
	}
	
	public void cacheEmergencyPhoneContacts(List<Contact> selectedContacts){
		String json = new Gson().toJson(selectedContacts);
		if (PrefsUtil.setEmergencyPhoneContacts(context, json)){
			Log.d(TAG, "Successfully Cached Phone Contacts");
		}else{
			Toast.makeText(context, "Unable to add Contacts to cache", Toast.LENGTH_SHORT).show();
		}
	}
	
	public List<Contact> convertToContactsList(JsonArray list){
		if(list == null) return null;
		List<Contact> contacts = new ArrayList<>();
		for(int i = 0; i < list.size(); i++){
			JsonElement current = list.get(i);
			try {
				JsonObject object = current.getAsJsonObject();
				String id = object.get("id").getAsString();
				String name = object.get("name").getAsString();
				String phone = object.get("phone").getAsString();
				String email = object.get("email").getAsString();
				String type = object.get("type").getAsString();
				String country = object.get("country").getAsString();
				String countryid = object.get("countryid").getAsString();
				
				String additional = "";
				if (!object.get("additional").isJsonNull()) {
					additional = object.get("additional").getAsString();
				}
				Contact c = new Contact(name, type, phone, email, country, additional, id, countryid);
				contacts.add(c);
			}catch (Exception e) {e.printStackTrace();}
		}
		
		return contacts;
	}
	
	public List<Contact> retrieveContactsFromCache(String type){
		List<Contact> phoneContactList = new ArrayList<>();
		String phoneContacts;
		try {
			if (type.equals("personal")) {
				phoneContacts = PrefsUtil.getEmergencyPhoneContacts(context);
			}else{
				phoneContacts = PrefsUtil.getEmergencyContacts(context);
			}
			if (phoneContacts != null) {
				JsonParser parser = new JsonParser();
				JsonElement element = parser.parse(phoneContacts);
				JsonArray list = element.getAsJsonArray();
				phoneContactList = convertToContactsList(list);
			}
		}catch (Exception e){e.printStackTrace();}
		return phoneContactList;
	}
	
	
	public void
	retrieveContacts(final ContactListener listener, boolean useServer){
		if (canUseCache() && !useServer){
			Log.d(TAG, "We have data. Using the cache");
			listener.onReceivedContact(retrieveContactsFromCache("system"));
		}else {
			final String url = NetUtil.API_URL + "emergencycontact";
			Log.d(TAG, "Attempting to retrieve contacts from: " + url);
			Ion.with(context)
					.load(url)
					.setHeader(NetUtil.API_KEY_PARAM, NetUtil.API_KEY)
					.asJsonArray()
					.setCallback(new FutureCallback<JsonArray>() {
						@Override
						public void onCompleted(Exception e, JsonArray result) {
							try {
								if (e == null) {
									Log.d(TAG, "Received " + result.size() + " contacts from online");
									final List<Contact> list = convertToContactsList(result);
									cacheContacts(result);
									listener.onReceivedContact(list);
								} else {
									Log.d(TAG, "Using the Cached values");
									listener.onReceivedContact(retrieveContactsFromCache("system"));
								}
							} catch (Exception ex) {
								Log.d(TAG, "ERROR: " + ex.getMessage());
								listener.onReceivedContact(null);
							}
						}
					});
		}
	}
	
	private void cacheContacts(JsonArray result){
		
		if (PrefsUtil.setEmergencyContacts(context, result.toString())) {
			Log.d(TAG, "Emergency Contact was successfully cached");
			updateCacheUse();
		}else
			Log.d(TAG, "Unable to cache contacts");
	}
	
	
	public interface ContactListener{
		void onReceivedContact(List<Contact> contacts);
	}
	
	public boolean canUseCache(){
		// If we are on WiFi, use online
//		if (NetUtil.isOnline(context) && NetUtil.isWiFi(context))return false;
		
		if (PrefsUtil.getEmergencyContacts(context) != null){ // we have data in the cache
			
			Calendar currTime = Calendar.getInstance();
			Calendar preTime = PrefsUtil.getContactCacheTime(context);
			if (preTime != null) {
				long diff = currTime.getTimeInMillis() - preTime.getTimeInMillis();
				long days = diff / (24 * 60 * 60 * 1000);
				if (days < MAX_DAYS){ // So if we are less than the time to live, use cache
					return true;
				}else{
					// We are greater than the time to live, but we are offline then use cache
					if (!NetUtil.isOnline(context))return true;
					// We are greater than the time to live but we are using mobile data then use cache
					return !NetUtil.isWiFi(context);
				}
			}
		}
		
		return false;
	}
	
	public void updateCacheUse(){
		Calendar currTime = Calendar.getInstance();
		PrefsUtil.setContactCacheTime(context, currTime);
	}
}
