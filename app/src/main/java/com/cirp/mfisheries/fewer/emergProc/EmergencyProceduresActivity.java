package com.cirp.mfisheries.fewer.emergProc;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.widget.Toast;

import com.cirp.mfisheries.R;
import com.cirp.mfisheries.core.download.InstallModuleService;
import com.cirp.mfisheries.podcast.PodcastActivity;
import com.cirp.mfisheries.util.FileUtil;

import java.io.File;
import java.util.ArrayList;

public class EmergencyProceduresActivity extends PodcastActivity {
    
    private ProgressDialog downloadProgress;
    private BroadcastReceiver installReceiver;
    
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        permissions = new String[]{
                Manifest.permission.WRITE_EXTERNAL_STORAGE
        };
        requestPermissions();
    }
    
    @Override
    public void init(){
        module = new EmergencyProcedures(this);
        fileFormat = ".mp4";
        fileDataType = "video/*";
        TAG = "EmergencyProcedures";
        
        // Check if we have the files. If not, confirm download with user
        if (!moduleResourceFolderExists()) confirmRequestForResources();
        else loadData();
    }
    
    @Override
    public int getColor(){
        return R.color.red;
    }
    
    @Override
    public int getColorDark(){
        return R.color.redDark;
    }
    
    /**
     * Determine if the extracted folder exists to prevent needless re-downloading and extraction of information that already exists
     * @return true if the resource folder exists
     */
    public boolean moduleResourceFolderExists(){
        String filePath = FileUtil.getFileUri("/" + module.getId()).toString();
        File file = new File(filePath);
        // Folder already exists for the specific module
        return file.exists() && FileUtil.directoryExists(filePath);
    }
    
    public void confirmRequestForResources(){
        AlertDialog.Builder myBuilder = new AlertDialog.Builder(this);
        myBuilder.setTitle("Download Procedures");
        myBuilder.setMessage("The emergency procedures are video files and are downloaded to the phone to ensure you have access to the information at all times. Do you wish to continue?")
                .setPositiveButton("Yes. Download Procedures", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialogInterface, int i) {
                        makeRequestForResources();
                    }
                }).setNegativeButton("No", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialogInterface, int i) {
                        finish();
                    }
                });
        myBuilder.show();
    }
    
    public void makeRequestForResources(){
        ArrayList <String> modules = new ArrayList<>();
        modules.add(this.module.getId());
        Log.d(TAG, "Module data is located at: " + this.module.getDataLoc());
    
        downloadProgress = new ProgressDialog(this);
        downloadProgress.setMessage(getString(R.string.download_alert));
        downloadProgress.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        downloadProgress.setIndeterminate(true);
        downloadProgress.setCancelable(false);
        downloadProgress.show();
    
        // Set Up Listeners to respond to installation. (Implementation based on Main Activity)
        installReceiver = new BroadcastReceiver(){
            public void onReceive(Context context, Intent intent) {
                String action = intent.getAction();
                if (action != null) {
                    // An individual module installation is completed
                    if (action.equals(InstallModuleService.INTENT_MODULE_COMPLETE)) {
                        if (downloadProgress.isShowing()) downloadProgress.dismiss();
                        final String moduleId = intent.getStringExtra(InstallModuleService.STATUS_INSTALL_INFO);
                        final String message = String.format("Completed Installing %s", moduleId);
                        Log.d(TAG, message);
                    }
                    // An individual module installation process was initiated
                    else if (action.equals(InstallModuleService.INTENT_MODULE_START)) {
                        final String message = String.format("Started download for %s", intent.getStringExtra(InstallModuleService.STATUS_INSTALL_INFO));
                        Log.d(TAG, message);
                    }
                    // All the modules were processed
                    else if (action.equals(InstallModuleService.INTENT_MODULES_COMPLETE)) {
                        final boolean result = intent.getBooleanExtra(InstallModuleService.RESULT_INSTALLED, false);
                        Log.d(TAG, "All Modules downloaded successfully");
                        if (downloadProgress.isShowing()) downloadProgress.dismiss();
                        if (result){
                            loadData();
                        }else{
                            Toast.makeText(getBaseContext(), "Unable to retrieve Data", Toast.LENGTH_SHORT).show();
                        }
                    }
                }
            }
        };
        registerReceiver(installReceiver, new IntentFilter(InstallModuleService.INTENT_MODULES_COMPLETE));
        registerReceiver(installReceiver, new IntentFilter(InstallModuleService.INTENT_MODULE_COMPLETE));
        registerReceiver(installReceiver, new IntentFilter(InstallModuleService.INTENT_MODULE_START));
    
        // Make request to the install module
        InstallModuleService.requestInstallModules(this, modules);
    }
    
    @Override
    protected void onPause() {
        try{
            if (installReceiver != null)unregisterReceiver(installReceiver);
        }catch (Exception e){e.printStackTrace();}
        super.onPause();
    }
}
