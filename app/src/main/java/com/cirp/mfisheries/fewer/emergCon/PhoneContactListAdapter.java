package com.cirp.mfisheries.fewer.emergCon;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;

import com.cirp.mfisheries.R;

import java.util.ArrayList;
import java.util.List;


public class PhoneContactListAdapter extends RecyclerView.Adapter<PhoneContactListAdapter.ViewHolder>{
    
    private final Context context;
    private List<Contact> contactList;
    private List<Contact> selectedContacts;
    private static final String TAG = "EmergencyContacts";
    

    PhoneContactListAdapter(List<Contact> contactList, Context context) {
        this.contactList = contactList;
        selectedContacts = new ArrayList<>();
        this.context = context;
    }

    List<Contact> getSelectedContacts() {
        return selectedContacts;
    }
	
	public void setSelectedContacts(List<Contact> selectedContacts) {
		this.selectedContacts = selectedContacts;
	}
	
	@Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.phone_contact_list_item, parent, false);
        return new PhoneContactListAdapter.ViewHolder(view);
    }

    private void addToSelectedContacts(Contact contact){
        if(selectedContacts.contains(contact)){
            selectedContacts.remove(contact);
            Log.d(TAG, contact + " removed from list");
        }
        else{
            selectedContacts.add(contact);
            Log.d(TAG, contact + " added to group");
        }
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        final Contact contact = contactList.get(position);
        // Bind contact details to item's view
        String iconLetter = contact.getName().substring(0, 1);
        holder.contactIcon.setText(iconLetter);
        holder.contactName.setText(contact.getName());
        holder.contactPhoneNumber.setText(contact.getPhone());

        // If contact in list then display as selected
        holder.selectCheckBox.setChecked(selectedContacts.contains(contact));

        holder.selectCheckBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
	        @Override
	        public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
	        	Log.d(TAG,"Selected: " + contact);
		        addToSelectedContacts(contact);
	        }
        });
    }

    @Override
    public int getItemCount() {
        if(contactList != null){
            return contactList.size();
        }
        return 0;
    }

    void setFilter(List<Contact> contacts){
        contactList = new ArrayList<>();
        contactList.addAll(contacts);
        notifyDataSetChanged();
    }

    static class ViewHolder extends RecyclerView.ViewHolder{

        TextView contactIcon;
        TextView contactName;
        TextView contactPhoneNumber;
        CheckBox selectCheckBox;

        public ViewHolder(View itemView) {
            super(itemView);
            contactIcon = itemView.findViewById(R.id.phone_contact_listing_icon);
            contactName = itemView.findViewById(R.id.phone_contact_name);
            contactPhoneNumber = itemView.findViewById(R.id.phone_contact_number);
            selectCheckBox = itemView.findViewById(R.id.phone_contact_checkbox);
        }
    }
}
