package com.cirp.mfisheries.fewer.msreport;

import android.content.Context;
import android.util.Log;

import com.cirp.mfisheries.R;
import com.cirp.mfisheries.core.module.Module;
import com.cirp.mfisheries.util.PrefsUtil;
import com.google.firebase.messaging.FirebaseMessaging;

public class MSReport extends Module {
	public MSReport() {
		super();
	}
	
	public MSReport(Context context){
		super(context);
	}
	
	@Override
	protected Module setModuleId() {
		this.moduleId = "MissingPersons";
		return this;
	}
	
	@Override
	protected Module setModuleName() {
		this.name = "Missing Persons";
		return this;
	}
	
	@Override
	protected Module setIsDisplayed() {
		this.displayed = true;
		return this;
	}
	
	@Override
	protected Module setImageResource() {
		this.imageResource = R.drawable.icon_missing_person;
		return this;
	}
	
	@Override
	protected Module setNeedsRegistration() {
		this.needsRegistration = true;
		return this;
	}
	
	@Override
	protected Module setActivityClass() {
		this.activityClass = MSReportActivity.class;
		return this;
	}
	
	@Override
	public void onInstalled(){
		try {
			final String country = PrefsUtil.getCountryId(context);
			if (country.length() > 0) {
				final String topic = String.format("missing_person_%s", country);
				Log.d(moduleId, "Missing Person module installed. Attempting to subscribe to" + topic);
				FirebaseMessaging.getInstance().subscribeToTopic(topic);
			} else {
				Log.d(moduleId, "Unable to subscribe to missing person notifications");
			}
		}catch (Exception e){e.printStackTrace();}
	}
}
