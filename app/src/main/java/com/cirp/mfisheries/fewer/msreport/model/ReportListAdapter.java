package com.cirp.mfisheries.fewer.msreport.model;

import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.cirp.mfisheries.R;
import com.cirp.mfisheries.fewer.msreport.ViewMSReportActivity;
import com.cirp.mfisheries.fewer.msreport.fragment.ReportListingFragment;
import com.cirp.mfisheries.util.GlideApp;
import com.cirp.mfisheries.util.NetUtil;

import java.util.List;


public class ReportListAdapter extends RecyclerView.Adapter<ReportListAdapter.MSReportViewHolder> {
	
	private final Context context;
	private final ReportListingFragment listingFragment;
	private List<PersonReport> reports;
	private String TAG = "MSReportListAdapter";
	
	public ReportListAdapter(ReportListingFragment listingFragment, List<PersonReport> reports) {
		this.context = listingFragment.getContext();
		this.listingFragment = listingFragment;
		this.reports = reports;
	}
	
	@Override
	public MSReportViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
		View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.ms_report, parent, false);
		return new MSReportViewHolder(view);
	}
	
	@Override
	public void onBindViewHolder(final MSReportViewHolder holder, int position) {
		final PersonReport person = reports.get(position);
		holder.setPersonReport(person);
		
		holder.icon.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				if (listingFragment != null)displayReport(person);
			}
		});
		holder.icon.setOnLongClickListener(new View.OnLongClickListener() {
			@Override
			public boolean onLongClick(View view) {
				if (listingFragment != null)listingFragment.delete(person);
				return true;
			}
		});
	}

	private void displayReport(final PersonReport report){
		ViewMSReportActivity.launch(report, this.context);
	}
	
	// https://stackoverflow.com/questions/37248654/android-progress-dialog-when-loading-image-on-dialog-popup

	
	@Override
	public int getItemCount() {
		return reports.size();
	}
	
	public class MSReportViewHolder extends RecyclerView.ViewHolder {
		TextView nameView;
		TextView userView;
		ImageView icon;
		LinearLayout reportDetails;
		
		
		public MSReportViewHolder(View view) {
			super(view);
			icon =  view.findViewById(R.id.icon);
			reportDetails = view.findViewById(R.id.report_details);
			nameView = view.findViewById(R.id.name);
			userView = view.findViewById(R.id.user);
		}
		
		public void setPersonReport(PersonReport personReport){
			nameView.setText(personReport.title);
			String url = NetUtil.SITE_URL + personReport.filepath;
			GlideApp.with(context)
					.load(url)
					.fallback(new ColorDrawable(Color.GRAY))
					.placeholder(R.drawable.loading)
					.centerCrop()
					.into(this.icon);
			userView.setText(String.format("If Found contact %s", personReport.contact));
			if (personReport.isFound()) {
				icon.setColorFilter(Color.GREEN, android.graphics.PorterDuff.Mode.MULTIPLY);
			}
		}
		
		@Override
		public String toString() {
			return super.toString() + " '" + nameView.getText() + "'";
		}
	}
}
