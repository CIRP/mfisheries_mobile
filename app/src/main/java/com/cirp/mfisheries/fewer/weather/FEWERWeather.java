package com.cirp.mfisheries.fewer.weather;

import android.content.Context;

import com.cirp.mfisheries.R;
import com.cirp.mfisheries.core.module.Module;
import com.cirp.mfisheries.weather.Weather;

public class FEWERWeather extends Weather {
	
	public FEWERWeather(Context context) {
		super(context);
	}
	@Override
	protected Module setModuleId() {
		this.moduleId = "Weather and Thresholds";
		return this;
	}
	@Override
	protected Module setModuleName() {
		this.name = "Weather and Thresholds";
		return this;
	}
	
	// Set the custom icon for the FEWER Alerts
	@Override
	protected Module setImageResource() {
		this.imageResource = R.drawable.icon_fewer_weather;
		return this;
	}
	
	@Override
	protected Module setActivityClass() {
		this.activityClass = FEWERWeatherActivity.class;
		return this;
	}
}
