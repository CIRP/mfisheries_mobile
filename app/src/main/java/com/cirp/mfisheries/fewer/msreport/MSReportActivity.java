package com.cirp.mfisheries.fewer.msreport;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.TabLayout;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AlertDialog;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.cirp.mfisheries.R;
import com.cirp.mfisheries.core.Country;
import com.cirp.mfisheries.core.CountryAdapter;
import com.cirp.mfisheries.core.module.ModuleActivity;
import com.cirp.mfisheries.fewer.msreport.fragment.ReportListingFragment;
import com.cirp.mfisheries.util.CountryUtil;
import com.cirp.mfisheries.util.NetUtil;
import com.cirp.mfisheries.util.PrefsUtil;
import com.cirp.mfisheries.util.adapters.ViewPagerAdapter;

import java.util.List;

public class MSReportActivity extends ModuleActivity {
	
	private final int CREATE_REPORT = 1;
	private MSReportUtil util;
	private ReportListingFragment myListingFragment;
	private ReportListingFragment allListingFragment;
	private String countryid;
	private String countryName;
	private TextView cNameView;
	Context context;


	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		context = this.getApplicationContext();


		if (!NetUtil.isOnline(context)) {
			Toast.makeText(this, "Currently Offline. Attempting to use Cache", Toast.LENGTH_SHORT).show();
		}
		
		module = new MSReport(this);
		util = new MSReportUtil(this);
		
		FloatingActionButton fab = findViewById(R.id.fab_report);
		fab.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				Intent intent = new Intent(MSReportActivity.this, CreateMSReportActivity.class);
				startActivityForResult(intent, CREATE_REPORT);
			}
		});
		
		countryid = PrefsUtil.getCountryId(this);
		countryName = PrefsUtil.getCountry(this);
		cNameView = findViewById(R.id.countryname);
		displayCountrySelection(countryName);
	}
	
	@Override
	public void onResume(){
		super.onResume();
		setupViewPager();
	}
	
	protected void setupViewPager() {
		final ViewPager viewPager = findViewById(R.id.tabanim_viewpager);
		ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());
		TabLayout tabLayout = findViewById(R.id.tabanim_tabs);
		
		if (viewPager != null) {
			addFragment2Adapter(adapter);
			viewPager.setAdapter(adapter);
			
			tabLayout.setupWithViewPager(viewPager);
			tabLayout.setBackgroundColor(ContextCompat.getColor(this, getColor()));
			tabLayout.setSelectedTabIndicatorColor(ContextCompat.getColor(this, getColorDark()));
		}
	}
	
	protected void addFragment2Adapter(ViewPagerAdapter adapter) {
		allListingFragment = new ReportListingFragment();
		adapter.addFrag(allListingFragment, "All Reports");
		
		myListingFragment = new ReportListingFragment();
		myListingFragment.setPersonal(true);
		adapter.addFrag(myListingFragment, "My Reports");
	}
	
	@Override
	public int getLayoutResourceId() {
		return R.layout.activity_msreport;
	}
	
	public void chooseCountry(View view) {
		CountryUtil.getInstance(this).retrieveCountries(new CountryUtil.CountryRetrievedListener() {
			@Override
			public void processCountries(List<Country> countries) {
				if (countries != null){
					displayCountries(countries);
				}else{
					Toast.makeText(MSReportActivity.this, "Unable to retrieve countries", Toast.LENGTH_SHORT).show();
				}
			}
		});
	}
	
	private void displayCountries(final List<Country> countries){
		final CountryAdapter arrayAdapter = new CountryAdapter(this, countries);
		
		new AlertDialog.Builder(this)
				.setTitle(getResources().getString(R.string.dialog_country))
				.setAdapter(arrayAdapter,
						new DialogInterface.OnClickListener() {
							@Override
							public void onClick(DialogInterface dialog, int which) {
								Country selectedCountry = countries.get(which);
								countryid = selectedCountry.getId();
								countryName = selectedCountry.getName();
								displayCountrySelection(countryName);
								updateListing();
							}
						})
				.show();
	}
	
	private void displayCountrySelection(String country){
		final String countryDesc = "Missing Persons for: " + country;
		cNameView.setText(countryDesc);
	}
	
	public void updateListing(){
		if (allListingFragment != null)allListingFragment.retrieveData(countryid);
	}
}
