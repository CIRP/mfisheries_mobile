package com.cirp.mfisheries.fewer.msreport;

import android.Manifest;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.cirp.mfisheries.R;
import com.cirp.mfisheries.core.module.ModuleActivity;
import com.cirp.mfisheries.core.module.ModuleFactory;
import com.cirp.mfisheries.fewer.msreport.model.PersonReport;
import com.cirp.mfisheries.nav.MapMarkerMS;
import com.cirp.mfisheries.nav.NavigationActivity;
import com.cirp.mfisheries.util.GlideApp;
import com.cirp.mfisheries.util.LocationUtil;
import com.cirp.mfisheries.util.NetUtil;
import com.cirp.mfisheries.util.PrefsUtil;
import com.google.gson.JsonObject;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;
import com.koushikdutta.ion.builder.Builders;

import java.util.Map;

public class ViewMSReportActivity extends ModuleActivity {

    private static final String TAG = "ViewMSReport";
    TextView title;
    TextView description;
    TextView contact;
    ImageView icon;
    TextView additional;
    String latitude;
    String longitude;
    TextView reportLoc;
    TextView user;
    TextView isFound;
    TextView date;
    TextView lastLoc;
    String resourceUrl;
    String share = "";
    Button markBtn;
    Button contactBtn;
	private MSReportUtil util;
	private PersonReport report;
	private Context context;
	
	public static void launch(PersonReport report, Context context) {
		Intent intent = new Intent(context, ViewMSReportActivity.class);
		intent.putExtra("report", report);
		context.startActivity(intent);
	}

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_missing_report, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case R.id.action_share:
                shareReport(report);
                Intent shares = new Intent(Intent.ACTION_SEND);
                shares.setType("text/plain");
                shares.putExtra(Intent.EXTRA_SUBJECT, "Missing Person Report by: "+ report.userid);
                shares.putExtra(Intent.EXTRA_TEXT,share);
                // If for Whats-app
                shares.putExtra(Intent.EXTRA_TEXT, share);
                shares.putExtra(Intent.EXTRA_STREAM, resourceUrl);
                
                startActivity(Intent.createChooser(shares,"Share Via"));
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        context = this.getBaseContext();
        super.onCreate(savedInstanceState);
        module = new MSReport(this);
        util = new MSReportUtil(this);
        init(); // Initialize UI Components
        // Request Permission needed to access internet, capture multimedia and store data
        permissions = new String[]{ Manifest.permission.ACCESS_FINE_LOCATION };
        requestPermissions();
        

        try {
            // Retrieve MS Report that was passed as a parcelable
            this.report = getIntent().getExtras().getParcelable("report");
            if (report != null){
                Log.d(TAG, "Retrieved the following report through activity intent: " + report);
                bindUI(report);
            }else{
                Log.d(TAG, "No report was retrieved");
                finish();
            }
        }catch (Exception e){e.printStackTrace(); finish();}

    }

    public void init(){
        title = findViewById(R.id.title);
        description = findViewById(R.id.description);
        user = findViewById(R.id.user);
        date = findViewById(R.id.date);
        isFound = findViewById(R.id.isFound);
        contact = findViewById(R.id.contact);
        reportLoc = findViewById(R.id.reportLoc);
        additional = findViewById(R.id.additional);
        icon =  findViewById(R.id.icon);
        markBtn = findViewById(R.id.mark_user);
        contactBtn = findViewById(R.id.contact_user);
    }

    public void bindUI(PersonReport report){
        if (report != null){
            // Title of report
            title.setText(String.format("Name: %s", report.title));
            // Full user name
            final String userNameStr = (report.fullname != null)? report.fullname : report.userid;
            user.setText(String.format("Created By: %s", userNameStr));
            // Description
            description.setText(String.format("Description: %s", report.description));
            // Is Found
            final String isFoundText = (report.isFound())? "Found" : "Not Found";
            isFound.setText(String.format("Status: %s", isFoundText));

            // If User is found Hide both buttons
            if (isFoundText.equalsIgnoreCase("Found")){
                markBtn.setVisibility(View.GONE);
                contactBtn.setVisibility(View.GONE);
            }else{ // display buttons based on if user is the creator of the report4
                if(report.userid.equals(String.valueOf(PrefsUtil.getUserId(context)))){
                    markBtn.setVisibility(View.VISIBLE);
                    contactBtn.setVisibility(View.GONE);
                }else{
                    markBtn.setVisibility(View.GONE);
                    contactBtn.setVisibility(View.VISIBLE);
                    final String number = report.contact;
                    contactBtn.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            makeCall(number);
                        }
                    });
                }
            }

            // Additional Report
            final String additionalText = (report.additional == null)? "None": report.additional;
            additional.setText(String.format("Additional: %s", additionalText));
            // The date fields
            date.setText(String.format("Date Created: %s", report.timecreated));
            // Contact details
            contact.setText(String.format("Contact: %s", report.contact));

            
            if (report.latitude != null && report.longitude != null) {
                // Using the same Strategy from Alert
                String loc = LocationUtil.getLocationString(Double.parseDouble(report.latitude), Double.parseDouble(report.longitude));
                MapMarkerMS mapMarkerMS = getMapMarker(report);
                reportLoc.setTag(mapMarkerMS);
                reportLoc.setText(loc);
                reportLoc.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        openMap(view);
                    }
                });
            }else{
                Log.d(TAG, "Post has no location data");
                reportLoc.setVisibility(View.GONE);
            }

            // Attempt to load Resource and set operations based on Type
            configureAttachedResource(report);
        }
    }

    private void configureAttachedResource(final PersonReport current) {
        View.OnClickListener listener = new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                launchViewer(current);
            }
        };

        displayImage(current);
        icon.setOnClickListener(listener);
    }

    private void launchViewer(final PersonReport report){
        try {
            final String imageURL = NetUtil.SITE_URL + report.filepath;
            resourceUrl = imageURL;
            Log.d(TAG, String.format("Attempting to read %s as %s", imageURL, "image"));
            Intent intent = new Intent();
            intent.setAction(Intent.ACTION_VIEW);
            intent.setDataAndType(Uri.parse(imageURL), "image");
            startActivity(intent);
        }catch (Exception e ){ e.printStackTrace(); }
    }

    private void displayImage(final PersonReport current){
        final String imageURL = NetUtil.SITE_URL + current.filepath;
        Log.d(TAG, "Requesting File at: " + imageURL);
        resourceUrl = imageURL;
        GlideApp.with(this)
                .load(imageURL)
                .placeholder(R.drawable.loading)
                .fallback(new ColorDrawable(Color.GRAY))
                .centerCrop()
                .into(icon);
    }

    @NonNull
    private MapMarkerMS getMapMarker(PersonReport report) {
        MapMarkerMS mapMarkerMS = new MapMarkerMS();
        mapMarkerMS.user_id = report.userid + "";
        mapMarkerMS.title = report.title;
        mapMarkerMS.latitude = report.latitude;
        mapMarkerMS.longitude = report.longitude;
        return mapMarkerMS;
    }

    private void openMap(View view) {
        final MapMarkerMS list = (MapMarkerMS) view.getTag();

        if (ModuleFactory.getInstance(this).isModuleInstalled("Navigation")) {
            Log.d(TAG, "Navigation Module was registered as installed");
            Intent intent = new Intent(this, NavigationActivity.class);
            intent.putExtra("name", list.title);
            intent.putExtra("details", list.details);
            intent.putExtra("lat", list.latitude);
            intent.putExtra("lng", list.longitude);
            startActivity(intent);
        }else{
            Log.d(TAG, "No Navigation module installed. Give user the user the option");
            Snackbar.make(view, "Navigation module is not installed", Snackbar.LENGTH_LONG)
                    .setAction("Open Google Maps", new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            String urlStr = String.format("google.streetview:cbll=%s,$s", list.latitude, list.longitude);
                            Uri gmmIntentUri = Uri.parse(urlStr); // Create a Uri from an intent string. Use the result to create an Intent.
                            Intent mapIntent = new Intent(Intent.ACTION_VIEW, gmmIntentUri);
                            mapIntent.setPackage("com.google.android.apps.maps");
                            startActivity(mapIntent);
                        }
                    })
                    .show();
        }
    }

    private void shareReport(final PersonReport report){
        // TODO - MissingPersons - Evaluate if it should be written as a narrative
        share += String.format("\nName: %s", report.title);
        final String userNameStr = (report.fullname != null)? report.fullname : report.userid;
        share += String.format("\nCreated By: %s", userNameStr);
        final String isFoundText = (report.isFound())? "Found" : "Not Found";
        share += String.format("\nStatus: %s", isFoundText);
        // Additional Report
        final String additionalText = (report.additional == null)? "None": report.additional;
        share += String.format("\nAdditional: %s", additionalText);
        share += String.format("\nContact: %s", report.contact);
        share += "\n\nCreated On: " + report.timecreated;
//        String locationText = String.valueOf("Location : https://www.google.com/maps/search/?api=1&query="+report.latitude+","+report.longitude);
//        share += "\n\n" + locationText;
        share += "\n\nResource: " + resourceUrl;
    }

    public void makeCall(String number){
        if (number != null) {
            Intent callIntent = new Intent(Intent.ACTION_CALL);
            callIntent.setData(Uri.parse("tel:" + number));

            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                Toast.makeText(this, "This Application doesn't have permission to make phone calls", Toast.LENGTH_SHORT).show();
                return;
            }
            this.startActivity(callIntent);
        }else{
            Toast.makeText(this, "Invalid Number. Unable to Make call.", Toast.LENGTH_SHORT).show();
        }
    }

    public void markFound(View view){
        Log.d(TAG,"Attempting to mark person as found");
        Toast.makeText(ViewMSReportActivity.this, "Marking Person as found", Toast.LENGTH_SHORT).show();
        
        final ProgressDialog progress = new ProgressDialog(this);
        progress.setTitle("Creating Missing Person Report");
        progress.setMessage(getString(R.string.posting));
        progress.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
        progress.setIndeterminate(true);
        progress.setCancelable(false);
        
        if(report.userid.equals(String.valueOf(PrefsUtil.getUserId(context)))){
            Log.d(TAG,"This is your report");
            AlertDialog.Builder myBuilder = new AlertDialog.Builder(ViewMSReportActivity.this);
                    myBuilder.setTitle("Missing Person")
                    .setMessage("Are you sure you want to mark this person as found?")
                    .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            progress.show();
                            report.isFound = "1";
                            final String url = NetUtil.API_URL + "missingpersons/" + report.id;
                            final Map<String, String> map = report.getValues();
	                        final Builders.Any.B builder = Ion.with(ViewMSReportActivity.this).load("PUT", url);
	                        for (Map.Entry<String, String> entry : map.entrySet()) {
                                builder.setMultipartParameter(entry.getKey(), entry.getValue());
                            }
                            
	
	                        builder.asJsonObject()
			                        .setCallback(new FutureCallback<JsonObject>() {
                                        @Override
                                        public void onCompleted(Exception e, JsonObject result) {
                                            if (progress.isShowing())progress.dismiss();
                                            
                                            if (e == null){
                                                Log.d(TAG, "User Marked as found");
                                                Snackbar.make(title, "Successfully Updated Report", Snackbar.LENGTH_SHORT).show();
	                                            bindUI(report);
                                            }else{
                                                e.printStackTrace();
                                                Snackbar.make(title, "Unable to update the Report", Snackbar.LENGTH_SHORT).show();
                                                Log.d(TAG, "Unable to mark as found  due to an unknown error");
                                            }
                                        }
                                    });
                        }
                    })
                    .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            dialogInterface.dismiss();
                        }
                    });
                    myBuilder.show();
        }else{
            Toast.makeText(context, "Only the user who created report can mark as found", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public int getLayoutResourceId() {
        return R.layout.activity_view_msreport;
    }

    @Override
    public int getColor(){
        return R.color.blue;
    }

}
