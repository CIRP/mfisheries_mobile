package com.cirp.mfisheries.fewer.dmgReport;

import android.content.Context;
import android.util.Log;

import com.cirp.mfisheries.R;
import com.cirp.mfisheries.core.module.Module;
import com.cirp.mfisheries.util.PrefsUtil;
import com.google.firebase.messaging.FirebaseMessaging;

public class DamageReport extends Module {

    public static final String DMReport_URL = "damagereport";
    public static final String DMReportCategories_URL = DMReport_URL + "/category";

    public DamageReport() {
        super();
    }

    public DamageReport(Context context) {
        super(context);
    }

    @Override
    protected Module setModuleId() {
        this.moduleId = "DamageReporting";
        return this;
    }

    @Override
    protected Module setModuleName() {
        this.name = "Damage Reporting";
        return this;
    }

    @Override
    protected Module setIsDisplayed() {
        this.displayed = true;
        return this;
    }

    @Override
    protected Module setImageResource() {
        this.imageResource = R.drawable.icon_dmg_report;
        return this;
    }

    @Override
    protected Module setNeedsRegistration() {
        this.needsRegistration = true;
        return this;
    }

    @Override
    protected Module setActivityClass() {
        this.activityClass = DamageReportActivity.class;
        return this;
    }
    
    @Override
    public void onInstalled(){
        try {
            final String country = PrefsUtil.getCountryId(context);
            if (country.length() > 0) {
                final String topic = String.format("damage_report_%s", country);
                Log.d(moduleId, "Damage Reporting module installed. Attempting to subscribe to" + topic);
                FirebaseMessaging.getInstance().subscribeToTopic(topic);
            } else {
                Log.d(moduleId, "Unable to subscribe to Damage Report notifications");
            }
        }catch (Exception e){e.printStackTrace();}
    }
}
