package com.cirp.mfisheries.fewer.msreport;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.cirp.mfisheries.R;
import com.cirp.mfisheries.core.module.ModuleActivity;
import com.cirp.mfisheries.databinding.ActivityCreateMsreportBinding;
import com.cirp.mfisheries.fewer.msreport.model.PersonReport;
import com.cirp.mfisheries.util.CrashReporter;
import com.cirp.mfisheries.util.FileUtil;
import com.cirp.mfisheries.util.NetUtil;
import com.cirp.mfisheries.util.PrefsUtil;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;
import com.koushikdutta.ion.builder.Builders;

import java.io.File;
import java.util.Calendar;
import java.util.Date;
import java.util.Map;

import id.zelory.compressor.Compressor;

import static com.cirp.mfisheries.util.MediaUtils.COMPRESS_QUALITY;

public class CreateMSReportActivity extends ModuleActivity {
	private static final int REQUEST_IMAGE_GET = 101;
	private static final String TAG = "CreateMSReport";
	private static final String REPORT_API_PATH = NetUtil.API_URL + "missingpersons";
	public String lastLoc;
	String currentFilePath = "";
	boolean detailsVisible;
	int[] ids = {
			R.id.title,
			R.id.description,
			R.id.contact,
			R.id.last_location
	};
	EditText loc;
	private MSReportUtil util;
	private PersonReport report;
	private String lastdate;

	public PersonReport getReport() {
		return report;
	}
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		module = new MSReport(this);
		util = new MSReportUtil(this);
		
		// Custom
		ActivityCreateMsreportBinding binding = DataBindingUtil.setContentView(this, R.layout.activity_create_msreport);
		// Set Up Toolbar
		Toolbar toolbar = findViewById(R.id.toolbar);
		if (toolbar != null) setToolBar(toolbar);
		// Attach the person to the UI via binding
		report = new PersonReport();
		report.userid = String.valueOf(PrefsUtil.getUserId(this));
		report.countryid = String.valueOf(PrefsUtil.getCountryId(this));
		// Setup Current Date for post creation
		Calendar c = Calendar.getInstance();
		//TODO figure out why this does not save the date
//		report.aDate = c.getTime().toString();
		report.timecreated = c.getTime().toString();
//		Log.d(TAG,report.aDate);
		Log.d(TAG,report.timecreated);

		binding.setReport(report);
		loc = findViewById(R.id.last_location);
		
		// Set up a listener for determining how user enters text
		for (int id : ids) {
			EditText view = findViewById(id);
			view.addTextChangedListener(new TextWatcher() {
				public void beforeTextChanged(CharSequence cs, int i, int i1, int i2) {
				}
				
				public void onTextChanged(CharSequence cs, int i, int i1, int i2) {
					validate();
				}
				
				public void afterTextChanged(Editable editable) {
				}
			});
		}
		
	}
	
	public void attach(View view) {
		Intent intent = new Intent(Intent.ACTION_GET_CONTENT, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
		intent.setType("image/*");
		if (intent.resolveActivity(getPackageManager()) != null) {
			startActivityForResult(intent, REQUEST_IMAGE_GET);
		}
	}
	
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		final ImageView thumb = findViewById(R.id.thumbnail);
		thumb.setVisibility(View.VISIBLE);
		if (requestCode == REQUEST_IMAGE_GET && resultCode == RESULT_OK) {
			Uri uri = data.getData();
			currentFilePath = FileUtil.getAbsolutePath(this, uri, "image");
			Glide.with(this).load(new File(currentFilePath)).into(thumb);
			Log.d(TAG, "Selected ");
			report.filepath = currentFilePath;
			validate();
		}
	}
	
	public void validate() {
		View btnView = findViewById(R.id.btn_create);
		if (report.title != null && report.title.length() > 3 &&
				report.contact != null && report.contact.length() > 5 &&
				report.filepath != null) {
			btnView.setEnabled(true);
			btnView.setBackgroundColor(getResources().getColor(R.color.blueDark));
		} else {
			btnView.setEnabled(false);
			btnView.setBackgroundColor(getResources().getColor(R.color.grey_300));
		}
	}

	public void toggleDetails(View view) {
		detailsVisible = !detailsVisible;
		int visibility;
		String message;
		if (detailsVisible){
			visibility = View.VISIBLE;
			message = getResources().getString(R.string.less_details);
		}
		else {
			visibility = View.GONE;
			message = getResources().getString(R.string.more_details);
		}
		findViewById(R.id.additional_report_details).setVisibility(visibility);
		((Button)findViewById(R.id.toggleButton)).setText(message);
	}

	public void pickDate(View view) {
		DatePickerFragment datePickerFragment = new DatePickerFragment();
		datePickerFragment.show(getSupportFragmentManager(), "datePicker");
		datePickerFragment.setListener(new DatePickerDialog.OnDateSetListener() {
			@Override
			public void onDateSet(DatePicker view, int year, int month, int day) {
				int actualMonth = month + 1;
				String date = day + "/" + actualMonth + "/" + year;
				Calendar c = Calendar.getInstance();
				c.set(year, month, day);
				Date postDate = c.getTime();

				Button datePicker = findViewById(R.id.datePicker);
				datePicker.setText(date);
				lastdate = date;
			}
		});

	}

	@Override
	public int getLayoutResourceId() {
		return R.layout.activity_create_msreport;
	}

	public void saveReport(View view) {
		Toast.makeText(getBaseContext(), "Creating Report Please wait", Toast.LENGTH_LONG).show();
		Log.d(TAG, "Last Date Seen: " + lastdate);
//		Log.d(TAG, "Date Created: " + report.aDate);
		Log.d(TAG, "Date Created: " + report.timecreated);

		lastLoc = loc.getText().toString();
		Log.d(TAG, "Last Location Seen: " + lastLoc);

		if (lastdate == null){
			lastdate = "Not Specified";
		}
		if (lastLoc.equals("")) {
			lastLoc = "Not Specified";
		}
		if (report.filepath != null && report.filepath.length() > 2) {

//			report.lastDate = lastdate;
			report.additional = "Last Date Seen: " + lastdate + "\nLast Location Seen: " + lastLoc;
			report.isFound = "0";

			Log.d(TAG, "Attempting to save: " + report);
			final ProgressDialog progress = new ProgressDialog(this);
			progress.setTitle("Creating Missing Person Report");
			progress.setMessage(getString(R.string.posting));
			progress.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
			progress.setIndeterminate(true);
			progress.setCancelable(false);
			
			File file = null;
			if (report.filepath != null && report.filepath.length() > 2) {
				file = new File(report.filepath);
				if (file.exists())Log.d(TAG, "File Exists at: " + file.getAbsolutePath());
			}
			final Map<String, String> map = report.getValues();
			final Builders.Any.B builder = Ion.with(this).load(REPORT_API_PATH);
			if (file != null) {
				// Try to compress the file if its an image
				try{
					Log.d(TAG, "Attempting to compress image.");
					file = new Compressor(this).setQuality(COMPRESS_QUALITY).compressToFile(file);
				}catch (Exception e){ Log.e(TAG, "Unable to compress images"); e.printStackTrace(); }
				builder.setMultipartFile("file", "image/*", file);
			}
			for (Map.Entry<String, String> entry : map.entrySet()) {
				builder.setMultipartParameter(entry.getKey(), entry.getValue());
			}

			PersonReport localReport = report;
			String CachedReport = new Gson().toJson(localReport);
			Log.d(TAG,"Cached Report: "+CachedReport);


			if (NetUtil.isOnline(this.getApplicationContext())){
				progress.show();
				builder.progressDialog(progress)
						.asJsonObject()
						.setCallback(new FutureCallback<JsonObject>() {
							@Override
							public void onCompleted(Exception e, JsonObject result) {
								progress.dismiss();
								if (e == null) {
									Toast.makeText(
											getBaseContext(),
											"Successfully Saved Report",
											Toast.LENGTH_SHORT
									).show();
									Intent intent = new Intent();
									intent.putExtra("report",report.filepath); // TODO make parceable;
									setResult(RESULT_OK, intent);
									finish();
								} else {
									Toast.makeText(
											getBaseContext(),
											"Unable to save the Report",
											Toast.LENGTH_SHORT
									).show();
									e.printStackTrace();
									CrashReporter
											.getInstance(getBaseContext())
											.log(Log.ERROR, TAG, "Attempting to Create Missing Person Report")
											.report(e);
								}
							}
						});
			}else{
				PrefsUtil.setUserMSReport(this.getApplicationContext(), CachedReport);
				Toast.makeText(getBaseContext(),
						"No internet connection. Try again later",
						Toast.LENGTH_SHORT).show();
				PrefsUtil.setMSToSend(this.getApplicationContext(), true);
			}


		}else{
			Toast.makeText(this, "Must select an image before saving report", Toast.LENGTH_SHORT).show();
		}
	}
	
	public static class DatePickerFragment extends DialogFragment {
		
		int day, month, year = 0;
		DatePickerDialog.OnDateSetListener listener;
		
		public void setListener(DatePickerDialog.OnDateSetListener listener) {
			this.listener = listener;
		}
		
		@Override
		@NonNull
		public Dialog onCreateDialog(Bundle savedInstanceState) {
			// Use the current date as the default date in the picker
			Calendar c = Calendar.getInstance();
			if (year == 0) {
				year = c.get(Calendar.YEAR);
				month = c.get(Calendar.MONTH);
				day = c.get(Calendar.DAY_OF_MONTH);
			}
			@SuppressWarnings("ConstantConditions")
			DatePickerDialog dialog = new DatePickerDialog(getActivity(), listener, year, month, day);
			dialog.getDatePicker().setMaxDate(new Date().getTime());
			// Create a new instance of DatePickerDialog and return it
			return dialog;
		}
	}
	
}
