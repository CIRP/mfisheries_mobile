package com.cirp.mfisheries.fewer.dmgReport;

import android.Manifest;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.location.Location;
import android.media.ThumbnailUtils;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.FileProvider;
import android.support.v7.app.AlertDialog;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.cirp.mfisheries.R;
import com.cirp.mfisheries.core.location.LocationService;
import com.cirp.mfisheries.core.module.ModuleActivity;
import com.cirp.mfisheries.fewer.dmgReport.model.DMCategory;
import com.cirp.mfisheries.fewer.dmgReport.model.Post;
import com.cirp.mfisheries.util.CrashReporter;
import com.cirp.mfisheries.util.FileUtil;
import com.cirp.mfisheries.util.LocationUtil;
import com.cirp.mfisheries.util.MediaUtils;
import com.cirp.mfisheries.util.NetUtil;
import com.cirp.mfisheries.util.PrefsUtil;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlacePicker;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;
import com.koushikdutta.ion.builder.Builders;

import java.io.File;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import id.zelory.compressor.Compressor;

import static android.provider.MediaStore.Video.Thumbnails.MINI_KIND;
import static com.cirp.mfisheries.util.MediaUtils.COMPRESS_QUALITY;

public class CreateReportActivity extends ModuleActivity {
    
    private static final String TAG = "CreateReport";

    final int PLACE_PICKER_REQUEST = 121;
    final int REQUEST_IMAGE_GET = 0x1;
    final int REQUEST_AUDIO_GET = 0x3;
    final int REQUEST_VIDEO_GET = 0x2;
    final int REQUEST_NEW_IMAGE_GET = 0x4;
    final int REQUEST_NEW_VIDEO_GET = 0x5;

    ImageView map;
    EditText nameView;
    EditText descView;
    EditText costView;
    ImageView thumbView;
    Button attachBtn;
    Button dateBtn;
    Button postBtn;
    Spinner typeSpin;
    Spinner catSpin;
    Location selectedLocation;
    String POST_API_PATH = "";
    String currentFileType = "text/*";
    String currentFilePath = "";
    Uri currentFileUri;
    DMReportUtil util;
    Date postDate;
    File currentFile;
    boolean detailsVisible;
    private MediaUtils mediaUtils;
    
    
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        module = new DamageReport(this);
        POST_API_PATH = NetUtil.API_URL + "damagereport";
        util = new DMReportUtil(this);
        detailsVisible = false;
        
        // Request Permission needed to access internet, capture multimedia and store data
        permissions = new String[]{
                Manifest.permission.ACCESS_FINE_LOCATION,
                Manifest.permission.CAMERA,
                Manifest.permission.RECORD_AUDIO,
                Manifest.permission.WRITE_EXTERNAL_STORAGE
        };
        
        requestPermissions();
        setUpUIComponents();
        retrieveData();
        mediaUtils = new MediaUtils(this);
        // TODO Ensure the following is set: userid and countryid
    }

    private void retrieveData() {
        util.retrieveCategories(new DMReportUtil.CategoryListener() {
            @Override
            public void onReceivedCategories(List<DMCategory> categories) {
                if (categories != null) {
                    ArrayList<String> categoriesStr = new ArrayList<>();
                    for (DMCategory category : categories) {
                        categoriesStr.add(category.name);
                    }
                    ArrayAdapter<String> catAdapter = new ArrayAdapter<>(CreateReportActivity.this, android.R.layout.simple_list_item_1, categoriesStr);
                    catSpin.setAdapter(catAdapter);
                }
            }
        }, false);
    }

    protected void setUpUIComponents(){
        map = findViewById(R.id.mapView);
        nameView = findViewById(R.id.name);
        descView = findViewById(R.id.description);
        costView = findViewById(R.id.cost);
        thumbView = findViewById(R.id.thumbnail);
        attachBtn = findViewById(R.id.attach);
        dateBtn = findViewById(R.id.datePicker);
        typeSpin = findViewById(R.id.reportTypeSpinner);
        catSpin = findViewById(R.id.selectCtgSpinner);
        postBtn = findViewById(R.id.post_btn);
        
        // Set up to ensure that text is entered
        nameView.addTextChangedListener(new TextWatcher() {
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {  }
            public void afterTextChanged(Editable editable) { }
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (charSequence.length() > 2){
                    postBtn.setEnabled(true);
                }else{
                    postBtn.setEnabled(false);
                }
            }
        });
        // Setup Current Date for button
        Calendar c = Calendar.getInstance();
        int year = c.get(Calendar.YEAR);
        int month = c.get(Calendar.MONTH) + 1;
        int day = c.get(Calendar.DAY_OF_MONTH);
        dateBtn.setText(String.format(Locale.US,"%d/%d/%d", day, month, year));
        postDate = c.getTime();
    }

    private void initializeLocation() {
        LocationUtil.startLocationServices(this);
        selectedLocation = LocationService.userLocation;
        showMapPreview(selectedLocation);
    }

    @Override
    public void onPermissionGranted(String permission) {
        super.onPermissionGranted(permission);
        initializeLocation();
    }
    
    @Override
    public void onPermissionRejected(){
        super.onPermissionRejected();
        finish();
    }

    @Override
    public int getLayoutResourceId() {
        return R.layout.activity_create_dmg_report;
    }
    
    @Override
    public int getColor(){
        return R.color.yellow;
    }

    public void pickPlace(View view) {
        if (NetUtil.isOnline(this)) {
            try {
                PlacePicker.IntentBuilder builder = new PlacePicker.IntentBuilder();
                startActivityForResult(builder.build(this), PLACE_PICKER_REQUEST);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public void pickDate(View view) {
        DatePickerFragment datePickerFragment = new DatePickerFragment();
        datePickerFragment.show(getSupportFragmentManager(), "datePicker");
        datePickerFragment.setListener(new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int month, int day) {
                int actualMonth = month + 1;
                String date = day + "/" + actualMonth + "/" + year;
                Calendar c = Calendar.getInstance();
                c.set(year, month, day);
                postDate = c.getTime();

                Button datePicker = findViewById(R.id.datePicker);
                datePicker.setText(date);
            }
        });

    }

    public void uploadData(View view) {
        final ProgressDialog progress = new ProgressDialog(this);
        progress.setTitle("Creating Damage Report");
        progress.setMessage(getString(R.string.posting));
        progress.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progress.setIndeterminate(true);
        progress.setCancelable(false);

        // If we have location then proceed to attempt to save, else notify the user and ask them to select location
        if (selectedLocation != null) {
            final Post post = new Post();
            post.name = nameView.getText().toString();
            post.description = descView.getText().toString();
            post.userid = PrefsUtil.getUserId(this) + "";
            post.countryid = PrefsUtil.getCountryId(this);
            post.latitude = selectedLocation.getLatitude() + "";
            post.longitude = selectedLocation.getLongitude() + "";
            post.filetype = currentFileType;
            post.filepath = currentFilePath;
            post.aDate = postDate.toString();
            if(catSpin.getSelectedItem().toString() == null){
                post.category = "Other";
            }else{
                post.category = catSpin.getSelectedItem().toString();
            }
            post.isPublic = typeSpin.getSelectedItem().toString();
            post.createdby = PrefsUtil.getUserId(this) + "";
            post.cost = costView.getText().toString();
            
            // Ensure that location is specified
            if (post.latitude.length() < 1 || post.longitude.length() < 1){
                displayNotice(
                        "Unable to Create Report. No Media File Selected.",
                        "Select Location",
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                pickPlace(null);
                            }
                        }
                );
                return;
            }
            
            // Ensure file to be uploaded is available
            File file;
            if (currentFile != null && currentFile.exists()){
                Log.d(TAG, "Using current file");
                file = currentFile;
            }
            else {
                Log.d(TAG, "Attempting to use file path: " + currentFilePath);
                file = new File(currentFilePath);
            }
    
            // Ensure if file type is text that file is none
            if(post.filetype.contains("text")) file = null;
            
            // Ensure that if file type was specified that it is not null
            if (!post.filetype.contains("text") && file == null) {
                displayNotice("Unable to Create Report. No Media File Selected.",
                        "Select Media",
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                attach(null);
                            }
                        }
                );
                return;
            }
    
            // At this point we have selected a different type and the path was set
            
            if (!post.filetype.contains("text") && file != null){
                if (!file.exists()){
                    Log.d(TAG, "File Does not exist");
                    displayNotice("Unable to Create Report. Unable to Load File.",
                            "Select Media",
                            new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogInterface, int i) {
                                    attach(null);
                                }
                            }
                    );
                    return;
                } else{
                    Log.d(TAG, "File Exists at: " + file.getAbsolutePath());
                }
            }
            
            // We have the file and location at this point
            if (post.isValid()) {
                progress.show();
                if (NetUtil.isOnline(this.getApplicationContext())){
                    Log.d(TAG, "Application is online");
                }else{
                    String toCache = new Gson().toJson(post);
                    String postFile = new Gson().toJson(file);
                    Toast.makeText(CreateReportActivity.this, "No Internet. Try again later", Toast.LENGTH_SHORT).show();
                    PrefsUtil.setUSerDMReport(this.getApplicationContext(), toCache, postFile);
                    PrefsUtil.setDMtoSend(this.getApplicationContext(), true);
                }

                    sendPost(post, file, new FutureCallback<JsonObject>() {
                    public void onCompleted(Exception e, JsonObject result) {
                        progress.dismiss();
                        if (e == null) {
                            Toast.makeText(CreateReportActivity.this, "Successfully Saved Report", Toast.LENGTH_SHORT).show();
                            Intent intent = new Intent();
                            intent.putExtra("post",post);
                            setResult(RESULT_OK, intent);
                            finish(); // TODO Go to Post Created (for now just finishing)
                        } else {
                            Toast.makeText(CreateReportActivity.this, "Unable to Save Report", Toast.LENGTH_SHORT).show();
                            e.printStackTrace();
                            CrashReporter
                                    .getInstance(getBaseContext())
                                    .log("Attempting to Create Damage Report")
                                    .report(e);
                        }
                    }
                });
            }else{
                displayNotice("Unable to Create Report. Ensure all fields are filled.");
            }
        }
        else{
            displayNotice("Unable to Create Report. No Location Selected.",
                "Select Location", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        pickPlace(null);
                    }
                });
        }
    }
    
    private void sendPost(@NonNull final Post post, File file, @NonNull final  FutureCallback<JsonObject> futureCallback) {
        Log.d(TAG, String.format("Attempting to send %s to %s ", POST_API_PATH, post));
        Map <String, String> map = post.getValues();
        Builders.Any.B builder = Ion.with(this).load(POST_API_PATH);
        if (file != null){
            // Try to compress the file if its an image
            if (post.filetype != null && post.filetype.equalsIgnoreCase("image/*")){
                try{
                    Log.d(TAG, "Attempting to compress image");
                    file = new Compressor(this).setQuality(COMPRESS_QUALITY).compressToFile(file);
                }catch (Exception e){ Log.e(TAG, "Unable to compress images"); e.printStackTrace(); }
            }
            builder.setMultipartFile("file", post.filetype, file);
        }
        // Add each value of the post as part of the request
        for (Map.Entry<String, String> entry : map.entrySet()) {
            builder.setMultipartParameter(entry.getKey(), entry.getValue());
        }
        builder.asJsonObject()
            .setCallback(futureCallback);
    }

    public void attach(View view) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        String[] items = {
                "Attach Image"
                ,"Attach Video"
                ,"Attach Audio"
                ,"Take Picture"
                ,"Record Video"
        };
        
        final ArrayAdapter<String> arrayAdapter = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, items);

        builder.setAdapter(arrayAdapter,
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        switch (which) {
                            case 0:
                                attachImage();
                                return;
                            case 1:
                                attachVideo();
                                return;
                            case 2:
                                attachAudio();
                                return;
                            case 3:
                                attachNewImage();
                                return;
                            case 4:
                                attachNewVideo();
                        }
                    }
                });
        builder.show();
    }

    //attach audio files
    public void attachAudio() {
        Intent intent = new Intent(Intent.ACTION_GET_CONTENT, MediaStore.Audio.Media.EXTERNAL_CONTENT_URI);
        intent.setType("audio/*");
        if (intent.resolveActivity(getPackageManager()) != null) {
            startActivityForResult(intent, REQUEST_AUDIO_GET);
        }
    }

    //attach video files
    public void attachVideo() {
        Intent intent = new Intent(Intent.ACTION_GET_CONTENT, MediaStore.Video.Media.EXTERNAL_CONTENT_URI);
        intent.setType("video/*");
        if (intent.resolveActivity(getPackageManager()) != null) {
            startActivityForResult(intent, REQUEST_VIDEO_GET);
        }
    }

    //attach image files
    public void attachImage() {
        Intent intent = new Intent(Intent.ACTION_GET_CONTENT, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        intent.setType("image/*");
        if (intent.resolveActivity(getPackageManager()) != null) {
            startActivityForResult(intent, REQUEST_IMAGE_GET);
        }
    }
    
    private void setupFileForMedia(Intent intent, int requestCode, String filename){
        String msg;
        if (intent.resolveActivity(getPackageManager()) != null){
            currentFile = FileUtil.getOutputMediaFile(this, filename);
            if (currentFile != null) {
                currentFilePath = currentFile.getAbsolutePath();
                Log.d(TAG, "File Created as :" + currentFile.getAbsolutePath());
                currentFileUri = FileProvider.getUriForFile(this,getPackageName() + ".provider", currentFile);
                Log.d(TAG, "URI For file is: " + currentFileUri);
                intent.putExtra(MediaStore.EXTRA_OUTPUT, currentFileUri);
                startActivityForResult(intent, requestCode);
            }else{
                msg = "Unable to Create Media. Ensure to enable file permissions when requested.";
                Log.d(TAG, msg);
                Toast.makeText(
                        getBaseContext(),
                        msg,
                        Toast.LENGTH_SHORT
                ).show();
                CrashReporter.getInstance(getBaseContext()).log(Log.ERROR, TAG, msg);
            }
        }else{
            msg = "Unable to Launch Camera. Please restart mFisheries application and try again.";
            Log.d(TAG, msg);
            Toast.makeText(
                    getBaseContext(),
                    msg,
                    Toast.LENGTH_SHORT
            ).show();
            CrashReporter.getInstance(getBaseContext()).log(Log.ERROR, TAG, msg);
        }
    }

    // Based on - https://developer.android.com/training/camera/photobasics.html#TaskPath
    public void attachNewImage() {
        Log.d(TAG, "Making request for new image");
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        setupFileForMedia(intent, REQUEST_NEW_IMAGE_GET, FileUtil.generateImageName(this));
    }
    
    // Based on https://developer.android.com/training/camera/videobasics.html
    public void attachNewVideo() {
        Log.d(TAG, "Making Request for new Video");
        Intent intent = new Intent(MediaStore.ACTION_VIDEO_CAPTURE);
        intent.putExtra(MediaStore.EXTRA_VIDEO_QUALITY, 0); //sets lowest resolution
        setupFileForMedia(intent, REQUEST_NEW_VIDEO_GET, FileUtil.generateVideoName(this));
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        mediaUtils.handleActivityResult(requestCode, resultCode, data, null);
        final ImageView thumb = findViewById(R.id.thumbnail);
        thumb.setVisibility(View.VISIBLE);
        
        if (requestCode == REQUEST_IMAGE_GET && resultCode == RESULT_OK) {
            Uri uri = data.getData();
            currentFilePath = FileUtil.getAbsolutePath(this, uri, "image");
            Glide.with(this).load(new File(currentFilePath)).into(thumb);
            currentFileType = "image/*";
        }
        
        if (requestCode == REQUEST_AUDIO_GET && resultCode == RESULT_OK) {
            Uri uri = data.getData();
            thumb.setImageResource(R.drawable.ic_mic_none_white_48dp);
            thumb.setBackgroundColor(ContextCompat.getColor(this, R.color.primary));
            currentFilePath = FileUtil.getAbsolutePath(this, uri, "audio");
            currentFileType = FileUtil.getFileType("audio", currentFilePath);
        }
        
        if (requestCode == REQUEST_VIDEO_GET && resultCode == RESULT_OK) {
            Uri uri = data.getData();
            currentFilePath = FileUtil.getAbsolutePath(this, uri, "video");
            Glide.with(this).load(new File(currentFilePath)).into(thumb);
            currentFileType = FileUtil.getFileType("video", currentFilePath);
        }
        
        // Handle Capturing Images from Camera
        if (requestCode == REQUEST_NEW_IMAGE_GET && resultCode == RESULT_OK) {
            Glide.with(this).load(new File(currentFilePath)).into(thumb);
            currentFileType = "image/*";
        }
        
        // Handling Capturing Video from camera
        if (requestCode == REQUEST_NEW_VIDEO_GET && resultCode == RESULT_OK) {
            thumb.setImageBitmap(ThumbnailUtils.createVideoThumbnail(currentFilePath, MINI_KIND));
            currentFileType = "video/mp4";
        }
        
        if (requestCode == PLACE_PICKER_REQUEST && resultCode == RESULT_OK) {
            thumb.setVisibility(View.GONE);
            Place place = PlacePicker.getPlace(this, data);
            String toastMsg = String.format("Place: %s", place.getName());
            Toast.makeText(this, toastMsg, Toast.LENGTH_LONG).show();
            selectedLocation = new Location("GPS");
            selectedLocation.setLatitude(place.getLatLng().latitude);
            selectedLocation.setLongitude(place.getLatLng().longitude);
            showMapPreview(selectedLocation);
        }
    }

    //shows an image of the fullname's current location on a map
    public void showMapPreview(Location location) {
        if (location != null) {
            final String url = "http://maps.google.com/maps/api/staticmap?center=" +
                    location.getLatitude() + "," + location.getLongitude() +
                    "&zoom=" + 14 + "&scale=2&size=" + 960 + "x" + 960 +
                    "&sensor=false&markers=color:red%7C" + location.getLatitude() + "," + location.getLongitude();
            Log.d(TAG, "Attempting to show Map Preview of : " + url);
            Glide.with(this)
                    .load(url)
                    .into((ImageView) findViewById(R.id.mapView));
        }
    }
    
    public void toggleDetails(View view) {
        detailsVisible = !detailsVisible;
        int visibility;
        String message;
        if (detailsVisible){
            visibility = View.VISIBLE;
            message = getResources().getString(R.string.less_details);
        }
        else {
            visibility = View.GONE;
            message = getResources().getString(R.string.more_details);
        }
        findViewById(R.id.additional_report_details).setVisibility(visibility);
        ((Button)findViewById(R.id.toggleButton)).setText(message);
    }
    
    
    private void displayNotice(String message){
        displayNotice(message, null, null);
    }
    
    private void displayNotice(String message, String button){
        displayNotice(message, button, null);
    }
    
    private void displayNotice(String message, String positive, DialogInterface.OnClickListener onClickListener){
        AlertDialog.Builder builder = new AlertDialog.Builder(this)
                .setTitle("Create Damage Report")
                .setMessage(message);
                
                
        
        // set positive action if
        if (onClickListener != null && positive != null){
            builder
                .setPositiveButton(positive, onClickListener)
                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                    }
                });
        }
        
        if (positive != null && onClickListener == null){
            builder.setNeutralButton("OK", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    dialogInterface.dismiss();
                }
            });
        }
        
        builder.show();
    }
    
    public static class DatePickerFragment extends DialogFragment {

        int day, month, year = 0;
        DatePickerDialog.OnDateSetListener listener;

        public void setListener(DatePickerDialog.OnDateSetListener listener){
            this.listener = listener;
        }

        @Override
        @NonNull
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            // Use the current date as the default date in the picker
            Calendar c = Calendar.getInstance();
            if (year == 0) {
                year = c.get(Calendar.YEAR);
                month = c.get(Calendar.MONTH);
                day = c.get(Calendar.DAY_OF_MONTH);
            }
            @SuppressWarnings("ConstantConditions")
            DatePickerDialog dialog = new DatePickerDialog(getActivity(), listener, year, month, day);
            dialog.getDatePicker().setMaxDate(new Date().getTime());
            // Create a new instance of DatePickerDialog and return it
            return dialog;
        }
    }
}
