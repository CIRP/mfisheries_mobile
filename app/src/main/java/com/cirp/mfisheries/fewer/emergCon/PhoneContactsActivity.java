package com.cirp.mfisheries.fewer.emergCon;

import android.Manifest;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.CheckBox;
import android.widget.Toast;

import com.cirp.mfisheries.R;
import com.cirp.mfisheries.core.module.ModuleActivity;
import com.cirp.mfisheries.messaging.ItemClickSupport;

import java.util.ArrayList;
import java.util.List;

public class PhoneContactsActivity extends ModuleActivity implements ItemClickSupport.OnItemClickListener, SearchView.OnQueryTextListener{

    private static final String TAG = "PhoneContacts";

    private RecyclerView phoneContactsRecyclerView;
    private SwipeRefreshLayout swLayout;

    private FloatingActionButton addSelectedContactsFab;

    private PhoneContactListAdapter myPhoneContactListAdapter;

    private List<Contact> contactList;
	private EmgContactUtil contactUtil;
	private List<Contact> existingSelectedContacts;
	
	
	public static void startActivity(Context context){
        Intent intent = new Intent(context, PhoneContactsActivity.class);
        context.startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        permissions = new String[]{
                Manifest.permission.READ_CONTACTS
        };
	    contactUtil = new EmgContactUtil(this);
	    setUpSwipeRefresh();
	    
        init();
	    requestPermissions();
    }

    @Override
    public void onPermissionGranted(String permission) {
        super.onPermissionGranted(permission);
        loadContacts();
    }

    @Override
    public int getLayoutResourceId() {
        return R.layout.activity_phone_contacts;
    }

    private void init(){
	    // Attempt to setup custom toolbar for searching contacts
    	Toolbar myToolbar = findViewById(R.id.phone_contact_listing_toolbar);
	    setToolBar(myToolbar);
	    
	    // Retrieve existing cached contacts
	    retrieveCachedPhoneContacts();
	    
	    phoneContactsRecyclerView = findViewById(R.id.phone_contact_recycler_view);
	    ItemClickSupport.addTo(phoneContactsRecyclerView).setOnItemClickListener(this);
	
	    contactList = new ArrayList<>();
	    myPhoneContactListAdapter = new PhoneContactListAdapter(contactList, getApplicationContext());
	    phoneContactsRecyclerView.setAdapter(myPhoneContactListAdapter);
	
	    addSelectedContactsFab = findViewById(R.id.add_selected_contacts_fab);
        addSelectedContactsFab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(PhoneContactsActivity.this, "Adding Contacts to List", Toast.LENGTH_LONG).show();
                List<Contact> selectedContacts = myPhoneContactListAdapter.getSelectedContacts();
                (new EmgContactUtil(PhoneContactsActivity.this)).cacheEmergencyPhoneContacts(selectedContacts);
                finish();
            }
        });
        
    }
	
	private void retrieveCachedPhoneContacts() {
		existingSelectedContacts = contactUtil.retrieveContactsFromCache("personal");
		Log.d(TAG, String.format("Received %s contacts from cache", existingSelectedContacts.size()));
	}

    private void loadContacts(){
        if (swLayout != null) swLayout.setRefreshing(true);
        // Create a New Thread to stop UI From freezing when requesting large amount of contacts
	    (new Thread(){
	    	public void run(){
			    final ContentResolver contentResolver = getContentResolver();
			    final Cursor cursor = contentResolver.query(
					    ContactsContract.Contacts.CONTENT_URI,
					    null,
					    null,
					    null,
					    ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME+" ASC");
			
			    if(cursor != null){
				    if (cursor.getCount() > 0) {
					    while (cursor.moveToNext()) {
						    final String id = cursor.getString(cursor.getColumnIndex(ContactsContract.Contacts._ID));
						    final String name = cursor.getString(cursor.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME));
						    final int hasPhoneNumber = Integer.parseInt(cursor.getString(cursor.getColumnIndex(ContactsContract.Contacts.HAS_PHONE_NUMBER)));
						    if (hasPhoneNumber > 0) {
							    final Cursor phoneNumberCursor = contentResolver.query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI, null, ContactsContract.CommonDataKinds.Phone.CONTACT_ID + " = ?", new String[]{id}, null);
							    if (phoneNumberCursor != null) {
								    while (phoneNumberCursor.moveToNext()) {
									    final String phoneNumber = phoneNumberCursor.getString(phoneNumberCursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));
									    final Contact currentContact = new Contact(name, "personal_phone_contact", phoneNumber, "", "", "", "", "");
									    contactList.add(currentContact);
								    }
								    phoneNumberCursor.close();
							    }
						    }
					    }
				    }
				    cursor.close();
			    }
			    
	    		// Notify UI That we have received the data
	    		runOnUiThread(new Runnable() {
				    @Override
				    public void run() {
					    myPhoneContactListAdapter.setSelectedContacts(existingSelectedContacts);
					    myPhoneContactListAdapter.notifyDataSetChanged();
					    if (swLayout != null) swLayout.setRefreshing(false);
				    }
			    });
	    		
		    }
	    }).start();

     
        
		
    }
    
    private void setUpSwipeRefresh() {
        swLayout = findViewById(R.id.phone_con_swipe_refresh);
        swLayout.setRefreshing(true);
        swLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                loadContacts();
            }
        });
    }
    
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
    	// TODO Removed Search until error resolved
//        getMenuInflater().inflate(R.menu.messaging_menu, menu);
//        MenuItem menuItem = menu.findItem(R.id.action_search);
//        SearchView searchView = (SearchView) menuItem.getActionView();
//        searchView.setOnQueryTextListener(this);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onItemClicked(RecyclerView recyclerView, int position, View v) {
        CheckBox myCheckBox = v.findViewById(R.id.phone_contact_checkbox);
        myCheckBox.toggle();
    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        return false;
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        String text = newText.toLowerCase();
        List<Contact> filteredList = new ArrayList<>();
        Log.d(TAG, "Text: " + newText);
        
        
        for(Contact contact: contactList){
            if(contact.getName().toLowerCase().contains(text)){
                filteredList.add(contact);
            }
        }
        
        myPhoneContactListAdapter.setFilter(filteredList);
        return false;
    }

    @Override
    public int getColor(){
        return R.color.yellow;
    }
}
