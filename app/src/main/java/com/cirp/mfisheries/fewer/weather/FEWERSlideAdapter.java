package com.cirp.mfisheries.fewer.weather;



import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.cirp.mfisheries.R;

import org.w3c.dom.Text;

public class FEWERSlideAdapter extends PagerAdapter {
    Context context;
    LayoutInflater inflater;

    //list of images
    public int[] lst_images = {
            R.drawable.fishing128,
            R.drawable.clipboard,
            R.drawable.help128,
            R.drawable.wrench
    };

    //list of module images
    public int[] lst_modules = {
            R.drawable.onboard_mitigate,
            R.drawable.onboard_prepare,
            R.drawable.onboard_response,
            R.drawable.onboard_recovery
    };

    //list of headings
    public String[] lst_headings = {
            "MITIGATE",
            "PREPARE",
            "RESPOND",
            "RECOVER"
    };

    //list of descriptions
    public String[] lst_descriptions = {
            "Reduce risks everyday",
            "Prepare before going to sea",
            "Respond during an emergency",
            "Recover after an emergency"
    };

    //list of background colours
    public int[] lst_backgroundcolours = {
  //          Color.rgb(138,229,74),
            Color.rgb(51,153,51),
            Color.rgb(1,188,212),
            Color.rgb(238,85,85),
            Color.rgb(110,49,89)
//            Color.rgb(1,188,212),
//            Color.rgb(1,188,212)



    };
//    public int[] lst_text_colours = {
//            Color.rgb(255,51,153),
//            Color.rgb(255,204,102),
//            Color.rgb(0,51,152),
//            Color.rgb(255,153,0)
//
//    };

    public FEWERSlideAdapter(Context context){
        this.context = context;
    }


    @Override
    public int getCount() {
        return lst_headings.length;
    }

    @Override
    public boolean isViewFromObject(@NonNull View view, @NonNull Object object) {
        return (view == (LinearLayout)object);
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        inflater = (LayoutInflater) context.getSystemService(context.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.slide, container, false);
        LinearLayout layoutslide = (LinearLayout) view.findViewById(R.id.slidelinearlayout);
        ImageView imgslide = (ImageView) view.findViewById(R.id.slideimg);
        ImageView imgmodule = (ImageView) view.findViewById(R.id.slidemodule);
        TextView headslide = (TextView) view.findViewById(R.id.slideheading);
        TextView descslide = (TextView) view.findViewById(R.id.slidedescription);
        layoutslide.setBackgroundColor(lst_backgroundcolours[position]);
        imgslide.setImageResource(lst_images[position]);
        imgmodule.setImageResource(lst_modules[position]);
        headslide.setText(lst_headings[position]);
        descslide.setText(lst_descriptions[position]);
//        headslide.setTextColor(lst_text_colours[position]);
//        descslide.setTextColor(lst_text_colours[position]);

//        if(position==2)
//        {
//            Button btn = (Button) view.findViewById(R.id.button2);
//            btn.setText("Finish");
//        }
        container.addView(view);
        return view;


    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((LinearLayout)object);
    }
}
