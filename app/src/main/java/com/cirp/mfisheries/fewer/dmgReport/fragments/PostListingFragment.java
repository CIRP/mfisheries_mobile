package com.cirp.mfisheries.fewer.dmgReport.fragments;

import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.cirp.mfisheries.R;
import com.cirp.mfisheries.fewer.dmgReport.DMReportUtil;
import com.cirp.mfisheries.fewer.dmgReport.DamageReport;
import com.cirp.mfisheries.fewer.dmgReport.ViewReportActivity;
import com.cirp.mfisheries.fewer.dmgReport.model.DMCategory;
import com.cirp.mfisheries.fewer.dmgReport.model.Post;
import com.cirp.mfisheries.fewer.dmgReport.model.PostListAdapter;
import com.cirp.mfisheries.util.NetUtil;
import com.cirp.mfisheries.util.PrefsUtil;
import com.google.gson.JsonObject;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;

import java.util.ArrayList;
import java.util.List;


public class PostListingFragment extends Fragment {

    private static final String TAG = "PostListing";
    int userid;
    List<Post> posts;
    SwipeRefreshLayout swLayout;
    boolean isPersonal;
    RecyclerView myRecyclerView;
    DMReportUtil util;
	private Context context;
	private PostListAdapter adapter;
	private String countryid;
    
    // Required empty public constructor
    public PostListingFragment() {
        isPersonal = false;
    }

    public void setPersonal(boolean personal) {
        isPersonal = personal;
    }
	
	public void setCountryid(String countryid) {
		this.countryid = countryid;
	}
	
	@Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_dm_report_listing, container, false);
        myRecyclerView = view.findViewById(R.id.report_list);
        myRecyclerView.setHasFixedSize(true);
        RecyclerView.LayoutManager layoutManager = new GridLayoutManager(getContext(), 2);
        myRecyclerView.setLayoutManager(layoutManager);
	
	    this.context = getContext();
	    userid = PrefsUtil.getUserId(getContext());
	    util = new DMReportUtil(getContext());
	    posts = new ArrayList<>();
		
		adapter = new PostListAdapter(this, posts);
        myRecyclerView.setAdapter(adapter);
        
        swLayout = view.findViewById(R.id.swipe_refresh);
        swLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                retrieveData();
            }
        });
        
        retrieveData();
        return view;
    }
    
    public void retrieveData(String countryid) {
        if (getContext() != null) {
            this.countryid = countryid;
            if (swLayout != null) swLayout.setRefreshing(true);
            util.retrieveCategories(new DMReportUtil.CategoryListener() {
                @Override
                public void onReceivedCategories(List<DMCategory> categories) {
	                if (categories != null)
		                Log.d(TAG, String.format("Retrieved %s categories", categories.size()));
	                else Log.d(TAG, "Unable to retrieve categories");
                }
            });
    
            if (isPersonal) {
                util.retrievePersonalPost(new DMReportUtil.PostListener() {
                    @Override
                    public void onReceivedPost(List<Post> posts) {
                        if (swLayout != null) swLayout.setRefreshing(false);
                        if (posts != null) {
                            Log.d(TAG, String.format("Retrieved %s posts", posts.size()));
                            updateUIWithPosts(posts);
                        } else Log.d(TAG, "Unable to retrieve posts");
                    }
                });
            } else {
                util.retrieve(countryid, new DMReportUtil.PostListener() {
                    @Override
                    public void onReceivedPost(List<Post> posts) {
                        if (swLayout != null) swLayout.setRefreshing(false);
                        if (posts != null) {
                            Log.d(TAG, String.format("Retrieved %s posts", posts.size()));
                            updateUIWithPosts(posts);
                        } else Log.d(TAG, "Unable to retrieve posts");
                    }
                });
            }
        } else Log.d(TAG, "Context of the PostListing Fragment was null when executed");
    }

    public void retrieveData(){
        if (this.countryid == null)
            countryid = PrefsUtil.getCountryId(getContext());
        retrieveData(countryid);
    }

    public void updateUIWithPosts(List<Post> posts){
        this.posts = posts;
        adapter = new PostListAdapter(this, this.posts);
        myRecyclerView.setAdapter(adapter);
    }
    
    public void displayPost(final Post post){
        ViewReportActivity.launch(post, getContext());
    }
    
    public void deletePost(final Post post){
        if (post.userid.equals(String.valueOf(PrefsUtil.getUserId(getContext())))){
            String url = NetUtil.API_URL + DamageReport.DMReport_URL +"/"+post.id;
            if (getContext() != null) {
                Ion.with(getContext())
                        .load("DELETE", url)
                        .setHeader(NetUtil.API_KEY_PARAM, NetUtil.API_KEY)
                        .asJsonObject()
                        .setCallback(new FutureCallback<JsonObject>() {
                            @Override
                            public void onCompleted(Exception e, JsonObject result) {
                                if (e != null) {
                                    Toast.makeText(getContext(), "Report Successfully Deleted", Toast.LENGTH_SHORT).show();
                                    posts.remove(post);
                                    adapter.notifyDataSetChanged();
                                } else {
                                    Toast.makeText(getContext(), "Unable to delete report", Toast.LENGTH_SHORT).show();
                                }
                            }
                        });
            }else{
                Toast.makeText(getContext(), "Unable to delete report", Toast.LENGTH_SHORT).show();
            }
        }
    }
    
    public boolean requestRemovePost(final Post post){
        if (getContext() != null) {
            AlertDialog.Builder myBuilder = new AlertDialog.Builder(getContext());
            myBuilder.setTitle("Remove Report");
            if (post.userid.equals(String.valueOf(userid))) {
                myBuilder.setMessage("Are you sure you would like to remove this report")
                        .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                deletePost(post);
                            }
                        }).setNegativeButton("No", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.cancel();
                    }
                });
            } else {
                myBuilder.setMessage("Can only delete reports that belong to you")
                        .setNeutralButton("OK", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                dialogInterface.dismiss();
                            }
                        });
            }
            myBuilder.show();
            return true;
        }
        return false;
    }
}
