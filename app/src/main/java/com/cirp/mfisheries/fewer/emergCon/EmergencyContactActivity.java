package com.cirp.mfisheries.fewer.emergCon;

import android.Manifest;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.cirp.mfisheries.R;
import com.cirp.mfisheries.core.Country;
import com.cirp.mfisheries.core.CountryAdapter;
import com.cirp.mfisheries.core.module.ModuleActivity;
import com.cirp.mfisheries.messaging.ItemClickSupport;
import com.cirp.mfisheries.util.CountryUtil;
import com.cirp.mfisheries.util.PrefsUtil;

import java.util.ArrayList;
import java.util.List;


public class EmergencyContactActivity extends ModuleActivity implements ItemClickSupport.OnItemLongClickListener{

    private static final String TAG = "EmergencyContacts";
    
    
    private EmergencyContactListAdapter myAdapter;
    private ArrayList<Contact> contacts;
    private SwipeRefreshLayout swLayout;
    private EmgContactUtil contactUtil;
    RecyclerView contactListRView;

    private AlertDialog dialog;
    private String countryid;
    private String countryName;
    private TextView cNameView;
    
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        super.onStart();

        module = new EmergencyContact(this);
        contactUtil = new EmgContactUtil(this);
        permissions = new String[]{
                Manifest.permission.CALL_PHONE
        };
        requestPermissions();

        contacts = new ArrayList<>();
        myAdapter = new EmergencyContactListAdapter(this, contacts);

        countryid = PrefsUtil.getCountryId(this);
        countryName = PrefsUtil.getCountry(this);
        cNameView = findViewById(R.id.countryname);
        displayCountrySelection(countryName);
        
        
        contactListRView = findViewById(R.id.contactList);
        contactListRView.setAdapter(myAdapter);
        contactListRView.setLayoutManager(new LinearLayoutManager(getApplicationContext()));

        ItemClickSupport.addTo(contactListRView).setOnItemLongClickListener(this);

        setUpSwipeRefresh();
    }
    
    @Override
    public void onResume(){
        super.onResume();
        retrieveEmergencyContacts(false);
    }
    

    private void setUpSwipeRefresh(){
        swLayout = findViewById(R.id.emg_con_swipe_refresh);
        swLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                retrieveEmergencyContacts(true);
            }
        });
    }

    public void retrieveEmergencyContacts(boolean useServer){
        if (swLayout != null) swLayout.setRefreshing(true);
        // clear the view
        contacts.clear();
        // Retrieve from the phone contacts first
        retrieveCachedPhoneContacts();
        contactUtil.retrieveContacts(new EmgContactUtil.ContactListener() {
            @Override
            public void onReceivedContact(List<Contact> contacts) {
                swLayout.setRefreshing(false);
                if (contacts != null && contacts.size() > 0)processResult(contacts);
                else Snackbar.make(contactListRView, "No Contacts Received", Snackbar.LENGTH_SHORT).setAction("Retry", new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        retrieveEmergencyContacts(false);
                    }
                }).show();
            }
        }, useServer);
        
    }

    

    private void retrieveCachedPhoneContacts(){
        final List<Contact> phoneContactsList = contactUtil.retrieveContactsFromCache("personal");
        contacts.addAll(phoneContactsList);
        myAdapter.notifyDataSetChanged();
    }

    public void processResult(final List<Contact> emergencyContactList){
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                List<Contact> countryContacts = new ArrayList<>();
                if (emergencyContactList != null){
                    for(Contact c: emergencyContactList){
                        if (c.getCountryid().equalsIgnoreCase(countryid))
                            countryContacts.add(c);
                    }
        
                    contacts.addAll(countryContacts);
                    Log.d(TAG, "Processed " + countryContacts.size() + " contacts");
                    myAdapter.notifyDataSetChanged();
                }
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_emergency_contacts, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id){
            case R.id.option_add_contact:
                PhoneContactsActivity.startActivity(this);
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public int getLayoutResourceId() {
        return R.layout.activity_emergency_contact;
    }

    @Override
    public int getColor(){
        return R.color.yellow;
    }

    @Override
    public boolean onItemLongClicked(RecyclerView recyclerView, int position, View v) {
        final Contact contact = contacts.get(position);
        Log.d(TAG, "Selected contact to be removed: " + contact);
        AlertDialog.Builder myBuilder = new AlertDialog.Builder(EmergencyContactActivity.this);
        myBuilder
            .setMessage("Remove Contact")
            .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    removeEmergencyPhoneContactFromCache(contact);
                }
            })
            .setNegativeButton("No", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    dialog.cancel();
                }
            });

        if(TextUtils.equals(contacts.get(position).getType(), Contact.PERSONAL)){
            dialog = myBuilder.create();
            dialog.show();
            return true;
        }else Log.d(TAG, "Attempted to Remove a system contact. Ignoring request.");
        
        return false;
    }

    private void removeEmergencyPhoneContactFromCache(Contact contact){
        List<Contact> localContacts = contactUtil.retrieveContactsFromCache("personal");
        for (Contact tContact : localContacts) { // Used because foreach crashes
            if (contact.getPhone().equals(tContact.getPhone())) {
                localContacts.remove(tContact);
                break;
            }
        }
        
        contacts.remove(contact);
        myAdapter.notifyDataSetChanged();
        contactUtil.cacheEmergencyPhoneContacts(localContacts);
    }

    public void makeCall(String number){
        if (number != null) {
            Intent callIntent = new Intent(Intent.ACTION_CALL);
            callIntent.setData(Uri.parse("tel:" + "+1"+number));
    
            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                Toast.makeText(this, "This Application doesn't have permission to make phone calls", Toast.LENGTH_SHORT).show();
                return;
            }
            this.startActivity(callIntent);
        }else{
            Toast.makeText(this, "Invalid Number. Unable to Make call.", Toast.LENGTH_SHORT).show();
        }
    }

    public void sendEmail(String email) {
        if (email != null) {
            Intent emailIntent = new Intent(Intent.ACTION_SEND);
            emailIntent.putExtra(Intent.EXTRA_EMAIL, new String[]{email});
            emailIntent.putExtra(Intent.EXTRA_SUBJECT, "Emergency");
            emailIntent.setType("message/rfc822");
            try {
                if (dialog != null)dialog.dismiss();
                this.startActivity(Intent.createChooser(emailIntent, "Send mail..."));
            } catch (android.content.ActivityNotFoundException ex) {
                Toast.makeText(this, "There is no email client installed.", Toast.LENGTH_SHORT).show();
            }
        }else{
            Toast.makeText(this, "Invalid email address received.", Toast.LENGTH_SHORT).show();
        }
    }

    public void chooseCountry(View view){
        CountryUtil.getInstance(this).retrieveCountries(new CountryUtil.CountryRetrievedListener() {
            @Override
            public void processCountries(List<Country> countries) {
                if (countries != null){
                    displayCountries(countries);
                }else{
                    Toast.makeText(EmergencyContactActivity.this, "Unable to retrieve countries", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }
    
    private void displayCountries(final List<Country> countries){
        final CountryAdapter arrayAdapter = new CountryAdapter(this, countries);
    
        new AlertDialog.Builder(this)
                .setTitle(getResources().getString(R.string.dialog_country))
                .setAdapter(arrayAdapter,
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                Country selectedCountry = countries.get(which);
                                countryid = selectedCountry.getId();
                                countryName = selectedCountry.getName();
                                displayCountrySelection(countryName);
                                retrieveEmergencyContacts(false);
                            }
                        })
                .show();
    }
    
    private void displayCountrySelection(String country){
        cNameView.setText(country);
    }
}
