package com.cirp.mfisheries.fewer.msreport;

import android.content.Context;
import android.support.annotation.NonNull;
import android.util.Log;
import android.widget.Toast;

import com.cirp.mfisheries.fewer.msreport.model.PersonReport;
import com.cirp.mfisheries.util.CrashReporter;
import com.cirp.mfisheries.util.NetUtil;
import com.cirp.mfisheries.util.PrefsUtil;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;
import com.koushikdutta.ion.builder.Builders;
import com.squareup.moshi.JsonAdapter;
import com.squareup.moshi.Moshi;
import com.squareup.moshi.Types;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Map;

import id.zelory.compressor.Compressor;

import static com.cirp.mfisheries.util.MediaUtils.COMPRESS_QUALITY;

@SuppressWarnings("WeakerAccess")
public class MSReportUtil {
	private final Context context;
	private final String TAG = "MissingPersonUtil";
	private static final long MAX_DAYS = 5;
	private ArrayList<PersonReport> list;

	public MSReportUtil(Context context) {
		this.context = context;
	}

	public void retrieveReports(final String countryid, final ReportListener listener){
		final String url = NetUtil.API_URL + "missingpersons?countryid=" + countryid;
		makeRequest(url, listener);
	}

	public void retrievePersonalReports(ReportListener listener) {
		final int userid = PrefsUtil.getUserId(context);
		final String url = NetUtil.API_URL + "missingpersons?userid=" + userid;
		makeRequest(url, listener);
	}

	private void makeRequest(@NonNull final String url, @NonNull final ReportListener listener) {
		Log.d(TAG, "Attempting to retrieve information from: " + url);
		if (NetUtil.isOnline(context)) {
			Ion.with(context)
					.load(url)
					.setHeader(NetUtil.API_KEY_PARAM, NetUtil.API_KEY)
					.asJsonArray()
					.setCallback(new FutureCallback<JsonArray>() {
						@Override
						public void onCompleted(Exception e, JsonArray result) {
							try {
								Log.d(TAG, "Callback in retrieve reading by country executed successfully");
								list = new ArrayList<>();
								if (e == null && result != null) {
									Moshi moshi = new Moshi.Builder().build();
									Type listOfCategoryType = Types.newParameterizedType(List.class, PersonReport.class);
									JsonAdapter<List<PersonReport>> jsonAdapter = moshi.adapter(listOfCategoryType);
									List<PersonReport> temp = jsonAdapter.fromJson(result.toString());
									if (temp != null) {
										list.addAll(temp);
										if (cacheReports(list))
											Log.d(TAG, "Successfully cached Reports");
									} else
										throw new Exception("Failed to convert Weather Readings to representation");
								} else if (e != null) throw new Exception(e);

								listener.onReceivedReport(list);
							} catch (Exception ex) {
								ex.printStackTrace();
								Log.d(TAG, "Failed to retrieve information from web service.");
								listener.onReceivedReport(null);
							}
						}
					});
		}else{
			if(canUseCache()){
				Log.d(TAG,"Phone offline preparing to use cache");
				List<PersonReport> reports = retrieveReportsFromCache();
				listener.onReceivedReport(reports);
			}else{
				Log.d(TAG,"Phone not connected, No reports saved in cache prior");
			}
		}
	}

	public boolean canUseCache(){
		// If we are on WiFi, use online
		if (NetUtil.isOnline(context) && NetUtil.isWiFi(context))return false;
		if (PrefsUtil.getReports(context) != null && PrefsUtil.getCategories(context) != null){ // we have data in the cache
			Calendar currTime = Calendar.getInstance();
			Calendar preTime = PrefsUtil.getDMReportCacheTime(context);
			if (preTime != null) {
				long diff = currTime.getTimeInMillis() - preTime.getTimeInMillis();
				long days = diff / (24 * 60 * 60 * 1000);
				return days < MAX_DAYS;
			}
		}
		return false;
	}

	public List<PersonReport> retrieveReportsFromCache(){
		List<PersonReport> reports  = new ArrayList<>();
		String cachedReports = PrefsUtil.getMSReports(context);
		try{
			if(cachedReports != null ){
				JsonParser parser = new JsonParser();
				JsonElement element = parser.parse(cachedReports);
				Moshi moshi = new Moshi.Builder().build();
				Type listOfPostType = Types.newParameterizedType(List.class, PersonReport.class);
				JsonAdapter<List<PersonReport>> jsonAdapter = moshi.adapter(listOfPostType);
				List<PersonReport> json = jsonAdapter.fromJson(element.getAsJsonArray().toString());
				if (json != null) {
					reports.addAll(json);
					Log.d(TAG,"JSON NOT NULL");
				}
			}
		}catch (IOException e){
			e.printStackTrace();
			Toast.makeText(context, "Unable to retrieve DM Reports from cache", Toast.LENGTH_SHORT).show();
			CrashReporter.getInstance(context).log("Retrieve MS Reports").report(e);
		}
		return reports;
	}

	private boolean cacheReports(ArrayList<PersonReport> readingList) {
		String json = new Gson().toJson(readingList);
		if (PrefsUtil.setMSReports(context, json)){
			Log.d(TAG, "Successfully saved Cached sources: " + readingList.size());
			return PrefsUtil.setMSRCacheTime(context, Calendar.getInstance());
		}else{
			Log.d(TAG, "Unable to add sources to cache");
			CrashReporter.getInstance(context).log(Log.ERROR,TAG, "Unable to add sources to cache");
		}
		return false;
	}
	
	public void saveReport(final PersonReport report, File file, final UpdateListener listener){
		final String url = NetUtil.API_URL + "/api/missingpersons";
		if (NetUtil.isOnline(context)){
			Map<String, String> map = report.getValues();
			Builders.Any.B builder = Ion.with(context).load(url);
			if (file != null){
				try{
					Log.d(TAG, "Attempting to compress image");
					file = new Compressor(context).setQuality(COMPRESS_QUALITY).compressToFile(file);
				}catch (Exception e){ Log.e(TAG, "Unable to compress images"); e.printStackTrace(); }
				builder.setMultipartFile("file", "image/*", file);
			}
			// Add each value of the post as part of the request
			for (Map.Entry<String, String> entry : map.entrySet()) {
				builder.setMultipartParameter(entry.getKey(), entry.getValue());
			}
			builder.asJsonObject()
					.setCallback(new FutureCallback<JsonObject>() {
						@Override
						public void onCompleted(Exception e, JsonObject result) {
							if (e == null){
								Log.d(TAG, "Report was successfully created");
								try{
									Moshi moshi = new Moshi.Builder().build();
									JsonAdapter<PersonReport> jsonAdapter = moshi.adapter(PersonReport.class);
									PersonReport temp = jsonAdapter.fromJson(result.toString());
									listener.onReportUpdated(temp);
								}catch (Exception ex){
									ex.printStackTrace();
									Log.d(TAG, "Unable to convert JSON to Object. Using initial report");
									listener.onReportUpdated(report);
								}
							}else{
								e.printStackTrace();
								Toast.makeText(context, "Unable to save Report", Toast.LENGTH_SHORT).show();
								CrashReporter.getInstance(context).report(e);
							}
						}
					});
		}else{
			Map<String, String> map = report.getValues();
			Builders.Any.B builder = Ion.with(context).load(url);
			if (file != null){
				try{
					Log.d(TAG, "Attempting to compress image");
					file = new Compressor(context).setQuality(COMPRESS_QUALITY).compressToFile(file);
				}catch (Exception e){ Log.e(TAG, "Unable to compress images"); e.printStackTrace(); }
				builder.setMultipartFile("file", "image/*", file);
			}
			// Add each value of the post as part of the request
			for (Map.Entry<String, String> entry : map.entrySet()) {
				builder.setMultipartParameter(entry.getKey(), entry.getValue());
			}
			Toast.makeText(context, "Device not online. " +
					"Unable to make post at this time. " +
					"Ensure device connected and try again", Toast.LENGTH_SHORT).show();
		}
	}
	
	public void deleteReport(PersonReport report, final UpdateListener listener) {
		if (report.userid.equals(String.valueOf(PrefsUtil.getUserId(context)))){
			final String url = NetUtil.API_URL + "/api/missingpersons/"+ report.id;
			Ion.with(context)
					.load("DELETE", url)
					.setHeader(NetUtil.API_KEY_PARAM, NetUtil.API_KEY)
					.asString()
					.setCallback(new FutureCallback<String>() {
						@Override
						public void onCompleted(Exception e, String result) {
						
						}
					});
		}
	}
	
	
	
	public interface ReportListener{
		void onReceivedReport(List<PersonReport> reports);
	}
	public interface UpdateListener {
		void onReportUpdated(PersonReport report);
	}
}
