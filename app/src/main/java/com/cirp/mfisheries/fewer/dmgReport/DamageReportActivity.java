package com.cirp.mfisheries.fewer.dmgReport;

import android.Manifest;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.TabLayout;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.cirp.mfisheries.R;
import com.cirp.mfisheries.core.Country;
import com.cirp.mfisheries.core.CountryAdapter;
import com.cirp.mfisheries.core.module.ModuleActivity;
import com.cirp.mfisheries.fewer.dmgReport.fragments.PostListingFragment;
import com.cirp.mfisheries.fewer.dmgReport.fragments.PostMapFragment;
import com.cirp.mfisheries.util.CountryUtil;
import com.cirp.mfisheries.util.NetUtil;
import com.cirp.mfisheries.util.PrefsUtil;
import com.cirp.mfisheries.util.adapters.ViewPagerAdapter;

import java.util.List;


public class DamageReportActivity extends ModuleActivity {

    static final int CREATE_ACTIVITY = 1;
    private static final String TAG = "DamageReport";
    DMReportUtil util;
    PostListingFragment myListingFragment;
    PostListingFragment allListingFragment;
    PostMapFragment mapFragment;
    TabLayout tabLayout;
    Context context;


    boolean setToMyPost = false;
    
    private String countryid;
    private String countryName;
    private TextView cNameView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        context = this.getApplicationContext();


        if (!NetUtil.isOnline(context)) {
            Toast.makeText(this, "Currently Offline. Attempting to use Cache", Toast.LENGTH_SHORT).show();
        }

        module = new DamageReport(this);
        util = new DMReportUtil(this);
        
        // Setup UI Components
        FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(DamageReportActivity.this, CreateReportActivity.class);
                startActivityForResult(intent, CREATE_ACTIVITY);
            }
        });

        // Request Permission needed to access internet, capture multimedia and store data
        permissions = new String[]{ Manifest.permission.ACCESS_FINE_LOCATION };
        requestPermissions();
    
        countryid = PrefsUtil.getCountryId(this);
        countryName = PrefsUtil.getCountry(this);
        cNameView = findViewById(R.id.countryname);
        displayCountrySelection(countryName);
    }

    @Override
    public void onResume(){
        super.onResume();
        ensureCountrySelected();
        setupViewPager();
    }
    
    public void ensureCountrySelected(){
        if (PrefsUtil.getCountryId(this).equals("-1")){
            new AlertDialog.Builder(this)
                    .setTitle("Damage Reporting")
                    .setMessage("Setup for FEWER was incomplete. Restart application and try again")
                    .setPositiveButton("Sign In", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            dialogInterface.dismiss();
                        }
                    })
                    .setNegativeButton("Go Back", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            finish();
                        }
                    })
                    .setOnDismissListener(new DialogInterface.OnDismissListener() {
                        @Override
                        public void onDismiss(DialogInterface dialog) {
                            DamageReportActivity.super.ensureRegistered();
                        }
                    })
                    .show();
        }
    }

    protected void setupViewPager() {
        final ViewPager viewPager = findViewById(R.id.tabanim_viewpager);
        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());
        tabLayout = findViewById(R.id.tabanim_tabs);
        
        if (viewPager != null){
            addFragment2Adapter(adapter);
            viewPager.setAdapter(adapter);
	
	        tabLayout.setupWithViewPager(viewPager);
            tabLayout.setBackgroundColor(ContextCompat.getColor(this,getColor()));
            tabLayout.setSelectedTabIndicatorColor(ContextCompat.getColor(this,getColorDark()));
            if (setToMyPost) tabLayout.getTabAt(1).select(); //TODO works but filter not working when loaded directly
        }
    }

    protected void addFragment2Adapter(ViewPagerAdapter adapter) {
	    // Setup the Fragment for all post generated within the country
	    if (allListingFragment == null)
		    allListingFragment = new PostListingFragment();
	    Bundle bundle = new Bundle();
	    bundle.putString("countryid", countryid);
	    allListingFragment.setArguments(bundle);
	    allListingFragment.setCountryid(countryid);
        adapter.addFrag(allListingFragment, getString(R.string.all_dmg_report));
	
	    // Setup the fragment for the personal posts
	    myListingFragment = new PostListingFragment();
        myListingFragment.setPersonal(true);
	    bundle = new Bundle();
	    bundle.putBoolean("personal", true);
	    allListingFragment.setArguments(bundle);
	    adapter.addFrag(myListingFragment, getString(R.string.private_dmg_report));

//        mapFragment = new PostMapFragment();
//        adapter.addFrag(mapFragment, getString(R.string.country_dmg_report));
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        // Check which request we're responding to
        if (requestCode == CREATE_ACTIVITY) {
            // Make sure the request was successful
            if (resultCode == RESULT_OK) {
                Log.d(TAG, "Damage Report was Successfully Created");
                setToMyPost = true;
            }
        }
    }

    @Override
    public int getLayoutResourceId() {
        return R.layout.activity_damage_report;
    }

    @Override
    public int getColor(){
        return R.color.yellow;
    }
    @Override
    public int getColorDark(){return  R.color.yellowDark; }
    
    public void chooseCountry(View view) {
        CountryUtil.getInstance(this).retrieveCountries(new CountryUtil.CountryRetrievedListener() {
            @Override
            public void processCountries(List<Country> countries) {
                if (countries != null){
                    displayCountries(countries);
                }else{
                    Toast.makeText(DamageReportActivity.this, "Unable to retrieve countries", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }
    
    private void displayCountries(final List<Country> countries){
        final CountryAdapter arrayAdapter = new CountryAdapter(this, countries);
        
        new android.support.v7.app.AlertDialog.Builder(this)
                .setTitle(getResources().getString(R.string.dialog_country))
                .setAdapter(arrayAdapter,
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                Country selectedCountry = countries.get(which);
                                countryid = selectedCountry.getId();
                                countryName = selectedCountry.getName();
                                displayCountrySelection(countryName);
                                updateListing();
                            }
                        })
                .show();
    }
    
    private void displayCountrySelection(String country){
        final String countryDesc = "Damage Reports for: " + country;
        cNameView.setText(countryDesc);
    }
    
    public void updateListing(){
	    setupViewPager();
    }

}
