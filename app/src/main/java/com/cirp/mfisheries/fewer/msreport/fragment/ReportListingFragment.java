package com.cirp.mfisheries.fewer.msreport.fragment;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.cirp.mfisheries.R;
import com.cirp.mfisheries.fewer.msreport.MSReportUtil;
import com.cirp.mfisheries.fewer.msreport.model.PersonReport;
import com.cirp.mfisheries.fewer.msreport.model.ReportListAdapter;
import com.cirp.mfisheries.util.GlideApp;
import com.cirp.mfisheries.util.NetUtil;
import com.cirp.mfisheries.util.PrefsUtil;

import java.util.ArrayList;
import java.util.List;

/**
 */
public class ReportListingFragment extends Fragment {
	
	private final String TAG = "MSReportListing";
	SwipeRefreshLayout swLayout;
	RecyclerView myRecyclerView;
	private Context context;
	private boolean isPersonal;
	private MSReportUtil util;
	private int userid;
	private ReportListAdapter adapter;
	private List<PersonReport> reports;
	private String countryid;
	
	/**
	 * Mandatory empty constructor for the fragment manager to instantiate the
	 * fragment (e.g. upon screen orientation changes).
	 */
	public ReportListingFragment() {
		isPersonal = false;
		this.context = getContext();
	}
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		this.context = getContext();
		userid = PrefsUtil.getUserId(getContext());
		util = new MSReportUtil(getContext());
		reports = new ArrayList<>();
	}
	
	@Override
	public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.report_list, container, false);
		myRecyclerView = view.findViewById(R.id.list);
		myRecyclerView.setHasFixedSize(true);
		RecyclerView.LayoutManager layoutManager = new GridLayoutManager(getContext(), 2);
		myRecyclerView.setLayoutManager(layoutManager);
		
		// Set the adapter
		adapter = new ReportListAdapter(this, reports);
		myRecyclerView.setAdapter(adapter);
		
		// Set the refresh item
		swLayout = view.findViewById(R.id.swipe_refresh);
		swLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
			@Override
			public void onRefresh() {
				retrieveData();
			}
		});
		
		retrieveData();
		return view;
	}
	
	
	public void delete(final PersonReport report){
		util.deleteReport(report, new MSReportUtil.UpdateListener() {
			@Override
			public void onReportUpdated(final PersonReport resultReport) {
				myRecyclerView.post(new Runnable() {
					@Override
					public void run() {
						if (resultReport != null){
							Snackbar.make(myRecyclerView, "Successfully deleted Report", Snackbar.LENGTH_SHORT).show();
							reports.remove(resultReport);
							adapter.notifyDataSetChanged();
						}else{
							Snackbar.make(myRecyclerView, "Unable to delete Report", Snackbar.LENGTH_SHORT).show();
						}
					}
				});
			}
		});
	}
	
	public void retrieveData(String countryid){
		this.countryid = countryid;
		if (swLayout != null)swLayout.setRefreshing(true);
		
		// The Listener is the same regardless of global or personal
		MSReportUtil.ReportListener listener = new MSReportUtil.ReportListener() {
			@Override
			public void onReceivedReport(List<PersonReport> reports) {
				swLayout.setRefreshing(false);
				if (reports != null) {
					Log.d(TAG, "Received:" + reports.size() + " reports");
					updateUIWithPosts(reports);
				}
				else{
					Log.d(TAG, "Unable to retrieve reports.");
					Toast.makeText(getContext(), "Unable to retrieve reports at this time. Try again later.", Toast.LENGTH_SHORT).show();
				}
			}
		};
		
		if (isPersonal){
			Log.d(TAG, "Retrieving personal reports");
			util.retrievePersonalReports(listener);
		}else{
			try {
				if(util == null){
					util = new MSReportUtil(getContext());
				}
				if(countryid == null){
					countryid = PrefsUtil.getCountryId(getContext());
				}
				util.retrieveReports(countryid, listener);
			}catch (Exception e){
				e.printStackTrace();
			}
		}
	}
	
	
	public void retrieveData() {
		if (this.countryid == null)
			countryid = PrefsUtil.getCountryId(getContext());
		retrieveData(countryid);
	}
	
	public void updateUIWithPosts(List<PersonReport> posts){
		this.reports = posts;
		adapter = new ReportListAdapter(this, this.reports);
		myRecyclerView.setAdapter(adapter);
	}
	
	public void setPersonal(boolean personal) {
		this.isPersonal = personal;
	}
	
	public void displayImage(PersonReport personReport){
		if (personReport.filepath != null) {
			Dialog builder = new Dialog(getContext());
			builder.requestWindowFeature(Window.FEATURE_NO_TITLE);
			if (builder.getWindow() != null)
				builder.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
			builder.setOnDismissListener(new DialogInterface.OnDismissListener() {
				@Override
				public void onDismiss(DialogInterface dialogInterface) {
				}
			});
			ImageView view = new ImageView(context);
			builder.addContentView(view, new RelativeLayout.LayoutParams(
					ViewGroup.LayoutParams.MATCH_PARENT,
					ViewGroup.LayoutParams.MATCH_PARENT));
			
			final ProgressDialog progressDialog = new ProgressDialog(context);
			progressDialog.setMessage("please wait");
			
			String url = NetUtil.SITE_URL + personReport.filepath;
			Log.d(TAG, "Attempting to view full details for: " + url);
			
			GlideApp.with(context)
					.load(url)
					.placeholder(R.drawable.loading)
					.centerCrop()
					.into(view);
			
			builder.show();
//			progressDialog.show();
		}else{
			Toast.makeText(context, "Unable to display report. No image available for report selected.", Toast.LENGTH_SHORT).show();
		}
	}
}
