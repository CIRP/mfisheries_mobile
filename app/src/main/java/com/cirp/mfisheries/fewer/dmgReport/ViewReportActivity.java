package com.cirp.mfisheries.fewer.dmgReport;

import android.Manifest;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.cirp.mfisheries.R;
import com.cirp.mfisheries.core.module.ModuleActivity;
import com.cirp.mfisheries.core.module.ModuleFactory;
import com.cirp.mfisheries.fewer.dmgReport.model.Post;
import com.cirp.mfisheries.nav.MapMarkerDM;
import com.cirp.mfisheries.nav.NavigationActivity;
import com.cirp.mfisheries.util.GlideApp;
import com.cirp.mfisheries.util.LocationUtil;
import com.cirp.mfisheries.util.NetUtil;

public class ViewReportActivity extends ModuleActivity {
    private static final String TAG = "ViewDMReport";
    private DMReportUtil util;
    private Post post;
    
    TextView name;
    TextView description;
    TextView username;
    TextView date;
    TextView category;
    TextView cost;
    TextView isPublic;
    TextView msgLocation;
	ImageView icon;
	String resourceUrl;
    String share = "";

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_damage_report, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case R.id.action_share:
                shareReport(post);
                Intent shares = new Intent(Intent.ACTION_SEND);
                shares.setType("text/plain");
                shares.putExtra(Intent.EXTRA_SUBJECT, "Damage Report by: "+ post.user);
                shares.putExtra(Intent.EXTRA_TEXT,share);
                startActivity(Intent.createChooser(shares,"Share Via"));
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
    
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        module = new DamageReport(this);
        util = new DMReportUtil(this);
        // Request Permission needed to access internet, capture multimedia and store data
        permissions = new String[]{
                Manifest.permission.ACCESS_FINE_LOCATION,
        };
        requestPermissions();
        // Initialize UI Components
        init();
        
        try {
            // Retrieve DM Report that was passed as a parcelable
            this.post = getIntent().getExtras().getParcelable("post");
            if (post != null){
	            Log.d(TAG, "Retrieve the following post through activity intent: " + post);
	            bindUI(post);
            }else{
            	Log.d(TAG, "No Post was retrieved");
            	finish();
            }
        }catch (Exception e){e.printStackTrace(); finish();}
    }
    
    public void init(){
        name = findViewById(R.id.name);
        description = findViewById(R.id.description);
        username = findViewById(R.id.username);
        date = findViewById(R.id.date);
        category = findViewById(R.id.category);
        cost = findViewById(R.id.cost);
        isPublic = findViewById(R.id.audience);
        msgLocation = findViewById(R.id.msgLocation);
	
	    icon =  findViewById(R.id.icon);
    }
    
    public void bindUI(Post post){
        if (post != null){
            name.setText(String.format("Title: %s", post.name));
	        username.setText(String.format("Created By: %s", post.fullname));
	        category.setText(String.format("Category: %s", this.post.category));
	        if (this.post.isPublic.equals("1"))isPublic.setText(String.format("Audience: %s", "Everyone"));
	        else isPublic.setText(String.format("Audience: %s", "Private"));
	        
            // Optional Fields
	        if (post.description != null && post.description.length() > 2) {
		        description.setText(String.format("Description:\n%s", post.description));
	        }else description.setVisibility(View.GONE);
	        
            if (post.aDate != null) {
            	if (!post.aDate.equalsIgnoreCase("none"))
            	    date.setText(String.format("Created On: %s", post.aDate));
	            else date.setVisibility(View.GONE);
            }
            else date.setVisibility(View.GONE);
            
            if (post.cost != null && post.cost.length() > 1){
            	if (!post.cost.equalsIgnoreCase("0.0"))
            	    cost.setText(String.format("Cost: %s", post.cost));
            	else cost.setVisibility(View.GONE);
            }else cost.setVisibility(View.GONE);
            
            
            
            
            if (post.latitude != null && post.longitude != null) {
                // Using the same Strategy from Alert
                String loc = LocationUtil.getLocationString(Double.parseDouble(post.latitude), Double.parseDouble(post.longitude));
                MapMarkerDM mapMarkerDM = getMapMarker(post);
                msgLocation.setTag(mapMarkerDM);
                msgLocation.setText(loc);
                msgLocation.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        openMap(view);
                    }
                });
            }else{
                Log.d(TAG, "Post has no location data");
                msgLocation.setVisibility(View.GONE);
            }
            
            // Attempt to load Resource and set operations based on Type
	        configureAttachedResource(post);
        }
    }
	
	private void configureAttachedResource(final Post current) {
    	View.OnClickListener listener = new View.OnClickListener() {
		    @Override
		    public void onClick(View view) {
				launchViewer(current);
		    }
	    };
    	
		if (current.filetype.contains("text")){
			icon.setImageResource(R.drawable.ic_content_paste_black);
			icon.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View view) {
					displayText(current);
				}
			});
		}else if (current.filetype.contains("image")){
			displayImage(current);
			icon.setOnClickListener(listener);
		}else if (current.filetype.contains("video")) {
			displayImage(current);
			icon.setOnClickListener(listener);
		}else if (current.filetype.contains("audio")) {
			icon.setImageResource(R.drawable.ic_play_circle_outline_black_36dp);
			icon.setOnClickListener(listener);
		}else{
			icon.setImageResource(R.drawable.ic_content_paste_black);
			icon.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View view) {
					displayText(current);
				}
			});
		}
	}
	
	private void launchViewer(final Post post){
    	try {
		    final String imageURL = NetUtil.SITE_URL + post.filepath;
		    resourceUrl = imageURL;
		    Log.d(TAG, String.format("Attempting to read %s as %s", imageURL, post.filetype));
		    Intent intent = new Intent();
		    intent.setAction(Intent.ACTION_VIEW);
		    intent.setDataAndType(Uri.parse(imageURL), post.filetype);
		    startActivity(intent);
	    }catch (Exception e ){ e.printStackTrace(); }
	}
	
	private void displayText(final Post post){
		(new AlertDialog.Builder(this))
				.setTitle("Report: " + post.name)
				.setMessage(post.description)
				.setNeutralButton("Close", new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialogInterface, int i) {
						dialogInterface.dismiss();
					}
				})
				.show();
	}
	
	private void displayImage(final Post current){
		final String imageURL = NetUtil.SITE_URL + current.filepath;
		Log.d(TAG, "Requesting File at: " + imageURL);
		resourceUrl = imageURL;
		GlideApp.with(this)
				.load(imageURL)
				.placeholder(R.drawable.loading)
				.fallback(new ColorDrawable(Color.GRAY))
				.centerCrop()
				.into(icon);
	}

    private void shareReport(final Post post){
	    share += "\nTitle: " + post.name;

	    share += "\nCreated By: " + post.fullname;

	    share += "\nCreated On: " + post.aDate;

	    share += "\nCategory: " + post.category;

        String locationText = String.valueOf("Location : https://www.google.com/maps/search/?api=1&query="+post.latitude+","+post.longitude);

        share += "\n" + locationText;

        if (post.filetype.contains("text")){
            share += "\nResource: " + post.description;
        }else if(post.filetype.contains("audio")){
            share += "\nResource: " + resourceUrl;
        }else {
            share += "\nResource: " + resourceUrl;
        }
    }

    @NonNull
    private MapMarkerDM getMapMarker(Post post) {
        MapMarkerDM mapMarkerDM = new MapMarkerDM();
        mapMarkerDM.name = post.userid + "";
        mapMarkerDM.details = post.name;
        mapMarkerDM.latitude = post.latitude;
        mapMarkerDM.longitude = post.longitude;
        return mapMarkerDM;
    }
    
    private void openMap(View view) {
        final MapMarkerDM list = (MapMarkerDM) view.getTag();
        
        if (ModuleFactory.getInstance(this).isModuleInstalled("Navigation")) {
        	Log.d(TAG, "Navigation Module was registered as installed");
            Intent intent = new Intent(this, NavigationActivity.class);
            intent.putExtra("name", list.name);
            intent.putExtra("details", list.details);
            intent.putExtra("lat", list.latitude);
            intent.putExtra("lng", list.longitude);
            startActivity(intent);
        }else{
            Log.d(TAG, "No Navigation module installed. Give user the user the option");
            Snackbar.make(view, "Navigation module is not installed", Snackbar.LENGTH_LONG)
                    .setAction("Open Google Maps", new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {

                            PackageManager pm = getApplicationContext().getPackageManager();
                            boolean isInstalled = isPackageInstalled("com.google.android.apps.maps", pm);

                            if(isInstalled){
                                String urlStr = String.format("google.streetview:cbll=%s,$s", list.latitude, list.longitude);
                                Uri gmmIntentUri = Uri.parse(urlStr); // Create a Uri from an intent string. Use the result to create an Intent.
                                Intent mapIntent = new Intent(Intent.ACTION_VIEW, gmmIntentUri);
                                mapIntent.setPackage("com.google.android.apps.maps");
                                startActivity(mapIntent);
                            }else{
                                Toast.makeText(getApplicationContext(), "Google Maps is not installed on your device cannot display location", Toast.LENGTH_LONG).show();
                            }


                        }
                    })
                    .show();
        }
    }

    private boolean isPackageInstalled(String packageName, PackageManager packageManager){
        try {
            packageManager.getPackageInfo(packageName, 0);
            return true;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
            return false;
        }
    }
    
    @Override
    public int getLayoutResourceId() {
        return R.layout.activity_report_view;
    }
    
    @Override
    public int getColor(){
        return R.color.yellow;
    }
    
    public static void launch(Post post, Context context){
	    Intent intent = new Intent(context, ViewReportActivity.class);
	    intent.putExtra("post",post);
	    context.startActivity(intent);
    }
}
