package com.cirp.mfisheries.fewer;

import android.content.Context;
import android.util.Log;

import com.cirp.mfisheries.R;
import com.cirp.mfisheries.core.module.Module;
import com.cirp.mfisheries.util.PrefsUtil;
import com.google.firebase.messaging.FirebaseMessaging;


public class Fewer extends Module {

    public Fewer(Context context){
        super(context);
    }

    @Override
    protected Module setModuleId() {
        this.moduleId = "FEWER";
        return this;
    }

    @Override
    protected Module setModuleName() {
        this.name = "FEWER";
        return this;
    }

    @Override
    protected Module setIsDisplayed() {
        this.displayed = true;
        return this;
    }

    @Override
    protected Module setImageResource() {
        this.imageResource = R.drawable.icon_fewer;
        return this;
    }

    @Override
    protected Module setNeedsRegistration() {
        this.needsRegistration = true;
        return this;
    }

    @Override
    protected Module setActivityClass() {
        this.activityClass = FewerActivity.class;
        return this;
    }

    @Override
    public void onInstalled() {
        final String countryName = PrefsUtil.getCountry(context).toLowerCase().replaceAll("\\s+","");
        if (countryName.length() > 2){
            final String topicName = String.format("cap-%s", countryName);
            Log.d(moduleId, String.format("%s Module Installed. Attempting to set app to subscript to: %s", name, topicName));
            FirebaseMessaging.getInstance().subscribeToTopic(topicName);
        }else{
            Log.d(moduleId, "Unable to Subscribe to alerts");
        }
    }
}
