package com.cirp.mfisheries.fewer.emergCon;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.GridView;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.cirp.mfisheries.R;

import java.util.ArrayList;

public class EmergencyContactListAdapter extends RecyclerView.Adapter<EmergencyContactListAdapter.MyViewHolder>{

    private static final String TAG = "EmgContactAdapter";
    private LayoutInflater inflater;
    private ArrayList <Contact>  data;
    private EmergencyContactActivity emergencyContactActivity;
    private AlertDialog dialog;

    EmergencyContactListAdapter(EmergencyContactActivity context, ArrayList<Contact> contacts){
        this.emergencyContactActivity = context;
        inflater = LayoutInflater.from(context);
        data = contacts;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.emergency_contact_list_layout, parent, false);
        overrideLayoutProperties(view);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {
        final Contact current = data.get(position);

        holder.contactNameTextView.setText(current.getName());
        holder.countryView.setText(current.getCountry());
        if(TextUtils.equals(current.getType(), Contact.PERSONAL)){
            holder.contactIcon.setImageResource(R.drawable.ic_person_outline_black_36dp);
        }
        // event for call action
        holder.callButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                emergencyContactActivity.makeCall(current.getPhone());
            }
        });
        // event for email  (removes if email not set)
        if (current.getEmail() != null && current.getEmail().length() > 4) {
            holder.emailButton.setVisibility(View.VISIBLE);
            holder.emailButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    emergencyContactActivity.sendEmail(current.getEmail());
                }
            });
        }else{
	        holder.emailButton.setVisibility(View.INVISIBLE);
        }

        holder.shareButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                shareContact(current);
            }
        });

        // display for additional details
        if (current.getDetails().size() > 0){
//            int num_fields = current.getDetails().size()/2;
            holder.numAdditionalView.setVisibility(View.VISIBLE);
            holder.numAdditionalView.setText(R.string.no_add_info);
        }

        holder.viewButton.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view){
                AlertDialog.Builder myBuilder = new AlertDialog.Builder(emergencyContactActivity);
                @SuppressLint("InflateParams")
                View myView = inflater.inflate(R.layout.emergency_contact_dialog, null);
                ImageButton callButton = myView.findViewById(R.id.myCallButton);
                ImageButton emailButton = myView.findViewById(R.id.myEmailButton);
                TextView contactName = myView.findViewById(R.id.contactName);
                TextView phoneNumber = myView.findViewById(R.id.phoneNumberView);
                TextView email = myView.findViewById(R.id.emailView);

                contactName.setText(current.getName());
                phoneNumber.setText(String.format(emergencyContactActivity.getString(R.string.phone_no), current.getPhone()));
                email.setText(String.format(emergencyContactActivity.getString(R.string.email), current.getEmail()));

                callButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        emergencyContactActivity.makeCall(current.getPhone());
                    }
                });

                // Only show button and textview if email is present
                if (current.getEmail() != null && current.getEmail().length() > 4) {
                    email.setVisibility(View.VISIBLE); // Make text visible
                    emailButton.setVisibility(View.VISIBLE); // make action button visible
                    emailButton.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            emergencyContactActivity.sendEmail(current.getEmail());
                        }
                    });
                }else{
                    email.setVisibility(View.GONE);
                }

                // process for additional details
                if (current.getDetails().size() > 0){
                    GridView gridView = myView.findViewById(R.id.additional_details);
                    gridView.setVisibility(View.VISIBLE);
                    gridView.setAdapter(new ArrayAdapter<>(emergencyContactActivity,R.layout.emergency_contact_add_cell,current.getDetails()));
                }

                myBuilder.setView(myView);
                dialog = myBuilder.create();
                if (dialog.getWindow() != null)
                    dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                dialog.show();
            }
        } );



        // Display Modal/Dialog when user selects the contact
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AlertDialog.Builder myBuilder = new AlertDialog.Builder(emergencyContactActivity);
                @SuppressLint("InflateParams")
                View myView = inflater.inflate(R.layout.emergency_contact_dialog, null);
                ImageButton callButton = myView.findViewById(R.id.myCallButton);
                ImageButton emailButton = myView.findViewById(R.id.myEmailButton);
                TextView contactName = myView.findViewById(R.id.contactName);
                TextView phoneNumber = myView.findViewById(R.id.phoneNumberView);
                TextView email = myView.findViewById(R.id.emailView);

                contactName.setText(current.getName());
                phoneNumber.setText(String.format(emergencyContactActivity.getString(R.string.phone_no), current.getPhone()));
                email.setText(String.format(emergencyContactActivity.getString(R.string.email), current.getEmail()));

                callButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        emergencyContactActivity.makeCall(current.getPhone());
                    }
                });

                // Only show button and textview if email is present
                if (current.getEmail() != null && current.getEmail().length() > 4) {
                    email.setVisibility(View.VISIBLE); // Make text visible
                    emailButton.setVisibility(View.VISIBLE); // make action button visible
                    emailButton.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            emergencyContactActivity.sendEmail(current.getEmail());
                        }
                    });
                }else{
	                email.setVisibility(View.GONE);
                }

                // process for additional details
                if (current.getDetails().size() > 0){
                    GridView gridView = myView.findViewById(R.id.additional_details);
                    gridView.setVisibility(View.VISIBLE);
                    gridView.setAdapter(new ArrayAdapter<>(emergencyContactActivity,R.layout.emergency_contact_add_cell,current.getDetails()));
                }

                myBuilder.setView(myView);
                dialog = myBuilder.create();
                if (dialog.getWindow() != null)
                    dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                dialog.show();
            }
        });
    }

    private void shareContact(final Contact contact){
        StringBuilder share = new StringBuilder();

        share.append("Name: ").append(contact.getName());

        share.append("\nType: ").append(contact.getType());

        share.append("\nCountry: ").append(contact.getCountry());

        share.append("\nPhone: ").append(contact.getPhone());

        share.append("\nEmail: ").append(contact.getEmail());

        if (contact.getDetails().size() > 0){
            ArrayList<String> details = new ArrayList<>();

            details = contact.getDetails();

            share.append("\n\nAdditional: ");

            for(int i = 0; i < details.size(); i+=2 ){
                share.append("\n").append(details.get(i)).append(" : ").append(details.get(i + 1));
            }

//            share += "\nAdditional: " + String.valueOf(contact.getDetails());
        }
        Intent shares = new Intent(Intent.ACTION_SEND);
        shares.setType("text/plain");

        shares.putExtra(Intent.EXTRA_SUBJECT, "Emergecny Contact for: "+ contact.getName());
        shares.putExtra(Intent.EXTRA_TEXT, share.toString());
        this.emergencyContactActivity.startActivity(Intent.createChooser(shares,"Share Via"));
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    class MyViewHolder extends RecyclerView.ViewHolder{

        TextView contactNameTextView;
        TextView countryView;
        TextView numAdditionalView;
        Button callButton;
        Button emailButton;
        Button shareButton;
        Button viewButton;
        ImageView contactIcon;

        MyViewHolder(View itemView){
            super(itemView);
            contactNameTextView = itemView.findViewById(R.id.name);
            countryView = itemView.findViewById(R.id.country);
            shareButton = itemView.findViewById(R.id.shareButton);
            viewButton = itemView.findViewById(R.id.infoButton);
            callButton = itemView.findViewById(R.id.myCallIcon);
            emailButton = itemView.findViewById(R.id.myEmailIcon);
            contactIcon =  itemView.findViewById(R.id.contact_icon);
            numAdditionalView = itemView.findViewById(R.id.num_additional);
        }
    }

    private void overrideLayoutProperties(View view){
        if(Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP){
            Log.d(TAG, "Detected less than lollipop");
            View card = view.findViewById(R.id.card_view);
            ViewGroup.LayoutParams params = card.getLayoutParams();
            params.height = 300;
            ((ViewGroup.MarginLayoutParams)params).setMargins(0,0,0,0);
            card.setLayoutParams(params);
        }
    }
}
