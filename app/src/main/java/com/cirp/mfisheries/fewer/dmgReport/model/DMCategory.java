package com.cirp.mfisheries.fewer.dmgReport.model;

public class DMCategory {
    public int countryid;
    public int id;
    public String name;

    public DMCategory() {
    }

    public DMCategory(int id, String name, int countryid) {
        this.id = id;
        this.name = name;
        this.countryid = countryid;
    }
}
