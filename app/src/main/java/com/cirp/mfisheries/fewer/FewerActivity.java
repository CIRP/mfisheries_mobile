package com.cirp.mfisheries.fewer;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.cirp.mfisheries.R;
import com.cirp.mfisheries.core.config.ConfigUtil;
import com.cirp.mfisheries.core.module.ModuleActivity;
import com.cirp.mfisheries.core.module.ModuleRecyclerAdapter;
import com.cirp.mfisheries.core.register.RegisterActivity;
import com.cirp.mfisheries.util.PrefsUtil;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.iid.FirebaseInstanceId;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;


public class FewerActivity extends ModuleActivity {

    private static final String TAG = "FEWERActivity";
	private List<String> fewerModules;
	private ModuleRecyclerAdapter adapter;
	
	@Override
    public int getLayoutResourceId() {
        return R.layout.layout_fewer;
    }

	@Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        module = new Fewer(this);
        module.onInstalled();
		Boolean isFirstRun = getSharedPreferences("PREFERENCE", MODE_PRIVATE)
				.getBoolean("isFirstRun", true);

		if (isFirstRun) {
			//show start activity
			startActivity(new Intent(getApplicationContext(), FEWEROnboard.class));
//            Toast.makeText(MainActivity.this, "First Run", Toast.LENGTH_LONG)
//                    .show();
		}
		getSharedPreferences("PREFERENCE", MODE_PRIVATE).edit()
				.putBoolean("isFirstRun", false).commit();
		
		(new ConfigUtil(this)).checkVersion();
	    setUpGrid();
	    setUpFEWERModules();
	    initializeFirebase();
    }
	
	@Override
	public void onResume() {
		super.onResume();
		setGreeting();
	}
	
	protected void setToolBar(Toolbar toolbar){
		super.setToolBar(toolbar);
		if (getSupportActionBar()!=null)getSupportActionBar().setDisplayHomeAsUpEnabled(false);
	}
	
	private void setUpGrid() {
		fewerModules = new ArrayList<>();
		RecyclerView gridView = this.findViewById(R.id.gridView);
		gridView.setHasFixedSize(true);
		RecyclerView.LayoutManager layoutManager = new GridLayoutManager(this, 2);
		gridView.setLayoutManager(layoutManager);
		
		adapter = new ModuleRecyclerAdapter(this, fewerModules);
		gridView.setAdapter(adapter);
	}

    private void setUpFEWERModules(){
	    (new Thread() {
		    public void run() {
			    fewerModules.addAll(Arrays.asList(getResources().getStringArray(R.array.fewer_submodules)));
			    Log.d(TAG, String.format("Found %d submodules", fewerModules.size()));
			    runOnUiThread(new Runnable() {
				    public void run() {
					    adapter.notifyDataSetChanged();
				    }
			    });
		    }
	    }).start();
    }
    
    @Override
    public int getColor(){
        return R.color.red;
    }
	
    /* ***** The Following methods are used to provide functionality to the bottom bar *****/
	
	/**
	 * The method will be used to launch the process to authenticate the user locally and through Firebase.
	 * @param view The view that is calling the method
	 */
	public void signIn(View view) {
		Intent signIn = new Intent(FewerActivity.this, RegisterActivity.class);
		startActivityForResult(signIn, RegisterActivity.SIGN_IN_REQUEST);
	}
	
	/**
	 * The signOut method will clear user related files and log out the user locally and within Firebase
	 * @param view The view that is calling the method
	 */
	public void signOut(View view) {
		FirebaseAuth.getInstance().signOut();
		
		if (PrefsUtil.clearPreferences(this)) {
			Toast.makeText(this, "Successfully logged out.", Toast.LENGTH_SHORT).show();
		} else {
			Toast.makeText(this, "Unable to successfully log out at this time. Try again later.", Toast.LENGTH_SHORT).show();
		}
		setGreeting();
	}
	
	/**
	 * The setGreeting method will be used to control the operations that the button will perform
	 */
	public void setGreeting() {
		final String username = PrefsUtil.getFirstName(this);
		final TextView greeting = findViewById(R.id.greetingText);
		final Button button = findViewById(R.id.register);
		greeting.setText(String.format("%s%s", getString(R.string.welcome), username));
		if (username.equalsIgnoreCase("guest")) {
			button.setText(R.string.register);
			button.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					signIn(v);
				}
			});
		} else {
			button.setText(R.string.logout);
			button.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					signOut(v);
				}
			});
		}
	}
	
	public void initializeFirebase(){
		PrefsUtil.setToken(this, FirebaseInstanceId.getInstance().getToken());
	}
}
