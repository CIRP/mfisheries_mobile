package com.cirp.mfisheries.fewer.alerts;

import android.content.Context;

import com.cirp.mfisheries.R;
import com.cirp.mfisheries.alerts.Alerts;
import com.cirp.mfisheries.core.module.Module;

public class FEWERAlerts extends Alerts {
	
	public FEWERAlerts(Context context) {
		super(context);
	}
	@Override
	protected Module setModuleId() {
		this.moduleId = "FEWER Alerts";
		return this;
	}
	@Override
	protected Module setModuleName() {
		this.name = "FEWER Alerts";
		return this;
	}
	
	
	// Set the custom icon for the FEWER Alerts
	@Override
	protected Module setImageResource() {
		this.imageResource = R.drawable.icon_fewer_alert;
		return this;
	}
	
	@Override
	protected Module setActivityClass() {
		this.activityClass = FEWERAlertsActivity.class;
		return this;
	}
}
