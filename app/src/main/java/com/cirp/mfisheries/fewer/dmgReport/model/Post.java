package com.cirp.mfisheries.fewer.dmgReport.model;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.HashMap;
import java.util.Map;

public class Post implements Parcelable{


    public String name;
    public String description;
    public String userid;
    public String countryid;
    public String latitude;
    public String longitude;
    public String filetype;
    public String filepath;
    public String aDate;
    public String category;
    public String isPublic;
    public String createdby;
    public String cost;
    public String fullname;
    public String user;
    public String id;

    public Post(){
    
    }
    
    // Order must be maintained with Write to Parcel
    Post(Parcel in) {
        name = in.readString();
        description = in.readString();
        userid = in.readString();
        countryid = in.readString();
        latitude = in.readString();
        longitude = in.readString();
        filetype = in.readString();
        filepath = in.readString();
        aDate = in.readString();
        category = in.readString();
        isPublic = in.readString();
        createdby = in.readString();
        cost = in.readString();
        fullname = in.readString();
        user = in.readString();
        id = in.readString();
    }
    
    public static final Creator<Post> CREATOR = new Creator<Post>() {
        @Override
        public Post createFromParcel(Parcel in) {
            return new Post(in);
        }
        
        @Override
        public Post[] newArray(int size) {
            return new Post[size];
        }
    };
    
    public Map<String, String> getValues(){
        HashMap<String, String> map = new HashMap<>();
        map.put("userid", userid);
        map.put("name", name);
        map.put("description", description);
        map.put("countryid", countryid);
        map.put("filetype", filetype);
        map.put("category", category);
        map.put("latitude", latitude);
        map.put("longitude", longitude);
        map.put("isPublic", isPublic);
        map.put("filepath", filepath);
        map.put("aDate", aDate);
        map.put("createdby", createdby);
        map.put("cost", cost);
        return  map;
    }
    
    public String toString(){
        StringBuilder stb = new StringBuilder();
        Map <String, String> map = getValues();
        stb.append("{");
        for (Map.Entry<String, String> entry : map.entrySet()) {
            stb.append("\"")
                .append(entry.getKey())
                .append("\"")
                .append(":")
                .append("\"")
                .append(entry.getValue())
                .append("\"")
                .append(",");
        }
        stb.append("}");
        return stb.toString();
    }

    public boolean isValid(){
        return userid.length() > 0 &&
                name.length() > 3 &&
                countryid.length() > 0 &&
                filetype.length() > 3 &&
                category.length() > 3 &&
                latitude.length() > 5 &&
                longitude.length() > 5 &&
                aDate.length() > 5 &&
                isPublic.length() > 0;
    }
    
    @Override
    public int describeContents() {
        return 0;
    }
    
    // Order must be maintained with constructor
    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(name);
        parcel.writeString(description);
        parcel.writeString(userid);
        parcel.writeString(countryid);
        parcel.writeString(latitude);
        parcel.writeString(longitude);
        parcel.writeString(filetype);
        parcel.writeString(filepath);
        parcel.writeString(aDate);
        parcel.writeString(category);
        parcel.writeString(isPublic);
        parcel.writeString(createdby);
        parcel.writeString(cost);
        parcel.writeString(fullname);
        parcel.writeString(user);
        parcel.writeString(id);
    }
}
