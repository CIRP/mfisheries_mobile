package com.cirp.mfisheries.fewer.supportFewer;

import android.content.Context;

import com.cirp.mfisheries.core.module.Module;

public class SupportFEWER extends Module{

    public SupportFEWER(){super();}

    public SupportFEWER(Context context){super(context);}

    @Override
    protected Module setModuleId() {
        this.moduleId = "SupportFEWER";
        return this;
    }

    @Override
    protected Module setModuleName() {
        this.name = "Support FEWER";
        return this;
    }

    @Override
    protected Module setIsDisplayed() {
        this.displayed = true;
        return this;
    }

    @Override
    protected Module setImageResource() {
        return null;
    }

    @Override
    protected Module setNeedsRegistration() {
        this.needsRegistration = true;
        return this;
    }

    @Override
    protected Module setActivityClass() {
        this.activityClass = supportFEWERActivity.class;
        return this;
    }
}
