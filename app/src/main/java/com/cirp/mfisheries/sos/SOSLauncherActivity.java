package com.cirp.mfisheries.sos;

import android.Manifest;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;

import com.cirp.mfisheries.App;
import com.cirp.mfisheries.MainActivity;
import com.cirp.mfisheries.R;
import com.cirp.mfisheries.core.Country;
import com.cirp.mfisheries.core.CountryLoc;
import com.cirp.mfisheries.core.config.ConfigUtil;
import com.cirp.mfisheries.core.location.Geofencer;
import com.cirp.mfisheries.core.location.LocationService;
import com.cirp.mfisheries.core.module.ModuleActivity;
import com.cirp.mfisheries.core.register.RegisterActivity;
import com.cirp.mfisheries.core.register.RegisterHelper;
import com.cirp.mfisheries.util.CountryUtil;
import com.cirp.mfisheries.util.ModuleUtil;
import com.cirp.mfisheries.util.PrefsUtil;
import com.cirp.mfisheries.util.StartupUtil;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.google.firebase.iid.FirebaseInstanceId;

import java.io.IOException;
import java.util.List;

import static com.cirp.mfisheries.core.location.LocationService.startLocationServices;
import static com.cirp.mfisheries.core.location.LocationService.trackingEnabled;

public class SOSLauncherActivity extends ModuleActivity {

    public String TAG = "SOSLAUNCHER";
    public String status = "Unexecuted";
    private SharedPreferences preferences;
    private CountryUtil countryUtil;
    private StartupUtil startupUtil;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        LocationService.startLocationServices(getApplicationContext());
        
        // Retrieve Utility for country-related requests
        countryUtil = CountryUtil.getInstance(this);
        // Retrieve Utility for startup-related requests
        startupUtil = StartupUtil.getInstance(this);
        //Retrieve SharedPreferences object
        preferences = PreferenceManager.getDefaultSharedPreferences(SOSLauncherActivity.this);

        new LocationAsync().execute();

        setContentView(R.layout.activity_soslauncher);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
    }

    public void SOS_Setup(){
        (new ConfigUtil(this)).checkVersion();
        retrieveBackgroundData();
        initializeFirebase();


        permissions = new String[]{
                Manifest.permission.CALL_PHONE,
                Manifest.permission.ACCESS_FINE_LOCATION
        };
        requestPermissions();
    }

    @Override
    public void onPermissionGranted(String permission) {
        super.onPermissionGranted(permission);
        Log.d(TAG,"Permissions have been granted");
//        startupUtil.checkRegistration();
        checkFirstOpen();
    }

    @Override
    public int getLayoutResourceId() {
        return R.layout.activity_soslauncher;
    }

    public void checkFirstOpen(){
        if ( startupUtil.isFirstLaunch()) {
            Log.d(TAG, "The SOS App was opened for the first time");
            Log.d(TAG, "Signing In Process Request");
            startupUtil.requestCountry(new StartupUtil.CountrySelectionHandler() {
                public void handleCountrySelection(Country selectedCountry) {
                    Intent signIn = new Intent(SOSLauncherActivity.this, RegisterActivity.class);
                    startActivityForResult(signIn, RegisterActivity.SIGN_IN_REQUEST);
                }
            });

        }else{
            Log.d(TAG, "The SOS App was previously opened");
            //launch SOS activity
            if (!preferences.getBoolean("loc_service_stopped", true) && PrefsUtil.hasCountry(this)){
//                startLocationServices();
                Log.d(TAG, "Launcher trying to start location service");
            }

        }
    }

    public void launchSOS(View view){
        try{
            Intent intent = new Intent(SOSLauncherActivity.this, SOSActivity.class);
            startActivity(intent);
        }catch (Exception e){
            Log.d(TAG, "Unable to launch SOS");

        }
    }

    private void startLocationServices() {
        LocationService.startLocationServices(getApplicationContext());
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data){
        RegisterHelper helper = new RegisterHelper(this);
        PrefsUtil.setFirstOpen(this, false);
        helper.onActivityResult(this, requestCode, resultCode, data);
//        Intent intent = new Intent(SOSLauncherActivity.this, SOSActivity.class);
//        startActivity(intent);
    }

    class LocationAsync extends AsyncTask<Void, Void, Void>{
        @Override
        protected Void doInBackground(Void... params) {
            LocationService.startLocationServices(getApplicationContext());
            return null;
        }
        @Override
        protected void onPostExecute(Void result) {
            Boolean status = preferences.getBoolean("loc_service_stopped", true);
            Log.d(TAG, "Location Service Stopped: " + status);
            SOS_Setup();
        }
    }


    // TODO put in separate class
    /**
     * A utility method to retrieve information that does not require user intervention
     */
    public void retrieveBackgroundData(){
        // Retrieve Country Locations for offline use
        CountryUtil.getInstance(this).retrieveCountryLocations(new CountryUtil.CountryLocRetrievedListener() {
            public void processCountryLocations(List<CountryLoc> countryLocs) {
                if (countryLocs != null) Log.d(TAG, "Retrieved locations: " + countryLocs);
                else Log.d(TAG, "Unable to retrieve country locations");
            }
        });
    }

    /**
     * A utility method to ensure that FireBase is available for the system's use
     */
    public void initializeFirebase(){
        PrefsUtil.setToken(this, FirebaseInstanceId.getInstance().getToken());
    }
}
