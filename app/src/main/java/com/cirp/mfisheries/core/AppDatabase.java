package com.cirp.mfisheries.core;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.RoomDatabase;

import com.cirp.mfisheries.alerts.model.Like;
import com.cirp.mfisheries.alerts.model.LikeDao;

@Database(entities = {Like.class}, version = 1)
public abstract class AppDatabase extends RoomDatabase {
    public abstract LikeDao likeDao();
}