package com.cirp.mfisheries.core.location;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.ActivityManager;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.IBinder;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.v4.app.NotificationCompat;
import android.support.v4.content.ContextCompat;
import android.telephony.PhoneStateListener;
import android.telephony.SignalStrength;
import android.telephony.TelephonyManager;
import android.util.Log;

import com.cirp.mfisheries.BuildConfig;
import com.cirp.mfisheries.R;
import com.cirp.mfisheries.core.DatabaseHandler;
import com.cirp.mfisheries.util.NetUtil;
import com.cirp.mfisheries.util.NotifyUtil;
import com.cirp.mfisheries.util.PrefsUtil;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;
import com.squareup.moshi.JsonAdapter;
import com.squareup.moshi.Moshi;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import static com.cirp.mfisheries.util.NotifyUtil.LOC_NOTIFY_ID;

public class LocationService extends Service implements LocationListener, GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener{

	public static long LOC_TIME_INTERVAL = 300000;
	public final static long LOC_DIST_INTERVAL = 100;
	private static final String TAG = "LocationService";
	public static boolean trackingEnabled, offlineTracksPresent;
	public static String signalStrengthInfo;
	public static Location userLocation;
	protected boolean mRequestingLocationUpdates = true;
	protected GoogleApiClient mGoogleApiClient;
	protected LocationRequest mLocationRequest;
	private int locationCounter, timeMultiplier = 1;
	private String trackType, trackingStatus;
	private NotificationCompat.Builder serviceNotification;
	private SharedPreferences.Editor editor;
	private NotifyUtil util;
	private int notifyID;
	
	@SuppressLint("CommitPrefEdits")
	@Override
	public void onCreate() {
		super.onCreate();
		if (BuildConfig.DEBUG){
			Log.d(TAG, String.valueOf(BuildConfig.DEBUG));
			LOC_TIME_INTERVAL = 600;
		}
		locationCounter = 0;
		Log.d(TAG, "Started mFisheries Location Service");
//		Toast.makeText(this, "Started mFisheries Location Service", Toast.LENGTH_LONG).show();

		editor = PreferenceManager.getDefaultSharedPreferences(LocationService.this).edit();
		util = new NotifyUtil(this);
		try {
			if (ContextCompat.checkSelfPermission(this,Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
				Log.d(TAG, "Creating service");
				getLastLocationState();
				initializePhoneStateListener();
				buildGoogleApiClient();
				displayNotification();
			}
		} catch (Exception ex) {
			Log.wtf(TAG, "Error " + ex.getLocalizedMessage());
		}
	}

	protected void stopLocationUpdates() {
		if(mGoogleApiClient != null && mGoogleApiClient.isConnected()) {
			LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, this);
		}
	}

	protected synchronized void buildGoogleApiClient() {
		mGoogleApiClient = new GoogleApiClient.Builder(this)
				.addApi(LocationServices.API)
				.addConnectionCallbacks(this)
				.addOnConnectionFailedListener(this)
				.build();
		mGoogleApiClient.connect();
	}

	public void onConnectionFailed(@NonNull ConnectionResult result) {
		Log.d(TAG, result.getErrorCode() + "");
	}

	public void onConnectionSuspended(int cause) {
		//   mGoogleApiClient.connect();
	}

	protected void createLocationRequest() {
		mLocationRequest = new LocationRequest();
		mLocationRequest.setInterval(LOC_TIME_INTERVAL);
		mLocationRequest.setFastestInterval(60000);
		mLocationRequest.setSmallestDisplacement(LOC_DIST_INTERVAL);
		mLocationRequest.setPriority(LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY);
	}

	public void onConnected(Bundle connectionHint) {
		Log.d(TAG, "Connected!");
		createLocationRequest();
		LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder().addLocationRequest(mLocationRequest);
		PendingResult<LocationSettingsResult> result = LocationServices.SettingsApi.checkLocationSettings(mGoogleApiClient, builder.build());
		
		result.setResultCallback(new ResultCallback<LocationSettingsResult>() {
			@Override
			public void onResult(@NonNull LocationSettingsResult result) {
				final Status status = result.getStatus();
				switch (status.getStatusCode()) {
					case LocationSettingsStatusCodes.SUCCESS:
						try {
//							userLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
							 LocationServices
									.getFusedLocationProviderClient(getBaseContext())
									.getLastLocation()
									 .addOnSuccessListener(new OnSuccessListener<Location>() {
										@Override
										public void onSuccess(Location location) {
											userLocation = location;
										}
									});
							if (mRequestingLocationUpdates) {
								startLocationUpdates();
							}
						} catch (SecurityException e) {
							e.printStackTrace();
						}
						break;
					case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
						break;
				}
			}
		});
	}

	protected void startLocationUpdates() {
		try {
			LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, this);
		} catch (SecurityException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void onDestroy() {
//		Toast.makeText(this, "Stopped mFisheries Location Service", Toast.LENGTH_LONG).show();
		Log.d(TAG, "Destroying Location Service");
		NotificationManager notifManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
		if (notifManager != null) {
			notifManager.cancel("LOCATION_SERVICE", LOC_NOTIFY_ID);
		}
		stopLocationUpdates();

		super.onDestroy();
	}

	@Override
	public int onStartCommand(Intent intent, int flags, int startId) {
		// Added the start sticky command to force the system to restart the service if terminated - https://developer.android.com/reference/android/app/Service.html
		return Service.START_STICKY;
	}

	private void getLastLocationState() {
		SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(LocationService.this);
		trackingEnabled = preferences.getBoolean("is_tracking_enabled", false);
		offlineTracksPresent = preferences.getBoolean("are_offline_tracks_present", false);
		trackType = preferences.getString("track_type", "start");
		trackingStatus = preferences.getString("tracking_status", "Tracking Disabled");
	}

	private void initializePhoneStateListener() {
		signalStrengthInfo = "No Data";
		PhoneStatusListener phoneStatusListener = new PhoneStatusListener();
		TelephonyManager telephonyManager = (TelephonyManager) this.getSystemService(Context.TELEPHONY_SERVICE);
		if (telephonyManager != null) {
			telephonyManager.listen(phoneStatusListener, PhoneStateListener.LISTEN_SIGNAL_STRENGTHS);
		}
	}

	private void displayNotification() {
		Log.i(TAG, "Displaying Notification");
		if (trackingStatus.equals("Tracking Disabled")) {
			updateNotification();
			return;
		}
		Intent stopServiceIntent = new Intent(this, StopLocationServiceActivity.class);
		PendingIntent pending_intent = PendingIntent.getActivity(this, 0, stopServiceIntent, 0);
		
//		serviceNotification = new NotificationCompat.Builder(this, "default")
//				.setSmallIcon(R.drawable.map_marker_radius)
//				.setContentTitle("mFisheries")
//				.setContentText(trackingStatus)
//				.addAction(R.drawable.ic_stop_white_24dp, "Stop GPS Polling", pending_intent)
//				.setOngoing(true)
//				.setPriority(10);
		
		// TODO Add action instead of hashmaps
		Map <String, Map>actions = new HashMap<>();
		@SuppressLint("UseSparseArrays")
		Map <Integer, Object> act = new HashMap<>();
		act.put(R.drawable.ic_stop_white_24dp, pending_intent);
		actions.put("Stop GPS Polling", act);
		
		util.addActions(actions);
		notifyID = util.notify(getString(R.string.app_name) + "- Tracking", trackingStatus, R.drawable.ic_map_marker_radius_black_24dp, stopServiceIntent);
	}

	// TODO Change to use the notification utility
	private void updateNotification() {
		String message;
		NotificationManager notifyManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
		serviceNotification = new NotificationCompat.Builder(this, "default")
				.setSmallIcon(R.drawable.ic_map_marker_radius_white_24dp)
				.setContentTitle(getString(R.string.app_name));
		
		if (trackingEnabled) {
			message = "Tracking Enabled";
			Log.d(TAG, message);
			Intent stopServiceIntent = new Intent(this, StopLocationServiceActivity.class);
			PendingIntent pending_intent = PendingIntent.getActivity(this, 0, stopServiceIntent, 0);
			serviceNotification.addAction(R.drawable.ic_stop_white_24dp, "Stop GPS Polling", pending_intent)
					.setOngoing(true)
					.setPriority(10)
					.setContentText(message);
			if (notifyManager != null) {
				notifyManager.notify("LOCATION_SERVICE", LOC_NOTIFY_ID, serviceNotification.build());
			}
		} else {
			message = "Tracking is Disabled";
			Log.d(TAG, message);
//			serviceNotification.setContentText(message);
//			notifyManager.cancel(LOC_NOTIFY_ID);
			if (notifyManager != null) {
				notifyManager.notify("LOCATION_SERVICE", LOC_NOTIFY_ID, serviceNotification.build()); // TODO Determine if this is appropriate
			}
		}
		editor.putString("tracking_status", message);
		editor.apply();
	}

	@Override
		public void onLocationChanged(Location loc) {
		Log.d(TAG, "Location Changed");
		userLocation = loc;
		Log.d(TAG, "Location: " + loc.getLatitude() + " " + loc.getLongitude());
		editor.putFloat("lat", (float) loc.getLatitude());
		editor.putFloat("lng", (float) loc.getLongitude());
		editor.apply();
		Log.d(TAG, "Executing Location Task");
		new LocationTask().execute(loc);
	}

	public void uploadTracks(final Location location, final String trackType) {
		List<TrackPointDM> points = new ArrayList<>();

		if (offlineTracksPresent) {
			try {
				Log.d(TAG, "Offline tracks present");
				DatabaseHandler database = new DatabaseHandler(this, DatabaseHandler.OFFLINE_TRACK_DB_HELPER);
				database.open();
				points = database.offlineTrackDBHelper.getAllOfflineTracks();
				database.close();
			} catch (Exception ex) {
				Log.wtf(TAG, "Error reading offline tracks " + ex.getLocalizedMessage());
			}
		}
		if (points == null)
			points = new ArrayList<>();

		if (location != null) {
			TrackPointDM trackPointDM = createTrackPoint(location, PrefsUtil.getUserId(this));
			Log.d(TAG, trackPointDM.toString());
			points.add(trackPointDM);
		}

		FutureCallback<JsonObject> callback = new FutureCallback<JsonObject>() {
			@Override
			public void onCompleted(Exception e, JsonObject result) {
				if (e == null && result != null) {
					Log.d(TAG, "track sent " + result.toString());

					DatabaseHandler database = new DatabaseHandler(LocationService.this, 2);
					database.open();
					database.offlineTrackDBHelper.deleteAllTracks();
					database.close();
					offlineTracksPresent = false;
				} else if (e != null){
					e.printStackTrace();
					Log.d(TAG, "track not sent " + e.getMessage());
					saveTrack(location, trackType);
				}
			}
		};

		if (points.size() > 0) {
			sendTracks(points, callback);
			Log.d("Points list", points.toString());
		}
	}

	public void sendTracks(List<TrackPointDM> points, FutureCallback<JsonObject> callback) {
		JsonArray jsonArray = getJsonElements(points);

		final String url =  NetUtil.API_URL + NetUtil.POST_TRACK_URL;
		Log.d(TAG, String.format("Attempting to send: %s to %s", jsonArray.toString(), url));
		
		Ion.with(getApplicationContext())
            .load(url)
			.setHeader(NetUtil.API_KEY_PARAM, NetUtil.API_KEY)
            .setJsonArrayBody(jsonArray)
            .asJsonObject()
            .setCallback(callback);
	}

	public JsonArray getJsonElements(List<TrackPointDM> points) {
		Moshi moshi = new Moshi.Builder().build();
		JsonAdapter<List> jsonAdapter = moshi.adapter(List.class);

		String json = jsonAdapter.toJson(points);

		JsonParser parser = new JsonParser();
		return (JsonArray) parser.parse(json);
	}

	public boolean saveTrack(Location location, String trackType) {
		if (location == null)
			return false;
		try {
			if (trackType != null) this.trackType = trackType;
			TrackPointDM trackPoint = createTrackPoint(location, PrefsUtil.getUserId(this));
			DatabaseHandler database = new DatabaseHandler(LocationService.this, DatabaseHandler.OFFLINE_TRACK_DB_HELPER);
			database.open();
			database.offlineTrackDBHelper.addOfflineTrack(trackPoint);
			database.close();
			offlineTracksPresent = true;
			Log.d(TAG, "Saved Track: " + trackPoint.toString());
			return true;
		} catch (Exception ex) {
			Log.wtf(TAG, "Error saving track point " + ex.getLocalizedMessage());
			return false;
		}
	}

	public TrackPointDM createTrackPoint(@NonNull Location location, @NonNull int userId) {
		TrackPointDM trackPointDM = new TrackPointDM();
		trackPointDM.acc = String.valueOf(location.getAccuracy());
		trackPointDM.brg = String.valueOf(location.getBearing());
		trackPointDM.lat = String.valueOf(location.getLatitude());
		trackPointDM.lng = String.valueOf(location.getLongitude());
		trackPointDM.spd = String.valueOf(location.getSpeed());
		Log.d(TAG, "Location Provider was given as: " + location.getProvider());
		if (location.getProvider() == null) {
			trackPointDM.prov = "-1";
		} else if (location.getProvider().equals(LocationManager.GPS_PROVIDER)) {
			trackPointDM.prov = "1";
		} else if (location.getProvider().equals(LocationManager.NETWORK_PROVIDER)) {
			trackPointDM.prov = "2";
		}else{
			trackPointDM.prov = "-1";
		}

		trackPointDM.rssi = signalStrengthInfo;
		SimpleDateFormat format = new SimpleDateFormat("yyyy-M-d hh:mm:ss", Locale.ENGLISH);
		trackPointDM.time = format.format(new Date());
		trackPointDM.type = trackType;
		trackPointDM.uId = userId;
		trackPointDM.user = PrefsUtil.getUser(this);
		Log.d("mfisheries LocService", "User ID :" + userId);
		if (trackPointDM.uId == -999)
			Log.d(TAG, "Unable to Extract User id from the system");

		Log.d(TAG, trackPointDM.toString() + "");
		return trackPointDM;
	}

	@Override
	public IBinder onBind(Intent intent) {
		return null;
	}
	
	
	
	private class PhoneStatusListener extends PhoneStateListener {
		@Override
		public void onSignalStrengthsChanged(SignalStrength signalStrength) {
			super.onSignalStrengthsChanged(signalStrength);
			signalStrengthInfo = signalStrength.toString();
		}
	}
	
	public class LocationTask extends AsyncTask<Location, Void, Void> {
		
		private Location location;
		
		protected Void doInBackground(Location... locations) {
			location = locations[0];
			if (locationCounter == 0) {
				Boolean withinCountry = Geofencer.isWithinCountry(LocationService.this, userLocation.getLatitude(), userLocation.getLongitude());
				Log.d(TAG, "Is Within Country = " + withinCountry);
				if (withinCountry != null) {
					if (withinCountry && trackingEnabled) { // Within country so we should disable tracking
						mLocationRequest.setInterval(LOC_TIME_INTERVAL);
						mLocationRequest.setFastestInterval(60000);
						mLocationRequest.setPriority(LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY);
						Log.d(TAG, "Tracking ended");
						trackingEnabled = false;
						trackType = "end";
					} else if (!withinCountry && !trackingEnabled) { // Out at sea and tracking disabled then start tracking and enable tracking
						mLocationRequest.setInterval(LOC_TIME_INTERVAL/5);
						mLocationRequest.setFastestInterval(LOC_TIME_INTERVAL/6);
						mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
						Log.d(TAG, "Tracking started");
						trackingEnabled = true;
						trackType = "start";
					}
				} else {
					Log.d(TAG, "Error occurred with tracking attempting to end tracking");
					trackingEnabled = false;
					trackType = "end";
				}
				editor.putBoolean("is_tracking_enabled", trackingEnabled);
				editor.putString("track_type", trackType);
				editor.commit();
				locationCounter = timeMultiplier;
			} else {
				locationCounter = locationCounter - 1;
			}
			updateNotification();
			Log.d(TAG, "Offline Tracks = " + offlineTracksPresent);
			if (trackingEnabled || offlineTracksPresent) {
				if (NetUtil.isOnline(getApplicationContext()))
					uploadTracks(location, trackType);
				else if (trackingEnabled)
					saveTrack(location, trackType);
				if (trackType.equals("start"))
					trackType = "track";
				editor.putString("track_type", trackType);
				editor.putBoolean("are_offline_tracks_present", offlineTracksPresent);
				editor.commit();
			}
			return null;
		}
	}
	
	public static void startLocationServices(Context context, boolean firstStart) {
		if (firstStart) {
			SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
			preferences.edit().putBoolean("loc_service_stopped", true).apply();
		}
		
		try {
			Log.d(TAG, "Attempting to Start Location Services");
			SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
			Log.d(TAG, "Is loc service stopped:"+ String.valueOf(preferences.getBoolean("loc_service_stopped", true)));
			if (preferences.getBoolean("loc_service_stopped", true)) {
				Log.d(TAG, "start location services");
				ActivityManager manager = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
				boolean isRunning = false;
				if (manager != null) {
					for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
						if (LocationService.class.getName().equals(service.service.getClassName())) {
							Log.d(TAG, "mFisheries location service already started");
							isRunning = true;
						}
					}
				}
				if (!isRunning) {
					Intent location_poll_service = new Intent(context, LocationService.class);
					context.startService(location_poll_service);
				}
				preferences.edit().putBoolean("loc_service_stopped", false).apply();
			}else{
				Log.d(TAG, "Location Service was previously started. Ignoring subsequent request");
			}
		} catch (Exception ex) {
			Log.e(TAG, "Unable to start Location Services");
			ex.printStackTrace();
		}
	}
	
	public static void startLocationServices(Context context) {
		startLocationServices(context, false);
	}
	
	public static void stopLocationServices(Context context){
		SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
		preferences.edit().putBoolean("loc_service_stopped", false).apply();
		// Don't actually stop service as service is needed, but marked as stop for system to go through initialization process upon run
	}
}
