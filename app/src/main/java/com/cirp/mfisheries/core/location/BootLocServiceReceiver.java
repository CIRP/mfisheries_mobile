package com.cirp.mfisheries.core.location;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;


public class BootLocServiceReceiver extends BroadcastReceiver {

	@Override
	public void onReceive(Context context, Intent intent) {
		if (intent.getAction() != null && intent.getAction().equals(Intent.ACTION_BOOT_COMPLETED)){
			try {
				Log.d("mFisheriesLocService", "Received Boot Action Complete");
				Intent i = new Intent(context, LocationService.class);
				context.startService(i);
			}catch (Exception e){e.printStackTrace();}
		}
	}
}
