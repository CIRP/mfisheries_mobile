package com.cirp.mfisheries.core.location;

import com.google.gson.annotations.SerializedName;

public class TrackPointDM {

	public int id;

	//start track id
	@SerializedName("startId")
	public int startId;

	//user id
	@SerializedName("uId")
	public int uId;

	//latitude
	@SerializedName("lat")
	public String lat;

	//longitude
	@SerializedName("lng")
	public String lng;

	//accuracy
	@SerializedName("acc")
	public String acc;

	//bearing
	@SerializedName("brg")
	public String brg;

	//speed
	@SerializedName("spd")
	public String spd;

	//provider
	@SerializedName("prov")
	public String prov;

	//rssi
	@SerializedName("rssi")
	public String rssi;

	//time
	@SerializedName("time")
	public String time;

	//user
	@SerializedName("user")
	public String user;

	//message
	@SerializedName("msg")
	public String msg;

	//track filetype
	@SerializedName("type")
	public String type;

	@Override
	public String toString() {
		return "Track: (" + lat + "," + lng + "," + brg + "," + spd + "," + user + "," + uId + ","
				+ time + "," + msg + "," + type + "," + "," + prov + "," + acc + ")";
	}
}
