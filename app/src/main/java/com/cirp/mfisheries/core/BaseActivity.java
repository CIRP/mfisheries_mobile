package com.cirp.mfisheries.core;

import android.content.pm.PackageManager;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.Toast;

public abstract class BaseActivity extends AppCompatActivity {

	protected String[] permissions;
	protected final int PERMISSIONS_REQUEST = 0x4;
	public boolean PERMISSIONS_GRANTED;
	
	public void requestPermissions(){
		ActivityCompat.requestPermissions(BaseActivity.this, permissions, PERMISSIONS_REQUEST);
//		String [] neededPermissions = new String[permissions.length];
//		int pos = 0;
//		// Check each of the requested permission to see if it was previously granted
//		for(String permission: permissions){
//			if (ContextCompat.checkSelfPermission(this, permission) != PackageManager.PERMISSION_GRANTED) {
//				// Check if we ned to show rationale for permissions
//				if (ActivityCompat.shouldShowRequestPermissionRationale(this, permission)) {
//					explainPermission();
//				}
//				// We add the permission to a list of needed permissions
//				neededPermissions[pos++] = permission;
//			}
//		}
//		// If the length is > 0, then at least one permission was not previously granted
//		if (neededPermissions.length > 0){
//			ActivityCompat.requestPermissions(this, neededPermissions, PERMISSIONS_REQUEST);
//		}else{ // Else all permissions was previously granted to move on
//			Log.d("Permission", "Already granted ");
//			onPermissionGranted(null);
//		}
	}

//	public void explainPermission(){
//		new AlertDialog.Builder(this)
//				.setTitle(getString(R.string.why_need_permissions))
//				.setMessage(getString(R.string.explain_permissions))
//				.setPositiveButton(getString(R.string.continue_text), new DialogInterface.OnClickListener() {
//					public void onClick(DialogInterface dialog, int whichButton) {
//						ActivityCompat.requestPermissions(BaseActivity.this, permissions, PERMISSIONS_REQUEST);
//						dialog.dismiss();
//					}
//				})
//				.show();
//	}

	@Override
	public void onRequestPermissionsResult(int requestCode, @NonNull String permissions[], @NonNull int[] grantResults) {
		switch (requestCode) {
			case PERMISSIONS_REQUEST: {
				if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
					Log.d("Permission", String.format("Permission Granted %s", this.getLocalClassName()));
					onPermissionGranted("");
				} else if(grantResults.length != 0){
					Log.d("Permission", "Permission Rejected " + this.getLocalClassName());
					Toast.makeText(this, "Rejected", Toast.LENGTH_SHORT).show();
					onPermissionRejected();
				}
				break;
			}
		}
	}

	public void onPermissionGranted(String permission) {
		PERMISSIONS_GRANTED = true;
	}

	public void onPermissionRejected() {
		PERMISSIONS_GRANTED = false;
	}

}
