package com.cirp.mfisheries.core.onboarding;


import android.content.Intent;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.RelativeLayout;

import com.cirp.mfisheries.MainActivity;
import com.cirp.mfisheries.R;

public class OnboardingActivity extends AppCompatActivity {
    private int pos =0;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_onboarding);


        final RelativeLayout myLayout = (RelativeLayout) findViewById(R.id.myrelative);
        final ViewPager viewpager = (ViewPager) findViewById(R.id.viewpager);
        final SlideAdapter myadapter = new SlideAdapter(this);

        viewpager.setAdapter(myadapter);
        Button nextButton = (Button)findViewById(R.id.button2);
        Button backButton = (Button)findViewById(R.id.button3);

        nextButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(pos==3) { // We at the end of the slideshow so close
                    startActivity(new Intent(OnboardingActivity.this, MainActivity.class));
                    finish(); // Clear up memory immediately (hopefully prevent back-button navigation - TODO test to ensure back button navigation removed)
                }
                else{
                    if(pos == 0) viewpager.setCurrentItem(1);
                    else if (pos == 1)viewpager.setCurrentItem(2);
                    else if (pos == 2)viewpager.setCurrentItem(3);
                }
            }
        });

        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(pos == 1)viewpager.setCurrentItem(0);
                else if(pos == 2)viewpager.setCurrentItem(1);
                else if (pos == 3)viewpager.setCurrentItem(2);
            }
        });

        viewpager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                pos=position;
                Button nextButton = (Button) findViewById(R.id.button2);
                Button backButton = (Button)findViewById(R.id.button3);
                String text = (String)nextButton.getText();
                if(position==0){
                    backButton.setVisibility(View.GONE);
                    //myLayout.setBackgroundColor(Color.parseColor("#01BCD4"));
                }
                else if(position==1){
                    backButton.setVisibility(View.VISIBLE);
                    //myLayout.setBackgroundColor(Color.parseColor("#EE5555"));
                }
                else if(position==3){
                    nextButton.setText(R.string.finish);
                    backButton.setVisibility(View.VISIBLE);
                    //yLayout.setBackgroundColor(Color.parseColor("#6E3159"));
                }
                else if(position != 2 && text.equals("Finish"))
                {
                    nextButton.setText(R.string.next);
                }

            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
    }
}