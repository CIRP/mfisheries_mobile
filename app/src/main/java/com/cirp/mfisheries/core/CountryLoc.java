package com.cirp.mfisheries.core;


public class CountryLoc {
	
	public int id;
	public String createdby;
	public int countryid;
	public String country;
	public String timecreated;
	public String zoom;
	public String longitude;
	public String latitude;
	
	@Override
	public String toString(){
		return String.format("%s(%s,%s)", country, latitude, longitude);
	}
	
}
