package com.cirp.mfisheries.core.config;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.util.Log;

import com.cirp.mfisheries.BuildConfig;
import com.cirp.mfisheries.util.NetUtil;
import com.google.gson.JsonArray;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;
import com.squareup.moshi.JsonAdapter;
import com.squareup.moshi.Moshi;
import com.squareup.moshi.Types;

import java.lang.reflect.Type;
import java.util.List;

public class ConfigUtil {
	
	private static final String TAG = "ConfigUtil";
	private final String min_version_api;
	private final String recommend_version_api;
	private Context context;
	
	public ConfigUtil(Context activity) {
		this.context = activity;
		this.min_version_api = NetUtil.API_URL +"config?key=app_version_min";
		this.recommend_version_api = NetUtil.API_URL +"config?key=app_version_recommend";
	}
	
	private void requestVersion(@NonNull final String type, @NonNull final ConfigHandler handler){
		final String url = (type.equalsIgnoreCase("minimum"))? this.min_version_api : this.recommend_version_api;
		Log.d(TAG, "Attempting to retrieve version from the url: " + url);
		Ion.with(context).load(url)
				.setHeader(NetUtil.API_KEY_PARAM, NetUtil.API_KEY)
				.asJsonArray()
				.setCallback(new FutureCallback<JsonArray>() {
					public void onCompleted(Exception e, JsonArray result) {
						if (e == null && result != null){
							try{
								Moshi moshi = new Moshi.Builder().build();
								Type listType = Types.newParameterizedType(List.class, Config.class);
								JsonAdapter<List<Config>> jsonAdapter = moshi.adapter(listType);
								List<Config> json = jsonAdapter.fromJson(result.toString());
								if (json != null && json.size() > 0)handler.onConfigReceived(json.get(0));
								else handler.onConfigReceived(null);
							}catch (Exception exc) {
								exc.printStackTrace();
								handler.onConfigReceived(null);
							}
						}
					}
				});
	}
	
	public interface ConfigHandler{
		void onConfigReceived(@Nullable Config config);
	}
	
	public void checkVersion(){
		// Check the minimum version first
		// If less than minimum then notify user and terminate the program
		// If greater, check the recommended
		// If less than recommended then notify the user with option to direct to update
		try {
			final int versionCode = BuildConfig.VERSION_CODE;
			Log.d(TAG, "Current version code received as: " + versionCode);

			requestVersion("minimum", new ConfigHandler() {
				@Override
				public void onConfigReceived(@Nullable Config config) {
					try {
						if (config != null) {
							int minVersion = Integer.parseInt(config.value);
							if (versionCode < minVersion) {
								notifyAndEnd();
								return;
							}
						}else Log.d(TAG, "We did not receive the minimum config version from the api");
						
						// We are more recent than minimum version so now check the recommended version
						requestVersion("recommended", new ConfigHandler() {
							@Override
							public void onConfigReceived(@Nullable Config config) {
								try{
									if (config != null){
										int recVersion = Integer.parseInt(config.value);
										if (versionCode < recVersion) {
											notifyAndRedirect();
										}else{
											Log.d(TAG, "We are running an appropriate version");
										}
									}else{
										Log.d(TAG, "We did not receive the recommended config version from the api");
									}
								}catch (Exception e){
									e.printStackTrace();
									Log.e(TAG, "Error when attempting to retrieve the recommended version: " + e.getMessage());
								}
							}
						});
					}catch (Exception e){
						e.printStackTrace();
						Log.e(TAG, "Error when attempting to retrieve minimum version: " + e.getMessage());
					}
				}
			});
		} catch (Exception e) {
			e.printStackTrace();
			Log.e(TAG, "Error when attempting to retrieve local version number: " + e.getMessage());
		}
		
	}
	
	private void notifyAndRedirect() {
		try {
			AlertDialog.Builder myBuilder = new AlertDialog.Builder(context);
			myBuilder.setTitle(getApplicationName())
					.setMessage("The application is out of date and features may not work as expected. Please update the application.")
					.setNegativeButton("Proceed", new DialogInterface.OnClickListener() {
						@Override
						public void onClick(DialogInterface dialogInterface, int i) {
							Log.d(TAG, "User choose to proceed");
							dialogInterface.dismiss();
						}
					})
					.setPositiveButton("Update", new DialogInterface.OnClickListener() {
						@Override
						public void onClick(DialogInterface dialogInterface, int i) {
							directUserToPlayStore();
							// context.finish();
						}
					})
					.show();
			
		}catch(Exception e){
			e.printStackTrace();
			Log.e(TAG, "Error occurred when try to notify the user of invalid version");
		}
	}
	
	private void notifyAndEnd(){
		try {
			AlertDialog.Builder myBuilder = new AlertDialog.Builder(context);
			myBuilder.setTitle(getApplicationName())
					.setMessage("The application is out of date and may not perform correctly. Please update the application and try again")
//					.setNegativeButton("Close", new DialogInterface.OnClickListener() {
//						@Override
//						public void onClick(DialogInterface dialogInterface, int i) {
//							context.finish();
//						}
//					})
					.setPositiveButton("Update", new DialogInterface.OnClickListener() {
						@Override
						public void onClick(DialogInterface dialogInterface, int i) {
							directUserToPlayStore();
//							context.finish();
						}
					})
					.show();
			
		}catch(Exception e){
			e.printStackTrace();
			Log.e(TAG, "Error occurred when try to notify the user of invalid version");
		}
	}
	
	private void directUserToPlayStore(){
		try{
			String appId = BuildConfig.APPLICATION_ID;
			Intent intent = new Intent(Intent.ACTION_VIEW);
			intent.setData(Uri.parse(String.format("market://details?id=%s", appId)));
			context.startActivity(intent);
		}catch (Exception e){
			e.printStackTrace();
			Log.e(TAG, "Unable to redirect user to the play store to update app");
		}
	}
	
	private String getApplicationName() {
		ApplicationInfo applicationInfo = context.getApplicationInfo();
		int stringId = applicationInfo.labelRes;
		return stringId == 0 ? applicationInfo.nonLocalizedLabel.toString() : context.getString(stringId);
	}
	
	
}
