package com.cirp.mfisheries.core.location;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;

public class StopLocationServiceActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		Intent location_poll_service = new Intent(getApplicationContext(), LocationService.class);
		startService(location_poll_service);
		stopService(location_poll_service);
		Log.d("mFisheries", "Stop Service Requesting Location Poll Destroyed...Initiated by user via notification");

		SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(this);
		preferences.edit().putBoolean("loc_service_stopped", true).apply();
		preferences.edit().putBoolean("is_tracking_enabled", false).apply();
		finish();
	}
}
