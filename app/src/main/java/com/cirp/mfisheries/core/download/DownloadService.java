package com.cirp.mfisheries.core.download;

import android.app.DownloadManager;
import android.app.IntentService;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.util.Log;

import com.cirp.mfisheries.core.module.Module;
import com.cirp.mfisheries.core.module.ModuleFactory;
import com.cirp.mfisheries.util.FileUtil;

import java.util.concurrent.CountDownLatch;

@SuppressWarnings("SameParameterValue")
public class DownloadService extends IntentService {

	public static final String DOWNLOADS_COMPLETE_BROADCAST = "downloads-complete-broadcast";
	private final static String TAG = "DownloadService";
	public static final String MODULE_ID = "moduleId";
	public static final String DOWNLOAD_STATUS = "status";
	public static final String DOWNLOAD_PATH = "path";
	
	public static ModuleDownloader downloader = new ModuleDownloader();
	private long lastReferenceId;
	private String lastModuleId;
	private long referenceId;
	
	public DownloadService() {
		super("Download Service");
	}

	@Override
	public void onHandleIntent(Intent intent) {
		if (intent.hasExtra(MODULE_ID)) {
			String moduleId = intent.getStringExtra(MODULE_ID);
			downloadModule(getApplicationContext(), moduleId);
		}
	}

	public void downloadModule(Context context, String moduleId){
		ModuleFactory factory = ModuleFactory.getInstance(context);
		Module module = factory.getModule(moduleId);
		downloadModule(context, module);
	}

	private void downloadModule(Context context, final Module module) {
		// Create request for android download manager
		final DownloadManager downloadManager = (android.app.DownloadManager)context.getSystemService(Context.DOWNLOAD_SERVICE);
		Uri uri;
		uri = Uri.parse(module.getDataLoc());
		if (Build.VERSION.SDK_INT < 18){
			Log.d(TAG, "Older version of android detected. Attempting to convert https to http");
			String dataLoc = module.getDataLoc();
			dataLoc = dataLoc.replace("https", "http");
			Log.d(TAG, "Attempting to load data from: " + dataLoc);
			uri = Uri.parse(dataLoc);
		}
		
		DownloadManager.Request request = new android.app.DownloadManager.Request(uri);

		//Setting title of request
		request.setTitle("Downloading " + module.getName() + " Module");
		//Setting description of request
		request.setDescription("Downloading modules resources");
		// Setting the location to save the file
		request.setDestinationUri(FileUtil.getAudioUri("/" + module.getId()+ ".zip"));
		final CountDownLatch downloadSignal = new CountDownLatch(1);
		//Setup Listener for Module
		BroadcastReceiver onComplete = new BroadcastReceiver() {
			@Override
			public void onReceive(Context context, Intent intent) {
//				long downloadId = intent.getLongExtra(DownloadManager.EXTRA_DOWNLOAD_ID, 0);
				DownloadManager.Query query = new DownloadManager.Query();
				query.setFilterById(referenceId);
				Cursor c = downloadManager.query(query);
				if (c.moveToFirst()) {
					int columnIndex = c.getColumnIndex(DownloadManager.COLUMN_STATUS);
					if (DownloadManager.STATUS_SUCCESSFUL == c.getInt(columnIndex)) {
						String uriString = c.getString(c.getColumnIndex(DownloadManager.COLUMN_LOCAL_URI));
						Log.d(TAG, "Download successfully completed");
						sendStatusBroadcast(module.getId(), true, uriString);
						downloadSignal.countDown();
						return;
					}
				}
				Log.d(TAG, String.format("Download of %s failed", module.getName()));
				sendStatusBroadcast(module.getId(), false);
				downloadSignal.countDown();
			}
		};
		registerReceiver(onComplete, new IntentFilter(DownloadManager.ACTION_DOWNLOAD_COMPLETE));
		
		//Enqueue download and save into referenceId
		if (downloadManager != null) {
//			downloader.lastReferenceId
			referenceId = downloadManager.enqueue(request);
			downloader.lastModuleId = module.getId();
			try {
				downloadSignal.await();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		
		unregisterReceiver(onComplete);
	}
	
	
	
	public static void setDownloader(ModuleDownloader downloader) {
		DownloadService.downloader = downloader;
	}
	
	public void sendStatusBroadcast(String moduleId, boolean status, String uriString) {
		Intent broadcastIntent = new Intent(DOWNLOADS_COMPLETE_BROADCAST);
		broadcastIntent.putExtra(MODULE_ID, moduleId);
		broadcastIntent.putExtra(DOWNLOAD_STATUS, status);
		broadcastIntent.putExtra(DOWNLOAD_PATH, uriString);
		sendBroadcast(broadcastIntent);
	}
	
	public void sendStatusBroadcast(String moduleId, boolean status){
		Intent broadcastIntent = new Intent(DOWNLOADS_COMPLETE_BROADCAST);
		broadcastIntent.putExtra(MODULE_ID, moduleId);
		broadcastIntent.putExtra(DOWNLOAD_STATUS, status);
		sendBroadcast(broadcastIntent);
	}
	
	public static void startDownloadService(Context context, String moduleId){
		Intent serviceIntent = new Intent(context, DownloadService.class);
		serviceIntent.putExtra(DownloadService.MODULE_ID, moduleId);
		context.startService(serviceIntent);
	}

}