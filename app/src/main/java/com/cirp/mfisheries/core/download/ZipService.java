package com.cirp.mfisheries.core.download;

import android.app.IntentService;
import android.app.NotificationManager;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.cirp.mfisheries.R;
import com.cirp.mfisheries.util.FileUtil;
import com.cirp.mfisheries.util.Zipper;

public class ZipService extends IntentService {

	private final static String TAG = "ZipService";
	public static final String MODULE_ID = "moduleId";
	public static final String PROCESS_ID = "processId";
	public static final String TO_DELETE = "delete";
	public static final String EXTRACT_STATUS = "status";
	public static final String EXTRACT_COMPLETE_BROADCAST = "completed";
	
	private boolean deleteAfter;
	private NotificationManager nm;
	
	public ZipService() {
		super("ZipService");
		deleteAfter = true;
	}

	@Override
	public void onHandleIntent(final Intent intent) {
		final String moduleId = intent.getStringExtra(MODULE_ID);
		final int id = intent.getIntExtra( PROCESS_ID, -1);
		if (intent.hasExtra(TO_DELETE)) deleteAfter = intent.getBooleanExtra(TO_DELETE, true);
		Log.d(TAG, String.format("Unzipping %s with an id of %d with request to delete as %s", moduleId, id, deleteAfter));
		
		extractResource(moduleId, id);
	}
	
	public void extractResource(final String moduleId, final int notifyId){
		nm = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
		if (nm != null) {
			final NotificationCompat.Builder builder = new NotificationCompat.Builder(this)
					.setContentTitle(String.format("%s %s %s", getString(R.string.installing), moduleId, getString(R.string.module)))
					.setSmallIcon(R.drawable.ic_download_white_24dp);
			
			builder.setProgress(100, 0, true);
			nm.notify(notifyId, builder.build());
		}
		
		
		Zipper zipper = new Zipper(FileUtil.getFileUri("/" + moduleId + ".zip").toString(), FileUtil.getFileUri("").toString());
		zipper.setDeleteAfterUnzip(deleteAfter);
		zipper.setOnZipFinishedListener(new Zipper.OnZipFinishedListener() {
			@Override
			public void onFinished(boolean status) {
				SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
				if (status) {
					Log.d(TAG, "Finished unzipping " + moduleId);
					sendStatusBroadcast(moduleId, true);
					//stores that module downloaded successfully and installed
					prefs.edit().putBoolean(moduleId, true).apply();
				}else{
					Log.d(TAG, "Finished but unsuccessful with extraction");
					sendStatusBroadcast(moduleId, false);
					//stores that module was unsuccessfully and not installed
					prefs.edit().putBoolean(moduleId, false).apply();
				}
				if (nm != null)nm.cancel(notifyId);
				ModuleDownloader downloader = DownloadService.downloader;
				downloader.getNextModule();
				stopSelf();
			}
		});
		
		zipper.unzip();
	}
	
	public void sendStatusBroadcast(String moduleId, boolean status){
		Intent broadcastIntent = new Intent(EXTRACT_COMPLETE_BROADCAST);
		broadcastIntent.putExtra(MODULE_ID, moduleId);
		broadcastIntent.putExtra(EXTRACT_STATUS, status);
		sendBroadcast(broadcastIntent);
	}
	
	public static void launchZipService(Context context, String moduleId){
		launchZipService(context, moduleId, true);
	}
	
	public static void launchZipService(Context context, String moduleId, boolean deleteAfter){
		Intent intent = new Intent(context, ZipService.class);
		intent.putExtra(MODULE_ID, moduleId);
		intent.putExtra(PROCESS_ID, (int) (Math.random() * 1024));
		intent.putExtra(TO_DELETE, deleteAfter);
		context.startService(intent);
	}
}