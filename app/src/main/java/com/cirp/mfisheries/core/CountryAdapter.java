package com.cirp.mfisheries.core;

import android.content.Context;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.List;

public class CountryAdapter extends ArrayAdapter<Country> {
	final Context mContext;
	final List<Country> countries;

	public CountryAdapter(Context context, List<Country> countries) {
		super(context, android.R.layout.simple_list_item_1, countries);
		this.mContext = context;
		this.countries = countries;
	}

	/*
		NB: The getView method handles the displaying of the data at the position passed in as a parameter
		More info: http://developer.android.com/reference/android/widget/ArrayAdapter.html#getView(int, android.view.View, android.view.ViewGroup)
	 */
	@NonNull
	@Override
	public View getView(int position, View convertView, @NonNull ViewGroup parent) {
		final LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		if (inflater != null) {
			final TextView rowView = (TextView) inflater.inflate(android.R.layout.simple_list_item_1, parent, false);
			final String country = countries.get(position).getName();
			final String countryName = (country.charAt(0) + "").toUpperCase() + country.substring(1);
			rowView.setText(countryName);
			return rowView;
		} else return convertView;
	}
	
	@Override
	public View getDropDownView(int position, View convertView, @NonNull ViewGroup parent) {
		return getView(position, convertView, parent);
	}

	@Override
	public void add(Country object) {
		this.notifyDataSetChanged();
	}
}
