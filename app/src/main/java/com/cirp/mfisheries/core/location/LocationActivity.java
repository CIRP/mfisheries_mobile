package com.cirp.mfisheries.core.location;

import android.Manifest;
import android.content.IntentSender;
import android.location.Location;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.util.Log;
import android.widget.Toast;

import com.cirp.mfisheries.core.module.ModuleActivity;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStatusCodes;

public abstract class LocationActivity extends ModuleActivity implements LocationListener, GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener{

    private static final String TAG = "LocationActivityBase";
    public Location mCurrentLocation; // mCurrentLocation
    protected double latitude; // latitude
    protected double longitude; // longitude
    protected boolean mRequestingLocationUpdates = true;
    protected GoogleApiClient mGoogleApiClient;
    protected LocationRequest mLocationRequest;
    protected int REQUEST_CHECK_SETTINGS = 7;
    protected String LOCATION_KEY = "filepath";
    protected boolean placeSelectedByUser = false;
    
    
    protected FusedLocationProviderClient mFusedLocationClient;
	protected LocationCallback mLocationCallback;
	
	
	@Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        permissions = new String[]{Manifest.permission.ACCESS_FINE_LOCATION};
        requestPermissions();
        updateValuesFromBundle(savedInstanceState);
	}

    @Override
    public void onPermissionGranted(String permission) {
        super.onPermissionGranted(permission);
	
//	    mLocationCallback = new LocationCallback() {
//		    @Override
//		    public void onLocationResult(LocationResult locationResult) {
//			    for (Location location : locationResult.getLocations()) {
//				    Log.d(TAG, String.format("Location (new api): (%s,%s)", location.getLatitude(),location.getLongitude()));
//			    }
//		    };
//	    };
	    
        buildGoogleApiClient();
    }

    protected void stopLocationUpdates() {
        if(mGoogleApiClient != null && mGoogleApiClient.isConnected()) {
            LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, this);
        }
        
//        if (mFusedLocationClient != null){
//        	//TODO remove location updates
//	        mFusedLocationClient.removeLocationUpdates(mLocationCallback);
//        }
    }

    protected synchronized void buildGoogleApiClient() {
        Log.d(TAG, "Attempting to Build the Google Client");
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addApi(LocationServices.API)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .build();
        mGoogleApiClient.connect();
	
//        // Implementing the new Location Client Strategy
//	    mFusedLocationClient = LocationServices.getFusedLocationProviderClient(this);
	
//	    startLocationUpdates();
    }

    public void onConnectionFailed(@NonNull ConnectionResult result) {
        Log.d(TAG, result.getErrorCode() + "");
    }

    public void onConnectionSuspended(int cause) {
        //   mGoogleApiClient.connect();
    }

    protected void createLocationRequest() {
        Log.d(TAG, "Creating Location Request");
        mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(30000);
        mLocationRequest.setFastestInterval(5000);
        mLocationRequest.setSmallestDisplacement(100);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
    }

    public void onConnected(Bundle connectionHint) {
        Log.d(TAG, "Connected to Google Service");
        createLocationRequest();

        // Checking system settings to ensure that user has filepath services enabled
        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder().addLocationRequest(mLocationRequest);
        PendingResult<LocationSettingsResult> result = LocationServices.SettingsApi.checkLocationSettings(mGoogleApiClient, builder.build());
//
//	    SettingsClient client = LocationServices.getSettingsClient(this);
//	    Task<LocationSettingsResponse> task = client.checkLocationSettings(builder.build());
//
//	    task.addOnFailureListener(this, new OnFailureListener() {
//		    @Override
//		    public void onFailure(@NonNull Exception e) {
//			    int statusCode = ((ApiException) e).getStatusCode();
//			    switch (statusCode) {
//				    case CommonStatusCodes.RESOLUTION_REQUIRED: // Location settings are not satisfied, but this can be fixed by showing the user a dialog.
//					    try {
//						    ResolvableApiException resolvable = (ResolvableApiException) e;
//						    resolvable.startResolutionForResult(LocationActivity.this, REQUEST_CHECK_SETTINGS);
//					    } catch (IntentSender.SendIntentException sendEx) { }
//					    break;
//				    case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
//					    // Location settings are not satisfied. However, we have no way to fix the settings so we won't show the dialog.
//					    break;
//			    }
//		    }
//	    });
	
	    // Perform different operations based on the system configuration
        result.setResultCallback(new ResultCallback<LocationSettingsResult>() {
            @Override
            public void onResult(@NonNull LocationSettingsResult result) {
                final Status status = result.getStatus();
                switch (status.getStatusCode()) {
                    case LocationSettingsStatusCodes.SUCCESS:
                        try {
                            Log.d(TAG, "Permissions correctly configured. Attempting to get Location");
                            mCurrentLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
                            if (mRequestingLocationUpdates) {
                                startLocationUpdates();
                            }
                        } catch (SecurityException e) {
                            e.printStackTrace();
                        }
                        break;
                    case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                        try {
                            Log.d(TAG, "Permissions not configured. Attempting to let user configure settings");
                            status.startResolutionForResult(LocationActivity.this, REQUEST_CHECK_SETTINGS);
                        } catch (IntentSender.SendIntentException e) {
                            Log.d("Location", "Error" + e.getMessage());
                        }
                        break;
                    case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                        // TODO Determine what is the most appropriate action here
                        Toast.makeText(LocationActivity.this, "Unable to get filepath from system", Toast.LENGTH_SHORT).show();
                        break;
                }
            }
        });
    }

    protected void startLocationUpdates() {
        try {
            Log.d(TAG, "Setting the app to receive filepath updates");
            if (mGoogleApiClient.isConnected())
                LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, this);
        
//            if (mFusedLocationClient != null && mLocationCallback != null){
//            	mFusedLocationClient.requestLocationUpdates(mLocationRequest, mLocationCallback, null);
//            }else{
//            	Log.d(TAG, "Something was null");
//            	if (mLocationCallback == null) Log.d(TAG, "Callback was null");
//            	if (mFusedLocationClient == null)Log.d(TAG, "Fused client was null");
//            }
			        
        } catch (SecurityException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onLocationChanged(Location location) {
        mCurrentLocation = location;
        Log.d(TAG, String.format("Location: (%s,%s)", location.getLatitude(),location.getLongitude()));
        boolean result = PreferenceManager
                .getDefaultSharedPreferences(this)
                .edit()
                .putFloat("lat", (float) location.getLatitude())
                .putFloat("lng", (float) location.getLongitude())
                .commit();
        Log.d(TAG, "Location cache status: "+ result);
    }

    @Override
    public void onSaveInstanceState(Bundle savedInstanceState) {
        savedInstanceState.putParcelable(LOCATION_KEY, mCurrentLocation);
        super.onSaveInstanceState(savedInstanceState);
    }

    public void updateValuesFromBundle(Bundle savedInstanceState) {
        if (savedInstanceState != null) {
            if (savedInstanceState.keySet().contains(LOCATION_KEY)) {
                mCurrentLocation = savedInstanceState.getParcelable(LOCATION_KEY);
            }
        }
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onDestroy(){
        stopLocationUpdates();
        super.onDestroy();
    }
    
    public interface MFishLocationListener{
    	void onLocationReceived(Location location);
    }
    
}

