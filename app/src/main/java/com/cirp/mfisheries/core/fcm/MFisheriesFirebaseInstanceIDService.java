package com.cirp.mfisheries.core.fcm;

import android.util.Log;

import com.cirp.mfisheries.App;
import com.cirp.mfisheries.messaging.MSGConstants;
import com.cirp.mfisheries.util.PrefsUtil;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;
public class MFisheriesFirebaseInstanceIDService extends FirebaseInstanceIdService {

    private static final String TAG = "MFishFirebaseIDService";

    /**
     * Called if InstanceID token is updated. This may occur if the security of
     * the previous token had been compromised. Note that this is called when the InstanceID token
     * is initially generated so this is where you would retrieve the token.
     */
    // [START refresh_token]
    @Override
    public void onTokenRefresh() {
        // Get updated InstanceID token.
        String refreshedToken = FirebaseInstanceId.getInstance().getToken();
        Log.d(TAG, "Refreshed token: " + refreshedToken);

        // If you want to send messages to this application instance or
        // manage this apps subscriptions on the server side, send the
        // Instance ID token to your app server.
        sendRegistrationToServer(refreshedToken);
    }
    // [END refresh_token]
    
    
    /**
     * Persist token to third-party servers.
     * <p>
     * Modify this method to associate the user's FCM InstanceID token with any server-side account
     * maintained by your application.
     * @param token The new token.
     */
    private void sendRegistrationToServer(final String token) {
	    PrefsUtil.setNotificationToken(getApplicationContext(), token);

        if (FirebaseAuth.getInstance().getCurrentUser() != null) {
	        final DatabaseReference userReference = App.getFBDatabase().getReference().child("users");
	        userReference.addListenerForSingleValueEvent(new ValueEventListener() {
		        @SuppressWarnings("ConstantConditions")
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    if(dataSnapshot.hasChild(FirebaseAuth.getInstance().getCurrentUser().getUid())){
                        // Update for Messaging
	                    userReference.child(FirebaseAuth.getInstance().getCurrentUser().getUid())
			                    .child(MSGConstants.ARG_FIREBASE_TOKEN)
                                .setValue(token);
                    }
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            });
        }
	
	
    }
}
