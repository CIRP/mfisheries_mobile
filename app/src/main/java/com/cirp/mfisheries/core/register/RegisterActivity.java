package com.cirp.mfisheries.core.register;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.widget.Toast;

import com.cirp.mfisheries.App;
import com.cirp.mfisheries.R;
import com.cirp.mfisheries.core.BaseActivity;
import com.cirp.mfisheries.core.Country;
import com.cirp.mfisheries.core.CountryAdapter;
import com.cirp.mfisheries.messaging.User;
import com.cirp.mfisheries.util.CountryUtil;
import com.cirp.mfisheries.util.CrashReporter;
import com.cirp.mfisheries.util.NetUtil;
import com.cirp.mfisheries.util.PrefsUtil;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInStatusCodes;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.GoogleAuthProvider;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.gson.JsonObject;
import com.koushikdutta.async.future.FutureCallback;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class RegisterActivity extends BaseActivity {

	public static final int SIGN_IN_REQUEST = 1;
	public static final String BROADCAST_ID = "mfisheries-require-register";
	public static final String HAS_REGISTER_SET = "registered_settings";
	private static final int RC_SIGN_IN = 0;
	private static final String TAG = "RegisterActivity";
	protected DatabaseReference databaseReference;
	boolean fireBaseSaved = false;
	boolean mFisheriesSaved = false;
	private FirebaseAuth myAuth;
	private GoogleSignInClient mGoogleSignInClient;
	private CountryUtil countryUtil;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_register);
		Toolbar toolbar = findViewById(R.id.toolbar);
		if (toolbar != null) setSupportActionBar(toolbar);
		
		myAuth = FirebaseAuth.getInstance();
		databaseReference = App.getFBDatabase().getReference();
		countryUtil = CountryUtil.getInstance(RegisterActivity.this
		);

		// Check if Currently Logged In
		if(myAuth.getCurrentUser() != null && PrefsUtil.getIsRegistered(this)){
			setResult(Activity.RESULT_OK);
			finish();
			return;
		}
		
		// User is not logged in. Start sign in process
		initGAPIClient();
		handleRegistration();
	}
	
	private void handleRegistration() {
		if (PrefsUtil.getCountryId(this).equals("-1")) startSignInProcessFromCountry();
		else signInWithGoogle();
	}
	
	
	private void startSignInProcessFromCountry() {
		if (!NetUtil.isOnline(this)) {
			NetUtil.retryDialog(this, new NetUtil.OnRetryClicked() {
				@Override
				public void retry() {
					startSignInProcessFromCountry();
				}
			});
			return;
		}
		final ProgressDialog progress = new ProgressDialog(this);
		progress.setMessage(getString(R.string.loading_countries));
		progress.setProgressStyle(ProgressDialog.STYLE_SPINNER);
		progress.setIndeterminate(true);
		progress.setCancelable(false);
		progress.show();
		
		countryUtil.retrieveCountries(new CountryUtil.CountryRetrievedListener() {
			@Override
			public void processCountries(List<Country> countries) {
				if (countries != null) {
					selectCountry(countries);
				} else {
					Toast.makeText(RegisterActivity.this, "Unable to Retrieve Countries", Toast.LENGTH_SHORT).show();
					NetUtil.retryDialog(RegisterActivity.this, new NetUtil.OnRetryClicked() {
						@Override
						public void retry() {
							startSignInProcessFromCountry();
						}
					});
				}
				progress.dismiss();
			}
		});
	}
	
	private void selectCountry(final List<Country> countries) {
		final CountryAdapter arrayAdapter = new CountryAdapter(getBaseContext(), countries);
		Log.d(TAG, "Attempting to display country list:" + countries.size());
		
		new android.app.AlertDialog
				.Builder(RegisterActivity.this)
				.setTitle(getResources().getString(R.string.dialog_country))
				.setCancelable(false)
				.setAdapter(arrayAdapter,
						new DialogInterface.OnClickListener() {
							@Override
							public void onClick(DialogInterface dialog, int which) {
								final Country selectedCountry = countries.get(which);
								Log.d(TAG, String.format(
										"Country: %s, Path: %s, ID: %s",
										selectedCountry.getName(),
										selectedCountry.getPath(),
										selectedCountry.getId()
								));
								countryUtil.saveCountryData(selectedCountry);
								//load available modules for country
								handleRegistration();
							}
						})
				.show();
	}
	
	protected void initGAPIClient() {
		final GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
				.requestIdToken(getString(R.string.default_web_client_id))
				.requestEmail()
				.build();
		mGoogleSignInClient = GoogleSignIn.getClient(this, gso);
	}
	
	private void signInWithGoogle() {
		Intent signInIntent = mGoogleSignInClient.getSignInIntent();
		startActivityForResult(signInIntent, RC_SIGN_IN);
	}
	
	public void onActivityResult(int requestCode, int responseCode, Intent data) {
		super.onActivityResult(requestCode, responseCode, data);
		if (requestCode == RC_SIGN_IN) {
			Task<GoogleSignInAccount> task = GoogleSignIn.getSignedInAccountFromIntent(data);
			handleSignInResult(task);
		}
	}
	
	private void handleSignInResult(Task<GoogleSignInAccount> task) {
		try {
			GoogleSignInAccount account = task.getResult(ApiException.class);
			setProfileInfo(account);
		} catch (ApiException e) {
			Log.w(TAG, "signInResult:failed code=" + e.getStatusCode());
			e.printStackTrace();
			handleSignInFailure(e.getStatusCode());
		}
	}
	
	private void handleSignInFailure(int statusCode) {
		AlertDialog.Builder myBuilder = new AlertDialog.Builder(this);
		myBuilder.setTitle("Registration");
		String message = "";
		switch (statusCode) {
			case GoogleSignInStatusCodes.SIGN_IN_CANCELLED:
				message = getString(R.string.register_cancel_retry_msg);
				break;
			case GoogleSignInStatusCodes.API_NOT_CONNECTED:
				initGAPIClient();
				message = getString(R.string.register_internal_error_msg);
				break;
			case GoogleSignInStatusCodes.RESOLUTION_REQUIRED:
				message = getString(R.string.register_phone_error_msg);
				break;
		}
		
		myBuilder
				.setMessage(message)
				.setIcon(android.R.drawable.ic_dialog_alert)
				.setPositiveButton("Retry", new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialogInterface, int i) {
						signInWithGoogle();
					}
				})
				.setNeutralButton("Close", new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialogInterface, int i) {
						setResult(Activity.RESULT_CANCELED);
						finish();
					}
				})
				.show();
		
	}
	
	private void setProfileInfo(GoogleSignInAccount acct) {
		PrefsUtil.setFirstName(this, acct.getGivenName());
		PrefsUtil.setLastName(this, acct.getFamilyName());
		PrefsUtil.setEmail(this, acct.getEmail());
		PrefsUtil.setGoogleId(this, acct.getId());
		
		firebaseAuthWithGoogle(acct);
		setResult(Activity.RESULT_OK);
	}

	private void firebaseAuthWithGoogle(final GoogleSignInAccount account){
		Log.d(TAG, "Attempting to Login to Firebase using Google account");
		AuthCredential credential = GoogleAuthProvider.getCredential(account.getIdToken(), null);
		myAuth.signInWithCredential(credential).addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
			@Override
			public void onComplete(@NonNull Task<AuthResult> task) {
				if(task.isSuccessful()){
					Log.d(TAG, "Firebase was signed successfully");
					FirebaseUser fbUser = myAuth.getCurrentUser();
					if (PrefsUtil.getNotificationToken(RegisterActivity.this).equals("-1"))
						attemptNotificationIDRetrieval();
					User user = generateUser(fbUser, account);
					addUserToFirebaseDatabase(user);
					addUserToMFisheriesDatabase(user);
				}
				else{
					Log.w(TAG, "Unable to Sign in to Firebase with Google Account", task.getException());
					setResult(Activity.RESULT_CANCELED);
					CrashReporter.getInstance(RegisterActivity.this)
							.log(Log.ERROR, TAG, "Google Sign-in Error")
							.report(task.getException());
					finish();
				}
			}
		});
	}
	
	private void attemptNotificationIDRetrieval() {
		try {
			String token = FirebaseInstanceId.getInstance().getToken();
			Log.d(TAG, "Retrieved token as: " + token);
			PrefsUtil.setNotificationToken(this, token);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	private User generateUser(FirebaseUser firebaseUser, GoogleSignInAccount account) {
		return new User(
				firebaseUser.getUid(),
				firebaseUser.getEmail(),
				account.getGivenName(),
				account.getFamilyName(),
				PrefsUtil.getNotificationToken(this),
				PrefsUtil.getCountryId(this),
				account.getId());
	}
	
	private void addUserToMFisheriesDatabase(final User user) {
		JsonObject postValues = user.toJson();
		RegisterHelper util = new RegisterHelper(this);
		util.sendSelectedModules(postValues, new FutureCallback<JsonObject>() {
			@Override
			public void onCompleted(Exception e, JsonObject result) {
				mFisheriesSaved = true;
				if (e == null && result != null) {
					JsonObject data = result.getAsJsonObject("data");
					setResult(Activity.RESULT_OK);
					if (!result.get("status").getAsString().equals("error")) {
						switch (result.get("status").getAsInt()) {
							case 201:
								Log.d(TAG, "Registration on Server was successful");
								PrefsUtil.setUserId(getApplicationContext(), data.get("id").getAsInt());
								break;
							case 200:
								Toast.makeText(getBaseContext(), "Welcome back, " + user.firstName, Toast.LENGTH_SHORT).show();
								PrefsUtil.setUserId(getApplicationContext(), data.get("id").getAsInt());
								Log.d(TAG, "User was previously registered");
								break;
							default:
								Toast.makeText(getBaseContext(), "Error, please register again", Toast.LENGTH_SHORT).show();
								setResult(Activity.RESULT_CANCELED);
								break;
						}
					}
				} else {
					Toast.makeText(getBaseContext(), R.string.error_system, Toast.LENGTH_SHORT).show();
					setResult(Activity.RESULT_CANCELED);
				}
				if (fireBaseSaved) finish();
			}
		});
	}
	
	public void addUserToFirebaseDatabase(User user) {
		Map<String, Object> postValues = user.toMap();
		Map<String, Object> childUpdates = new HashMap<>();
		childUpdates.put("/users/" + user.getUid(), postValues);
		databaseReference.updateChildren(childUpdates).addOnCompleteListener(new OnCompleteListener<Void>() {
			@Override
			public void onComplete(@NonNull Task<Void> task) {
				fireBaseSaved = true;
				if (mFisheriesSaved) finish();
			}
		});
	}
}
