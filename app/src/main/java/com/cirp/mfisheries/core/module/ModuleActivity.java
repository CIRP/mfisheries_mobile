package com.cirp.mfisheries.core.module;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Toast;

import com.cirp.mfisheries.App;
import com.cirp.mfisheries.R;
import com.cirp.mfisheries.SettingsActivity;
import com.cirp.mfisheries.fewer.supportFewer.SupportFEWER;
import com.cirp.mfisheries.fewer.supportFewer.supportFEWERActivity;
import com.cirp.mfisheries.sms.addAlertNumber.AddAlertNumberActivity;
import com.cirp.mfisheries.core.BaseActivity;
import com.cirp.mfisheries.core.Country;
import com.cirp.mfisheries.core.CountryAdapter;
import com.cirp.mfisheries.core.register.RegisterActivity;
import com.cirp.mfisheries.core.register.RegisterHelper;
import com.cirp.mfisheries.fewer.abtFewer.AboutFEWERActivity;
import com.cirp.mfisheries.util.CountryUtil;
import com.cirp.mfisheries.util.NetUtil;
import com.cirp.mfisheries.util.PrefsUtil;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.google.gson.JsonObject;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;

import java.util.List;

public abstract class ModuleActivity extends BaseActivity {

	public Module module;
	private Tracker mTracker;
	private String moduleID;
	private String moduleName;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(getLayoutResourceId());

		// Set Up Toolbar
		Toolbar toolbar = findViewById(R.id.toolbar	);
		if (toolbar != null) {
			setToolBar(toolbar);
		}

		App application = (App) getApplication();
		mTracker = application.getDefaultTracker();
	}
	
	protected void setToolBar(Toolbar toolbar){
		//set the colour for the toolbar and status bar
		toolbar.setBackgroundColor(ContextCompat.getColor(this, getColor()));
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
			Window window = getWindow();
			window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
			window.setStatusBarColor(ContextCompat.getColor(this, getColorDark()));
		}
		setSupportActionBar(toolbar);
		if (getSupportActionBar()!=null)getSupportActionBar().setDisplayHomeAsUpEnabled(true);
	}

	@Override
	public void onStart() {
		super.onStart();
		if(module != null) {
			if (mTracker != null) {
				String name = module.getName();
				mTracker.setScreenName(String.format("Module: - %s", name));
				mTracker.send(new HitBuilders.ScreenViewBuilder().build());
			}
			// Determine if user is registered
			if (module.needsRegistration){
				Log.d(module.moduleId, String.format("%s requires registration", module.moduleId));
				ensureRegistered();
			}
		}
	}

	public abstract int getLayoutResourceId();

	public int getColor(){
		return R.color.blue;
	}

	public int getColorDark(){
		return R.color.blueDark;
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.menu_module, menu);
		return true;
	}
	

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();

		//noinspection SimplifiableIfStatement
		if (id == R.id.action_settings) {
			Intent settings = new Intent(this, SettingsActivity.class);
			startActivity(settings);
			return true;
		}
//		if (id ==R.id.action_sendSms){
//			startActivity(new Intent(this, AddAlertNumberActivity.class));
//			return true;
//		}
		
		if (id == R.id.action_viewabout){
			startActivity(new Intent(this, AboutFEWERActivity.class));
			return true;
		}

//		if(id == R.id.action_support){
//			startActivity(new Intent(this, supportFEWERActivity.class));
//			return true;
//		}
		
//		if (id==R.id.action_testBroadcast){
//			//TODO: Remove this after broadcasting works
//			new android.support.v7.app.AlertDialog.Builder(this)
//					.setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
//						@Override
//						public void onClick(DialogInterface dialogInterface, int i) {
//							Ion.with(ModuleActivity.this)
//									.load("POST", NetUtil.API_URL + NetUtil.TEST_BROADCAST)
//									.setHeader(NetUtil.API_KEY_PARAM, NetUtil.API_KEY)
//									.setMultipartParameter("message","Broadcast Message Test")
//									.setMultipartParameter("simCountry",PrefsUtil.getCountryId(ModuleActivity.this))
//									.asJsonObject()
//									.setCallback(new FutureCallback<JsonObject>() {
//										@Override
//										public void onCompleted(Exception e, JsonObject result) {
//											if (e==null &&result!=null){
//												Toast.makeText(ModuleActivity.this,"I should receive a text soon",Toast.LENGTH_SHORT).show();
//											}
//										}
//									});
//						}
//					})
//					.setNegativeButton(android.R.string.no,null)
//					.setMessage("Would you like to test broadcast?")
//					.setTitle("Add number")
//					.show();
//
//		}

		return super.onOptionsItemSelected(item);
	}
	
	public void ensureRegistered(){
		moduleID = "Module";
		moduleName = "Application Module";
		if (module != null){
			moduleID = module.getId();
			moduleName = module.getName();
		}
		// If user is not logged In
		boolean isNotRegistered = PrefsUtil.getUserId(this) == -999;
		Log.d(moduleName, "Is Registered:"+String.valueOf(isNotRegistered));
		Log.d(moduleName, "ID is:"+String.valueOf(PrefsUtil.getUserId(this)));

		// If country is not specified
		isNotRegistered = isNotRegistered || PrefsUtil.getCountryId(this).equalsIgnoreCase("-1");
		Log.d(moduleName, "Is Registered:"+String.valueOf(isNotRegistered));
		Log.d(moduleName, "Country ID is:"+String.valueOf(PrefsUtil.getCountryId(this).equalsIgnoreCase("-1")));
		Log.d(moduleName, "Country ID is:"+String.valueOf(PrefsUtil.getCountry(this)));


		isNotRegistered = isNotRegistered || PrefsUtil.getCountry(this).equals("");
		Log.d(moduleName, "Is Registered:"+String.valueOf(isNotRegistered));
		Log.d(moduleName, "Country ID is:"+String.valueOf(PrefsUtil.getCountry(this).equals("")));
		Log.d(moduleName, "Country ID is:"+String.valueOf(PrefsUtil.getCountry(this)));


		// Ensures the userid and the country id is set
		if (isNotRegistered){
			new AlertDialog.Builder(this)
					.setTitle("Sign In Required")
					.setMessage("Registration is required to use " + moduleName + ". Would you like to sign-in now")
					.setIcon(android.R.drawable.ic_dialog_alert)
					.setPositiveButton("Sign-in", new DialogInterface.OnClickListener() {
						@Override
						public void onClick(DialogInterface dialogInterface, int i) {
							dialogInterface.dismiss();
						}
					})
					.setNeutralButton("No", new DialogInterface.OnClickListener() {
						@Override
						public void onClick(DialogInterface dialogInterface, int i) {
							finish();
						}
					})
					.setOnDismissListener(new DialogInterface.OnDismissListener() {
						@Override
						public void onDismiss(DialogInterface dialog) {
							handleRegistration();
						}
					})
					.show();
		}else  Log.d(moduleID, "Previously Registered");
	}
	
	protected void handleRegistration(){

		if (PrefsUtil.getCountryId(this).equals("-1")){
			selectCountry();
		}else{
			launchRegistration();
		}
	}
	
	protected void launchRegistration(){
		Intent signIn = new Intent(ModuleActivity.this, RegisterActivity.class);
		startActivityForResult(signIn, RegisterActivity.SIGN_IN_REQUEST);
	}
	
	protected void selectCountry() {
		final CountryUtil countryUtil = CountryUtil.getInstance(this);
		countryUtil.retrieveCountries(new CountryUtil.CountryRetrievedListener() {
			@Override
			public void processCountries(final List<Country> countries) {
				if (countries != null){
					//show country dialog
					for (Country c : countries) Log.d(moduleID, c.toString());
					final CountryAdapter arrayAdapter = new CountryAdapter(ModuleActivity.this, countries);
					Log.d(moduleID, "Attempting to display country list:" + countries.size());
					
					new AlertDialog
							.Builder(ModuleActivity.this)
							.setTitle(getResources().getString(R.string.dialog_country))
							.setCancelable(false)
							.setAdapter(arrayAdapter,
									new DialogInterface.OnClickListener() {
										@Override
										public void onClick(DialogInterface dialog, int which) {
											Country selectedCountry = countries.get(which);
											String country = selectedCountry.getName();
											Log.d(moduleID, String.format(
													"Country: %s, Path: %s, ID: %s",
													selectedCountry.getName(),
													selectedCountry.getPath(),
													selectedCountry.getId()
											));
											countryUtil.saveCountryData(selectedCountry);
											//load available modules for country
											handleRegistration();
										}
									})
							.show();
				}
			}
		});
		
	}
	
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data){
		RegisterHelper helper = new RegisterHelper(this);
		helper.onActivityResult(this, requestCode, resultCode, data);
	}
}
