package com.cirp.mfisheries.core;

import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.cirp.mfisheries.alerts.GroupDBHelper;
import com.cirp.mfisheries.alerts.model.AlertDBHelper;
import com.cirp.mfisheries.core.location.OfflineTrackDBHelper;
import com.cirp.mfisheries.lek.models.LEKDBHelper;
import com.cirp.mfisheries.nav.MapMarkerDBHelper;

import java.util.ArrayList;

public class DatabaseHandler {

	private static final String DATABASE_NAME = "mfisheries.db";
	private static final int DATABASE_VERSION = 3;

	public static final byte MAP_MARKER_DB_HELPER = 1;
	public static final byte OFFLINE_TRACK_DB_HELPER = 2;
	public static final byte GCM_HISTORY_DB_HELPER = 3;
	public static final byte ALERT_DB_HELPER = 4;
	public static final byte LEK_DB_HELPER = 5;
	public static final byte GROUP_DB_HELPER = 6;

	private DatabaseHelper databaseHelper;

	public OfflineTrackDBHelper offlineTrackDBHelper;
	public MapMarkerDBHelper mapMarkerDBHelper;
	public LEKDBHelper offlineLEKDBHelper;
	public AlertDBHelper alertDBHelper;
	public GroupDBHelper groupDBHelper;

	public static SQLiteDatabase database;

	public DatabaseHandler(Context context, int application_type) {
		databaseHelper = new DatabaseHelper(context);
		switch (application_type) {
			case MAP_MARKER_DB_HELPER:
				mapMarkerDBHelper = new MapMarkerDBHelper();
				break;
			case OFFLINE_TRACK_DB_HELPER:
				offlineTrackDBHelper = new OfflineTrackDBHelper();
				break;
			case ALERT_DB_HELPER:
				alertDBHelper = new AlertDBHelper();
				break;
			case GCM_HISTORY_DB_HELPER:
				// TODO Removed History of GCM messages
				break;
			case LEK_DB_HELPER:
				offlineLEKDBHelper = new LEKDBHelper();
				break;
			case GROUP_DB_HELPER:
				groupDBHelper = new GroupDBHelper();
				break;
			default:
				break;
		}
	}

	public DatabaseHandler open() throws SQLException {
		database = databaseHelper.getWritableDatabase();
		return this;
	}

	public void close() {
		databaseHelper.close();
	}

	private static class DatabaseHelper extends SQLiteOpenHelper {
		DatabaseHelper(Context context) {
			super(context, DATABASE_NAME, null, DATABASE_VERSION);
		}

		@Override
		public void onCreate(SQLiteDatabase db) {
			db.execSQL(MapMarkerDBHelper.CREATE_MAP_MARKER_TABLE);
			db.execSQL(OfflineTrackDBHelper.CREATE_OFFLINE_TRACK_TABLE);
			db.execSQL(AlertDBHelper.CREATE_ALERT_TABLE);
			db.execSQL(LEKDBHelper.CREATE_OFFLINE_LEK_TABLE);
			db.execSQL(GroupDBHelper.CREATE_GROUP_TABLE);
		}

		@Override
		public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
			Log.d("DatabaseHelper", String.format("Upgrading database from %d to %d", oldVersion, newVersion));
			dropAllTables(db);
			onCreate(db);
		}

		// Implementation based on https://stackoverflow.com/questions/38672579/delete-all-tables-from-sqlite-database
		private void dropAllTables(SQLiteDatabase db){
			// query to obtain the names of all tables in your database
			Cursor c = db.rawQuery("SELECT name FROM sqlite_master WHERE type='table'", null);
			//noinspection TryFinallyCanBeTryWithResources
			try {
				ArrayList<String> tables = new ArrayList<>(c.getCount());
				// iterate over the result set, adding every table name to a list
				while (c.moveToNext()) {
					tables.add(c.getString(0));
				}
				// call DROP TABLE on every table name
				for (String table : tables) {
					if (table.startsWith("sqlite_")) {
						continue;
					}
					String dropQuery = "DROP TABLE IF EXISTS " + table;
					db.execSQL(dropQuery);
				}
			}catch(Exception e){
				e.printStackTrace();
			}finally {
				c.close();
			}
		}
	}
}
