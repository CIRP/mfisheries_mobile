package com.cirp.mfisheries.core.download;

import android.app.IntentService;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.util.Log;

import com.cirp.mfisheries.core.module.Module;
import com.cirp.mfisheries.core.module.ModuleFactory;
import com.cirp.mfisheries.util.FileUtil;

import java.io.File;
import java.util.ArrayList;
import java.util.concurrent.CountDownLatch;


@SuppressWarnings("SameParameterValue")
public class InstallModuleService extends IntentService {
	
	// IntentService can perform, e.g. ACTION_FETCH_NEW_ITEMS
	public static final String INTENT_MODULE_COMPLETE = "com.cirp.mfisheries.core.download.id.install_module_complete";
	public static final String INTENT_MODULES_COMPLETE = "com.cirp.mfisheries.core.download.id.install_modules_complete";
	public static final String INTENT_MODULE_START = "com.cirp.mfisheries.core.download.id.install_module_start";
	private static final String ACTION_INSTALL_MODULES = "com.cirp.mfisheries.core.download.action.install_modules";
	
//	private static final String RESULT_FAILED = "com.cirp.mfisheries.core.download.result.failed";
	public static final String RESULT_INSTALLED = "com.cirp.mfisheries.core.download.result.installed";
//	public static final String STATUS_INFO = "com.cirp.mfisheries.core.download.status.info";
	public static final String STATUS_INSTALL_INFO = "com.cirp.mfisheries.core.download.status.install_info";
	
	// TODO: Rename parameters
	private static final String EXTRA_MODULES = "com.cirp.mfisheries.core.download.extra.EXTRA_MODULES";
	private final static String TAG = "InstallModuleService";
	
	// Download-related attributes
	private CountDownLatch downloadSignal;
	private BroadcastReceiver onDownloadCompleted;
	private boolean isDownloadRegistered;
	
	// Extract-related attributes
	private CountDownLatch extractSignal;
	private BroadcastReceiver onExtractCompleted;
	private boolean isExtractRegistered;
	private boolean downloadResult;
	private boolean extractResults;
	
	public InstallModuleService() {
		super("InstallModuleService");
	}
	
	/**
	 * This static method is used to launch the Service. The information and parameters needed are
	 * defined within the method to hide details for creating the service
	 * @param context The current service or activity context needed to launch the service
	 * @param modules The list of module ids that are installed. The service will filter modules that require downloads
	 */
	public static void requestInstallModules(Context context, ArrayList<String> modules) {
		Intent intent = new Intent(context, InstallModuleService.class);
		intent.setAction(ACTION_INSTALL_MODULES);
		Bundle bundle = new Bundle();
		bundle.putSerializable(EXTRA_MODULES, modules);
		intent.putExtras(bundle);
		context.startService(intent);
	}
	
	/**
	 * Starting point of the Service
	 * @param intent Information needed to initialize the service
	 */
	@SuppressWarnings("unchecked")
	@Override
	protected void onHandleIntent(Intent intent) {
		if (intent != null) {
			final String action = intent.getAction();
			if (ACTION_INSTALL_MODULES.equals(action)) {
				Bundle bundle = intent.getExtras();
				if (bundle != null && bundle.containsKey(EXTRA_MODULES)) { // Ensure we have the bundle
					ArrayList<String> modules = (ArrayList<String>) bundle.getSerializable(EXTRA_MODULES);
					this.requestInstallModules(modules);
				}
			}
		}
	}
	
	/**
	 * Sends the Broadcast to notify subscribers the state of the installation for the module identified
	 * @param module The id of the module currently being processed
	 * @param success The status of the installation process (true if completed and false if failed)
	 */
	public void sendInstallBroadcast(String module, boolean success){
		Intent intent = new Intent(INTENT_MODULE_COMPLETE);
		intent.putExtra(STATUS_INSTALL_INFO, module);
		intent.putExtra(RESULT_INSTALLED, success);
		sendBroadcast(intent);
	}
	
	/**
	 * Sends a notification to listeners that the module is currently being processed
	 * @param module The module id that is being processed
	 */
	public void sendInstallStartBroadcast(String module){
		Intent intent = new Intent(INTENT_MODULE_START);
		intent.putExtra(STATUS_INSTALL_INFO, module);
		sendBroadcast(intent);
	}
	
	/**
	 * Sends a notification when all the passed modules are processed
	 * @param successModules The list of module id that were successfully completed. Will be empty if no modules were completed
	 */
	public void sendCompletedBroadcast(ArrayList<String> successModules){
		Intent intent = new Intent(INTENT_MODULES_COMPLETE);
		boolean result = (successModules.size() > 0);
		
		Bundle bundle = new Bundle();
		bundle.putSerializable(EXTRA_MODULES, successModules);
		bundle.putBoolean(RESULT_INSTALLED, result);
		intent.putExtras(bundle);
		sendBroadcast(intent);
	}
	
	/**
	 * Determine based on the URL of the module if a resource is required
	 * @param module The module currently being processed
	 * @return true if a module needs an additional resource to be download and false otherwise
	 */
	public boolean moduleHasDownload(Module module){
		final String dataLoc = module.getDataLoc();
		return dataLoc != null && !dataLoc.equals("") && dataLoc.length() > 4;
	}
	
	/**
	 * Determine if the zip file exists to prevent needless re-downloading of information that already exists
	 * @param module The module currently being processed
	 * @return true if the zip file exists
	 */
	public boolean moduleResourceZipExists(Module module){
		String filePath = FileUtil.getFileUri("/" + module.getId()+ ".zip").toString();
		File file = new File(filePath);
		// File already exists for the specific module
		return file.exists() || FileUtil.directoryExists(filePath);
	}
	
	/**
	 * Determine if the extracted folder exists to prevent needless re-downloading and extraction of information that already exists
	 * @param module The module currently being processed
	 * @return true if the resource folder exists
	 */
	public boolean moduleResourceFolderExists(Module module){
		String filePath = FileUtil.getFileUri("/" + module.getId()).toString();
		File file = new File(filePath);
		// Folder already exists for the specific module
		return file.exists() && FileUtil.directoryExists(filePath);
	}
	
	/**
	 * The method that will make request to download and unzip services. The system signals to synchronize the implementation
	 * by waiting on responses by the methods
	 * @param modules The list of all method id be installed
	 */
	public void requestInstallModules(ArrayList<String> modules) {
		ArrayList<String> successModules = new ArrayList<>();
		for(String moduleName: modules) {
			try {
				ModuleFactory factory = ModuleFactory.getInstance(this);
				Module module = factory.getModule(moduleName);
				// TODO - Install Module - If structure needlessly complicated due to haste. Consider simplification
				if (module != null) {
					Log.d(TAG, "Module is: " + module.getName());
					sendInstallStartBroadcast(module.getId());
					// Ensure we have have a URL for the module
					if (moduleHasDownload(module)) {
						// if the resource folder exists (i.e. module successfully download and extracted)
						if (moduleResourceFolderExists(module)){
							Log.d(TAG, String.format("Within Loop - %s module was previously completed successfully", module.getName()));
							module.onInstalled();
							successModules.add(module.getId());
							sendInstallBroadcast(module.getId(), true);
						}
						// If File previously downloaded but not extracted
						else if (moduleResourceZipExists(module)){
							// unzip existing resource
							if (unzipModule(module)) {
								Log.d(TAG, String.format("Within Loop - Extraction for %s module was completed successfully", module.getName()));
								module.onInstalled();
								successModules.add(module.getId());
								sendInstallBroadcast(module.getId(), true);
							}else {
								Log.d(TAG, String.format("Within Loop - Download for %s module was not completed successfully", module.getName()));
								sendInstallBroadcast(module.getId(), false);
							}
						}
						// Download Module Resources
						else if (downloadModule(module)) {
							Log.d(TAG, String.format("Within Loop - Download for %s module was completed successfully", module.getName()));
							// If download Successful then unzip resources
							if (unzipModule(module)) {
								Log.d(TAG, String.format("Within Loop - Extraction for %s module was completed successfully", module.getName()));
								module.onInstalled();
								successModules.add(module.getId());
								sendInstallBroadcast(module.getId(), true);
							} else {
								Log.d(TAG, String.format("Within Loop - Extraction for %s module was not completed successfully", module.getName()));
								sendInstallBroadcast(module.getId(), false);
							}
						} else {
							Log.d(TAG, String.format("Within Loop - Download for %s module was not completed successfully", module.getName()));
							sendInstallBroadcast(module.getId(), false);
						}
					}else{
						Log.d(TAG, String.format("Within Loop - Module %s did not have a resource to download", module.getName()));
						sendInstallBroadcast(module.getId(), true);
						successModules.add(module.getId());
					}
				}
			} catch (Exception e) {
				sendInstallBroadcast(moduleName, false);
				e.printStackTrace();
			}
		}
		Log.d(TAG, "Completed Processing all Modules");
		sendCompletedBroadcast(successModules);
	}
	
	/**
	 * This method will utilize the mFisheries DownloadService to download and store the additional services for the application
	 * @param module The module currently being processed
	 * @return true if broadcast identified that download was successful and false otherwise
	 * @throws InterruptedException Throws the exception if unable to lock execution needed to provide synchronous execution
	 */
	public boolean downloadModule(final Module module)throws InterruptedException{
		return downloadModule(module, null);
	}
	
	/**
	 * This method will utilize the mFisheries DownloadService to download and store the additional services for the application
	 * @param module The module currently being processed
	 * @param onDownloadCompleted The BroadcastReceiver that will listener to broadcast from the download service. The system will use a default if null is supplied
	 * @return true if broadcast identified that download was successful and false otherwise
	 * @throws InterruptedException Throws the exception if unable to lock execution needed to provide synchronous execution
	 */
	public boolean downloadModule(final Module module, BroadcastReceiver onDownloadCompleted) throws InterruptedException {
		downloadResult = false;
		// Setup Listener
		downloadSignal = new CountDownLatch(1);
		if (onDownloadCompleted == null) {
			onDownloadCompleted = new BroadcastReceiver() {
				@Override
				public void onReceive(Context context, Intent intent) {
					// If we received the module we asked for
					if (intent.hasExtra(DownloadService.MODULE_ID) && intent.getStringExtra(DownloadService.MODULE_ID).equals(module.getId())) {
						// we check the status and update the result boolean
						downloadResult = intent.getBooleanExtra(DownloadService.DOWNLOAD_STATUS, false);
					}
					downloadSignal.countDown();
				}
			};
		}
		this.onDownloadCompleted = onDownloadCompleted;
		// Register Listener and update tracking variables
		registerReceiver(onDownloadCompleted, new IntentFilter(DownloadService.DOWNLOADS_COMPLETE_BROADCAST));
		isDownloadRegistered = true;
		// Make Download Request with the built in DownloadManager
		DownloadService.startDownloadService(this, module.getId());
		// block method until download is completed
		downloadSignal.await();
		unregisterReceiver(onDownloadCompleted);
		isDownloadRegistered = false;
		return downloadResult;
	}
	
	/**
	 * This method will utilize the Zip Service to unzip the downloaded resource for the identified module. Utilizes the default broadcast receiver for zip service
	 * @param module The module that has a downloaded module within the mFisheries folder
	 * @return returns true if the unzip was executed successfully.
	 * @throws InterruptedException Throws the exception if unable to lock execution needed to provide synchronous execution
	 */
	public boolean unzipModule(final Module module)throws InterruptedException{
		return unzipModule(module, null);
	}
	
	/**
	 * This method will utilize the Zip Service to unzip the downloaded resource for the identified module.
	 * @param module The module that has a downloaded module within the mFisheries folder
	 * @param onExtractCompleted The BroadcastReceiver that will listener to broadcast from the zip service. The system will use a default if null is supplied
	 * @return returns true if the unzip was executed successfully.
	 * @throws InterruptedException Throws the exception if unable to lock execution needed to provide synchronous execution
	 */
	public boolean unzipModule(final Module module, BroadcastReceiver onExtractCompleted) throws InterruptedException {
		extractResults = false;
		// setup listener
		extractSignal = new CountDownLatch(1);
		if (onExtractCompleted == null) {
			onExtractCompleted = new BroadcastReceiver() {
				public void onReceive(Context context, Intent intent) {
					// If we received the module we asked for
					if (intent.hasExtra(ZipService.MODULE_ID) && intent.getStringExtra(ZipService.MODULE_ID).equals(module.getId())) {
						extractResults = intent.getBooleanExtra(ZipService.EXTRACT_STATUS, false);
					}
					extractSignal.countDown();
				}
			};
		}
		this.onExtractCompleted = onExtractCompleted;
		registerReceiver(onExtractCompleted, new IntentFilter(ZipService.EXTRACT_COMPLETE_BROADCAST));
		isExtractRegistered = true;
		// Perform Zip extraction
		ZipService.launchZipService(this, module.getId());
		// block method until extraction is completed
		extractSignal.await();
		unregisterReceiver(onExtractCompleted);
		isExtractRegistered = false;
		return extractResults;
	}
	
	/**
	 * Overrides onDestroy to ensure all listeners are properly disposed to prevent memory leakage.
	 * This is necessary in-case the system clears the service (such as in low memory situations)
	 */
	@Override
	public void onDestroy(){
		if (isDownloadRegistered && onDownloadCompleted != null)unregisterReceiver(onDownloadCompleted);
		if (isExtractRegistered && onExtractCompleted != null)unregisterReceiver(onExtractCompleted);
		super.onDestroy();
	}
}
