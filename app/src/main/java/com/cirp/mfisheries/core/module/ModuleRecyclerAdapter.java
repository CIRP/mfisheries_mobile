package com.cirp.mfisheries.core.module;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.cirp.mfisheries.R;
import com.cirp.mfisheries.util.ModuleUtil;

import java.util.List;

public class ModuleRecyclerAdapter extends RecyclerView.Adapter<ModuleRecyclerAdapter.ModuleHolder> {
	
	private final Context mContext;
	private final List<String> modules;
	private View.OnClickListener placeholderListener;
	
	public ModuleRecyclerAdapter(Context context, List<String> modules) {
		this.mContext = context;
		this.modules = modules;
	}
	
	@Override
	public ModuleHolder onCreateViewHolder(ViewGroup parent, int viewType) {
		View rowView = LayoutInflater.from(mContext).inflate(R.layout.layout_module, parent, false);
		return new ModuleHolder(rowView);
	}
	
	@Override
	public void onBindViewHolder(ModuleHolder holder, int position) {
		final String moduleName = modules.get(position);
		if (moduleName.equals(ModuleUtil.PLACEHOLDER_TILE))
			createPlaceholder(holder);
		else{
			holder.moduleName.setText(moduleName);
			ModuleFactory moduleFactory = ModuleFactory.getInstance(mContext);
			final Module module = moduleFactory.getModule(moduleName);
			if (module != null) { // What should the system do if a module is not available
				if (module.getImageResource() != 0)
					holder.imageView.setImageResource(module.getImageResource());
				holder.imageView.setOnClickListener(new View.OnClickListener() {
					@Override
					public void onClick(View v) {
						module.display();
					}
				});
			} else {
				Log.wtf(ModuleAdapter.class.getSimpleName(), "No Module Created/Defined for: " + moduleName);
			}
		}
	}
	
	//creates a placeholder tile if the number of selected modules is odd
	public void createPlaceholder(ModuleHolder holder) {
		Log.d("Placeholder", "Creating placeholder");
		holder.moduleName.setText(mContext.getResources().getString(R.string.action_add));
		holder.imageView.setImageResource(R.drawable.icon_placeholder);
		
		if (this.placeholderListener != null)
			holder.imageView.setOnClickListener(this.placeholderListener);
	}
	
	@Override
	public int getItemCount() {
		return modules.size();
	}
	
	public void setPlaceholderListener(View.OnClickListener placeholderListener) {
		Log.d("Placeholder", "Setting placeholder");
		this.placeholderListener = placeholderListener;
	}
	
	class ModuleHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
		ImageView imageView;
		TextView moduleName;
		
		public ModuleHolder(View itemView) {
			super(itemView);
			itemView.setOnClickListener(this);
			imageView = itemView.findViewById(R.id.moduleImage);
			moduleName = itemView.findViewById(R.id.moduleName);
		}
		
		@Override
		public void onClick(View view) {
		
		}
	}
}
