package com.cirp.mfisheries.core.fcm;

// TODO Merge representation between the two use of users
public class User {

    public final String uid;
    public final String userid;
    public final String token;
    private final String email;
    private final String firstName;
    private final String lastName;
    private final String firebaseToken;

    public User(String userid, String token) {
        this.userid = userid;
        this.token = token;

        this.uid = userid;
        this.email = null;
        this.firstName = null;
        this.lastName = null;
        this.firebaseToken = token;
    }

    public User(String uid, String email, String firstName, String lastName, String firebaseToken) {
        this.uid = uid;
        this.email = email;
        this.firstName = firstName;
        this.lastName = lastName;
        this.firebaseToken = firebaseToken;

        this.userid = uid;
        this.token = firebaseToken;

    }
}
