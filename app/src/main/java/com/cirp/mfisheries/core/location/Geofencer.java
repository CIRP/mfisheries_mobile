package com.cirp.mfisheries.core.location;

import android.content.Context;
import android.os.Environment;
import android.support.annotation.Nullable;
import android.util.Log;

import com.cirp.mfisheries.R;
import com.cirp.mfisheries.util.CountryUtil;
import com.cirp.mfisheries.util.CrashReporter;
import com.cirp.mfisheries.util.FileUtil;
import com.cirp.mfisheries.util.PrefsUtil;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

public class Geofencer {
	
	private static final String TAG = "Geofencer";
	private static String JSON_FILE_NAME = "_geofence.json";
	private static ArrayList<Polygon> polygons = new ArrayList<>();
	private static int num_islands;
	
	
	private static Polygon generatePolygon(JSONArray points) throws JSONException {
		List<Double> lats = new ArrayList<>();
		List<Double> lngs = new ArrayList<>();
		for (int i = 0; i < points.length(); i++) {
			lats.add(points.getJSONObject(i).getDouble("latitude"));
			lngs.add(points.getJSONObject(i).getDouble("longitude"));
		}
		return new Polygon(lats, lngs);
	}
	
	public static Boolean isWithinCountry(Context context, double latitude, double longitude, boolean reset) {
		if (reset)polygons = new ArrayList<>();
		return isWithinCountry(context, latitude, longitude);
	}
	
	public static Boolean isWithinCountry(final Context context, final double latitude, final double longitude) {
		try {
			if(polygons.size() < 1) {
//				String json = loadJsonFromFile(context);
//				if (json == null) {
//					Log.d(TAG, "Loading from asset");
//					json = loadJsonFromAsset(context);
//				}
				final String json = loadJsonFromAsset(context);
				// Store the full geofence json file in a variable called "baseJSON"
				final JSONObject baseJSON = new JSONObject(json);
				// Evaluate if the current country JSON is for a multi-island country
				num_islands = (baseJSON.has("num_islands") ? baseJSON.getInt("num_islands") : 1); // shorthand if
				// retrieve the points (if multiple islands, then points is a 2D array else an array of points)
				final JSONArray points = baseJSON.getJSONArray("points");
				if (num_islands > 1){
					for(int i = 0; i < num_islands; i++) {
						JSONArray island = points.getJSONArray(i);
						Polygon islandPoly = generatePolygon(island);
						polygons.add(islandPoly);
					}
				}else{
					polygons.add(generatePolygon(points));
				}
			}
			// The polygons array list has either a single polygon or a set of polygons for each island
			for(Polygon island: polygons){
				if (island.isPointInPolygon(latitude, longitude)){ // if point is located within any polygon
					return true;
				}
			}
			return false;
		} catch (JSONException ex) {
			ex.printStackTrace();
			final String message = "Error processing Geofence: " + ex.getLocalizedMessage();
			Log.d(TAG, message);
			CrashReporter.getInstance(context).log(Log.ERROR ,TAG,message);
			return false;
		}
	}

	public static String getJSONFilePath(Context context) {
		return Environment.getExternalStorageDirectory().getPath() + FileUtil.FOLDER +
				PrefsUtil.getCountry(context).toLowerCase() + JSON_FILE_NAME;
	}

	@Nullable
	public static String loadJsonFromFile(Context context) {
		try {
			final String file = getJSONFilePath(context);
			final String countryName = PrefsUtil.getCountry(context).toLowerCase();
			//checks if data is cached and load if it is
			if (FileUtil.fileExists(file))
				return FileUtil.readJSONFile(countryName + JSON_FILE_NAME);
		}catch (Exception e){
			e.printStackTrace();
		}
		return null;
	}

	@Nullable
	public static String loadJsonFromAsset(Context context) {
		try {
			InputStream inputStream;
			final String country = PrefsUtil.getCountry(context);
			Log.d(TAG, String.format("Retrieved current country as %s", country));

			if (country.equalsIgnoreCase(CountryUtil.GRENADA)) {
				inputStream = context.getResources().openRawResource(R.raw.grenada_geofence);
			}
			else if (country.equalsIgnoreCase(CountryUtil.TOBAGO)) {
				inputStream = context.getResources().openRawResource(R.raw.tobago_geofence);
			}
			else if (country.equalsIgnoreCase(CountryUtil.STVINCENT)) {
				inputStream = context.getResources().openRawResource(R.raw.stvincent_geofence);
			}
			else if (country.equalsIgnoreCase(CountryUtil.STLUCIA)) {
				inputStream = context.getResources().openRawResource(R.raw.stlucia_geofence);
			}
			else if (country.equalsIgnoreCase(CountryUtil.DOMINICA)) {
				inputStream = context.getResources().openRawResource(R.raw.dominica_geofence);
			}
			else if (country.equalsIgnoreCase(CountryUtil.STKITTS)) {
				inputStream = context.getResources().openRawResource(R.raw.stkitts_geofence);
			}
			else if (country.equalsIgnoreCase(CountryUtil.ANTIGUA)) {
				inputStream = context.getResources().openRawResource(R.raw.antigua_geofence);
			}
			else if (country.equalsIgnoreCase(CountryUtil.TRINIDAD)) {
				inputStream = context.getResources().openRawResource(R.raw.trinidad_geofence);
			}
			else if (country.equalsIgnoreCase(CountryUtil.BARBADOS)) {
				inputStream = context.getResources().openRawResource(R.raw.barbados_geofence);
			}
			else {
				inputStream = context.getResources().openRawResource(R.raw.trinidad_geofence);
			}

			int size = inputStream.available();
			byte[] buffer = new byte[size];
			inputStream.read(buffer);
			inputStream.close();
			return new String(buffer, "UTF-8");
		} catch (Exception ex) {
			ex.printStackTrace();
			Log.wtf(TAG, "Error retrieving the GeoFence: " + ex.getLocalizedMessage());
			return null;
		}
	}

	private static class Polygon {

		private List<Double> latPoints;
		private List<Double> longPoints;
		private int numberOfNodes;

		public Polygon(List<Double> latitudePoints, List<Double> longitudePoints) {
			this.latPoints = latitudePoints;
			this.longPoints = longitudePoints;
			numberOfNodes = latitudePoints.size();
		}

		// NB: Implementation seems to be based on the Ray casting algorithm => https://rosettacode.org/wiki/Ray-casting_algorithm
		public boolean isPointInPolygon(double latitude, double longitude) {
			boolean polygon_result = false;
			for (int i = 0, j = numberOfNodes - 1; i < numberOfNodes; j = i++) {
				if ((latPoints.get(i) < latitude && latPoints.get(j) >= latitude) ||
						(latPoints.get(j) < latitude && latPoints.get(i) >= latitude)) {
					if (longPoints.get(i) + (latitude - latPoints.get(i)) / (latPoints.get(j) - latPoints.get(i)) * (longPoints.get(j) - longPoints.get(i)) < longitude) {
						polygon_result = !polygon_result;
					}
				}
			}
			return polygon_result;
		}
	}
}
