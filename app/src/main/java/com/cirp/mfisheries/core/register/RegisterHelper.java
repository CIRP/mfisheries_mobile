package com.cirp.mfisheries.core.register;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.util.Log;

import com.cirp.mfisheries.util.CrashReporter;
import com.cirp.mfisheries.util.FileUtil;
import com.cirp.mfisheries.util.NetUtil;
import com.cirp.mfisheries.util.PrefsUtil;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;
import com.squareup.moshi.JsonAdapter;
import com.squareup.moshi.Moshi;

import java.util.ArrayList;
import java.util.List;

public class RegisterHelper {
	
	private static final String TAG = "RegisterHelper";
	
	final protected Context context;
	
	public RegisterHelper(Context context) {
		this.context = context;
	}
	
	public void onActivityResult(final Activity activity, int requestCode, int resultCode, Intent data){
		if (requestCode == RegisterActivity.SIGN_IN_REQUEST){
			if (resultCode == Activity.RESULT_OK){
				Log.d(TAG, "Received Registration Intent as successful");
				PrefsUtil.setIsRegistered(activity, true);
			}else{
				Log.d(TAG, "Received Registration Intent as unsuccessful");
				new AlertDialog.Builder(activity)
						.setTitle("Sign In Required")
						.setMessage("Sign In attempt failed. Ensure you are connected to the Internet and try again")
						.setIcon(android.R.drawable.ic_dialog_alert)
						.setPositiveButton("Try again", new DialogInterface.OnClickListener() {
							@Override
							public void onClick(DialogInterface dialogInterface, int i) {
								Intent intent = new Intent(activity, RegisterActivity.class);
								activity.startActivityForResult(intent, RegisterActivity.SIGN_IN_REQUEST);
							}
						})
						.setNeutralButton("No", new DialogInterface.OnClickListener() {
							@Override
							public void onClick(DialogInterface dialogInterface, int i) {
								activity.finish();
							}
						})
						.show();
			}
		}
	}
	
	public void sendSelectedModules(JsonObject data, FutureCallback<JsonObject> callback) {
		Log.d(TAG, "Send Selected Modules Called");
		try {
			final JsonObject json = new JsonObject();
			final JsonArray modules = new JsonArray();
			List<String> selectedModules = new ArrayList<>();
			List<String> tempList = FileUtil.loadSavedModules();
			for (String module : tempList) {
				selectedModules.add(module.replaceAll("\\s", ""));
			}
			
			Moshi moshi = new Moshi.Builder().build();
			JsonAdapter<List> jsonAdapter = moshi.adapter(List.class);
			
			String selected = jsonAdapter.toJson(selectedModules);
			
			JsonParser parser = new JsonParser();
			JsonArray mods = (JsonArray) parser.parse(selected);
			modules.addAll(mods);
			
			json.add("data", data);
			json.add("modules", modules);
			
			Log.d(TAG, String.format("Making Request at: %s%s with data: %s ", NetUtil.API_URL, NetUtil.ADD_GOOGLE_USER, json.toString()));
			saveGoogleInfo(json, callback);
		} catch (Exception ex) {
			Log.e(TAG, "Error generating json to send");
			CrashReporter.getInstance(context)
					.log("Unable to submit data to server")
					.report(ex);
		}
	}
	
	
	private void saveGoogleInfo(JsonObject json, FutureCallback<JsonObject> callback) {
		final String url = NetUtil.API_URL + NetUtil.ADD_GOOGLE_USER;
		Ion.with(context)
				.load("POST", url)
				.setHeader(NetUtil.API_KEY_PARAM, NetUtil.API_KEY)
				.setJsonObjectBody(json)
				.asJsonObject()
				.setCallback(callback);
	}
}
