package com.cirp.mfisheries.core.download;

import android.content.Context;
import android.content.Intent;
import android.util.Log;

import com.cirp.mfisheries.core.module.Module;
import com.cirp.mfisheries.core.module.ModuleFactory;
import com.cirp.mfisheries.util.FileUtil;
import com.cirp.mfisheries.util.NetUtil;

import java.io.File;
import java.util.List;

/**
 * A legacy class for controlling the process of downloading and extracting additional resources.
 * The logic of this class was migrated to an IntentService to facilitate a better background-oriented
 * process for managing the request
 */
@SuppressWarnings("WeakerAccess")
public class ModuleDownloader {

    private static final String TAG = "ModuleDownloader";
    private OnAllDownloadsFinishedListener mListener;
    private Context mContext;
    public List<String> queue;

//    public long lastReferenceId;
    public String lastModuleId;

    public ModuleDownloader() {
    }

    public ModuleDownloader(Context mContext, OnAllDownloadsFinishedListener mListener) {
        this.mContext = mContext;
        this.mListener = mListener;
        Log.d(TAG, "Module Downloader was created");
    }

    public void getModules(List<String>modules){
    	getModules(mContext, modules);
    }
    
    //downloads the given list of modules
    public void getModules(Context context, List<String> modules) {
        Log.d(TAG, String.format("Get Modules was created for %s modules", modules.size()));
        if (modules.isEmpty()) {
            if (mListener != null) mListener.onFinished();
            return;
        }
        
        queue = modules;
        mContext = context;
        DownloadService.setDownloader(this);
        getNextModule();
    }

    //starts the download service if given module has not already been downloaded or is set up incorrectly
    public void getModule(String moduleName) {
        Log.d(TAG, "Attempting to get module: " + moduleName);
        ModuleFactory factory = ModuleFactory.getInstance(mContext);
        Module module = factory.getModule(moduleName);
        
        if (module == null) {
            Log.d(TAG, moduleName + " does not exist in factory");
            getNextModule();
            return;
        }
        Log.d(TAG, String.format("Found location for the module %s at %s", module.getName(), String.valueOf(module.getDataLoc())));

        // If module has no resource to download
        if (module.getDataLoc() == null || module.getDataLoc().equals("")) {
            Log.d(TAG, moduleName + " has no downloadable data");
            module.onInstalled();
            getNextModule();
	        if (mListener != null)
	        	mListener.onNotification(String.format("Completed installation of %s modules", moduleName),
				        OnAllDownloadsFinishedListener.MSG_TYPE_NOTIFY);
            return;
        }

        String filePath = FileUtil.getFileUri("/" + module.getId()+ ".zip").toString();
        File file = new File(filePath);
        // File already exists for the specific module
        if (file.exists() || FileUtil.directoryExists(filePath)) {
            Log.d(TAG, moduleName + " data already downloaded. Launching Un-zipper");
	        ZipService.launchZipService(mContext, module.getId());
            module.onInstalled();
            getNextModule();
	        if (mListener != null)
		        mListener.onNotification(String.format("Download for of %s modules completed.", moduleName),
				        OnAllDownloadsFinishedListener.MSG_TYPE_NOTIFY);
            return;
        }

        if (NetUtil.isOnline(mContext)) {
            // Start service to perform download in the background
            Intent intent = new Intent(mContext, DownloadService.class);
            intent.putExtra("moduleId", module.getName());
            mContext.startService(intent);
        } else {
            Log.d(TAG, "Notify the User that No Internet Connection");
            if (mListener != null)
                mListener.onNotification("Unable to Connect to Internet", OnAllDownloadsFinishedListener.MSG_TYPE_ERR);
        }
    }

    //downloads the next module in the queue
    public void getNextModule() {
        if (queue == null || queue.isEmpty()) {
            Log.d(TAG, "All modules downloaded");
            if (mListener != null) mListener.onFinished();
            return;
        }
        getModule(queue.remove(queue.size() - 1));
    }

    
    
    //defines functionality to call for specific outcomes
    public interface OnAllDownloadsFinishedListener {

        String MSG_TYPE_NOTIFY = "NOTIFY";
        String MSG_TYPE_ERR = "ERROR";

        void onFinished();

        void onNotification(String message, String type);

    }
}
