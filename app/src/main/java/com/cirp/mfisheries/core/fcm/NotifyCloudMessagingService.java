package com.cirp.mfisheries.core.fcm;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.DrawableRes;
import android.support.annotation.NonNull;
import android.util.Log;

import com.cirp.mfisheries.App;
import com.cirp.mfisheries.R;
import com.cirp.mfisheries.alerts.AlertDetailsActivity;
import com.cirp.mfisheries.alerts.AlertsActivity;
import com.cirp.mfisheries.alerts.model.AlertDM;
import com.cirp.mfisheries.fewer.FewerActivity;
import com.cirp.mfisheries.fewer.msreport.ViewMSReportActivity;
import com.cirp.mfisheries.fewer.msreport.model.PersonReport;
import com.cirp.mfisheries.messaging.ChatActivity;
import com.cirp.mfisheries.messaging.MSGConstants;
import com.cirp.mfisheries.util.NotifyUtil;
import com.cirp.mfisheries.util.PrefsUtil;
import com.cirp.mfisheries.weather.SourceActivity;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import java.util.HashMap;
import java.util.Map;

public class NotifyCloudMessagingService extends FirebaseMessagingService {

    private final static String TAG = "NotifyCloudService";

    @Override
    public void onMessageReceived(final RemoteMessage remoteMessage) {
	    try {
		    Log.d(TAG, "From: " + remoteMessage.getFrom() + " type:" + remoteMessage.getData().get("type"));
	    } catch (Exception e) {
		    e.printStackTrace();
	    }
        
        // Check if message contains a data payload.
        if (remoteMessage.getData().size() > 0 && remoteMessage.getNotification() == null) {
	        final String type = (remoteMessage.getData().containsKey("type")) ? remoteMessage.getData().get("type") : "default";
	        Log.d(TAG, type);
	
	        // Handle Cloud Messages generated for CAP-based alerts
            if(type.equals("cap")) handleCAPMessage(this, remoteMessage);
            // Handle Cloud Messages generated for weather threshold values
            else if(type.equals("weather"))handleWeatherMessage(this, remoteMessage);
            // Handle Cloud Messages generated within the chat(messaging) module
            else if (type.equalsIgnoreCase(MSGConstants.ARG_ONE_TO_ONE))handleMsgMessage(this, remoteMessage);
            else if (type.equalsIgnoreCase(MSGConstants.ARG_GROUP_CHAT))handleMsgMessage(this, remoteMessage);
            // Handle Cloud Messages generated within the peer-based alert module
            else if(type.equals("alert")) handleAlertMessage(this, remoteMessage);
            // Handle Cloud Messages generated for Missing Persons module
            else if(type.equals("missing_person")) handleMSMessage (this, remoteMessage);
            // Handle all other Cloud Messages
            else{
	            final String title = (remoteMessage.getData().containsKey("title")) ? remoteMessage.getData().get("title") : "mFisheries";
	            final String message = remoteMessage.getData().get("message");
                final Intent intent = new Intent(this, App.getHomeClass());
                sendNotification(title, message, R.drawable.ic_warning_white_24dp, intent);
            }
        }
        // Check if message contains a notification payload.
        else if (remoteMessage.getNotification() != null) {
            Log.e(TAG, "Message Notification Body: " + remoteMessage.getNotification().getBody());
	
	        String title = remoteMessage.getNotification().getTitle();
	        String message = remoteMessage.getNotification().getBody();
	        if (title == null) title = getString(R.string.app_name);
	
	        final Intent intent = new Intent(this, App.getHomeClass());
            sendNotification(title, message, R.mipmap.ic_launcher, intent);
        }
        else{
            final String title = getString(R.string.app_name);
            final String message = title + " requires your attention";
            final Intent intent = new Intent(this, App.getHomeClass());
            sendNotification(title, message, R.mipmap.ic_launcher, intent);
        }
    }
    
    private void sendAck(final String alertId) {
        final String userId = String.valueOf(PrefsUtil.getUserId(getApplicationContext()));
        final Map <String, Object> values = new HashMap<>();
        
        final DatabaseReference mDatabase = App.getFBDatabase().getReference();
        mDatabase
            .child("cap_alert_delivery")
                .child(alertId)
                .child(userId)
                .setValue(true);
    }
    
    private void sendNotification(final String title, final String message, @DrawableRes int icon, @NonNull final Intent intent) {
        NotifyUtil util = new NotifyUtil(this);
        util.notify(title, message, icon, intent);
    }
    
    // Methods to be refactor to appropriate strategy with more time
    private void handleCAPMessage(Context context, RemoteMessage remoteMessage){
        final String alertId = remoteMessage.getData().get("alertId");
        sendAck(alertId);
        final Intent intent = new Intent(context, FewerActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        final String title = remoteMessage.getData().get("title");
        final String message = remoteMessage.getData().get("message");
        sendNotification(title, message, R.drawable.ic_warning_white_24dp, intent);
    }
    
    private void handleWeatherMessage(Context context, RemoteMessage remoteMessage){
        final Intent intent = new Intent(context, SourceActivity.class);
        intent.putExtra("selectedSource", remoteMessage.getData().get("source"));
        final String title = remoteMessage.getData().get("title");
        final String message = remoteMessage.getData().get("message");
        sendNotification(title, message, R.drawable.ic_warning_white_24dp, intent);
    }

    private void handleMSMessage(Context context, RemoteMessage remoteMessage){
        Log.d(TAG,"Message is: "+String.valueOf(remoteMessage.getData()));
        final Intent intent = new Intent(context, ViewMSReportActivity.class);

        String id = remoteMessage.getData().get("id");
        String title = remoteMessage.getData().get("title");
        String description = remoteMessage.getData().get("description");
        String additional = remoteMessage.getData().get("additional");
        String contact = remoteMessage.getData().get("contact");
        String filepath = remoteMessage.getData().get("filepath");
        String latitude = remoteMessage.getData().get("latitude");
        String longitude = remoteMessage.getData().get("longitude");
        String userid = remoteMessage.getData().get("userid");
        String countryid = remoteMessage.getData().get("countryid");
        String isFound = remoteMessage.getData().get("isFound");
        String aDate = remoteMessage.getData().get("aDate");
        String timecreated = remoteMessage.getData().get("timeccreated");
        String fullname = remoteMessage.getData().get("fullname");
        String user = remoteMessage.getData().get("user");
        String country = remoteMessage.getData().get("country");

        PersonReport report = new PersonReport();
        report.id = id;
        report.title = title;
        report.description = description;
        report.additional = additional;
        report.contact = contact;
        report.filepath = filepath;
        report.latitude = latitude;
        report.longitude = longitude;
        report.userid = userid;
        report.countryid = countryid;
        report.isFound = isFound;
        report.aDate = aDate;
        report.timecreated = timecreated;
        report.fullname = fullname;
        report.user = user;
        report.country = country;

        intent.putExtra("report", report);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

        final String notifTitle = remoteMessage.getData().get("title");
        final String message = String.format(
                "Missing Person by %s",
                remoteMessage.getData().get("user")
        );

        // Only notify of message if it was not sent by me
        if (!String.valueOf(PrefsUtil.getUser(this)).equals(remoteMessage.getData().get("user"))) {
            sendNotification(title, message, R.drawable.ic_warning_white_24dp, intent);
        }

    }
    
    private void handleAlertMessage(Context context, RemoteMessage remoteMessage){
        Log.d(TAG,"Message is: "+String.valueOf(remoteMessage.getData()));
        final Intent intent = new Intent(context , AlertDetailsActivity.class);
//        intent.putExtra("groupName", remoteMessage.getData().get("group"));
//        intent.putExtra("groupId", remoteMessage.getData().get("groupid"));

        int id = Integer.parseInt(remoteMessage.getData().get("id"));
        int userid = Integer.parseInt(remoteMessage.getData().get("userid"));
        int groupid = Integer.parseInt(remoteMessage.getData().get("groupid"));
        String username = remoteMessage.getData().get("username");
        String latitude = remoteMessage.getData().get("latitude");
        String longitude = remoteMessage.getData().get("longitude");
        String deviceTimestamp = remoteMessage.getData().get("deviceTimestamp");
        String messagecontent = remoteMessage.getData().get("messagecontent");
        String messageDescription = remoteMessage.getData().get("messageDescription");
        String messageSeverity = remoteMessage.getData().get("messageSeverity");
        boolean isPublic = Boolean.parseBoolean(remoteMessage.getData().get("isPublic"));
        boolean isVerified = Boolean.parseBoolean(remoteMessage.getData().get("isVerified"));
        int likes = Integer.parseInt(remoteMessage.getData().get("likes"));
        int dislikes = Integer.parseInt(remoteMessage.getData().get("dislikes"));
        String fullname = remoteMessage.getData().get("fullname");


//        AlertDM alertDM = new AlertDM(userid, groupid, latitude, longitude, deviceTimestamp, messagecontent, messageDescription, messageSeverity);
        AlertDM alertDM = new AlertDM();
        alertDM.userid = userid;
        alertDM.groupid = groupid;
        alertDM.latitude = latitude;
        alertDM.longitude = longitude;
        alertDM.deviceTimestamp = deviceTimestamp;
        alertDM.messagecontent = messagecontent;
        alertDM.messageDescription = messageDescription;
        alertDM.messageSeverity = messageSeverity;
        alertDM.id = id;
        alertDM.username = username;
        alertDM.isPublic = isPublic;
        alertDM.isVerified = isVerified;
        alertDM.likes = likes;
        alertDM.dislikes = dislikes;
        alertDM.fullname = fullname;

        intent.putExtra("alert", alertDM);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
    
        final String title = remoteMessage.getData().get("title");
        final String message = String.format(
                "%s by %s",
                remoteMessage.getData().get("messageDescription"),
                remoteMessage.getData().get("fullname")
        );
    
        // Only notify of message if it was not sent by me
        if (!String.valueOf(PrefsUtil.getUserId(this)).equals(remoteMessage.getData().get("userid"))) {
            sendNotification(title, message, R.drawable.ic_warning_white_24dp, intent);
        }
    }
    
    private void handleMsgMessage(Context context, RemoteMessage remoteMessage) {
        final String type = remoteMessage.getData().get("type");
	
	    if (type.equalsIgnoreCase(MSGConstants.ARG_ONE_TO_ONE)) {
		    Log.e(TAG, "Recognized as a chat to a specific user");
            final String firebaseToken = remoteMessage.getData().get("firebaseToken");
            final String senderUid = remoteMessage.getData().get("Uid");
            final String title = remoteMessage.getData().get("title");
            final String message = remoteMessage.getData().get("body");
            
            final Intent intent = new Intent(context, ChatActivity.class);
            intent.putExtra(MSGConstants.ARG_TYPE, MSGConstants.ARG_ONE_TO_ONE);
            intent.putExtra(MSGConstants.ARG_RECEIVER, title);
            intent.putExtra(MSGConstants.ARG_RECEIVER_UID, senderUid);
            intent.putExtra(MSGConstants.ARG_FIREBASE_TOKEN, firebaseToken);
            
            Log.e(TAG, "Launching process to send");
            sendNotification(title, message, R.mipmap.ic_launcher, intent);
        }
        else if(type.equalsIgnoreCase(MSGConstants.ARG_GROUP_CHAT)){ // Alert from Group
            final String title = remoteMessage.getData().get("title");
            final String message = remoteMessage.getData().get("body");
            final Intent intent = new Intent(context, ChatActivity.class);
            intent.putExtra(MSGConstants.ARG_TYPE, MSGConstants.ARG_GROUP_CHAT);
            sendNotification(title, message, R.mipmap.ic_launcher, intent);
        }
    }
    
    
}
