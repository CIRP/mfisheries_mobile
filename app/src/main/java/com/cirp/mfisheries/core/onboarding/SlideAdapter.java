package com.cirp.mfisheries.core.onboarding;



import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.cirp.mfisheries.R;
import com.cirp.mfisheries.util.PrefsUtil;

import org.w3c.dom.Text;

public class SlideAdapter extends PagerAdapter {
    private final Context context;

    //list of images
    private final TypedArray lst_images;
    //list of module images
    private final TypedArray  lst_modules;
    //list of headings
    final private String[] lst_headings;
    //list of descriptions
    final private String[] lst_descriptions;
    //list of background colours
    final private int[] lst_backgroundcolours;

    public SlideAdapter(Context context){
        this.context = context;
        final Resources resources = context.getResources();
        //list of images
        lst_images = resources.obtainTypedArray(R.array.on_board_images);
        //list of module images
        lst_modules = resources.obtainTypedArray(R.array.on_board_modules);
        //list of headings
        lst_headings = resources.getStringArray(R.array.on_board_headings);
        //list of descriptions
        lst_descriptions = resources.getStringArray(R.array.on_board_descriptions);
        //list of background colours
        lst_backgroundcolours = resources.getIntArray(R.array.on_board_background);
    }


    @Override
    public int getCount() {
        return lst_headings.length;
    }

    @Override
    public boolean isViewFromObject(@NonNull View view, @NonNull Object object) {
        return (view == (LinearLayout)object);
    }

    @NonNull
    @Override
    public Object instantiateItem(@NonNull ViewGroup container, int position) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.slide, container, false);

        if (view != null){
            LinearLayout layoutslide = (LinearLayout) view.findViewById(R.id.slidelinearlayout);
            ImageView imgslide = (ImageView) view.findViewById(R.id.slideimg);
            ImageView imgmodule = (ImageView) view.findViewById(R.id.slidemodule);
            TextView headslide = (TextView) view.findViewById(R.id.slideheading);
            TextView descslide = (TextView) view.findViewById(R.id.slidedescription);


//            imgslide.setImageResource(lst_images[position]);
//            imgmodule.setImageResource(lst_modules[position]);

            // Retrieve image resources based on => https://stackoverflow.com/questions/6945678/storing-r-drawable-ids-in-xml-array
            imgslide.setImageResource(lst_images.getResourceId(position, R.mipmap.ic_launcher));
            imgmodule.setImageResource(lst_modules.getResourceId(position, R.mipmap.ic_launcher));
            layoutslide.setBackgroundColor(lst_backgroundcolours[position]);
            headslide.setText(lst_headings[position]);
            descslide.setText(lst_descriptions[position]);
            container.addView(view);
        }

        return view;
    }

    @Override
    public void destroyItem(@NonNull ViewGroup container, int position, @NonNull Object object) {
        container.removeView((View)object);
    }
}