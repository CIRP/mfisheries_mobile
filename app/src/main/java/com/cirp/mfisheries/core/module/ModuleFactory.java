package com.cirp.mfisheries.core.module;

import android.content.Context;
import com.cirp.mfisheries.alerts.Alerts;
import com.cirp.mfisheries.fewer.Fewer;
import com.cirp.mfisheries.fewer.abtFewer.AboutFEWER;
import com.cirp.mfisheries.fewer.alerts.FEWERAlerts;
import com.cirp.mfisheries.fewer.dmgReport.DamageReport;
import com.cirp.mfisheries.fewer.emergCon.EmergencyContact;
import com.cirp.mfisheries.fewer.emergProc.EmergencyProcedures;
import com.cirp.mfisheries.fewer.msreport.MSReport;
import com.cirp.mfisheries.fewer.weather.FEWERWeather;
import com.cirp.mfisheries.firstaid.FirstAid;
import com.cirp.mfisheries.lek.LEK;
import com.cirp.mfisheries.messaging.Messaging;
import com.cirp.mfisheries.nav.Navigation;
import com.cirp.mfisheries.photos.PhotoDiary;
import com.cirp.mfisheries.podcast.Podcast;
import com.cirp.mfisheries.sos.SOS;
import com.cirp.mfisheries.tracking.Tracking;
import com.cirp.mfisheries.util.FileUtil;
import com.cirp.mfisheries.weather.Weather;

import java.lang.reflect.Constructor;
import java.util.HashMap;
import java.util.Map;

public class ModuleFactory {

	private static ModuleFactory instance;
	private final String TAG = "ModuleFactory";
	private Context context;
	private Map<String, Module> modules;
	
	
	private Class<?>[] moduleClasses = {
			FirstAid.class,
			Podcast.class,
			PhotoDiary.class,
			LEK.class,
			Weather.class,
			SOS.class,
			Navigation.class,
			Alerts.class,
			Messaging.class,
			Tracking.class,
			// FEWER Modules
			Fewer.class,
			EmergencyContact.class,
			DamageReport.class,
			EmergencyProcedures.class,
			AboutFEWER.class,
			MSReport.class,
			FEWERWeather.class,
			FEWERAlerts.class,
	};
	
	
	private ModuleFactory() {
		modules = new HashMap<>();
		createModuleMappings();
	}
	
	private void createModuleMappings(){
		for (Class<?> classM : moduleClasses){
			try { // Use the class definition to make a constructor
				Constructor<?> construct = classM.getConstructor(Context.class);
				Module m = (Module) construct.newInstance(context);
				
				// Add the class via its id
				modules.put(m.getId(), m);
				modules.put(m.getName(), m);
				
			} catch (Exception e) { e.printStackTrace(); }
		}
	}
	
	public Map<String, Module> getModules(){
		return modules;
	}

	public static ModuleFactory getInstance(Context ctx) {
		if (instance == null)
			instance = new ModuleFactory();
		instance.context = ctx;
		return instance;
	}

	public Module getModule(String module) {
//		return modules.get(module);
		
		if (module.equals("First Aid") || module.equals("FirstAid")) {
			return new FirstAid(context);
		}
		if (module.equals("Podcast")) {
			return new Podcast(context);
		}
		if (module.equals("Photo Diary") || module.equals("PhotoDiary")) {
			return new PhotoDiary(context);
		}
		if (module.equals("LEK")) {
			return new LEK(context);
		}
		if (module.equals("Weather")) {
			return new Weather(context);
		}
		if (module.equals("SOS")) {
			return new SOS(context);
		}
		if (module.equals("Navigation")) {
			return new Navigation(context);
		}
		if (module.equals("Alerts")) {
			return new Alerts(context);
		}
        if (module.equals("Messaging")){
			return new Messaging(context);
		}
		if (module.equals("Tracking")){
			return new Tracking(context);
		}

		// FEWER Related Modules
		if (module.equals("FEWER")) {
			return new Fewer(context);
		}
		if (module.equals("Emergency Contacts")){
			return new EmergencyContact(context);
		}
		if (module.equals("Damage Reporting")){
			return new DamageReport(context);
		}
		if (module.equals("Emergency Procedures") || module.equals("EmergencyProcedures")){
			return new EmergencyProcedures(context);
		}
		if (module.equals("About FEWER")){
			return new AboutFEWER(context);
		}
		if (module.equals("Missing Persons")){
			return new MSReport(context);
		}
		if (module.equals("Weather and Thresholds")){
			return new FEWERWeather(context);
		}
		if (module.equals("FEWER Alerts")){
			return new FEWERAlerts(context);
		}
		return null;
	}
	
	@SuppressWarnings("SameParameterValue")
	public boolean isModuleInstalled(final String moduleName){
		Module module = getModule(moduleName);
		String filePath = FileUtil.getFileUri("/" + module.getId()).toString();
		boolean hasFiles = true; // by default set to true to accommodate resources that do not need files
		
		if (module.hasDownload() || module.getDataLoc() != null) // IF module has resources to download, check that directory exists
			hasFiles = FileUtil.directoryExists(filePath);
		
//		SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
//		Log.d(TAG, String.format("DownloadNeeded for:%s Installation Status:%s, hasFiles:%s ", module.getId(),preferences.getBoolean(module.getId(), false),  hasFiles));
//		if (!preferences.getBoolean(module.getId(), false) ||  !hasFiles)
//			return true;
//		return false;
		return hasFiles;
	}

}
