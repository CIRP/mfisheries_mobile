package com.cirp.mfisheries.core;

import android.annotation.SuppressLint;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.util.Log;

import com.cirp.mfisheries.core.location.LocationService;
import com.cirp.mfisheries.core.location.TrackPointDM;
import com.cirp.mfisheries.fewer.msreport.MSReportUtil;
import com.cirp.mfisheries.fewer.msreport.model.PersonReport;
import com.cirp.mfisheries.lek.models.LEKPost;
import com.cirp.mfisheries.util.CrashReporter;
import com.cirp.mfisheries.util.NetUtil;
import com.cirp.mfisheries.util.PrefsUtil;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;
import com.koushikdutta.ion.builder.Builders;
import com.squareup.moshi.JsonAdapter;
import com.squareup.moshi.Moshi;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import id.zelory.compressor.Compressor;

import static com.cirp.mfisheries.util.MediaUtils.COMPRESS_QUALITY;

public class NetworkChangeReceiver extends BroadcastReceiver {

	private static final String TAG = "NetworkChangeReceiver";
	private boolean isConnected = false;
	private Context context;

	@Override
	public void onReceive(Context context, Intent intent) {
		Log.v(TAG, "Received notification about network status");
		this.context = context;
		if (intent.getAction() != null){
			if (intent.getAction().equals(ConnectivityManager.CONNECTIVITY_ACTION)){
				if (isNetworkAvailable(context)) {
					//do task now that network is available
					new SyncTask().execute();
				}
			}
		}
	}

	private void sendCachedData() {
		Log.d(TAG, "Sending cached data");
		if (LocationService.offlineTracksPresent) {
			Log.d(TAG, "Sending cached tracks");
			sendCachedTracks();
		}
		if(PrefsUtil.hasLekToSync(context)){
			Log.d(TAG, "There is cached LEK to send");
//			sendCachedLEK();
			sendCachedLEKPost();
		}
		if(PrefsUtil.hasMSToSend(context)){
			Log.d(TAG, "There is cached MS to send");
//			sendCachedMS();
		}
		if(PrefsUtil.hasDMtoSend(context)){
			Log.d(TAG, "There is cached DM to send");
		}
	}

	private void sendCachedLEKPost(){
		String cachedReport = PrefsUtil.getUserLEKPost(context);
		final LEKPost post = new Gson().fromJson(cachedReport, LEKPost.class);

		File file = null;
		if(post.filepath != null){file = new File(post.filepath);}

		FutureCallback<JsonObject> callback = new FutureCallback<JsonObject>() {
			@Override
			public void onCompleted(Exception e, JsonObject result) {
				if(e==null && result != null){
					try {
						Log.d(TAG, "Post Result: " + result.toString());
						PrefsUtil.setLekToSync(context, false);
						PrefsUtil.setUserLEKPost(context, null);
						Log.d(TAG, "LEK post has been sent to server");
					}catch (Exception exc){
						exc.printStackTrace();
						Log.d(TAG, "Post Error: Unable to save LEK Post");
					}
				}else if (e != null){
					Log.d(TAG, "Post Error: Unable to save LEK Post at this time");
					e.printStackTrace();
				}
			}
		};

		sendLEKPost(post, file, callback);
	}

	private void sendLEKPost(final LEKPost post, File file, final FutureCallback<JsonObject> futureCallback){
		final String url = NetUtil.API_URL + NetUtil.ADD_LEK_PATH;
		Log.d(TAG, String.format("Attempting to send %s to %s ", url, post));

		Map<String, String> map = post.getValues();
		Builders.Any.B builder = Ion.with(context)
				.load(url)
				.setHeader(NetUtil.API_KEY_PARAM, NetUtil.API_KEY);
		if (file != null){
			try{
				Log.d(TAG, "Attempting to compress image");
				file = new Compressor(context).setQuality(COMPRESS_QUALITY).compressToFile(file);
			}catch (Exception e){ Log.e(TAG, "Unable to compress images"); e.printStackTrace(); }
			
			builder.setMultipartFile("file", post.filetype, file);
		}
		else builder.setMultipartParameter("filepath", "");
		// Add each value of the post as part of the request
		for (Map.Entry<String, String> entry : map.entrySet()) {
			builder.setMultipartParameter(entry.getKey(), entry.getValue());
//			Log.d(TAG, String.format("Sending: %s-%s", entry.getKey(), entry.getValue()));
		}
		builder.asJsonObject().setCallback(futureCallback);
	}

	private void sendCachedTracks() {
		List<TrackPointDM> points;

		Log.d(TAG, "Attempting to send cache points from database");
		try {
            DatabaseHandler database = new DatabaseHandler(context, 2);
            database.open();
            points = database.offlineTrackDBHelper.getAllOfflineTracks();
            database.close();

            if (points == null)
                points = new ArrayList<>();

            Log.d("Offline Points", points.toString());

            if (points.size() > 0) {
				Log.d("Points list", points.toString());
                Moshi moshi = new Moshi.Builder().build();
                JsonAdapter<List> jsonAdapter = moshi.adapter(List.class);

                String json = jsonAdapter.toJson(points);

                JsonParser parser = new JsonParser();
                JsonArray jsonArray = (JsonArray) parser.parse(json);

				final String url = NetUtil.API_URL + NetUtil.POST_TRACK_URL;
				Log.d(TAG, String.format("Attempting to send %s to %s", jsonArray.toString(), url));

				Ion.with(context)
                        .load(url)
						.setHeader(NetUtil.API_KEY_PARAM, NetUtil.API_KEY)
                        .setJsonArrayBody(jsonArray)
                        .asJsonObject()
                        .setCallback(new FutureCallback<JsonObject>() {
                            @Override
                            public void onCompleted(Exception e, JsonObject result) {
                                if (e == null && result != null) {
                                    Log.d(TAG, "track sent " + result.toString());

                                    DatabaseHandler database = new DatabaseHandler(context, 2);
                                    database.open();
                                    database.offlineTrackDBHelper.deleteAllTracks();
                                    database.close();
                                    LocationService.offlineTracksPresent = false;
                                }else if (e != null){
                                	Log.d(TAG, "Unable to send tracks successfully");
	                                e.printStackTrace();
								}
                            }
                        });
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
	}

	private void sendCachedMS(){
		String cachedReport = PrefsUtil.getUserMSReport(context);
		final PersonReport report = new Gson().fromJson(cachedReport, PersonReport.class);

		final String REPORT_API_PATH = NetUtil.API_URL + "missingpersons";

		File file = null;
		if (report.filepath != null && report.filepath.length() > 2) {
			file = new File(report.filepath);
			if (file.exists())Log.d(TAG, "File Exists at: " + file.getAbsolutePath());
		}
		Map<String, String> map = report.getValues();
		Builders.Any.B builder = Ion.with(context).load(REPORT_API_PATH);
		if (file != null) {
			try{
				Log.d(TAG, "Attempting to compress image");
				file = new Compressor(context).setQuality(COMPRESS_QUALITY).compressToFile(file);
			}catch (Exception e){ Log.e(TAG, "Unable to compress images"); e.printStackTrace(); }
			builder.setMultipartFile("file", "image/*", file);
		}
		Log.d(TAG, report.filepath);
		for (Map.Entry<String, String> entry : map.entrySet()) {
			builder.setMultipartParameter(entry.getKey(), entry.getValue());
		}

		final MSReportUtil.UpdateListener listener = new MSReportUtil.UpdateListener() {
			@Override
			public void onReportUpdated(PersonReport report) {
				Log.d(TAG,"Report Updated"+ report);
			}
		};

		if (NetUtil.isOnline(context)){
			builder.asJsonObject()
					.setCallback(new FutureCallback<JsonObject>() {
						@Override
						public void onCompleted(Exception e, JsonObject result) {
							if (e == null) {
								Log.d(TAG, "Report was successfully created");
								try{
									Moshi moshi = new Moshi.Builder().build();
									JsonAdapter<PersonReport> jsonAdapter = moshi.adapter(PersonReport.class);
									PersonReport temp = jsonAdapter.fromJson(result.toString());
									listener.onReportUpdated(temp);
									PrefsUtil.setMSToSend(context, false);
								}catch (Exception ex){
									ex.printStackTrace();
									Log.d(TAG, "Unable to convert JSON to Object. Using initial report");
									listener.onReportUpdated(report);
								}
							} else {
								Log.d(TAG, "Unable to create report");
								e.printStackTrace();
								CrashReporter
										.getInstance(context)
										.log(Log.ERROR, TAG, "Attempting to Create Missing Person Report")
										.report(e);
							}
						}
					});
		}

	}

	private void sendChachedDM(){
		String cachedReport = PrefsUtil.getUserDMReport(context);
		final Object report = new Gson().fromJson(cachedReport, Object.class);
		Log.d(TAG, "Report: "+report);

	}

	private void sendCachedLEK() {
		try {
			final DatabaseHandler database = new DatabaseHandler(context, DatabaseHandler.LEK_DB_HELPER);
			database.open();
			List<LEKPost> lekPosts = database.offlineLEKDBHelper.getLEKToSync();
			Log.d("NetworkReceiver", "Posts to sync = " + lekPosts.size());
			database.close();

			for(int i = 0; i < lekPosts.size(); i++) {
				final int id = lekPosts.get(i).id;
				Log.d("LEK", lekPosts.get(i).toString());
				uploadLEK(lekPosts.get(i), new FutureCallback<JsonObject>() {
					@Override
					public void onCompleted(Exception e, JsonObject result) {
						if (e == null && result != null) {
							Log.d(TAG, "LEK uploaded");
							database.open();
							database.offlineLEKDBHelper.updateLEKStatus(id);
							database.close();
						}
						else if(e != null){
							Log.d(TAG, "LEK not uploaded " + e.getMessage());
						}
					}
				});
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	private void uploadLEK(LEKPost post, FutureCallback<JsonObject> callback){
		if (post.filepath != null) {
			File file = new File(post.filepath);
			
			try{
				Log.d(TAG, "Attempting to compress image");
				file = new Compressor(context).setQuality(COMPRESS_QUALITY).compressToFile(file);
			}catch (Exception e){ Log.e(TAG, "Unable to compress images"); e.printStackTrace(); }
			
			Ion.with(context)
					.load(NetUtil.API_URL + NetUtil.ADD_LEK_PATH)
					.setHeader(NetUtil.API_KEY_PARAM, NetUtil.API_KEY)
					.setMultipartFile("file", post.filetype, file)
					.setMultipartParameter("latitude", post.latitude)
					.setMultipartParameter("longitude", post.longitude)
					.setMultipartParameter("text", post.text)
					.setMultipartParameter("filetype", post.filetype)
					.setMultipartParameter("isSpecific", "true")
					.setMultipartParameter("userid", post.userid)
					.setMultipartParameter("aDate", post.aDate)
					.setMultipartParameter("category", post.category)
					.setMultipartParameter("countryid", PrefsUtil.getCountryId(context))
					.asJsonObject()
					.setCallback(callback);
		} else {//if just text

			Ion.with(context)
					.load(NetUtil.API_URL + NetUtil.ADD_LEK_PATH)
					.setHeader(NetUtil.API_KEY_PARAM, NetUtil.API_KEY)
					.setMultipartParameter("latitude", post.latitude)
					.setMultipartParameter("longitude", post.longitude)
					.setMultipartParameter("text", post.text)
					.setMultipartParameter("filetype", "text/*")
					.setMultipartParameter("isSpecific", "true")
					.setMultipartParameter("userid", post.userid)
					.setMultipartParameter("aDate", post.aDate)
					.setMultipartParameter("category", post.category)
					.setMultipartParameter("countryid", PrefsUtil.getCountryId(context))
					.asJsonObject()
					.setCallback(callback);
		}
	}

	private boolean isNetworkAvailable(Context context) {
		ConnectivityManager connectivity = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
		if (connectivity != null) {
			NetworkInfo info = connectivity.getActiveNetworkInfo();
			if (info != null) {
				if (info.getState() == NetworkInfo.State.CONNECTED) {
					if (!isConnected) {
						isConnected = true;
						Log.v(TAG, "You are connected to Internet!");
					}
					return true;
				}
			}
		}
		Log.v(TAG, "You are not connected to Internet!");
		isConnected = false;
		return false;
	}
	
	@SuppressLint("StaticFieldLeak")
	private class SyncTask extends AsyncTask<Void, Void, Boolean> {
		
		protected Boolean doInBackground(Void... params) {
			try {
				sendCachedData();
			} catch (Exception e) {
				Log.d("LEK", "LEK DB LOADING: " + e.getMessage());
			}
			return false;
		}
		
		protected void onPostExecute(Boolean finished) {
		
		}
	}


}
