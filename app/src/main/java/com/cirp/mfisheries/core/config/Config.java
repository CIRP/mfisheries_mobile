package com.cirp.mfisheries.core.config;

public class Config {
	String id;
	String key;
	String value;
	String description;
	
	public Config() {
	}
	
	public Config(String id, String key, String value, String description) {
		this.id = id;
		this.key = key;
		this.value = value;
		this.description = description;
	}
	
	
}
