package com.cirp.mfisheries.sms;

import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

import com.cirp.mfisheries.R;

public class SendSMSActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_send_sms);
        findViewById(R.id.btn_smsSend).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                sendSms();
            }
        });
    }

    private void sendSms() {
        //reset error flags for text input
        TextInputLayout til_body = findViewById(R.id.til_body);
        TextInputLayout til_recipients= findViewById(R.id.til_recipients);
        TextInputEditText tiet_recipients = findViewById(R.id.tiet_recipients);
        TextInputEditText tiet_body = findViewById(R.id.tiet_body);

        til_body.setError(null);
        til_recipients.setError(null);

        String rev = tiet_recipients.getText().toString();
        String body = tiet_body.getText().toString();

        boolean hasError = false;

        if (!isValidPhoneNumber(rev)){
            til_recipients.setError("Invalid number, please make sure the number has the format +1<country code><area code><7 digit number> eg. +18683214567");
            hasError=true;
        }
        if (body.isEmpty()){
            til_body.setError("Please enter a message");
            hasError=true;
        }
        if (hasError) return;

        SMS sms = new SMS(this);
        sms.setBody(body);
        sms.addRecepient(rev);

        try{
            sms.send();
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    private boolean isValidPhoneNumber(String rev){
        return !rev.isEmpty() && rev.charAt(0)=='+' && rev.length()>=12 && rev.length()<=16;
    }
}
