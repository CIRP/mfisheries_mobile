package com.cirp.mfisheries.sms;

import android.content.Context;
import android.util.Log;
import android.widget.Toast;

import com.cirp.mfisheries.util.NetUtil;
import com.cirp.mfisheries.util.PrefsUtil;
import com.google.gson.JsonObject;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;

import java.util.ArrayList;

/**
 * Created by kwasi on 19/01/2018.
 */

public class SMS {
    private final Context context;
    private ArrayList<String> recipients;
    private String body;

    public SMS(Context context){
        this.context=context;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public void addRecepient(String rec){
        if (recipients ==null){
            recipients =new ArrayList<>();
        }
        recipients.add(rec);
    }
    public void send() throws Exception {
        if (recipients == null || body == null) {
            throw new Exception("Invalid recipients or body for sms");
        }
        Toast.makeText(context, "Should send "+body+" to "+recipients.get(0) + "On URL "+NetUtil.SITE_URL, Toast.LENGTH_SHORT).show();

        //Testing an async task for msg sending
        Ion.with(context)
                .load("POST",NetUtil.SITE_URL + "api/" + SMSUtil.SMSEndpoint)
                .setHeader(NetUtil.API_KEY_PARAM, NetUtil.API_KEY)
                .setMultipartParameter("recipients",recipients.get(0))
                .setMultipartParameter("body",body)
                .setMultipartParameter("countryID", PrefsUtil.getCountryId(context))
                .asJsonObject()
                .setCallback(new FutureCallback<JsonObject>() {
                    @Override
                    public void onCompleted(Exception e, JsonObject result) {
                        if (e == null && result != null) {
                            Toast.makeText(context,"SMS sent successfully to recipients",Toast.LENGTH_SHORT).show();
                        }
                        else{
                            Toast.makeText(context,"An error occured",Toast.LENGTH_SHORT).show();
                            if (result!=null){
                                Log.e("SMSErr",result.toString());
                            }
                            if (e!=null){
                                e.printStackTrace();
                            }
                    }
                }});
    }
}
