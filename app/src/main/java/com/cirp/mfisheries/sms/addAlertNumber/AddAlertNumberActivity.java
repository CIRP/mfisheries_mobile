package com.cirp.mfisheries.sms.addAlertNumber;

import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.cirp.mfisheries.R;
import com.cirp.mfisheries.util.NetUtil;
import com.cirp.mfisheries.util.PrefsUtil;
import com.google.gson.JsonObject;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.StringWriter;
import java.io.UnsupportedEncodingException;
import java.io.Writer;

public class AddAlertNumberActivity extends AppCompatActivity {
    private TextInputLayout tilCountryCode;
    private TextInputLayout tilNumber;
    private TextInputEditText tietCountryCode;
    private TextInputEditText tietNumber;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_yuh_padna);
        tilCountryCode = findViewById(R.id.til_country_dial_code);
        tilNumber = findViewById(R.id.til_phone_number);
        tietCountryCode = findViewById(R.id.tiet_country_dial_code);
        tietNumber = findViewById(R.id.tiet_phone_number);

        Toolbar tb = findViewById(R.id.tb_toolbar);
        setSupportActionBar(tb);
        if (getSupportActionBar()!=null){
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }

        populateCountry();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void populateCountry() {
        //Look to automatically get the user's country and display it in a way they can edit or leave if it's correct
        InputStream is = getResources().openRawResource(R.raw.country_codes);
        Writer writer = new StringWriter();
        char[] buffer = new char[1024];
        try {
            Reader reader = new BufferedReader(new InputStreamReader(is, "UTF-8"));
            int n;
            while ((n = reader.read(buffer)) != -1) {
                writer.write(buffer, 0, n);
            }
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                is.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        JSONArray arr = null;
        try {
            arr = new JSONArray(writer.toString());
            Log.w("JSON",arr.toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }

        TelephonyManager tm = (TelephonyManager)getSystemService(Context.TELEPHONY_SERVICE);
        if (tm != null) {
            //Read the country codes JSON file and store it to lookup
            String simCountryCode = tm.getSimCountryIso().toUpperCase();

            //lookup in JSON for a matching simcountry
            if (arr!=null){
                int count = arr.length();
                for (int a=0;a<count;a++){
                    try {
                        JSONObject obj = arr.getJSONObject(a);
                        if (obj.getString("code").equals(simCountryCode)){
                            tietCountryCode.setText(obj.getString("dial_code").replace(" ",""));
                            break;
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }

        }
    }


    public void resetErrors(){
        tilCountryCode.setError(null);
        tilNumber.setError(null);
    }

    public void addSubscription(View view) {
        resetErrors();

        String countryCode = tietCountryCode.getText().toString();
        String number = tietNumber.getText().toString().replace("-","").replace(" ","");

        boolean hasError = false;

        if (countryCode.isEmpty()){
            hasError = true;
            tilCountryCode.setError("Please set a valid country code");
        }

        if (number.length()<7){
            hasError = true;
            tilNumber.setError("Please enter a valid phone number");
        }

        final String wholeNumber = countryCode+number;

        if (!isValidPhoneNumber(wholeNumber)){
            hasError=true;
            Toast.makeText(this,"Please enter a properly formatted phone number",Toast.LENGTH_SHORT).show();
        }

        if (hasError) return;

        Log.e("SEND",NetUtil.API_URL + NetUtil.ADD_ALERT_NUMBER);
        new AlertDialog.Builder(this)
                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        Ion.with(AddAlertNumberActivity.this)
                                .load("POST", NetUtil.API_URL + NetUtil.ADD_ALERT_NUMBER)
                                .setHeader(NetUtil.API_KEY_PARAM, NetUtil.API_KEY)
                                .setMultipartParameter("number",wholeNumber)
                                .setMultipartParameter("simCountry",PrefsUtil.getCountryId(AddAlertNumberActivity.this))
                                .asJsonObject()
                                .setCallback(new FutureCallback<JsonObject>() {
                                    @Override
                                    public void onCompleted(Exception e, JsonObject result) {
                                        if (e==null &&result!=null){
                                            if (result.get("status").getAsInt()!=200){
                                                Toast.makeText(AddAlertNumberActivity.this,result.get("response").getAsString(),Toast.LENGTH_SHORT).show();
                                            }
                                            else{
                                                Toast.makeText(AddAlertNumberActivity.this,"Number added successfully",Toast.LENGTH_SHORT).show();
                                            }
                                        }
                                        else{
                                            Toast.makeText(AddAlertNumberActivity.this,"An error occured",Toast.LENGTH_SHORT).show();
                                            if (result!=null){
                                                Log.e("AddAlertNumberErr",result.toString());
                                            }
                                            if (e!=null){
                                                e.printStackTrace();
                                            }
                                        }
                                    }
                                });
                    }
                })
                .setNegativeButton(android.R.string.no,null)
                .setMessage("Would you like to add "+wholeNumber+" to receive alert SMS?")
                .setTitle("Add number")
                .show();


    }

    public void removeSubscription(View view) {
        resetErrors();

        String countryCode = tietCountryCode.getText().toString();
        String number = tietNumber.getText().toString().replace("-","").replace(" ","");

        boolean hasError = false;

        if (countryCode.isEmpty()){
            hasError = true;
            tilCountryCode.setError("Please set a valid country code");
        }

        if (number.length()<7){
            hasError = true;
            tilNumber.setError("Please enter a valid phone number");
        }

        final String wholeNumber = countryCode+number;

        if (!isValidPhoneNumber(wholeNumber)){
            hasError=true;
            Toast.makeText(this,"Please enter a properly formatted phone number",Toast.LENGTH_SHORT).show();
        }

        if (hasError) return;

        new AlertDialog.Builder(this)
                .setMessage("Would you like to remove "+wholeNumber+" from receiving alert SMS?")
                .setTitle("Remove number")
                .setNegativeButton(android.R.string.no,null)
                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        Ion.with(AddAlertNumberActivity.this)
                                .load("POST", NetUtil.API_URL + NetUtil.REMOVE_ALERT_NUMBER)
                                .setHeader(NetUtil.API_KEY_PARAM, NetUtil.API_KEY)
                                .setMultipartParameter("number",wholeNumber)
                                .setMultipartParameter("simCountry", PrefsUtil.getCountryId(AddAlertNumberActivity.this))
                                .asJsonObject()
                                .setCallback(new FutureCallback<JsonObject>() {
                                    @Override
                                    public void onCompleted(Exception e, JsonObject result) {
                                        Log.e("ERRRRRRR",result.toString());

                                        if (e==null &&result!=null){
                                            if (result.get("status").getAsInt()!=200){
                                                Toast.makeText(AddAlertNumberActivity.this,result.get("response").getAsString(),Toast.LENGTH_SHORT).show();
                                            }
                                            else{
                                                Toast.makeText(AddAlertNumberActivity.this,"Number removed successfully",Toast.LENGTH_SHORT).show();                                            }

                                        }
                                        else{
                                            Toast.makeText(AddAlertNumberActivity.this,"An error occured",Toast.LENGTH_SHORT).show();
                                            if (result!=null){
                                                Log.e("AddAlertNumberErr",result.toString());
                                            }
                                            if (e!=null){
                                                e.printStackTrace();
                                            }
                                        }
                                    }
                                });
                    }
                })
                .show();
    }

    private boolean isValidPhoneNumber(String rev){
        return !rev.isEmpty() && rev.charAt(0)=='+' && rev.length()>10 && rev.length()<=16;
    }
}
