package com.cirp.mfisheries.nav;

public class MapMarkerDM {
	public int id;
	public int type;
	public int user_id;
	public String name;
	public String details;
	public String latitude;
	public String longitude;
	public String timestamp;
}
