package com.cirp.mfisheries.nav;

import android.content.ContentValues;
import android.database.Cursor;
import android.util.Log;

import com.cirp.mfisheries.core.DatabaseHandler;

import java.util.ArrayList;
import java.util.List;

public class MapMarkerDBHelper {
	private static final String MAP_MARKER_TABLE = "MapMarkers";
	private static final String MAP_MARKER_ID = "id";
	private static final String MAP_MARKER_TYPE = "type";
	private static final String MAP_MARKER_NAME = "name";
	private static final String MAP_MARKER_DESCRIPTON = "description";
	private static final String MAP_MARKER_LATITUDE = "latitude";
	private static final String MAP_MARKER_LONGITUDE = "longitude";
	private static final String MAP_MARKER_TIMESTAMP = "timestamp";
	private static final String MAP_MARKER_USER_ID = "user_id";
	
	public static String CREATE_MAP_MARKER_TABLE = "CREATE TABLE "
			+ MAP_MARKER_TABLE + " ("
			+ MAP_MARKER_ID + " INTEGER PRIMARY KEY autoincrement,"
			+ MAP_MARKER_TYPE + " INTEGER,"
			+ MAP_MARKER_NAME + " TEXT,"
			+ MAP_MARKER_DESCRIPTON + " TEXT,"
			+ MAP_MARKER_LATITUDE + " TEXT,"
			+ MAP_MARKER_LONGITUDE + " TEXT,"
			+ MAP_MARKER_TIMESTAMP + " TEXT,"
			+ MAP_MARKER_USER_ID + " INTEGER" + ")";
	
	public int addMapMarker(MapMarkerDM marker_point) {
		try {
			ContentValues marker_values = new ContentValues();
			marker_values.put(MAP_MARKER_TYPE, marker_point.type);
			marker_values.put(MAP_MARKER_NAME, marker_point.name);
			marker_values.put(MAP_MARKER_DESCRIPTON, marker_point.details);
			marker_values.put(MAP_MARKER_LATITUDE, marker_point.latitude);
			marker_values.put(MAP_MARKER_LONGITUDE, marker_point.longitude);
			marker_values.put(MAP_MARKER_TIMESTAMP, marker_point.timestamp);
			marker_values.put(MAP_MARKER_USER_ID, marker_point.user_id);
			return (int) DatabaseHandler.database.insert(MAP_MARKER_TABLE, null, marker_values);
		} catch (Exception ex) {
			Log.wtf("mFisheries", "MapMarker DB Helper, Error adding new marker " + ex.getLocalizedMessage());
		}
		return 0;
	}
	
	public int getMapMarkerCount() {
		try {
			String marker_query = "SELECT * FROM " + MAP_MARKER_TABLE;
			Cursor cursor = DatabaseHandler.database.rawQuery(marker_query, null);
			int count = cursor.getCount();
			cursor.close();
			return count;
		} catch (Exception ex) {
			Log.wtf("mFisheries", "Map Marker DB Helper, Error acquiring marker count" + ex.getLocalizedMessage());
		}
		return 0;
	}
	
	public List<MapMarkerDM> getAllMarkers() {
		List<MapMarkerDM> marker_points = new ArrayList<>();
		try {

			String selectQuery = "SELECT  * FROM " + MAP_MARKER_TABLE;
			Cursor cursor = DatabaseHandler.database.rawQuery(selectQuery, null);
			if (cursor.moveToFirst()) {
				do {
					MapMarkerDM marker_point = new MapMarkerDM();
					marker_point.id = Integer.parseInt(cursor.getString(0));
					marker_point.type = Integer.parseInt(cursor.getString(1));
					marker_point.name = cursor.getString(2);
					marker_point.details = cursor.getString(3);
					marker_point.latitude = cursor.getString(4);
					marker_point.longitude = cursor.getString(5);
					marker_point.timestamp = cursor.getString(6);
					marker_point.user_id = Integer.valueOf(cursor.getString(7));
					marker_points.add(marker_point);
				}
				while (cursor.moveToNext());
			}
			cursor.close();
		} catch (Exception ex) {
			Log.wtf("mFisheries", "Map Marker Db Helper, Error acquiring all markers " + ex.getLocalizedMessage());
		}
		return marker_points;
	}
	
	public void deleteMarker(String marker_id) {
		if (!marker_id.equals("0")) {
			try {
				DatabaseHandler.database.delete(MAP_MARKER_TABLE, MAP_MARKER_ID + " = ?", new String[]{marker_id});
			} catch (Exception ex) {
				Log.wtf("mFisheries", "Map Marker db Helper, Error deleting marker " + ex.getLocalizedMessage());
			}
		}
	}
}
