package com.cirp.mfisheries.nav;

public class MapMarkerMS {
    public int id;
    public int type;
    public String user_id;
    public String title;
    public String details;
    public String latitude;
    public String longitude;
    public String timestamp;
}
