package com.cirp.mfisheries.nav;

import android.os.Environment;

import com.google.android.gms.maps.model.Tile;
import com.google.android.gms.maps.model.TileProvider;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.InputStream;
import java.util.zip.ZipFile;

public class CustomZipTileProvider implements TileProvider {

	private static final int TILE_WIDTH = 256;
	private static final int TILE_HEIGHT = 256;
	private static final int BUFFER_SIZE = 16384;
	private static File directory;

	public CustomZipTileProvider() {
		directory = Environment.getExternalStorageDirectory();
		
	}

	@Override
	public Tile getTile(int x, int y, int zoom) {
		byte[] image = ReadTileImage(x, y, zoom);
		return image == null ? null : new Tile(TILE_WIDTH, TILE_HEIGHT, image);
	}
	
	private byte[] ReadTileImage(int x, int y, int zoom) {
		InputStream in_stream = null;
		ByteArrayOutputStream buffer = null;
		try {
			in_stream = GetStream(x, y, zoom);
			if (in_stream != null) {
				buffer = new ByteArrayOutputStream();
				byte[] data = new byte[BUFFER_SIZE];
				int nth_read;
				while ((nth_read = in_stream.read(data, 0, BUFFER_SIZE)) != -1) {
					buffer.write(data, 0, nth_read);
				}
				buffer.flush();
				return buffer.toByteArray();
			}
		} catch (Exception e) {
			return null;
		} finally {
			if (in_stream != null) {
				try {
					in_stream.close();
				} catch (Exception ignored) {
					
				}
			}
			if (buffer != null) {
				try {
					buffer.close();
				} catch (Exception ignored) {
					
				}
			}
		}
		return null;
	}

	private InputStream GetStream(int x, int y, int zoom) {
		File targetFile = new File(directory, "mFisheries/Navigation/map_tiles/" + zoom + "/" + x + ".zip");
		try {
			@SuppressWarnings("resource")
			ZipFile zip_file = new ZipFile(targetFile);
			return zip_file.getInputStream(zip_file.getEntry(x + "/" + y + ".png"));
		} catch (Exception ex) {
			return null;
		}
	}
}
