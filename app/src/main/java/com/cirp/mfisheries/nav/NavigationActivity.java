package com.cirp.mfisheries.nav;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.location.Location;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.androidmapsextensions.GoogleMap;
import com.androidmapsextensions.GoogleMap.InfoWindowAdapter;
import com.androidmapsextensions.GoogleMap.OnInfoWindowClickListener;
import com.androidmapsextensions.GoogleMap.OnMapLongClickListener;
import com.androidmapsextensions.Marker;
import com.androidmapsextensions.MarkerOptions;
import com.androidmapsextensions.OnMapReadyCallback;
import com.androidmapsextensions.SupportMapFragment;
import com.androidmapsextensions.TileOverlay;
import com.androidmapsextensions.TileOverlayOptions;
import com.cirp.mfisheries.R;
import com.cirp.mfisheries.core.DatabaseHandler;
import com.cirp.mfisheries.core.location.LocationActivity;
import com.cirp.mfisheries.util.LocationUtil;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;

import java.util.ArrayList;
import java.util.List;

public class NavigationActivity extends LocationActivity implements GoogleMap.OnMarkerClickListener,
		OnMapReadyCallback, OnMapLongClickListener, OnInfoWindowClickListener, InfoWindowAdapter {

	private GoogleMap googleMap;
	private Marker targetMarker, userMarker;
	private TileOverlay tileOverlay;

	private ImageView compass, compassArrow, navigate;
	
	private ListView markerListView;
	private MarkerListAdapter markerListAdapter;
	
	private boolean MARKER_LIST_ENABLED, NAVIGATION_ENABLED, COMPASS_ENABLED;

	private float headingOffset = 0;
	private Location destination;

	private static final byte ADD_MARKER_ACTIVITY = 11;
	
	private List<MapMarkerDM> mapMarkers = new ArrayList<>();
//	private List<GCMHistoryDM> sosMarkers = new ArrayList<>();

	private String destinationName = "";

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		module = new Navigation(this);
		markerListView = findViewById(R.id.navigation_marker_list);

		requestPermissions();
	}

	@Override
	public void onPermissionGranted(String permission) {
		super.onPermissionGranted(permission);
		Log.d("Nav", "permission granted");
		try {
			((SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.navigation_map_screen))
					.getExtendedMapAsync(this);

		} catch (Exception ex) {
			Log.wtf("mFisheries", "Map Screen On Create, Error loading the map screen " + ex.getMessage());
		}
		try {
			initializeCompass(sensorEventListener);
		} catch (Exception ex) {
			Log.wtf("mFisheries", "Map Screen On Create, Error ");
		}
		try {
			new GetMarkersTask().execute();
		} catch (Exception ex) {
			Log.wtf("mFisheries", "Map Screen, Error starting thread " + ex.getLocalizedMessage());
		}
	}

	public void onMapReady(GoogleMap googleMap){
		this.googleMap = googleMap;
		this.googleMap.getUiSettings().setZoomControlsEnabled(false);
		this.googleMap.getUiSettings().setTiltGesturesEnabled(false);
		this.googleMap.getUiSettings().setCompassEnabled(false);
		this.googleMap.getUiSettings().setRotateGesturesEnabled(false);
		this.googleMap.setOnMapLongClickListener(this);
		this.googleMap.setOnInfoWindowClickListener(this);
		this.googleMap.setInfoWindowAdapter(this);
		LatLng zoom_position = new LatLng(10.934606, -61.178558);
		this.googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(zoom_position, 7));
		if (mCurrentLocation != null) {
			updateUserPosition(mCurrentLocation);
		}
	}

	@SuppressWarnings("deprecation")
	public void initializeCompass(SensorEventListener eventListener) {
		SensorManager sensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
		Sensor compass_sensor = sensorManager.getDefaultSensor(Sensor.TYPE_ORIENTATION);
		compass = findViewById(R.id.navigation_compass_a);
		compassArrow = findViewById(R.id.navigation_compass_b);
		compassArrow.setVisibility(ImageView.GONE);
		if (compass_sensor != null) {
			sensorManager.registerListener(eventListener, compass_sensor, SensorManager.SENSOR_DELAY_FASTEST);
			TextView info = findViewById(R.id.navigation_compass_exists);
			info.setVisibility(TextView.GONE);
		} else {
			TextView info = findViewById(R.id.navigation_compass_exists);
			info.setVisibility(TextView.VISIBLE);
			info.setText(R.string.no_compass);
		}
	}
	
	private SensorEventListener sensorEventListener = new SensorEventListener() {
		@Override
		public void onSensorChanged(SensorEvent event) {
			float north_offset = event.values[0];
			compass.setRotation(-north_offset);
			if (NAVIGATION_ENABLED) {
				compassArrow.setRotation(headingOffset - north_offset);
			}
		}

		@Override
		public void onAccuracyChanged(Sensor sensor, int accuracy) { }
	};
	
	public void openAddMarkerActivity(View view) {
		if (mCurrentLocation != null) {
			Intent map_click = new Intent(getApplicationContext(), AddMarkerActivity.class);
			map_click.putExtra("user_id", 0);
			map_click.putExtra("latitude", mCurrentLocation.getLatitude());
			map_click.putExtra("longitude", mCurrentLocation.getLongitude());
			startActivityForResult(map_click, ADD_MARKER_ACTIVITY);
		} else {
			Toast.makeText(NavigationActivity.this, "Cannot Get Lock On Location", Toast.LENGTH_LONG).show();
		}
	}
	
	private void refreshNavigationActivity() {
		Log.d("Nav", "Recreating");
		if(googleMap != null) {
			googleMap.setMapType(GoogleMap.MAP_TYPE_NONE);
			tileOverlay = googleMap.addTileOverlay(new TileOverlayOptions().tileProvider(new CustomZipTileProvider()));
			displayAllMarkers();
		}
	}

	private void addAlertMarker(String name, String details, String lat, String lng, String timestamp) {
		Log.d("nav", "adding");
		MapMarkerDM new_point = new MapMarkerDM();
		new_point.id = 0x3;
		new_point.type = 0;
		new_point.name = name;
		new_point.details = details;
		new_point.latitude = lat;
		new_point.longitude = lng;
		new_point.timestamp = timestamp;
		new_point.user_id = 0;
		displaySingleMarker(new_point);
	}

	private void displaySingleMarker(MapMarkerDM marker) {
		try {
			String message = marker.name + " " + marker.details;
			int type = marker.type;
			markerListAdapter.notifyDataSetChanged();
			MarkerOptions marker_options = new MarkerOptions();
			marker_options.position(new LatLng(Double.valueOf(marker.latitude), Double.valueOf(marker.longitude)));
			marker_options.title(String.valueOf(marker.id));
			marker_options.snippet(message);
			if (type == 0)
				marker_options.icon(BitmapDescriptorFactory.fromResource(R.drawable.marker_anchor));
			else if (type == 1)
				marker_options.icon(BitmapDescriptorFactory.fromResource(R.drawable.marker_hazard));
			googleMap.addMarker(marker_options);
		} catch (Exception ex) {
			Log.wtf("mFisheries", "Map Screen, Error displaying marker " + ex.getLocalizedMessage());
		}
	}

	private void displayAllMarkers() {
		if (mapMarkers.size() > 0) {
			for (MapMarkerDM marker : mapMarkers)
				displaySingleMarker(marker);
			mapMarkers.clear();
		}
//		if (sosMarkers.size() > 0) {
//			sosMarkers.clear();
//		}
	}
	
//	private void displaySingleSOS(GCMHistoryDM sos) {
//		try {
//			MarkerOptions markerOptions = new MarkerOptions();
//			markerOptions.position(new LatLng(Double.valueOf(sos.latitude), Double.valueOf(sos.longitude)));
//			markerOptions.title(String.valueOf(sos.id));
//			String type = sos.type;
//			if (type.equals("SOS")) {
//				String message = "SOS " + sos.user;
//				markerOptions.snippet(message);
//				markerOptions.icon(BitmapDescriptorFactory.fromResource(R.drawable.marker_sos));
//				googleMap.addMarker(markerOptions);
//				markerListAdapter.notifyDataSetChanged();
//			} else if (type.equals("Alert")) {
//				String message = "Alert " + sos.user + "\n" + sos.message;
//				markerOptions.snippet(message);
//				markerOptions.icon(BitmapDescriptorFactory.fromResource(R.drawable.marker_hazard));
//				googleMap.addMarker(markerOptions);
//				markerListAdapter.notifyDataSetChanged();
//			}
//		} catch (Exception ex) {
//			Log.wtf("mFisheries", "Map Screen, Error displaying marker " + ex.getLocalizedMessage());
//		}
//	}

	private class GetMarkersTask extends AsyncTask<Void, Void, Boolean> {

		protected Boolean doInBackground(Void... params) {
			try {
				DatabaseHandler db = new DatabaseHandler(NavigationActivity.this, 1);
				db.open();
				mapMarkers = db.mapMarkerDBHelper.getAllMarkers();
				db.close();
				db = new DatabaseHandler(NavigationActivity.this, 3);
				db.open();
//				sosMarkers = db.gcmHistoryDBHelper.getAllGCMHistory();
				db.close();
				return true;
			} catch (Exception ex) {
				Log.wtf("mFisheries", "Map Screen, Error acquiring markers " + ex.getLocalizedMessage());
				return false;
			}
		}

		protected void onPostExecute(Boolean success) {
			if(success) {
				markerListAdapter = new MarkerListAdapter(NavigationActivity.this);
				markerListView.setAdapter(markerListAdapter);
				markerListView.setOnItemClickListener(marker_list_click_listener);
				markerListView.setOnItemLongClickListener(marker_list_long_click_listener);
				Intent intent = getIntent();
				Bundle extras = intent.getExtras();
				if (extras != null) {
					addAlertMarker(extras.getString("name"), extras.getString("details"), extras.getString("lat"), extras.getString("lng"), extras.getString("timestamp"));
				}
				refreshNavigationActivity();
			}
		}
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		switch (requestCode) {
			case ADD_MARKER_ACTIVITY:
				if (resultCode == Activity.RESULT_OK) {
					MapMarkerDM mapMarkerDM = new MapMarkerDM();
					mapMarkerDM.id = data.getIntExtra("id", 0);
					mapMarkerDM.type = data.getIntExtra("type", 0);
					mapMarkerDM.name = data.getStringExtra("name");
					mapMarkerDM.details = data.getStringExtra("description");
					mapMarkerDM.latitude = data.getStringExtra("latitude");
					mapMarkerDM.longitude = data.getStringExtra("longitude");
					mapMarkerDM.timestamp = data.getStringExtra("timestamp");
					mapMarkerDM.user_id = data.getIntExtra("user_id", 0);
					displaySingleMarker(mapMarkerDM);
				}
		}
	}
	
	@SuppressLint("InflateParams")
	@Override
	public View getInfoContents(Marker arg0) {
		View infoWindow = getLayoutInflater().inflate(R.layout.custom_info_window, null);

		TextView markerDescription = infoWindow.findViewById(R.id.info_window_marker_description);
		TextView markerLat = infoWindow.findViewById(R.id.info_window_marker_lat);
		TextView markerLng = infoWindow.findViewById(R.id.info_window_marker_lng);

		markerDescription.setText(arg0.getSnippet());
		markerLat.setText(getString(R.string.lat_display) + LocationUtil.convertLatitude(arg0.getPosition().latitude));
		markerLng.setText(getString(R.string.lng_display) + LocationUtil.convertLongitude(arg0.getPosition().longitude));

		return infoWindow;
	}

	@Override
	public View getInfoWindow(Marker arg0) {
		return null;
	}

	@Override
	public void onInfoWindowClick(Marker arg0) {
		final Marker marker = arg0;

		new AlertDialog.Builder(this)
				.setMessage("Navigate to this point?")
				.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int id) {
						try {
							NAVIGATION_ENABLED = true;
							updateHeading(marker);
							Button navigateStop = findViewById(R.id.navigation_info_button);
							navigateStop.setVisibility(ImageView.VISIBLE);
							compassArrow.setVisibility(ImageView.VISIBLE);

							Toast.makeText(NavigationActivity.this, "Navigation Mode Activated", Toast.LENGTH_LONG).show();

							if (MARKER_LIST_ENABLED) {
								MARKER_LIST_ENABLED = !MARKER_LIST_ENABLED;
								markerListView.setVisibility(ListView.GONE);
							}
							marker.hideInfoWindow();
						} catch (Exception ex) {
							Log.wtf("mFisheries", "Map Screen, Error deleting marker" + ex.getLocalizedMessage());
						}
					}
				})
				.setNegativeButton("No", new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int id) {
						if (NAVIGATION_ENABLED) {
							NAVIGATION_ENABLED = false;
						}
						marker.hideInfoWindow();
						dialog.cancel();
					}
				})
				.show();
	}

	@Override
	public void onMapLongClick(LatLng arg0) {
		Intent intent = new Intent(getApplicationContext(), AddMarkerActivity.class);
		intent.putExtra("user_id", 0);
		intent.putExtra("latitude", arg0.latitude);
		intent.putExtra("longitude", arg0.longitude);
		startActivityForResult(intent, ADD_MARKER_ACTIVITY);
	}

	@Override
	public boolean onMarkerClick(Marker marker) {

		if (marker.isInfoWindowShown()) {
			marker.hideInfoWindow();

		} else {
			marker.showInfoWindow();
			googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(marker.getPosition(), 6));
			if (mCurrentLocation != null) {
				updateUserPosition(mCurrentLocation);

			}
		}
		return true;
	}
	
	private void updateHeading(Marker marker) {
		if (targetMarker != null) {
			targetMarker.remove();
			targetMarker = null;
		}
		if (marker != null) {
			MarkerOptions marker_options = new MarkerOptions().position(marker.getPosition()).title("0").snippet("Destination");
			marker_options.icon(BitmapDescriptorFactory.fromResource(R.drawable.marker_crosshair));
			marker_options.flat(true);
			targetMarker = googleMap.addMarker(marker_options);
			if (mCurrentLocation != null) {
				destination = new Location(mCurrentLocation);
				destination.setLatitude(marker.getPosition().latitude);
				destination.setLongitude(marker.getPosition().longitude);
				destinationName = marker.getSnippet();
				headingOffset = mCurrentLocation.bearingTo(destination);
				LatLng zoom_position = new LatLng(marker.getPosition().latitude, marker.getPosition().longitude);
				googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(zoom_position, 6));
				
				if (mCurrentLocation != null)
					updateUserPosition(mCurrentLocation);
			}
		}
		markerListAdapter.notifyDataSetChanged();
		markerListAdapter.notifyDataSetInvalidated();
	}
	
	private void updateUserPosition(Location position) {
		
		if (userMarker != null) {
			userMarker.remove();
			userMarker = null;
		}
		MarkerOptions marker_options = new MarkerOptions()
				.position(new LatLng(position.getLatitude(), position.getLongitude()))
				.title("0")
				.snippet("Your Location");
		marker_options.icon(BitmapDescriptorFactory.fromResource(R.drawable.marker_user));
		marker_options.flat(true);
		userMarker = googleMap.addMarker(marker_options);
		if(markerListAdapter != null)
			markerListAdapter.notifyDataSetChanged();
		
		if (NAVIGATION_ENABLED && destination != null) {
			headingOffset = position.bearingTo(destination);
			try {
				TextView distance_to = findViewById(R.id.navigation_distance);
				distance_to.setText("Dist To Dest: " + String.valueOf(position.distanceTo(destination)) + " m");
				distance_to.setVisibility(TextView.VISIBLE);
				TextView distance_name_tv = findViewById(R.id.navigation_destination);
				distance_name_tv.setText(getString(R.string.dest_display) + destinationName);
				distance_name_tv.setVisibility(TextView.VISIBLE);
			} catch (Exception ex) {
				Log.wtf("mFisheries", ex.getLocalizedMessage());
			}
		} else {
			try {
				TextView distance_to = findViewById(R.id.navigation_distance);
				distance_to.setVisibility(TextView.GONE);
				distance_to.setText("");
				TextView distance_name_tv = findViewById(R.id.navigation_destination);
				distance_name_tv.setVisibility(TextView.GONE);
				distance_name_tv.setText("");
			} catch (Exception ex) {
				Log.wtf("mFisheries", ex.getLocalizedMessage());
			}
		}
		
		TextView user_postion = findViewById(R.id.navigation_position);
		TextView user_bearing = findViewById(R.id.navigation_bearing);

		user_postion.setText("Lat: " + LocationUtil.convertLatitude(position.getLatitude()) + "\nLng: " + LocationUtil.convertLongitude(position.getLongitude()));
		user_bearing.setText(getString(R.string.bearing_display) + String.valueOf(position.getBearing()));
	}

	@Override
	public void onLocationChanged(Location location) {
		Log.d("Nav", "Location changed");
		super.onLocationChanged(location);
		updateUserPosition(location);
	}

	private class MarkerListAdapter extends BaseAdapter {

		private LayoutInflater layout_inflator;

		public MarkerListAdapter(Context context) {
			layout_inflator = LayoutInflater.from(context);
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			ViewHolder view_holder;

			if (convertView == null) {
				convertView = layout_inflator.inflate(R.layout.custom_marker_list_item, parent, false);
				view_holder = new ViewHolder();
				view_holder.txt_title = convertView.findViewById(R.id.marker_list_text);
				view_holder.img_icon = convertView.findViewById(R.id.marker_list_icon);
				convertView.setTag(view_holder);
			} else {
				view_holder = (ViewHolder) convertView.getTag();
			}
			Marker marker = (Marker) getItem(position);
			if (marker.getSnippet().contains("Private")) {
				view_holder.img_icon.setBackgroundResource(R.drawable.marker_anchor);
			} else if (marker.getSnippet().contains("Hazard")) {
				view_holder.img_icon.setBackgroundResource(R.drawable.marker_hazard);
			} else if (marker.getSnippet().contains("Destination")) {
				view_holder.img_icon.setBackgroundResource(R.drawable.marker_crosshair);
			} else if (marker.getSnippet().contains("Your Location")) {
				view_holder.img_icon.setBackgroundResource(R.drawable.marker_user);
			} else if (marker.getSnippet().contains("SOS")) {
				view_holder.img_icon.setBackgroundResource(R.drawable.marker_sos);
			}
			view_holder.txt_title.setText(marker.getSnippet());
			return convertView;
		}

		private class ViewHolder {
			ImageView img_icon;
			TextView txt_title;
		}

		@Override
		public int getCount() {
			if(googleMap == null)
				return 0;
			return googleMap.getMarkers().size();
		}

		@Override
		public Object getItem(int position) {
			if(googleMap == null)
				return 0;
			return googleMap.getMarkers().get(position);
		}

		@Override
		public long getItemId(int position) {
			return position;
		}
	}

	private OnItemClickListener marker_list_click_listener = new OnItemClickListener() {
		@Override
		public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
			try {
				if (!COMPASS_ENABLED) {
					Marker requested_marker = googleMap.getMarkers().get(position);
					LatLng zoom_position = new LatLng(requested_marker.getPosition().latitude, requested_marker.getPosition().longitude - 0.25);
					googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(zoom_position, 9));
					requested_marker.showInfoWindow();
				}
			} catch (Exception ex) {
				Log.wtf("mFisheries", "Map Screen, Error accessing item " + ex.getLocalizedMessage());
				Toast.makeText(NavigationActivity.this, "Marker Not Available", Toast.LENGTH_LONG).show();
			}
			
			markerListView.setVisibility(ListView.GONE);
		}
	};

	private OnItemLongClickListener marker_list_long_click_listener = new OnItemLongClickListener() {
		@Override
		public boolean onItemLongClick(AdapterView<?> parent, View view, final int position, long id) {

			final Marker marker = googleMap.getMarkers().get(position);
			AlertDialog.Builder builder = new AlertDialog.Builder(NavigationActivity.this);
			
			if (NAVIGATION_ENABLED) {
				builder.setMessage("Navigate to this point?").setPositiveButton("Yes", new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int id) {
						try {
							Button navigateStop = findViewById(R.id.navigation_info_button);
							navigateStop.setVisibility(ImageView.VISIBLE);

							updateHeading(marker);
							if (MARKER_LIST_ENABLED) {
								MARKER_LIST_ENABLED = !MARKER_LIST_ENABLED;
								markerListView.setVisibility(ListView.GONE);
							}
						} catch (Exception ex) {
							Log.wtf("mFisheries", "Map Screen, Error deleting marker" + ex.getLocalizedMessage());
						}
					}
				})
						.setNegativeButton("No", new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog, int id) {
								marker.hideInfoWindow();
								dialog.cancel();
							}
						});
			} else {
				builder.setMessage("Delete this point?").setPositiveButton("Yes", new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int id) {
						try {
							if (marker.getSnippet().contains("SOS")) {
								DatabaseHandler database = new DatabaseHandler(NavigationActivity.this, 3);
								database.open();
//								database.gcmHistoryDBHelper.deleteMarker(marker.getTitle());
								database.close();
								marker.remove();
							} else {
								DatabaseHandler database = new DatabaseHandler(NavigationActivity.this, 1);
								database.open();
								database.mapMarkerDBHelper.deleteMarker(marker.getTitle());
								database.close();
								marker.remove();
							}
							markerListAdapter.notifyDataSetChanged();
						} catch (Exception ex) {
							Log.wtf("mFisheries", "Map Screen, Error deleting marker" + ex.getLocalizedMessage());
						}
					}
				})
						.setNegativeButton("No", new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog, int id) {
								marker.hideInfoWindow();
								dialog.cancel();
							}
						});
			}
			AlertDialog alert = builder.create();
			alert.show();
			return true;
		}
	};

	public void toggleMarkerList(View view) {
		MARKER_LIST_ENABLED = !MARKER_LIST_ENABLED;
		if (MARKER_LIST_ENABLED)
			markerListView.setVisibility(ListView.VISIBLE);
		else
			markerListView.setVisibility(ListView.GONE);
	}

	public void toggleCompassView(View view) {
		ImageView zoomin = findViewById(R.id.navigation_zoom_in);
		ImageView zoomout = findViewById(R.id.navigation_zoom_out);
		ImageView compass = findViewById(R.id.navigation_compass_toggle);
		navigate = findViewById(R.id.navigation_navigate);
		
		zoomin.setVisibility(ImageView.GONE);
		zoomout.setVisibility(ImageView.GONE);

		if (!COMPASS_ENABLED) {
			findViewById(R.id.compass_view).setVisibility(View.VISIBLE);
//			googleMap.getUiSettings().setScrollGesturesEnabled(false);
			tileOverlay.clearTileCache();
			zoomin.setVisibility(ImageView.GONE);
			zoomout.setVisibility(ImageView.GONE);
			compass.setBackgroundResource(R.drawable.widget_return_map);
		} else {
			findViewById(R.id.compass_view).setVisibility(View.GONE);
			zoomin.setVisibility(ImageView.VISIBLE);
			zoomout.setVisibility(ImageView.VISIBLE);
			compass.setBackgroundResource(R.drawable.widget_compass_screen);
		}
		COMPASS_ENABLED = !COMPASS_ENABLED;
	}

	//zoom in and zoom out functions
	public void zoomIn(View view) {
		float zoom = googleMap.getCameraPosition().zoom + 1;
		if (zoom > 5 && zoom < 15) {
			if (!COMPASS_ENABLED) {
				googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(googleMap.getCameraPosition().target, zoom));
			}
		}
	}
	
	public void zoomOut(View view) {
		float zoom = googleMap.getCameraPosition().zoom - 1;
		if (zoom > 5 && zoom < 15) {
			if (!COMPASS_ENABLED) {
				googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(googleMap.getCameraPosition().target, zoom));
			}
		}
	}
	
	public void updateUserPosition(View view) {
		if (mCurrentLocation == null) {
			Toast.makeText(NavigationActivity.this, "Cannot Get Lock On Location", Toast.LENGTH_LONG).show();
		}

		if (!COMPASS_ENABLED && mCurrentLocation != null) {
			updateUserPosition(mCurrentLocation);
			if (userMarker != null) {
				LatLng zoom_position = new LatLng(userMarker.getPosition().latitude, userMarker.getPosition().longitude);
				googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(zoom_position, 12));
				userMarker.showInfoWindow();
			}
		} else if (COMPASS_ENABLED && mCurrentLocation != null) {
			updateUserPosition(mCurrentLocation);
		}
	}
	
	//New navigation
	public void navigateToMarker(View view) {
		NAVIGATION_ENABLED = !NAVIGATION_ENABLED;
		if (NAVIGATION_ENABLED) {
			compassArrow.setVisibility(ImageView.VISIBLE);
			if (!MARKER_LIST_ENABLED) {
				MARKER_LIST_ENABLED = !MARKER_LIST_ENABLED;
				markerListView.setVisibility(ListView.VISIBLE);
				updateUserPosition(navigate);
			}
			Toast.makeText(NavigationActivity.this, "Long-press on any marker in list to select as destination", Toast.LENGTH_LONG).show();
		} else {
			updateHeading(null);
			compassArrow.setVisibility(ImageView.GONE);
			if (MARKER_LIST_ENABLED) {
				MARKER_LIST_ENABLED = !MARKER_LIST_ENABLED;
				markerListView.setVisibility(ListView.GONE);
			}
			Button navigateStop = findViewById(R.id.navigation_info_button);
			navigateStop.setVisibility(ImageView.GONE);
			findViewById(R.id.navigation_distance).setVisibility(TextView.GONE);
			findViewById(R.id.navigation_destination).setVisibility(TextView.GONE);
			Toast.makeText(NavigationActivity.this, "Navigation Mode Deactivated", Toast.LENGTH_LONG).show();
		}
	}

	@Override
	public int getLayoutResourceId() {
		return R.layout.activity_navigation;
	}

	@Override
	public int getColor(){
		return R.color.yellow;
	}

	@Override
	public int getColorDark(){
		return R.color.yellowDark;
	}
}
