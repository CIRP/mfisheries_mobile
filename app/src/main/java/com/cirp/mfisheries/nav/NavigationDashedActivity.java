package com.cirp.mfisheries.nav;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.location.Location;
import android.location.LocationListener;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.androidmapsextensions.Marker;
import com.androidmapsextensions.MarkerOptions;
import com.cirp.mfisheries.R;
import com.cirp.mfisheries.core.DatabaseHandler;
import com.cirp.mfisheries.core.location.LocationActivity;
import com.cirp.mfisheries.core.module.ModuleActivity;
import com.cirp.mfisheries.util.LocationUtil;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;

import java.util.ArrayList;
import java.util.List;

public class NavigationDashedActivity extends LocationActivity implements LocationListener {

    private TextView mTextMessage;
    private TextView mLatitude;
    private TextView mLongitude;
    private LinearLayout Location;
    private LinearLayout Compass;
    private LinearLayout markerView;
    private ListView markerListView;
    private NavigationDashedActivity.MarkerListAdapter markerListAdapter;

    private Location userLoc, destination;
    private String destinationName = "";

    public  String share = "";

    private boolean MARKER_LIST_ENABLED, NAVIGATION_ENABLED, COMPASS_ENABLED;
    private ImageView compass, compassArrow;
    private List<MapMarkerDM> mapMarkers = new ArrayList<>();

//    private MarkerListAdapter markerListAdapter;
    private float headingOffset = 0;
    private static final byte ADD_MARKER_ACTIVITY = 11;




    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            switch (item.getItemId()) {
                case R.id.navigation_home:
                    mTextMessage.setText("My Location");
                    Compass.setVisibility(View.GONE);
                    markerView.setVisibility(View.GONE);
                    Location.setVisibility(View.VISIBLE);
                    COMPASS_ENABLED = false;
                    MARKER_LIST_ENABLED = false;


                    return true;
                case R.id.navigation_dashboard:
                    mTextMessage.setText("Markers");
                    Location.setVisibility(View.GONE);
                    Compass.setVisibility(View.GONE);
                    markerView.setVisibility(View.VISIBLE);
                    MARKER_LIST_ENABLED = true;
                    COMPASS_ENABLED = false;



                    return true;
                case R.id.navigation_notifications:
                    mTextMessage.setText("Compass");
                    Location.setVisibility(View.GONE);
                    markerView.setVisibility(View.GONE);
                    Compass.setVisibility(View.VISIBLE);
                    COMPASS_ENABLED = true;
                    MARKER_LIST_ENABLED = false;


                    return true;
            }
            return false;
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //setContentView(R.layout.activity_navigation_dashed);
        permissions = new String[]{Manifest.permission.ACCESS_FINE_LOCATION};


        module = new Navigation(this);
        markerView = findViewById(R.id.marker_list);
        markerListView = findViewById(R.id.navigation_marker_list);

        requestPermissions();

        NAVIGATION_ENABLED = false;
        COMPASS_ENABLED = false;


        mTextMessage = (TextView) findViewById(R.id.message);
        mLatitude = (TextView) findViewById(R.id.latitude);
        mLongitude = (TextView) findViewById(R.id.longitude);
        Location = (LinearLayout) findViewById(R.id.mylocation);
        Compass = (LinearLayout) findViewById(R.id.compass_view);
        BottomNavigationView navigation = (BottomNavigationView) findViewById(R.id.navigation);
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);

        try {
            initializeCompass(sensorEventListener);
        } catch (Exception ex) {
            Log.wtf("mFisheries", "Map Screen On Create, Error ");
        }
        try {
            new NavigationDashedActivity.GetMarkersTask().execute();
        } catch (Exception ex) {
            Log.wtf("mFisheries", "Map Screen, Error starting thread " + ex.getLocalizedMessage());
        }
    }

    public void setLocation(Location location) {
        latitude = location.getLatitude();
        longitude = location.getLongitude();

    }

    public void initializeCompass(SensorEventListener eventListener) {
        SensorManager sensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
        Sensor compass_sensor = sensorManager.getDefaultSensor(Sensor.TYPE_ORIENTATION);
        compass = findViewById(R.id.navigation_compass_a);
        compassArrow = findViewById(R.id.navigation_compass_b);
        compassArrow.setVisibility(ImageView.GONE);
        if (compass_sensor != null) {
            sensorManager.registerListener(eventListener, compass_sensor, SensorManager.SENSOR_DELAY_FASTEST);
            TextView info = findViewById(R.id.navigation_compass_exists);
            info.setVisibility(TextView.GONE);
        } else {
            TextView info = findViewById(R.id.navigation_compass_exists);
            info.setVisibility(TextView.VISIBLE);
            info.setText(R.string.no_compass);
            Toast.makeText(getApplicationContext(), "No Compass on device", Toast.LENGTH_LONG).show();
        }
    }

    private SensorEventListener sensorEventListener = new SensorEventListener() {
        @Override
        public void onSensorChanged(SensorEvent event) {
            float north_offset = event.values[0];
            compass.setRotation(-north_offset);
            if (NAVIGATION_ENABLED) {
                compassArrow.setRotation(headingOffset - north_offset);
            }
        }

        @Override
        public void onAccuracyChanged(Sensor sensor, int accuracy) { }
    };

    public void openAddMarkerActivity(View view) {
        if (mCurrentLocation != null) {
            Intent map_click = new Intent(getApplicationContext(), AddMarkerActivity.class);
            map_click.putExtra("user_id", 0);
            map_click.putExtra("latitude", mCurrentLocation.getLatitude());
            map_click.putExtra("longitude", mCurrentLocation.getLongitude());
            startActivityForResult(map_click, ADD_MARKER_ACTIVITY);
        } else {
            Toast.makeText(NavigationDashedActivity.this, "Cannot Get Lock On Location", Toast.LENGTH_LONG).show();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case ADD_MARKER_ACTIVITY:
                if (resultCode == Activity.RESULT_OK) {
                    new NavigationDashedActivity.GetMarkersTask().execute();
                }
        }
    }

    private class GetMarkersTask extends AsyncTask<Void, Void, Boolean> {

        protected Boolean doInBackground(Void... params) {
            try {
                DatabaseHandler db = new DatabaseHandler(NavigationDashedActivity.this, 1);
                db.open();
                mapMarkers = db.mapMarkerDBHelper.getAllMarkers();
                db.close();
                db = new DatabaseHandler(NavigationDashedActivity.this, 3);
                db.open();
//				sosMarkers = db.gcmHistoryDBHelper.getAllGCMHistory();
                db.close();
                return true;
            } catch (Exception ex) {
                Log.wtf("mFisheries", "Map Screen, Error acquiring markers " + ex.getLocalizedMessage());
                return false;
            }
        }

        protected void onPostExecute(Boolean success) {
            if(success) {
                markerListAdapter = new MarkerListAdapter(NavigationDashedActivity.this);
                markerListView.setAdapter(markerListAdapter);
                markerListView.setOnItemClickListener(marker_list_click_listener);
                markerListView.setOnItemLongClickListener(marker_list_long_click_listener);
                Intent intent = getIntent();
                Bundle extras = intent.getExtras();
                if (extras != null) {
                    addAlertMarker(extras.getString("name"), extras.getString("details"), extras.getString("lat"), extras.getString("lng"), extras.getString("timestamp"));
                }
//                refreshNavigationActivity();
            }
        }
    }

    private void addAlertMarker(String name, String details, String lat, String lng, String timestamp) {
        Log.d("nav", "adding");
        MapMarkerDM new_point = new MapMarkerDM();
        new_point.id = 0x3;
        new_point.type = 0;
        new_point.name = name;
        new_point.details = details;
        new_point.latitude = lat;
        new_point.longitude = lng;
        new_point.timestamp = timestamp;
        new_point.user_id = 0;
    }

    private class MarkerListAdapter extends BaseAdapter {

        private LayoutInflater layout_inflator;

        public MarkerListAdapter(Context context) {
            layout_inflator = LayoutInflater.from(context);
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            NavigationDashedActivity.MarkerListAdapter.ViewHolder view_holder;

            if (convertView == null) {
                convertView = layout_inflator.inflate(R.layout.custom_marker_list_item, parent, false);
                view_holder = new NavigationDashedActivity.MarkerListAdapter.ViewHolder();
                view_holder.txt_title = convertView.findViewById(R.id.marker_list_text);
                view_holder.img_icon = convertView.findViewById(R.id.marker_list_icon);
                convertView.setTag(view_holder);
            } else {
                view_holder = (NavigationDashedActivity.MarkerListAdapter.ViewHolder) convertView.getTag();
            }
            MapMarkerDM marker = (MapMarkerDM) getItem(position);
            view_holder.txt_title.setText(marker.name);
            return convertView;
        }

        private class ViewHolder {
            ImageView img_icon;
            TextView txt_title;
        }

        @Override
        public int getCount() {
            if(mapMarkers == null)
                return 0;
            return mapMarkers.size();
        }

        @Override
        public Object getItem(int position) {
            if(mapMarkers == null)
                return 0;
            return mapMarkers.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }
    }

    private AdapterView.OnItemClickListener marker_list_click_listener = new AdapterView.OnItemClickListener() {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, final int position, long id) {

            final AlertDialog.Builder builder = new AlertDialog.Builder(NavigationDashedActivity.this);

            builder.setMessage("Navigate to this point?")
                    .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            try {

                                Button navigateStop = findViewById(R.id.navigation_info_button);
                                navigateStop.setVisibility(ImageView.VISIBLE);
                                compassArrow.setVisibility(ImageView.VISIBLE);
                                updateHeading(mapMarkers.get(position));


                                double latitude = Double.parseDouble(mapMarkers.get(position).latitude);
                                double longitude = Double.parseDouble(mapMarkers.get(position).longitude);
                                destination.setLatitude(latitude);
                                destination.setLongitude(longitude);
                                destinationName = mapMarkers.get(position).name;

                                Toast.makeText(getApplicationContext(), "Navigation Mode Activated", Toast.LENGTH_LONG).show();
                                Log.d("NAVDASHED","Marker list is:" + MARKER_LIST_ENABLED);

                                if (MARKER_LIST_ENABLED) {
                                    Log.d("NAVDASHED","Marker list is Visible");
                                    MARKER_LIST_ENABLED = !MARKER_LIST_ENABLED;
                                    markerView.setVisibility(ListView.GONE);
                                    Compass.setVisibility(View.VISIBLE);
                                    COMPASS_ENABLED = true;
                                    navigateToMarker(Compass);
                                }
                            } catch (Exception ex) {
                                Log.wtf("mFisheries", "Map Screen, Error deleting marker" + ex.getLocalizedMessage());
                            }
                        }
                    })
                    .setNegativeButton("No", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            if (NAVIGATION_ENABLED) {
                                NAVIGATION_ENABLED = false;
                            }
                            builder.setMessage("View Point on a map?")
                                    .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                            try{
                                                String latitude = mapMarkers.get(position).latitude;
                                                String longitude = mapMarkers.get(position).longitude;
                                                String urlStr = String.format("geo:0,0?q=" + latitude + "," + longitude+"?z=9");
                                                Uri gmmIntentUri = Uri.parse(urlStr); // Create a Uri from an intent string. Use the result to create an Intent.
                                                Intent mapIntent = new Intent(Intent.ACTION_VIEW, gmmIntentUri);
                                                mapIntent.setPackage("com.google.android.apps.maps");
                                                mapIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                                getApplicationContext().startActivity(mapIntent);
                                            }catch (Exception ex){
                                                Log.wtf("mFisheries", "Map Screen, Error showing marker" + ex.getLocalizedMessage());
                                            }
                                        }
                                    })
                                    .setNegativeButton("No", new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                            dialog.cancel();
                                        }
                                    }).show();
//                            dialog.cancel();
                        }
                    })
                    .show();
            }
    };

    private AdapterView.OnItemLongClickListener marker_list_long_click_listener = new AdapterView.OnItemLongClickListener() {
        @Override
        public boolean onItemLongClick(AdapterView<?> parent, View view, final int position, long id) {

            final MapMarkerDM marker = mapMarkers.get(position);
            AlertDialog.Builder builder = new AlertDialog.Builder(NavigationDashedActivity.this);

            builder.setMessage("Delete this point?").setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int id) {
                    try {
                        if (marker.name.contains("SOS")) {
                            DatabaseHandler database = new DatabaseHandler(NavigationDashedActivity.this, 3);
                            database.open();
//								database.gcmHistoryDBHelper.deleteMarker(marker.getTitle());
                            database.close();
                            mapMarkers.remove(position);
                        } else {
                            DatabaseHandler database = new DatabaseHandler(NavigationDashedActivity.this, 1);
                            database.open();
                            database.mapMarkerDBHelper.deleteMarker(String.valueOf(marker.id));
                            database.close();
                            mapMarkers.remove(position);

                        }
                        markerListAdapter.notifyDataSetChanged();
                    } catch (Exception ex) {
                        Log.wtf("mFisheries", "Map Screen, Error deleting marker" + ex.getLocalizedMessage());
                    }
                }
            })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });
            AlertDialog alert = builder.create();
            alert.show();
            return true;
        }
    };


    public void navigateToMarker(View view) {
        NAVIGATION_ENABLED = !NAVIGATION_ENABLED;
        if (NAVIGATION_ENABLED) {
            compassArrow.setVisibility(ImageView.VISIBLE);
            if (!MARKER_LIST_ENABLED) {
                MARKER_LIST_ENABLED = !MARKER_LIST_ENABLED;
                markerListView.setVisibility(ListView.VISIBLE);
                updateUserPosition(mCurrentLocation);

            }
        } else {
            updateHeading(null);
            compassArrow.setVisibility(ImageView.GONE);
            if (MARKER_LIST_ENABLED) {
                MARKER_LIST_ENABLED = !MARKER_LIST_ENABLED;
                markerListView.setVisibility(ListView.GONE);
            }
            Button navigateStop = findViewById(R.id.navigation_info_button);
            navigateStop.setVisibility(ImageView.GONE);
            findViewById(R.id.navigation_distance).setVisibility(TextView.GONE);
            findViewById(R.id.navigation_destination).setVisibility(TextView.GONE);
            Toast.makeText(NavigationDashedActivity.this, "Navigation Mode Deactivated", Toast.LENGTH_LONG).show();
        }
    }

    private void updateUserPosition(Location position) {

//        if (userMarker != null) {
//            userMarker.remove();
//            userMarker = null;
//        }
//        MarkerOptions marker_options = new MarkerOptions()
//                .position(new LatLng(position.getLatitude(), position.getLongitude()))
//                .title("0")
//                .snippet("Your Location");
//        marker_options.icon(BitmapDescriptorFactory.fromResource(R.drawable.marker_user));
//        marker_options.flat(true);
//        userMarker = googleMap.addMarker(marker_options);
        if(markerListAdapter != null)
            markerListAdapter.notifyDataSetChanged();

        if (NAVIGATION_ENABLED && destination != null) {
            headingOffset = position.bearingTo(destination);
            try {
                TextView distance_to = findViewById(R.id.navigation_distance);
                distance_to.setText("Dist To Dest: " + String.valueOf(position.distanceTo(destination)) + " m");
                distance_to.setVisibility(TextView.VISIBLE);
                TextView distance_name_tv = findViewById(R.id.navigation_destination);
                distance_name_tv.setText(getString(R.string.dest_display) + destinationName);
                distance_name_tv.setVisibility(TextView.VISIBLE);
            } catch (Exception ex) {
                Log.wtf("mFisheries", ex.getLocalizedMessage());
            }
        } else {
            try {
                TextView distance_to = findViewById(R.id.navigation_distance);
                distance_to.setVisibility(TextView.GONE);
                distance_to.setText("");
                TextView distance_name_tv = findViewById(R.id.navigation_destination);
                distance_name_tv.setVisibility(TextView.GONE);
                distance_name_tv.setText("");
            } catch (Exception ex) {
                Log.wtf("mFisheries", ex.getLocalizedMessage());
            }
        }

        TextView user_postion = findViewById(R.id.navigation_position);
        TextView user_bearing = findViewById(R.id.navigation_bearing);

        user_postion.setText("Lat: " + LocationUtil.convertLatitude(position.getLatitude()) + "\nLng: " + LocationUtil.convertLongitude(position.getLongitude()));
        user_bearing.setText(getString(R.string.bearing_display) + String.valueOf(position.getBearing()));
    }

    public void shareLocation(View view){
        Intent shares = new Intent(Intent.ACTION_SEND);
        shares.setType("text/plain");
        Log.d("Sharing", "New Share Value is:" + share);
        shares.putExtra(Intent.EXTRA_SUBJECT, "User Located at");
        shares.putExtra(Intent.EXTRA_TEXT,share);
        startActivity(Intent.createChooser(shares,"Share Via"));
    }

    private void updateHeading(MapMarkerDM marker) {

        if (marker != null) {
            if (mCurrentLocation != null) {
                destination = new Location(mCurrentLocation);
                destination.setLatitude(Double.parseDouble(marker.latitude));
                destination.setLongitude(Double.parseDouble(marker.longitude));
                destinationName = marker.name;
                headingOffset = mCurrentLocation.bearingTo(destination);
                if (mCurrentLocation != null)
                    updateUserPosition(mCurrentLocation);
            }
        }
        markerListAdapter.notifyDataSetChanged();
        markerListAdapter.notifyDataSetInvalidated();
    }

    @Override
    public int getLayoutResourceId() {
        return R.layout.activity_navigation_dashed;
}

    @Override
    public void onLocationChanged(android.location.Location location) {
        Log.d("Nav", "Location changed");
        super.onLocationChanged(location);
        mLatitude.setText(LocationUtil.convertLatitude(location.getLatitude()));
        mLongitude.setText(LocationUtil.convertLongitude(location.getLongitude()));
        share += "Lat: " + LocationUtil.convertLatitude(location.getLatitude()) + "\nLng: " + LocationUtil.convertLongitude(location.getLongitude());

    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {

    }

    @Override
    public void onProviderDisabled(String provider) {

    }

    @Override
    public int getColor(){
        return R.color.yellow;
    }

    @Override
    public int getColorDark(){
        return R.color.yellowDark;
    }
}
