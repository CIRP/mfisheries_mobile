package com.cirp.mfisheries.firstaid;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.cirp.mfisheries.R;

import java.util.List;

public class StepsAdapter extends ArrayAdapter<String> {
	private final Context context;
	private final List<String> steps;

	static class ViewHolder {
		public TextView number;
		public TextView text;
	}

	public StepsAdapter(Context context, List<String> steps) {
		super(context, R.layout.layout_step, steps);
		this.context = context;
		this.steps = steps;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		View rowView = convertView;
		if (rowView == null) {
			LayoutInflater inflater = (LayoutInflater) context
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			rowView = inflater.inflate(R.layout.layout_step, parent, false);

			ViewHolder viewHolder = new ViewHolder();
			viewHolder.number = rowView.findViewById(R.id.step_number);
			viewHolder.text = rowView.findViewById(R.id.step_text);
			rowView.setTag(viewHolder);
		}
		String step = steps.get(position);

		ViewHolder holder = (ViewHolder) rowView.getTag();
		holder.text.setText(step);
		holder.number.setText(position + 1 + "");

		return rowView;
	}

	@Override
	public void add(String object) {
		steps.add(object);
		this.notifyDataSetChanged();
	}

}