package com.cirp.mfisheries.firstaid;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.cirp.mfisheries.R;
import com.cirp.mfisheries.util.FileUtil;
import com.squareup.moshi.JsonAdapter;
import com.squareup.moshi.Moshi;

import java.util.List;

public class FirstAidFragment extends Fragment {

	private static final String ARG_TYPE = "type";
	private static final String TAG = "FirstAid";
	private String type;
	private static Context mContext;
	private ListView listView;
	private MediaPlayer mp;
	private boolean stop_flag;
	private List<String> audio;
	private List<String> images;
	private ImageView imageView;
	public int position = 0;
	private Button playButton;
	private Button pauseButton;
	private Button stopButton;
	private static FirstAid firstAid;


	public static FirstAidFragment newInstance(Context context, String type, FirstAid module) {
		FirstAidFragment fragment = new FirstAidFragment();
		Bundle args = new Bundle();
		args.putString(ARG_TYPE, type);
		fragment.setArguments(args);
		mContext = context;
		firstAid = module;
		return fragment;
	}

	public FirstAidFragment() {
		// Required empty public constructor
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setHasOptionsMenu(true);
		if (getArguments() != null) {
			type = getArguments().getString(ARG_TYPE);
		}
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
	                         Bundle savedInstanceState) {
		// Inflate the layout for this fragment
		View view = inflater.inflate(R.layout.fragment_first_aid, container, false);

		TextView title = view.findViewById(R.id.first_aid_title);
		title.setText(type.toUpperCase());

		playButton = view.findViewById(R.id.cmd_play);
		pauseButton = view.findViewById(R.id.cmd_pause);
		stopButton = view.findViewById(R.id.cmd_stop);

		playButton.setOnClickListener(new View.OnClickListener() {
			public void onClick(View arg0) {
				if (stop_flag) {
					playAudio(0);
				} else
					mp.start();
				playButton.setVisibility(View.GONE);
				pauseButton.setVisibility(View.VISIBLE);
			}
		});

		pauseButton.setOnClickListener(new View.OnClickListener() {
			public void onClick(View arg0) {
				pauseAudio();
			}
		});

		stopButton.setOnClickListener(new View.OnClickListener() {
			public void onClick(View arg0) {
				stopAudio();
			}
		});

		try {
			//loads json
			String json = FileUtil.readJSONFile(firstAid.getId() + "/" + type + "/info.json");
			//parses json
			Moshi moshi = new Moshi.Builder().build();
			JsonAdapter<Info> jsonAdapter = moshi.adapter(Info.class);

			Info info = jsonAdapter.fromJson(json);
			if (info == null) {
				Toast.makeText(mContext, "Module did not download correctly", Toast.LENGTH_LONG).show();
				return view;
			}
			audio = info.audio;
			images = info.images;

			Log.i(TAG, audio.toString());

			//creates adapter of steps for particular item
			StepsAdapter adapter = new StepsAdapter(mContext, info.steps);
			listView = view.findViewById(R.id.steps_list_view);
			listView.setAdapter(adapter);
			//play audio for specific step on click
			listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
				@Override
				public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
					if (position < images.size() && !images.isEmpty())
						imageView.setImageURI(FileUtil.getFileUri(firstAid.getId() + "/" + type + "/" + images.get(position)));
					playAudio(position);
				}
			});
		} catch (Exception e) {
			e.printStackTrace();
		}
		imageView = view.findViewById(R.id.first_aid_image);
		if (images != null && !images.isEmpty()) {
			imageView.setImageURI(FileUtil.getFileUri(firstAid.getId() + "/" + type + "/" + images.get(0)));
			//shows image in dialog when clicked
			imageView.setOnClickListener(new View.OnClickListener() {
				public void onClick(View arg0) {
					showFullSizeImage();
				}
			});
		}
		if(audio == null) {
            try {
                //loads json
                String json = FileUtil.readJSONFile(firstAid.getId() + "/" + type + "/info.json");
                //parses json
                Moshi moshi = new Moshi.Builder().build();
                JsonAdapter<Info> jsonAdapter = moshi.adapter(Info.class);

                Info info = jsonAdapter.fromJson(json);
                if (info == null) {
                    Toast.makeText(mContext, "Module did not download correctly", Toast.LENGTH_LONG).show();
                    return view;
                }
                audio = info.audio;
            } catch (Exception e) {
                Log.d(TAG,  "Unable to retrieve audio.");
                Toast.makeText(mContext, "Unable to retrieve Audio please try again", Toast.LENGTH_LONG).show();
                e.printStackTrace();
            }
        }else{
            Log.d(TAG, audio.size() + "");
            mp = MediaPlayer.create(mContext, FileUtil.getFileUri(firstAid.getId() + "/" + type + "/" + audio.get(0)));
            playAudio(position);
        }
        return view;
    }

	public void showFullSizeImage() {
		Dialog dialog = null;
		try{
			dialog = createImageDialog();
		}catch (IndexOutOfBoundsException e){
			e.printStackTrace();
		}
		dialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialog) {
                resumeAudio();
            }
        });
		if (!stop_flag) {
            try {
                mp.pause();
                mp.prepareAsync();
            } catch (IllegalStateException e) {
                e.printStackTrace();
            }
            pauseButton.setVisibility(View.GONE);
            playButton.setVisibility(View.VISIBLE);
        }
		dialog.show();
	}

	public Dialog createImageDialog() {
		final Dialog dialog = new Dialog(mContext, R.style.Base_Theme_AppCompat_DialogWhenLarge);
		dialog.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
		dialog.setContentView(R.layout.image_dialog);
		ImageView imageFull = dialog.findViewById(R.id.image_full);

		if(position >= images.size()){
			imageFull.setImageURI(FileUtil.getFileUri(firstAid.getId() + "/" + type + "/" + images.get(images.size()-1)));
		}else{
			imageFull.setImageURI(FileUtil.getFileUri(firstAid.getId() + "/" + type + "/" + images.get(position)));
		}
		//dismiss dialog and continue playing on dialog image click
		imageFull.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				if (!stop_flag) {
					mp.start();
					playButton.setVisibility(View.GONE);
					pauseButton.setVisibility(View.VISIBLE);
				}
				dialog.dismiss();
			}
		});
		return dialog;
	}

	public void pauseAudio() {
		try {
			mp.pause();
			mp.prepareAsync();
		} catch (Exception e) {
			e.printStackTrace();
		}
		pauseButton.setVisibility(View.GONE);
		playButton.setVisibility(View.VISIBLE);
	}

	public void stopAudio() {
		try {
			mp.stop();
			mp.release();
			stop_flag = true;
			position = 0;
		} catch (Exception e) {
			e.printStackTrace();
		}
		if (!playButton.isShown()) {
			pauseButton.setVisibility(View.GONE);
			playButton.setVisibility(View.VISIBLE);
		}
		changeItem(0);
	}

	//plays audio file for a specific step
	public void playAudio(final int position) {
		this.position = position;
		if (position >= audio.size()) {
			pauseButton.setVisibility(View.GONE);
			playButton.setVisibility(View.VISIBLE);
			return;
		}
		if (!stop_flag) {
			mp.stop();
			mp.release();
		} else
			stop_flag = false;

		pauseButton.setVisibility(View.VISIBLE);
		playButton.setVisibility(View.GONE);
		mp = MediaPlayer.create(mContext, FileUtil.getFileUri(firstAid.getId() + "/" + type + "/" + audio.get(position)));

		changeItem(position);
		mp.start();
		mp.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
			@Override
			public void onCompletion(MediaPlayer mp) {
				if (position >= images.size()){
					imageView.setImageURI(FileUtil.getFileUri(firstAid.getId() + "/" + type + "/" + images.get(position-1)));
					return;
				}
				if (!images.isEmpty())
					imageView.setImageURI(FileUtil.getFileUri(firstAid.getId() + "/" + type + "/" + images.get(position)));
				playAudio(position + 1);
			}
		});
	}

	public void resumeAudio() {
		if (stop_flag) {
			playAudio(position);
		} else
			mp.start();
		playButton.setVisibility(View.GONE);
		pauseButton.setVisibility(View.VISIBLE);
	}


	public void changeItem(int position) {
		if(position >= audio.size()){
			return;
		}
			listView.smoothScrollToPosition(position);
			listView.setSelection(position);
	}

	@Override
	public void onAttach(Context context) {
		super.onAttach(context);
	}

	@Override
	public void onDetach() {
		stopAudio();
		super.onDetach();
	}
}
