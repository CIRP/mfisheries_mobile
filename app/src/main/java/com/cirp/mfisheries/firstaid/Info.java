package com.cirp.mfisheries.firstaid;

import java.util.List;

//model of first aid item json
public class Info {

	public String name;
	public List<String> steps;
	public List<String> audio;
	public List<String> images;
}
