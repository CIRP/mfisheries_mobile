package com.cirp.mfisheries.firstaid;

import android.content.Context;

import com.cirp.mfisheries.R;
import com.cirp.mfisheries.core.module.Module;
import com.cirp.mfisheries.util.NetUtil;
import com.cirp.mfisheries.util.PrefsUtil;

public class FirstAid extends Module {

	public FirstAid(Context context){
		super(context);
	}

	@Override
	public Module initialize() {
		if (this.context != null && !PrefsUtil.getPath(this.context).equals("")) // ensure country path is set
			this.dataUrl = NetUtil.SITE_URL + PrefsUtil.getPath(this.context) + "/FirstAid.zip";
		return super.initialize();
	}


	@Override
	protected Module setModuleId() {
		this.moduleId = "FirstAid";
		return this;
	}

	@Override
	protected Module setModuleName() {
		this.name = "First Aid";
		return this;
	}

	@Override
	protected Module setIsDisplayed() {
		this.displayed = true;
		return this;
	}

	@Override
	protected Module setImageResource() {
		this.imageResource = R.drawable.icon_firstaid;
		return this;
	}

	@Override
	protected Module setNeedsRegistration() {
		this.needsRegistration = false;
		return this;
	}

	@Override
	protected Module setActivityClass() {
		this.activityClass = FirstAidActivity.class;
		return this;
	}
}
