package com.cirp.mfisheries.lek.models;

import android.os.Parcel;
import android.os.Parcelable;
import android.util.Log;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.squareup.moshi.JsonAdapter;
import com.squareup.moshi.Moshi;

import java.util.HashMap;
import java.util.Map;

public class LEKPost  implements Parcelable {

	public int id;
	public String userid;
	public String fullname;
	public String category;
	public String text;
	public String latitude;
	public String longitude;
	public String filetype;
	public String filepath;
	public String timestamp;
	public String aDate;
	public String isSpecific = "0";
	public boolean synced;
	public String location;
	public String countryid;

	
	public LEKPost() {
	
	}
	
	// Order must be maintained with Write to Parcel
	public LEKPost(Parcel in) {
		text = in.readString();
		userid = in.readString();
		countryid = in.readString();
		latitude = in.readString();
		longitude = in.readString();
		filetype = in.readString();
		filepath = in.readString();
		aDate = in.readString();
		category = in.readString();
		fullname = in.readString();
		isSpecific = in.readString();
		id = in.readInt();
	}
	
	public String [] getRequiredFields(){
		return new String[]{
			"userid",
			"countryid",
			"filetype",
			"filepath",
			"text",
			"category",
			"latitude",
			"longitude",
			"aDate"
		};
	}
	
	public static final Creator<LEKPost> CREATOR = new Creator<LEKPost>() {
		@Override
		public LEKPost createFromParcel(Parcel in) {
			return new LEKPost(in);
		}
		
		@Override
		public LEKPost[] newArray(int size) {
			return new LEKPost[size];
		}
	};
	
	@Override
	public int describeContents() {
		return 0;
	}
	
	
	// Order must be maintained with constructor
	@Override
	public void writeToParcel(Parcel parcel, int i) {
		parcel.writeString(text);
		parcel.writeString(userid);
		parcel.writeString(countryid);
		parcel.writeString(latitude);
		parcel.writeString(longitude);
		parcel.writeString(filetype);
		parcel.writeString(filepath);
		parcel.writeString(aDate);
		parcel.writeString(category);
		parcel.writeString(fullname);
		parcel.writeString(String.valueOf(isSpecific));
		parcel.writeInt(id);
	}
	
	
	public Map<String, String> getValues(){
		HashMap<String, String> map = new HashMap<>();
		map.put("text", text);
		map.put("userid", userid);
		map.put("countryid", countryid);
		map.put("latitude", latitude);
		map.put("longitude", longitude);
		map.put("filetype", filetype);
		map.put("filepath", filepath);
		map.put("aDate", aDate);
		map.put("category", category);
		map.put("fullname", fullname);
		map.put("isSpecific", isSpecific);
		if (id != 0)map.put("id", String.valueOf(id));
		return  map;
	}
	
	public String toString(){
		StringBuilder stb = new StringBuilder();
		Map <String, String> map = getValues();
		stb.append("{");
		for (Map.Entry<String, String> entry : map.entrySet()) {
			stb.append("\"")
					.append(entry.getKey())
					.append("\"")
					.append(":")
					.append("\"")
					.append(entry.getValue())
					.append("\"")
					.append(",");
		}
		stb.append("}");
		return stb.toString();
	}
	
	public boolean isValid(){
		return userid.length() > 0 &&
				countryid.length() > 0 &&
				filetype.length() > 3 &&
				category.length() > 3 &&
				latitude.length() > 5 &&
				longitude.length() > 5 &&
				aDate.length() > 5;
	}
	
	public JsonObject getJsonObject() {
		try {
			Moshi moshi = new Moshi.Builder().build();
			JsonAdapter<LEKPost> jsonAdapter = moshi.adapter(LEKPost.class);
			String json = jsonAdapter.toJson(this);
			Log.d("LEKPosts:", "JSON: " + json);
			JsonParser parser = new JsonParser();
			return (JsonObject) parser.parse(json);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
}
