package com.cirp.mfisheries.lek.models;

public class LEKCategory {

	public int id;
	public String name;
	public String countryid;
	public String country;
	public String timecreated;
	public String timemodified;
	public String createdby;
	
	

	public LEKCategory() {
	}

	public LEKCategory(int id, String name) {
		this.id = id;
		this.name = name;
	}
	
	
}
