package com.cirp.mfisheries.lek;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.location.Location;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.DialogFragment;
import android.support.v4.content.FileProvider;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.cirp.mfisheries.R;
import com.cirp.mfisheries.core.Country;
import com.cirp.mfisheries.core.CountryAdapter;
import com.cirp.mfisheries.core.location.LocationDependentActivity;
import com.cirp.mfisheries.core.location.LocationService;
import com.cirp.mfisheries.lek.models.CategoryAdapter;
import com.cirp.mfisheries.lek.models.LEKCategory;
import com.cirp.mfisheries.lek.models.LEKPost;
import com.cirp.mfisheries.util.CountryUtil;
import com.cirp.mfisheries.util.FileUtil;
import com.cirp.mfisheries.util.LocationUtil;
import com.cirp.mfisheries.util.MediaUtils;
import com.cirp.mfisheries.util.NetUtil;
import com.cirp.mfisheries.util.PrefsUtil;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlacePicker;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;
import com.koushikdutta.ion.builder.Builders;
import com.squareup.moshi.JsonAdapter;
import com.squareup.moshi.Moshi;
import com.squareup.picasso.Picasso;

import java.io.File;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import id.zelory.compressor.Compressor;

import static com.cirp.mfisheries.util.MediaUtils.COMPRESS_QUALITY;

public class LEKActivity extends LocationDependentActivity implements OnMapReadyCallback, GoogleMap.InfoWindowAdapter, GoogleMap.OnInfoWindowClickListener, DatePickerDialog.OnDateSetListener, GoogleMap.OnInfoWindowCloseListener {

	private static final int PLACE_PICKER_REQUEST = 121;

	private static Date postDate;
	private String postCategory = "";

	private String currentFilePath;
	private String currentFileType = "text/*";
	private Uri currentFileUri;

	private Location selectedLocation;
	private List<LEKPost> posts = new ArrayList<>();
	private GoogleMap map;
	private String selectedType = "all";
	private String selectedCategory = "all";
	private Date selectedDate;
	private String datePickerType = "post";
	private boolean offlinePosts;

	private TextView pickDateTextView;

	private int selectedPostIndex;

	private boolean IS_ZOOMED;
	private boolean FILTER_DISPLAYED;
	private boolean SHARE_DISPLAYED;
	private LEKUtils util;
	private String TAG = "LEKActivity";
	private MediaUtils mediaUtil;
	private int lekAttempts = 0;
	private int categoryAttempts = 0;
	private MenuItem miActionProgressItem;
	private List<Country> countries;
	private boolean isCountrySpinnerTouched;
	private boolean isUserSpinnerTouched;
	private boolean isCategorySpinnerTouched;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		Log.d(TAG, "On Create executed");

		module = new LEK(this);
		util = new LEKUtils(getApplicationContext());
		mediaUtil = new MediaUtils(this);
		countries = new ArrayList<>();

		//initialize map
		MapFragment mapFragment = (MapFragment) getFragmentManager().findFragmentById(R.id.map);
		mapFragment.getMapAsync(this);

		//set click listener for fab
		FloatingActionButton fab = findViewById(R.id.lek_fab);
		fab.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				togglePostBox();
			}
		});

		//set today's date
		Calendar c = Calendar.getInstance();
		postDate = c.getTime();
		c.add(Calendar.DAY_OF_YEAR, -7);
		selectedDate = c.getTime();

		permissions = new String[]{
				Manifest.permission.ACCESS_FINE_LOCATION,
				Manifest.permission.CAMERA,
				Manifest.permission.RECORD_AUDIO,
				Manifest.permission.WRITE_EXTERNAL_STORAGE
		};
		requestPermissions(); // TODO - LEK - request permissions also done in the parent class
		
		resetFilters("");
		
		setFilterDropdown();

//		getLEK();
	}

	private void setFilterDropdown() {
		Log.d(TAG, "Set Filter Dropdown executed");
		findViewById(R.id.filter).setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				View grid = findViewById(R.id.filter_grid);
				TextView filter = findViewById(R.id.filter);
				if(FILTER_DISPLAYED) {
					grid.setVisibility(View.GONE);
					filter.setCompoundDrawablesWithIntrinsicBounds(null , null,
							getResources().getDrawable(R.drawable.ic_chevron_down_grey600_36dp), null);
				}else{
					grid.setVisibility(View.VISIBLE);
					filter.setCompoundDrawablesWithIntrinsicBounds(null , null,
							getResources().getDrawable(R.drawable.ic_chevron_up_grey600_36dp), null);
				}
				FILTER_DISPLAYED = !FILTER_DISPLAYED;
			}
		});
	}

	protected void updateMap(Location location){
		Log.d(TAG, "Update Map (default zoom)");
		updateMap(location, 10);
	}
	
	protected void updateMap(Location location, float zoom){
		Log.d(TAG, "Update Map with custom zoom");
		if (this.map != null) {
			Log.d(TAG, "Retrieved Location from Service");
			LatLng position = new LatLng(location.getLatitude(), location.getLongitude());
			this.map.animateCamera(CameraUpdateFactory.newLatLngZoom(position, zoom));
			IS_ZOOMED = true;
		}
	}
	
	@Override
	public void onMapReady(GoogleMap map) {
		Log.d(TAG, "On Map Ready");
		this.map = map;
		Location location = LocationService.userLocation;
		if(location != null) {
			updateMap(location);
		}else{
			Log.d(TAG, "Location is Null");
		}
		this.map.setOnInfoWindowClickListener(this);
		this.map.setInfoWindowAdapter(this);
		this.map.setOnInfoWindowCloseListener(this);
		if(NetUtil.isOnline(this))
			getLEK();
		else {
			Toast.makeText(this, "Unable to retrieve Posts. Phone is currently Offline.", Toast.LENGTH_SHORT).show();
		}
	}

	@Override
	public void onPermissionGranted(String permission) {
		Log.d(TAG, "On Permission Granted");
		super.onPermissionGranted(permission);
		getLocation();
	}

	private void getLocation(){
		Log.d(TAG, "Get Location");
		selectedLocation = LocationService.userLocation;
		if (selectedLocation == null){
			startLocationUpdates();
		}
	}
	
	// TODO - LEK - Retrieve LEK only on the specific country
	//gets LEK data stored on server
	private void getLEK() {
		Log.d(TAG, "Retrieving all LEK posts");
		showProgressBar();
		util.retrieve(new LEKUtils.PostReceiveListener() {
			@Override
			public void onReceived(List<LEKPost> rPosts) {
				hideProgressBar();
				if (rPosts != null){
					Log.d(TAG, "Retrieved: " + rPosts.size() + " posts");
					posts = rPosts;
					addMarkers(posts);
				}else{
					Log.d(TAG, "Unable to retrieve posts");
					// Retry attempt to request
					if (lekAttempts++ < 3) // Retry up to 3 times
						getLEK();
					else lekAttempts = 0;
				}
			}
		});
	}

	private void getLEKByDate(int day, int month, int year) {
		Log.d(TAG, "Getting LEK by date");
//		getLEK(NetUtil.GET_LEK_DATE(PrefsUtil.getUserId(this), selectedType, selectedCategory, day, month, year));
	}
	
	private void getLEKByUserID(String userid) {
		Log.d(TAG, "Retrieve LEK by user id");
		showProgressBar();
		util.retrieveBy("userid", userid, new LEKUtils.PostReceiveListener() {
			@Override
			public void onReceived(List<LEKPost> rPosts) {
				hideProgressBar();
				clearMarkers();
				if (rPosts != null) {
					posts = rPosts;
					addMarkers(posts);
				}
			}
		});
	}
	
	private void getLEKPersonal() {
		Log.d(TAG, "Retrieving person LEK posts");
		showProgressBar();
		util.retrievePersonalPost(new LEKUtils.PostReceiveListener() {
			@Override
			public void onReceived(List<LEKPost> rPosts) {
				hideProgressBar();
				clearMarkers();
				if (rPosts != null) {
					posts = rPosts;
					addMarkers(posts);
				}
			}
		});
	}
	
	private void getLEKByCountry(Country country) {
		Log.d(TAG, "Get LEK by country");
		showProgressBar();
		util.retrieveBy("countryid", country.getId(), new LEKUtils.PostReceiveListener() {
			@Override
			public void onReceived(List<LEKPost> rPosts) {
				hideProgressBar();
				clearMarkers();
				if (rPosts != null) {
					posts = rPosts;
					addMarkers(posts);
				}
			}
		});
	}
	
	private void getLEKByCategory(final String selectedCategory) {
		Log.d(TAG, "Get LEK By Category");
		showProgressBar();
		util.retrieveBy("category", selectedCategory, new LEKUtils.PostReceiveListener() {
			@Override
			public void onReceived(List<LEKPost> rPosts) {
				hideProgressBar();
				clearMarkers();
				posts = rPosts;
				if (posts != null) addMarkers(posts);
				else {
					Log.d(TAG, "Received no posts");
					if (categoryAttempts++ < 3)
						getLEKByCategory(selectedCategory);
					else {
						categoryAttempts = 0;
						// TODO - LEK - Notify user that request failed
					}
				}
			}
		});
	}
	
	@Override
	public void onLocationChanged(Location location){
		super.onLocationChanged(location);
		Log.d(TAG, "On Location Changed");
		if (!placeSelectedByUser){
			selectedLocation = location;
			this.updateMap(location);
		}
	}
	
	public void addMarkers(List<LEKPost> posts) {
		Log.d(TAG, "Add Markers");
		for(int i = 0; i < posts.size(); i ++){
			addMarker(posts.get(i), i);
		}
	}

	private void addMarker(LEKPost post, int i){
		Log.d(TAG, "Add Marker");
		LatLng latLng = new LatLng(Double.parseDouble(post.latitude), Double.parseDouble(post.longitude));
		Marker marker = map.addMarker(new MarkerOptions()
				.position(latLng)
				.title(post.text)
				.snippet(post.latitude + ", " + post.longitude));

		marker.setTag(i);
		if(!IS_ZOOMED){
			this.map.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng, 12));
			IS_ZOOMED = true;
		}
	}

	private void clearMarkers(){
		Log.d(TAG, "Clear Markerts");
		map.clear();
		posts = new ArrayList<>();
	}

	@Override
	public View getInfoContents(Marker marker) {
		Log.d(TAG, "Get Info Contents");
		selectedPostIndex = (int)marker.getTag();
		LEKPost post = posts.get(selectedPostIndex);
		Log.d(TAG, "Getting info contents for " + post.toString());

		SHARE_DISPLAYED = true;
		invalidateOptionsMenu();

		View infoWindow = getLayoutInflater().inflate(R.layout.layout_lek_window, null);

		((TextView) infoWindow.findViewById(R.id.lek_user)).setText(post.fullname);
		((TextView) infoWindow.findViewById(R.id.lek_date)).setText(post.timestamp);
		((TextView) infoWindow.findViewById(R.id.lek_text)).setText(post.text);
		((TextView) infoWindow.findViewById(R.id.lek_category)).setText(String.format("Category: %s", post.category));
		((TextView) infoWindow.findViewById(R.id.lek_location)).setText(String.format("%s %s", LocationUtil.convertLatitude(Double.parseDouble(post.latitude)), LocationUtil.convertLongitude(Double.parseDouble(post.longitude))));

		Log.d(TAG, "File Path of selected Post was set to: " + post.filepath);
		
		String type = "";
		if(post.filetype.contains("image")){
			type = "image";
			((Button)infoWindow.findViewById(R.id.lek_button)).setText(String.format("Open %s", type));
		}
		else if(post.filetype.contains("video")){
			type = "video";
			((Button)infoWindow.findViewById(R.id.lek_button)).setText(String.format("Open %s", type));
		}
		else if(post.filetype.contains("audio")){
			type = "audio";
			((Button)infoWindow.findViewById(R.id.lek_button)).setText(String.format("Open %s", type));
		}
		else{
			infoWindow.findViewById(R.id.lek_button).setVisibility(View.GONE);
		}

		return infoWindow;
	}

	@Override
	public View getInfoWindow(Marker marker) {
		return null;
	}

	@Override
	public void onInfoWindowClose(Marker marker){
		SHARE_DISPLAYED = false;
		invalidateOptionsMenu();
	}

	@Override
	public void onInfoWindowClick(Marker marker) {
		Log.d(TAG, "On Info Window Click");
		selectedPostIndex = (int)marker.getTag();
		LEKPost post = posts.get(selectedPostIndex);
		if(post.filetype.contains("text") || post.filepath == null)
			return;

		Uri data = Uri.parse(NetUtil.SITE_URL + post.filepath);
		Log.d(TAG, "Opening LEK: " + NetUtil.SITE_URL + post.filepath);

		Intent intent = new Intent(android.content.Intent.ACTION_VIEW);
		intent.setDataAndType(data, post.filetype);
		intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
		startActivity(intent);
	}

	private Uri getLocalFileUri(LEKPost post) {
		Log.d(TAG, "Get Local File URI");
		File file = new File(post.filepath);
		Uri uri = FileUtil.getOutputMediaFileUri(file);
		if (Build.VERSION.SDK_INT >= 24)
			uri = FileProvider.getUriForFile(this, getPackageName() + ".provider", file);
		return uri;
	}

	private void getLEKCategories(){
		Log.d(TAG, "Get LEK Categories");
		util.retrieveCategories(new LEKUtils.CategoryListener() {
			public void onReceived(List<LEKCategory> categoryList) {
				if (categoryList != null) {
					Log.d(TAG, "Retrieved " + categoryList.size() + " LEK Categories");
					setCategorySpinner(categoryList);
				}else Log.d(TAG, "Unable to retrieve Categories");
			}
		}, false);
	}

	private void setCategorySpinner(List<LEKCategory> categories){
		Log.d(TAG, "Set Category Spinner");
		try {
			final LEKCategory unspecified = new LEKCategory(0, "Unspecified");
			categories.add(0, unspecified);
			
			// Process List for Create a LEK Post
			final Spinner categoryCreateSpinner = findViewById(R.id.selectCtgSpinner);
			CategoryAdapter adapter2 = new CategoryAdapter(LEKActivity.this, categories);
			categoryCreateSpinner.setAdapter(adapter2);
			categoryCreateSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
				public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
					postCategory = ((LEKCategory)adapterView.getItemAtPosition(i)).name.toLowerCase();
				}
				public void onNothingSelected(AdapterView<?> adapterView) { }
			});
			
			// Process List for Filter
			isCategorySpinnerTouched = false;
			final Spinner categoryFilterSpinner = findViewById(R.id.categoriesSpinner);
			categories.add(0, new LEKCategory(0, "All"));
			CategoryAdapter adapter = new CategoryAdapter(this, categories);
			categoryFilterSpinner.setAdapter(adapter);
			categoryFilterSpinner.setOnTouchListener(new View.OnTouchListener() {
				@Override
				public boolean onTouch(View view, MotionEvent motionEvent) {
					switch (motionEvent.getAction()) {
						case MotionEvent.ACTION_DOWN:
							isCategorySpinnerTouched = true;
							return false;
						case MotionEvent.ACTION_UP:
							view.performClick();
							break;
						default:
							break;
					}
					return true;
				}
			});
			categoryFilterSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
				public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
					if (isCategorySpinnerTouched) {
						selectedCategory = ((LEKCategory) adapterView.getItemAtPosition(i)).name.toLowerCase();
						getLEKByCategory(selectedCategory);
						resetFilters("category");
					}
					isCategorySpinnerTouched = false;
				}
				
				public void onNothingSelected(AdapterView<?> adapterView) {
					isCategorySpinnerTouched = false;
				}
			});
		}
		catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void setUserFilter(){
		Log.d(TAG, "Set User Filter");
		final Spinner userSpinner = findViewById(R.id.userSpinner);
		isUserSpinnerTouched = false;
		userSpinner.setOnTouchListener(new View.OnTouchListener() {
			@Override
			public boolean onTouch(View view, MotionEvent motionEvent) {
				switch (motionEvent.getAction()) {
					case MotionEvent.ACTION_DOWN:
						isUserSpinnerTouched = true;
						return false;
					case MotionEvent.ACTION_UP:
						view.performClick();
						break;
					default:
						break;
				}
				return true;
			}
		});
		userSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
			@Override
			public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
				if (isUserSpinnerTouched) {
					clearMarkers();
					String user = (String) adapterView.getItemAtPosition(i);
					Log.d(TAG, "Selected user: " + user);
					if (user.equalsIgnoreCase("all")) {
						getLEK();
					} else {
						getLEKPersonal();
					}
					resetFilters("user");
				}
				isUserSpinnerTouched = false;
			}

			@Override
			public void onNothingSelected(AdapterView<?> adapterView) {
				isUserSpinnerTouched = false;
			}
		});
		userSpinner.setSelection(0);
	}
	
	public void setCountryFilter() {
		Log.d(TAG, "Set Country Filter");
		final Spinner countrySpinner = findViewById(R.id.countrySpinner);
		final CountryAdapter adapter = new CountryAdapter(getBaseContext(), countries);
		isCountrySpinnerTouched = false;
		countrySpinner.setAdapter(adapter);
		// Setup Listeners
		countrySpinner.setOnTouchListener(new View.OnTouchListener() {
			@Override
			public boolean onTouch(View view, MotionEvent motionEvent) {
				switch (motionEvent.getAction()) {
					case MotionEvent.ACTION_DOWN:
						isCountrySpinnerTouched = true;
						return false;
					case MotionEvent.ACTION_UP:
						view.performClick();
						break;
					default:
						break;
				}
				return true;
			}
		});
		countrySpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
			@Override
			public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
				if (countries != null) {
					if (isCountrySpinnerTouched) {
						Country country = countries.get(position);
						getLEKByCountry(country);
						resetFilters("country");
					}
				}
				isCountrySpinnerTouched = false;
			}
			
			@Override
			public void onNothingSelected(AdapterView<?> parent) {
				isCountrySpinnerTouched = false;
			}
		});
		
		// Retrieve Countries and populate to filter
		final View countryLbl = findViewById(R.id.conutry_lbl);
		CountryUtil.getInstance(this).retrieveCountries(new CountryUtil.CountryRetrievedListener() {
			@Override
			public void processCountries(List<Country> countryList) {
				if (countryList != null && countryList.size() > 0) {
					countries.clear();
					countries.addAll(countryList);
					countries.add(0, new Country("0", "All", ""));
					countrySpinner.post(new Runnable() {
						@Override
						public void run() {
							countrySpinner.setVisibility(View.VISIBLE);
							countryLbl.setVisibility(View.VISIBLE);
							adapter.notifyDataSetChanged();
						}
					});
					countrySpinner.setSelection(0);
				}
			}
		});
		
	}
	
	public void setTypesFilter(){
		Spinner spinner = findViewById(R.id.typeSpinner);
		spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
			@Override
			public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
				selectedType = ((String)adapterView.getItemAtPosition(i)).toLowerCase();
				getLEK();
			}

			@Override
			public void onNothingSelected(AdapterView<?> adapterView) { }
		});
	
		spinner.setVisibility(View.GONE);
		findViewById(R.id.types_lbl).setVisibility(View.GONE);
	}

	public void setDatesFilter(){
		Spinner spinner = findViewById(R.id.dateSpinner);
		ArrayAdapter <String> adapter = new ArrayAdapter<>(this, R.layout.spinner_item2, getResources().getStringArray(R.array.lek_dates));
		spinner.setAdapter(adapter);
		spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
			@Override
			public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
				Log.d(TAG, "item selected");
				Calendar calendar = Calendar.getInstance();
				if(i == 0){
					calendar.add(Calendar.DAY_OF_YEAR, -7);
					selectedDate = calendar.getTime();
				}
				else if (i == 1){
					calendar.add(Calendar.MONTH, -1);
					selectedDate = calendar.getTime();
				}
				else if (i == 2){
					calendar.add(Calendar.YEAR, -1);
					selectedDate = calendar.getTime();
				}
				else {
					pickDateTextView = (TextView)view;
					datePickerType = "filter";
					DatePickerFragment datePickerFragment = new DatePickerFragment();
					datePickerFragment.setListener(LEKActivity.this);
					datePickerFragment.show(getSupportFragmentManager(), "datePicker");
					return;
				}
				getLEK();
			}

			@Override
			public void onNothingSelected(AdapterView<?> adapterView) {
				Log.d(TAG, "nothing selected");
			}
		});
		
		spinner.setVisibility(View.GONE);
		findViewById(R.id.ddate_lbl).setVisibility(View.GONE);
	}
	
	public void resetFilters(@NonNull String except) {
		Log.d(TAG, "Reset filter");
		if (!except.equalsIgnoreCase("user")) setUserFilter();
		if (!except.equalsIgnoreCase("country")) setCountryFilter();
		if (!except.equalsIgnoreCase("category")) getLEKCategories();
		else {
			// Retrieve Categories to populate filter and creator
			getLEKCategories();
			// Use XML to populate User filter
			setUserFilter();
			// Retrieve Countries and populate filter
			setCountryFilter();
			// Use XML to populate filter types
			//setTypesFilter();
			// Generates the to and from dates for filtering
			//setDatesFilter();
		}
	}
	
	//toggles displayThresholds of box to create a new lek post
	public void togglePostBox() {
		Log.d(TAG, "Toggle Post Box");
		requestPermissions();
		FrameLayout layout;
		layout = findViewById(R.id.post_box);
		if (layout.isShown()) {
			layout.setVisibility(View.GONE);
		}else {
			layout.setVisibility(View.VISIBLE);
			Calendar c = Calendar.getInstance();
			int year = c.get(Calendar.YEAR);
			int month = c.get(Calendar.MONTH) + 1;
			int day = c.get(Calendar.DAY_OF_MONTH);

			Button datePicker = findViewById(R.id.datePicker);
			datePicker.setText(String.format(Locale.US,"%d/%d/%d", day, month, year));

			if (selectedLocation != null) {
				showMapPreview(selectedLocation);
			}
		}
	}

	public void reset(){
		Log.d(TAG, "reset");
		EditText lek_text2 = findViewById(R.id.lek_text2);
		lek_text2.setText("");
		ImageView imageView = findViewById(R.id.thumbnail);
		imageView.setImageBitmap(null);

		Calendar c = Calendar.getInstance();
		int year = c.get(Calendar.YEAR);
		int month = c.get(Calendar.MONTH) + 1;
		int day = c.get(Calendar.DAY_OF_MONTH);

		Button datePicker = findViewById(R.id.datePicker);
		datePicker.setText(String.format(Locale.US,"%d/%d/%d", day, month, year));

		getLEKCategories();
	}

	//shows an image of the fullname's current location on a map
	public void showMapPreview(Location location) {
		Log.d(TAG, "Show Map Preview");
		String url = "http://maps.google.com/maps/api/staticmap?center=" +
				location.getLatitude() + "," + location.getLongitude() +
				"&zoom=" + 14 + "&scale=2&size=" + 960 + "x" + 960 +
				"&sensor=false&markers=color:red%7C" + location.getLatitude() + "," + location.getLongitude();
		Picasso.with(this)
				.load(url)
				.into((ImageView) findViewById(R.id.mapView));

	}

	//attaches different types of filepath
	public void attach(View view) {
		mediaUtil.attach(view);
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		Log.d(TAG, "On Activity Result");
		try {
			ImageView thumb = findViewById(R.id.thumbnail);
			// Pass over handling request to utility
			mediaUtil.handleActivityResult(requestCode, resultCode, data, thumb, new MediaUtils.MediaListener() {
				@Override
				public void onMediaReceived(Uri uri, String filePath, String fileType) {
					currentFileUri = uri;
					currentFilePath = filePath;
					currentFileType = fileType;
					if (currentFileUri == null) Log.d(TAG, "currentFileUri was null");
					if (currentFilePath == null) Log.d(TAG, "currentFilePath was null");
					if (currentFileType == null) Log.d(TAG, "currentFileType was null");
				}
			});
			
			if (requestCode == PLACE_PICKER_REQUEST && resultCode == RESULT_OK) {
				Place place = PlacePicker.getPlace(this, data);
				placeSelectedByUser = true;
				selectedLocation = new Location("GPS");
				selectedLocation.setLatitude(place.getLatLng().latitude);
				selectedLocation.setLongitude(place.getLatLng().longitude);
				showMapPreview(selectedLocation);
			}
		}catch (Exception e){
			e.printStackTrace();
			Toast.makeText(this, "Unable to successfully attach media to post", Toast.LENGTH_SHORT).show();
		}
	}

	//creates an LEKPost object with all entered data
	public LEKPost createLEKPost() {
		Log.d(TAG, "Create LEK Post");
		try {
			if (selectedLocation == null) {
				Toast.makeText(LEKActivity.this, "Could not get your location", Toast.LENGTH_LONG).show();
				return null;
			}
			EditText editText = findViewById(R.id.lek_text2);
			LEKPost lekPost = new LEKPost();
			lekPost.latitude = selectedLocation.getLatitude() + "";
			lekPost.longitude = selectedLocation.getLongitude() + "";
			lekPost.aDate = String.valueOf(postDate.getTime());
			Log.d(TAG, "Setting Post to: " + lekPost.aDate);
			String text = editText.getText().toString();
			if (text.equals("")) {
				Toast.makeText(LEKActivity.this, "Must enter some text", Toast.LENGTH_LONG).show();
				return null;
			}
			lekPost.text = text;
			lekPost.timestamp = new Date().toString();
			lekPost.filepath = currentFilePath;
			lekPost.filetype = currentFileType;
			if(postCategory.equals("all"))
				lekPost.category = "Other";
			else
				lekPost.category = postCategory;
			Log.d(TAG, PrefsUtil.getUserId(this) + " - " + currentFilePath);
			
			lekPost.userid = PrefsUtil.getUserId(this) + "";
			lekPost.fullname = PrefsUtil.getUser(this);
			lekPost.countryid = PrefsUtil.getCountryId(this);
			
			return lekPost;
		} catch (Exception e) {
			e.printStackTrace();
		}
		Toast.makeText(this, "Error saving post, try again", Toast.LENGTH_SHORT).show();
		return null;
	}

	//uploads data to the server
	public synchronized void uploadData(View view) {
		Log.d(TAG, "Upload Data");
		final ProgressDialog progress = new ProgressDialog(this);
		progress.setMessage(getString(R.string.posting));
		progress.setProgressStyle(ProgressDialog.STYLE_SPINNER);
		progress.setIndeterminate(true);
		progress.setCancelable(false);
		progress.show();

		final LEKPost post = createLEKPost();
		if (post == null) {
			progress.dismiss();
			return;
		}

		// TODO Need to revisit comprehensive offline strategy
		if(!NetUtil.isOnline(this)){
			Log.d(TAG, "Offline, Attempting to use offline cache");
			String LEKCache = new Gson().toJson(post);
			PrefsUtil.setLekToSync(LEKActivity.this, true);
			PrefsUtil.setUserLEKPost(this, LEKCache);
			Toast.makeText(LEKActivity.this, "Offline: Saving post to cache to be uploaded later", Toast.LENGTH_LONG).show();
//			saveLEK(post);
			progress.dismiss();
			togglePostBox();
//			Toast.makeText(LEKActivity.this, "Offline: Please connect to internet and try again", Toast.LENGTH_LONG).show();
			return;
		}
		Log.d(TAG, "Online Attempting to send data to server");
		
		File file = null;
		if (post.filepath != null)
			file = new File(post.filepath);

		FutureCallback<JsonObject> callback = new FutureCallback<JsonObject>() {
			@Override
			public void onCompleted(Exception e, JsonObject result) {
				if (e == null && result != null) {
					try {
						Log.d(TAG, "Post Result: " + result.toString());
						Toast.makeText(LEKActivity.this, "Post successful!", Toast.LENGTH_LONG).show();
						togglePostBox();
						clearMarkers();
						
						Moshi moshi = new Moshi.Builder().build();
						JsonAdapter<LEKPost> postJsonAdapter = moshi.adapter(LEKPost.class);
						LEKPost lekPost = postJsonAdapter.fromJson(result.toString());
						
						posts.add(lekPost);
						addMarkers(posts);
						Location location = new Location("");
						location.setLatitude(Double.parseDouble(lekPost.latitude));
						location.setLongitude(Double.parseDouble(lekPost.longitude));
						
						updateMap(location, 12);
						
					} catch (Exception exc) {
//						PrefsUtil.setLekToSync(LEKActivity.this, true);
						exc.printStackTrace();
						Toast.makeText(LEKActivity.this, "Error: Unable to save LEK Post", Toast.LENGTH_LONG).show();
					}
				} else if (e != null) {
//					PrefsUtil.setLekToSync(LEKActivity.this, true);
					Toast.makeText(LEKActivity.this, "Error: Unable to save LEK Post at this time.", Toast.LENGTH_LONG).show();
					e.printStackTrace();
				}

//				saveLEK(post);
				progress.dismiss();
			}
		};

		sendLEK(post, file, callback);
	}

	public void sendLEK(@NonNull final LEKPost post, File file, @NonNull FutureCallback<JsonObject> callback) {
		Log.d(TAG, "Send LEK");
		final String url = NetUtil.API_URL + NetUtil.ADD_LEK_PATH;
		Log.d(TAG, String.format("Attempting to send %s to %s ", url, post));
		Map<String, String> map = post.getValues();
		Builders.Any.B builder = Ion.with(this)
				.load(url)
				.setHeader(NetUtil.API_KEY_PARAM, NetUtil.API_KEY);
		if (file != null){
			// Try to compress the file if its an image
			if (post.filetype != null && post.filetype.equalsIgnoreCase("image/*")){
				try{
					Log.d(TAG, "Attempting to compress image");
					file = new Compressor(this).setQuality(COMPRESS_QUALITY).compressToFile(file);
				}catch (Exception e){ Log.e(TAG, "Unable to compress images"); e.printStackTrace(); }
			}
			builder.setMultipartFile("file", post.filetype, file);
		}
		else builder.setMultipartParameter("filepath", "");
		// Add each value of the post as part of the request
		for (Map.Entry<String, String> entry : map.entrySet()) {
			builder.setMultipartParameter(entry.getKey(), entry.getValue());
//			Log.d(TAG, String.format("Sending: %s-%s", entry.getKey(), entry.getValue()));
		}
		builder.asJsonObject().setCallback(callback);
		reset();
	}

	//shows place picker
	public void pickPlace(View view) {
		Log.d(TAG, "Pick Place");
		if (NetUtil.isOnline(this)) {
			try {
				PlacePicker.IntentBuilder builder = new PlacePicker.IntentBuilder();
				startActivityForResult(builder.build(this), PLACE_PICKER_REQUEST);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}else{
			Toast.makeText(this, "Cannot change location at this time. Will use default/current location ", Toast.LENGTH_SHORT).show();
		}
	}
	
	//shows data picker
	public void pickDate(View view) {
		Log.d(TAG, "Pick Date");
		datePickerType = "post";
		DatePickerFragment datePickerFragment = new DatePickerFragment();
		datePickerFragment.setListener(this);
		datePickerFragment.show(getSupportFragmentManager(), "datePicker");
	}

	@Override
	public void onDateSet(DatePicker view, int year, int month, int day) {
		int actualMonth = month + 1;
		String date = day + "/" + actualMonth + "/" + year;
		if(datePickerType.equals("post")) {

			Calendar c = Calendar.getInstance();
			c.set(year, month, day);
			postDate = c.getTime();

			Button datePicker = findViewById(R.id.datePicker);
			datePicker.setText(date);
		}
		else{
			pickDateTextView.setText(date);
			getLEKByDate(day, month, year);
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.menu_lek, menu);
		
		MenuItem menuItem = menu.findItem(R.id.action_share);
		menuItem.setVisible(SHARE_DISPLAYED);
		menuItem = menu.findItem(R.id.action_delete);
		menuItem.setVisible(SHARE_DISPLAYED);
		
		return true;
	}
	
	@Override
	public boolean onPrepareOptionsMenu(Menu menu) {
		// Store instance of the menu item containing progress
		miActionProgressItem = menu.findItem(R.id.miActionProgress);
		// Extract the action-view from the menu item
		if (miActionProgressItem != null && miActionProgressItem.getActionView() instanceof ProgressBar) {
			Log.d(TAG, "Received Progress bar correctly");
		} else Log.d(TAG, "Unable to configure progress bar");
		// Return to finish
		return super.onPrepareOptionsMenu(menu);
	}
	
	public void showProgressBar() {
		// Show progress item
		if (miActionProgressItem != null) miActionProgressItem.setVisible(true);
	}
	
	public void hideProgressBar() {
		// Hide progress item
		if (miActionProgressItem != null) miActionProgressItem.setVisible(false);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		int id = item.getItemId();

		if (id == R.id.action_share) {
			sharePost();
			return true;
		}
		
		if (id == R.id.action_delete){
			requestRemoveLEK();
			return true;
		}

		return super.onOptionsItemSelected(item);
	}

	private void sharePost() {
		Log.d(TAG, "Share Post");
		LEKPost post = posts.get(selectedPostIndex);
		String text = post.text + " by " + post.fullname;
		Log.d(TAG, "Sharing post " + post.filetype);
		if(!post.filetype.contains("text")){
			text += " " + NetUtil.SITE_URL + post.location;
		}
		Intent sendIntent = new Intent();
		sendIntent.setAction(Intent.ACTION_SEND);
		sendIntent.putExtra(Intent.EXTRA_TEXT, text);
		sendIntent.setType("text/plain");
		startActivity(sendIntent);
	}

	private void requestRemoveLEK(){
		final LEKPost post = posts.get(selectedPostIndex);
		AlertDialog.Builder myBuilder = new AlertDialog.Builder(this);
		myBuilder.setTitle("Remove Report");
		final String userid = String.valueOf(PrefsUtil.getUserId(this));
		
		if (post.userid.equals(String.valueOf(userid))) {
			myBuilder.setMessage("Are you sure you would like to remove this report")
					.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
						@Override
						public void onClick(DialogInterface dialogInterface, int i) {
							deletePost(post);
						}
					}).setNegativeButton("No", new DialogInterface.OnClickListener() {
				@Override
				public void onClick(DialogInterface dialogInterface, int i) {
					dialogInterface.cancel();
				}
			});
		} else {
			myBuilder.setMessage("Can only delete reports that belong to you")
					.setNeutralButton("OK", new DialogInterface.OnClickListener() {
						@Override
						public void onClick(DialogInterface dialogInterface, int i) {
							dialogInterface.dismiss();
						}
					});
		}
		myBuilder.show();
	}
	
	private void deletePost(LEKPost post) {
		if (post.userid.equals(String.valueOf(PrefsUtil.getUserId(this)))){
			showProgressBar();
			util.delete(post, new LEKUtils.PostUpdateListener() {
				@Override
				public void onUpdated(LEKPost post, boolean status) {
					hideProgressBar();
					final String msg = (status) ? "Post Deleted" : "Unable to delete Post";
					AlertDialog.Builder myBuilder = new AlertDialog.Builder(getApplicationContext());
					myBuilder.setTitle("Remove Report");
					myBuilder.setMessage(msg);
					myBuilder.show();
				}
			});
		}
	}
	
	@Override
	public void onBackPressed() {
		Log.d(TAG, "On Back Pressed");
		FrameLayout layout;
		layout = findViewById(R.id.post_box);
		if (layout.isShown())
			layout.setVisibility(View.GONE);
		else
			super.onBackPressed();
	}
	
	@Override
	public int getLayoutResourceId() {
		return R.layout.activity_lek;
	}
	
	public static class DatePickerFragment extends DialogFragment {
		
		int day, month, year = 0;
		DatePickerDialog.OnDateSetListener listener;
		
		public void setListener(DatePickerDialog.OnDateSetListener listener) {
			this.listener = listener;
		}
		
		@SuppressLint("WrongConstant")
		@Override
		@NonNull
		public Dialog onCreateDialog(Bundle savedInstanceState) {
			// Use the current date as the default date in the picker
			Calendar c = Calendar.getInstance();
			if (year == 0) {
				year = c.get(Calendar.YEAR);
				month = c.get(Calendar.MONTH);
				day = c.get(Calendar.DAY_OF_MONTH);
			}
			DatePickerDialog dialog = new DatePickerDialog(getActivity(), listener, year, month, day);
			dialog.getDatePicker().setMaxDate(new Date().getTime());
			// Create a new instance of DatePickerDialog and return it
			return dialog;
		}
	}
}
