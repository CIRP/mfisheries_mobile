package com.cirp.mfisheries.lek;

import android.content.Context;

import com.cirp.mfisheries.R;
import com.cirp.mfisheries.core.module.Module;

public class LEK extends Module {
	
	public static final int PUBLIC = 2;
	public static final int PERSONAL = 1;

	public LEK(Context context){
		super(context);
	}

	@Override
	protected Module setModuleId() {
		this.moduleId = "LEK";
		return this;
	}

	@Override
	protected Module setModuleName() {
		this.name = "LEK";
		return this;
	}

	@Override
	protected Module setIsDisplayed() {
		this.displayed = true;
		return this;
	}

	@Override
	protected Module setImageResource() {
		this.imageResource = R.drawable.icon_lek;
		return this;
	}

	@Override
	protected Module setNeedsRegistration() {
		this.needsRegistration = true;
		return this;
	}

	@Override
	protected Module setActivityClass() {
		this.activityClass = LEKActivity.class;
		return this;
	}
}
