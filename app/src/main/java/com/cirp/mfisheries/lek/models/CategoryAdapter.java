package com.cirp.mfisheries.lek.models;

import android.content.Context;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.cirp.mfisheries.R;

import java.util.List;

public class CategoryAdapter extends ArrayAdapter<LEKCategory> {

	private final Context context;
	private final List<LEKCategory> categories;

	public CategoryAdapter(Context context, List<LEKCategory> categories) {
		super(context, R.layout.spinner_item2, categories);
		this.context = context;
		this.categories = categories;
	}
	
	@NonNull
	@Override
	public View getView(int position, View convertView, @NonNull ViewGroup parent) {
		View rowView = convertView;
		if (rowView == null) {
			LayoutInflater inflater = (LayoutInflater) context
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			rowView = inflater.inflate(R.layout.spinner_item2, parent, false);
			
			ViewHolder viewHolder = new ViewHolder();
			viewHolder.name = (TextView) rowView;
			rowView.setTag(viewHolder);
		}
		final LEKCategory category = categories.get(position);
		
		ViewHolder holder = (ViewHolder) rowView.getTag();
		holder.name.setText(category.name);
		
		return rowView;
	}

	@Override
	public View getDropDownView(int position, View convertView, ViewGroup parent) {
		View rowView = convertView;
		if (rowView == null) {
			LayoutInflater inflater = (LayoutInflater) context
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			rowView = inflater.inflate(R.layout.spinner_item2, parent, false);

			ViewHolder viewHolder = new ViewHolder();
			viewHolder.name = (TextView) rowView;
			rowView.setTag(viewHolder);
		}
		final LEKCategory category = categories.get(position);
		ViewHolder holder = (ViewHolder) rowView.getTag();
		holder.name.setText(category.name);
		return rowView;
	}
	
	static class ViewHolder {
		public TextView name;
	}
}