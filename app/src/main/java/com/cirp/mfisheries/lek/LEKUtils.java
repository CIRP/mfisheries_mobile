package com.cirp.mfisheries.lek;

import android.content.Context;
import android.support.annotation.Nullable;
import android.util.Log;
import android.widget.Toast;

import com.cirp.mfisheries.lek.models.LEKCategory;
import com.cirp.mfisheries.lek.models.LEKPost;
import com.cirp.mfisheries.util.CrashReporter;
import com.cirp.mfisheries.util.NetUtil;
import com.cirp.mfisheries.util.PrefsUtil;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;
import com.squareup.moshi.JsonAdapter;
import com.squareup.moshi.Moshi;
import com.squareup.moshi.Types;

import java.io.IOException;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

@SuppressWarnings("ALL")
public class LEKUtils {
	private static final String TAG = "LEKUtils";
	final long MAX_DAYS = 5;
	final Context context;
	final String noInternetMsg;
	final String errorMsg;
	
	final String GET = "GET";
	final String UPDATE = "PUT";
	final String DELETE = "DELETE";
	final String SAVE = "POST";
	
	List<LEKPost> records;
	List<LEKCategory>categories;
	
	public LEKUtils(Context context) {
		this.context = context;
		errorMsg = "Unable to retrieve data.";
		noInternetMsg = " Phone not connected. Using Cache values.";
		init();
	}
	
	private void init() {
		records = new ArrayList<>();
	}
	
	public void retrieve(final PostReceiveListener listener){
		final String url = getAPI();
		Log.d(TAG, "Attempting to retrieve LEK Posts from: " + url);
		makeRetrieveRequest(url, listener);
	}
	
	
	public void retrieveBy(final String fieldType, final String fieldValue, final PostReceiveListener listener){
		if(fieldValue.equals("all")){
			final String url = getAPI() + "?" + "posts";
			makeRetrieveRequest(url, listener);
		}else{
			final String url = getAPI() + "?" + fieldType +"="+ fieldValue;
			makeRetrieveRequest(url, listener);
		}
	}
	
	protected void makeRetrieveRequest(String url,  final PostReceiveListener listener ){
		Log.d(TAG, "Attempting to retrieve LEK Posts from: " + url);
		if (NetUtil.isOnline(context)) {
			Ion.with(context)
					.load(GET, url)
					.setHeader(NetUtil.API_KEY_PARAM, NetUtil.API_KEY)
					.asJsonArray()
					.setCallback(new FutureCallback<JsonArray>() {
						@Override
						public void onCompleted(Exception e, JsonArray result) {
							if (e != null){
								listener.onReceived(null);
								e.printStackTrace();
							}
							else {
								try {
									Moshi moshi = new Moshi.Builder().build();
									Type listOfCategoryType = Types.newParameterizedType(List.class, LEKPost.class);
									JsonAdapter<List<LEKPost>> jsonAdapter = moshi.adapter(listOfCategoryType);
									List<LEKPost> posts = new ArrayList<>();
									List<LEKPost> json = jsonAdapter.fromJson(result.toString());
									if (json != null) {
										posts.addAll(json);
										listener.onReceived(posts);
									}
								} catch (Exception exc) {
									exc.printStackTrace();
									Toast.makeText(context, errorMsg, Toast.LENGTH_SHORT).show();
								}
							}
						}
					});
		}else{
			Toast.makeText(context, "Phone offline. Unable to retrieve LEK Posts", Toast.LENGTH_SHORT).show();
			listener.onReceived(null);
		}
	}
	
	private boolean cache(final  List<LEKPost> posts){
		return false;
	}
	
	protected void makeRequest(final String HTTPRequestType, final String url, final PostUpdateListener listener, @Nullable final LEKPost post){
		//https://stackoverflow.com/questions/8898590/short-form-for-java-if-statement
		JsonObject jsonObject = (post == null) ? null : post.getJsonObject();
		try {
			if (NetUtil.isOnline(context)) {
				Ion.with(context)
						.load(HTTPRequestType, url)
						.setHeader(NetUtil.API_KEY_PARAM, NetUtil.API_KEY)
						.setJsonObjectBody(jsonObject)
						.asJsonObject()
						.setCallback(new FutureCallback<JsonObject>() {
							@Override
							public void onCompleted(Exception e, JsonObject result) {
								if (e == null) {
									listener.onUpdated(post, true);
								} else {
									listener.onUpdated(post, false);
									e.printStackTrace();
								}
							}
						});
			}
		}catch (Exception ex){
			ex.printStackTrace();
			listener.onUpdated(post, false);
		}
	}
	
	public void get(final LEKPost post, final PostUpdateListener listener){
		final String url = getAPI() + "/" + post.id;
		makeRequest(GET, url, listener, post);
	}
	
	public void save(final LEKPost post, final PostUpdateListener listener){
		final String url = getAPI();
		makeRequest(SAVE, url, listener, post);
	}
	
	public void update(final LEKPost post, final PostUpdateListener listener){
		final String url = getAPI() + "/" + post.id;
		makeRequest(UPDATE, url, listener, post);
	}
	
	public void delete(final LEKPost post, final PostUpdateListener listener){
		final String url = getAPI() + "/" + post.id;
		makeRequest(DELETE, url, listener, post);
	}
	
	protected String getAPI(){
		return NetUtil.API_URL + "lek";
	}
	
	// ***** Categories *******
	
	public void retrieveCategories(final CategoryListener listener){
		retrieveCategories(listener, true);
	}
	
	public void retrieveCategories(final CategoryListener listener, final boolean useAll){
		if (!canUseCategoryCache()) {
			String url = getCategoriesAPI() + "?countryid=" + PrefsUtil.getCountryId(context);
			if (NetUtil.isOnline(context)){
				Log.d(TAG, "Requesing categories from: " + url );
				Ion.with(context)
						.load(url)
						.setHeader(NetUtil.API_KEY_PARAM, NetUtil.API_KEY)
						.asJsonArray()
						.setCallback(new FutureCallback<JsonArray>() {
							@Override
							public void onCompleted(Exception e, JsonArray result) {
								try {
									if (e == null && result != null) {
										Moshi moshi = new Moshi.Builder().build();
										Type listOfCategoryType = Types.newParameterizedType(List.class, LEKCategory.class);
										JsonAdapter<List<LEKCategory>> jsonAdapter = moshi.adapter(listOfCategoryType);
										List<LEKCategory> categories = new ArrayList<>();
										if (useAll) categories.add(new LEKCategory(0, "All"));
										List<LEKCategory> json = jsonAdapter.fromJson(result.toString());
										if (json != null) {
											categories.addAll(json);
											cacheCategories(categories);
											listener.onReceived(categories);
											return;
										}
									}else{
										e.printStackTrace();
									}
								}catch (Exception ex){
									ex.printStackTrace();
								}
								List<LEKCategory> categories = retrieveLEKCategoriesFromCache();
								listener.onReceived(categories);
							}
						});
			}else{
				Log.d(TAG,"Phone offline preparing to use cache");
				List<LEKCategory> categories = retrieveLEKCategoriesFromCache();
				listener.onReceived(categories);
			}
		}else{
			Log.d(TAG, "Using Cache. TTYL not exceeded");
			List<LEKCategory> categories = retrieveLEKCategoriesFromCache();
			listener.onReceived(categories);
		}
	}

	private  List<LEKCategory> retrieveLEKCategoriesFromCache(){
		List<LEKCategory> categories = new ArrayList<>();
		String cahcedCategories = PrefsUtil.getLEKCategories(context);
		try{
			if (cahcedCategories != null){
				JsonParser parser = new JsonParser();
				JsonElement element = parser.parse(cahcedCategories);
				Moshi moshi = new Moshi.Builder().build();
				Type listType = Types.newParameterizedType(List.class, LEKCategory.class);
				JsonAdapter<List<LEKCategory>> jsonAdapter = moshi.adapter(listType);
				List<LEKCategory> json = jsonAdapter.fromJson(element.getAsJsonArray().toString());
				if(json != null){
					categories.addAll(json);
				}
			}
		}catch (IOException e){
			e.printStackTrace();
			CrashReporter.getInstance(context).report(e);
			Toast.makeText(context, "Unable to retrieve LEK Categories from cache", Toast.LENGTH_SHORT).show();
		}
		Log.d(TAG, String.format("Retrieve %s categories from cache", categories.size()));
		return categories;
	}
	
	private void cacheCategories(final  List<LEKCategory> categories){
		String json = new Gson().toJson(categories);
		if(PrefsUtil.setLEKCategories(context, json)){
			Log.d(TAG, "Successfully Cahced LEK Categories");
			PrefsUtil.setLEKCategoryCacheTime(context, Calendar.getInstance());
		}else{
			Toast.makeText(context, "Unable to add LEK categories to cache", Toast.LENGTH_SHORT).show();
			CrashReporter.getInstance(context).log(Log.ERROR,TAG, "Unable to add LEK Categories to cache");
		}
	}
	
	public boolean canUseCategoryCache() {
		// If we are on WiFi, use online
		Calendar preTime = PrefsUtil.getDMCategoryCacheTime(context);
		boolean useCache = true;
		if (preTime != null) {
			Calendar currTime = Calendar.getInstance();
			long diff = currTime.getTimeInMillis() - preTime.getTimeInMillis();
			long days = diff / (24 * 60 * 60 * 1000);
			if (days > MAX_DAYS) {
				if (NetUtil.isOnline(context) && NetUtil.isWiFi(context)) {
					useCache = false;
				}
			}
		} else useCache = false;
		
		return useCache;
	}
	protected String getCategoriesAPI(){
		return NetUtil.API_URL + "lek/categories";
	}
	
	public void retrievePersonalPost(PostReceiveListener listener) {
		final int userid = PrefsUtil.getUserId(context);
		final String url = getAPI() + "?userid=" +
				userid +
				"&type=all";
		makeRetrieveRequest(url, listener);
	}
	
	public interface PostReceiveListener {
		void onReceived(List<LEKPost> posts);
	}
	public interface  PostUpdateListener{
		void onUpdated(LEKPost post, boolean status);
	}
	public interface CategoryListener {
		void onReceived(@Nullable List<LEKCategory> categories);
	}
}
