package com.cirp.mfisheries.lek.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Spinner;

import com.cirp.mfisheries.R;
import com.cirp.mfisheries.lek.models.LEKAdapter;
import com.cirp.mfisheries.lek.models.LEKPost;
import com.cirp.mfisheries.util.NetUtil;
import com.cirp.mfisheries.util.PrefsUtil;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;
import com.squareup.moshi.JsonAdapter;
import com.squareup.moshi.Moshi;
import com.squareup.moshi.Types;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

public class LEKPostFragment extends Fragment {

	private static Context mContext;
	private View currentView;
	private List<LEKPost> posts = new ArrayList<>();
	private ListView mListView;
	private LEKAdapter mAdapter;

	public static LEKPostFragment newInstance(Context context) {
		LEKPostFragment fragment = new LEKPostFragment();
		mContext = context;
		return fragment;
	}

	public LEKPostFragment() {
		// Required empty public constructor
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.fragment_lek_posts, container, false);
		currentView = view;
		mListView = currentView.findViewById(R.id.lek_list);
		return view;
	}

	//use this method to display a list of LEKPost after creating the fragment
	//allows fragment data to be easily changed at any time
	public LEKPostFragment update(List<LEKPost> posts) {
		this.posts = posts;
		mListView = currentView.findViewById(R.id.lek_list);
		mAdapter = new LEKAdapter(mContext, posts);
		mListView.setAdapter(mAdapter);
		return this;
	}

	//display lek categories
	public void displayCategories(String[] categories){
		Spinner spinner = currentView.findViewById(R.id.spinner);
		spinner.setVisibility(View.VISIBLE);
		ArrayAdapter<CharSequence> adapter = new ArrayAdapter<CharSequence>(mContext,
				android.R.layout.simple_dropdown_item_1line, categories);

		spinner.setAdapter(adapter);
		spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
			@Override
			public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
				filterBy((String)adapterView.getItemAtPosition(i));
			}

			@Override
			public void onNothingSelected(AdapterView<?> adapterView) {

			}
		});
	}

	private void filterBy(String category) {
		Log.d("LEK", NetUtil.API_URL + NetUtil.GET_LEK_PATH + PrefsUtil.getUserId(mContext) + "/all/" + "0");
		if (NetUtil.isOnline(mContext)) {
			Ion.with(mContext)
					.load(NetUtil.API_URL + NetUtil.GET_LEK_PATH + PrefsUtil.getUserId(mContext) + "/" + category + "/" + "0")
					.setHeader(NetUtil.API_KEY_PARAM, NetUtil.API_KEY)
					.asJsonObject()
					.setCallback(new FutureCallback<JsonObject>() {
						@Override
						public void onCompleted(Exception e, JsonObject result) {
							try {
								JsonArray array = result.getAsJsonArray("data");
								Moshi moshi = new Moshi.Builder().build();
								Type listOfLEKType = Types.newParameterizedType(List.class, LEKPost.class);
								JsonAdapter<List<LEKPost>> jsonAdapter = moshi.adapter(listOfLEKType);
								List<LEKPost> posts = jsonAdapter.fromJson(array.toString());
								update(posts);
							} catch (Exception exc) {
								exc.printStackTrace();
							}
						}
					});
		}
	}

	@Override
	public void onStop() {
		try {
			mAdapter.stopAudio();
		} catch (Exception e) {
			e.printStackTrace();
		}
		super.onStop();
	}
}
