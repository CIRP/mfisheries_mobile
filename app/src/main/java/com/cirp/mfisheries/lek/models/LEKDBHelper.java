package com.cirp.mfisheries.lek.models;

import android.content.ContentValues;
import android.database.Cursor;
import android.util.Log;

import com.cirp.mfisheries.core.DatabaseHandler;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

public class LEKDBHelper {

	private static final String OFFLINE_LEK_TABLE = "LEKPosts";
	private static final String OFFLINE_LEK_ID = "id";
	private static final String OFFLINE_LEK_TEXT = "text";
	private static final String OFFLINE_LEK_LATITUDE = "lat";
	private static final String OFFLINE_LEK_LONGITUDE = "lng";
	private static final String OFFLINE_LEK_TYPE = "filetype";
	private static final String OFFLINE_LEK_DATE_POSTED = "timestamp";
	private static final String OFFLINE_LEK_APPLICABLE_DATE = "aDate";
	private static final String OFFLINE_LEK_MEDIA = "mediaPath";
	private static final String OFFLINE_LEK_USER_ID = "uId";
	private static final String OFFLINE_LEK_USER = "fullname";
	private static final String OFFLINE_LEK_SYNCED = "synced";
	private static final String OFFLINE_LEK_CATEGORY = "category";

	public static String CREATE_OFFLINE_LEK_TABLE =
			"CREATE TABLE " + OFFLINE_LEK_TABLE + " ("
					+ OFFLINE_LEK_ID + " INTEGER PRIMARY KEY autoincrement,"
					+ OFFLINE_LEK_LATITUDE + " TEXT,"
					+ OFFLINE_LEK_LONGITUDE + " TEXT,"
					+ OFFLINE_LEK_TYPE + " TEXT,"
					+ OFFLINE_LEK_DATE_POSTED + " INTEGER,"
					+ OFFLINE_LEK_APPLICABLE_DATE + " TEXT,"
					+ OFFLINE_LEK_MEDIA + " TEXT,"
					+ OFFLINE_LEK_USER_ID + " TEXT,"
					+ OFFLINE_LEK_USER + " TEXT,"
					+ OFFLINE_LEK_SYNCED + " TEXT,"
					+ OFFLINE_LEK_CATEGORY + " TEXT,"
					+ OFFLINE_LEK_TEXT + " TEXT " + ")";
	
	public float addOfflineLEK(LEKPost lekPost) {
		try {
			ContentValues lekValues = new ContentValues();
			lekValues.put(OFFLINE_LEK_LATITUDE, lekPost.latitude);
			lekValues.put(OFFLINE_LEK_LONGITUDE, lekPost.longitude);
			lekValues.put(OFFLINE_LEK_TYPE, lekPost.filetype);

			DateFormat format = new SimpleDateFormat("EEE MMM d HH:mm:ss z yy", Locale.ENGLISH);
			Date date = format.parse(lekPost.timestamp);
			lekValues.put(OFFLINE_LEK_DATE_POSTED, date.getTime());
			lekValues.put(OFFLINE_LEK_APPLICABLE_DATE, lekPost.aDate);
			lekValues.put(OFFLINE_LEK_MEDIA, lekPost.filepath);
			lekValues.put(OFFLINE_LEK_USER_ID, lekPost.userid);
			lekValues.put(OFFLINE_LEK_USER, lekPost.fullname);
			lekValues.put(OFFLINE_LEK_SYNCED, lekPost.synced + "");
			lekValues.put(OFFLINE_LEK_CATEGORY, lekPost.category);
			lekValues.put(OFFLINE_LEK_TEXT, lekPost.text);
			return DatabaseHandler.database.insert(OFFLINE_LEK_TABLE, null, lekValues);
		} catch (Exception ex) {
			Log.wtf("LEKDBHelper", "Add Offline LEK: " + ex.getLocalizedMessage());
		}
		return 0;
	}
	
	public int getOfflineLEKCount() {
		try {
			String offline_tracks_query = "SELECT * FROM " + OFFLINE_LEK_TABLE;
			Cursor cursor = DatabaseHandler.database.rawQuery(offline_tracks_query, null);
			int count = cursor.getCount();
			cursor.close();
			return count;
		} catch (Exception ex) {
			Log.wtf("LEKDBHelper", "Get LEK Count: " + ex.getLocalizedMessage());
		}
		return 0;
	}
	
	public List<LEKPost> getAllLEK() {
		try {
			List<LEKPost> lekPosts = new ArrayList<>();
			String selectQuery = "SELECT  * FROM " + OFFLINE_LEK_TABLE;
			Cursor cursor = DatabaseHandler.database.rawQuery(selectQuery, null);
			if (cursor.moveToFirst()) {
				do {
					LEKPost lekPost = new LEKPost();
					lekPost.id = Integer.parseInt(cursor.getString(0));
					lekPost.latitude = cursor.getString(1);
					lekPost.longitude = cursor.getString(2);
					lekPost.filetype = cursor.getString(3);
					lekPost.timestamp = new Date(cursor.getLong(4)).toString();
					lekPost.aDate = cursor.getString(5);
					lekPost.filepath = cursor.getString(6);
					lekPost.userid = cursor.getString(7);
					lekPost.fullname = cursor.getString(8);
					lekPost.synced = Boolean.getBoolean(cursor.getString(9));
					lekPost.category = cursor.getString(10);
					lekPost.text = cursor.getString(11);

					lekPosts.add(lekPost);
				}
				while (cursor.moveToNext());
			}
			cursor.close();
			return lekPosts;
		} catch (Exception ex) {
			Log.wtf("LEKDBHelper", "Get Offline LEK: " + ex.getLocalizedMessage());
		}
		return null;
	}

	public List<LEKPost> getLEKToSync() {
		try {
			List<LEKPost> lekPosts = new ArrayList<>();
			String selectQuery = "SELECT  * FROM " + OFFLINE_LEK_TABLE + " WHERE " + OFFLINE_LEK_SYNCED + " = " + " 'false'";
			Cursor cursor = DatabaseHandler.database.rawQuery(selectQuery, null);
			if (cursor.moveToFirst()) {
				do {
					LEKPost lekPost = new LEKPost();
					lekPost.id = Integer.parseInt(cursor.getString(0));
					lekPost.latitude = cursor.getString(1);
					lekPost.longitude = cursor.getString(2);
					lekPost.filetype = cursor.getString(3);
					lekPost.timestamp = new Date(cursor.getLong(4)).toString();
					lekPost.aDate = cursor.getString(5);
					lekPost.filepath = cursor.getString(6);
					lekPost.userid = cursor.getString(7);
					lekPost.fullname = cursor.getString(8);
					lekPost.synced = Boolean.getBoolean(cursor.getString(9));
					lekPost.category = cursor.getString(10);
					lekPost.text = cursor.getString(11);
					lekPosts.add(lekPost);
				}
				while (cursor.moveToNext());
			}
			cursor.close();
			return lekPosts;
		} catch (Exception ex) {
			Log.wtf("LEKDBHelper", "Get Offline LEK: " + ex.getLocalizedMessage());
		}
		return new ArrayList<>();
	}

	public List<LEKPost> filterLEK(String category, String type, Date date1, Date date2) {
		try {
			Log.d("LEKDBHelper", "Filtering LEK by (" + type + ", " + category + ", " + date1 + ", " + date2);
			List<LEKPost> lekPosts = new ArrayList<>();
			String selectQuery = "SELECT  * FROM " + OFFLINE_LEK_TABLE + " WHERE " +
					OFFLINE_LEK_DATE_POSTED + " BETWEEN " +  date1.getTime() + " AND " +
					date2.getTime();

			if(!type.equals("all"))
					selectQuery += " AND " + OFFLINE_LEK_TYPE + " LIKE '" + type + "'";
			if(!category.equals("all"))
				selectQuery += " AND " + OFFLINE_LEK_CATEGORY + " = '" + category + "'";

			Log.d("LEKDBHelper", "Query = " + selectQuery);
			Cursor cursor = DatabaseHandler.database.rawQuery(selectQuery, null);
			if (cursor.moveToFirst()) {
				do {
					LEKPost lekPost = new LEKPost();
					lekPost.id = Integer.parseInt(cursor.getString(0));
					lekPost.latitude = cursor.getString(1);
					lekPost.longitude = cursor.getString(2);
					lekPost.filetype = cursor.getString(3);
					lekPost.timestamp = new Date(cursor.getLong(4)).toString();
					lekPost.aDate = cursor.getString(5);
					lekPost.filepath = cursor.getString(6);
					lekPost.userid = cursor.getString(7);
					lekPost.fullname = cursor.getString(8);
					lekPost.synced = Boolean.getBoolean(cursor.getString(9));
					Log.d("LEKDBHelper", "LEK Status = " + lekPost.synced);
					lekPost.category = cursor.getString(10);
					lekPost.text = cursor.getString(11);
					lekPosts.add(lekPost);
				}
				while (cursor.moveToNext());
			}
			cursor.close();
			return lekPosts;
		} catch (Exception ex) {
			Log.wtf("LEKDBHelper", "Filter LEK: " + ex.getLocalizedMessage());
		}
		return null;
	}

	public int updateLEKStatus(int postId) {
		try {
			ContentValues lekValues = new ContentValues();
			lekValues.put(OFFLINE_LEK_SYNCED, true);

			String selection = OFFLINE_LEK_ID + " = ?";
			String[] selectionArgs = { postId + "" };

			return DatabaseHandler.database.update(OFFLINE_LEK_TABLE, lekValues, selection, selectionArgs);
		} catch (Exception ex) {
			Log.wtf("LEKDBHelper", "Add Offline LEK: " + ex.getLocalizedMessage());
		}
		return 0;
	}

	public void deleteAllLEK() {
		try {
			DatabaseHandler.database.delete(OFFLINE_LEK_TABLE, null, null);
		} catch (Exception ex) {
			Log.wtf("LEKDBHelper", "Delete all LEK: " + ex.getLocalizedMessage());
		}
	}
}
