package com.cirp.mfisheries;

import android.app.Application;
import android.app.ActivityManager;
import android.arch.persistence.room.Room;
import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Build;
import android.support.multidex.MultiDexApplication;
import android.util.Log;

import com.cirp.mfisheries.core.AppDatabase;
import com.cirp.mfisheries.core.location.LocationService;
import com.cirp.mfisheries.fewer.FewerActivity;
import com.cirp.mfisheries.nav.NavigationDashedActivity;
import com.cirp.mfisheries.sos.SOSLauncherActivity;
import com.cirp.mfisheries.util.CrashReporter;
import com.cirp.mfisheries.util.FileUtil;
import com.cirp.mfisheries.util.PrefsUtil;
//import com.crashlytics.android.Crashlytics;
import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.analytics.Tracker;
import com.google.firebase.FirebaseApp;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.perf.FirebasePerformance;

import java.io.File;

//import io.fabric.sdk.android.Fabric;

public class App extends MultiDexApplication {
	private static final String TAG = "APP";
	private static final String DATABASE_NAME = "MFISHERIES_DB";
	private static AppDatabase db;
	private static FirebaseDatabase fbdb;
	private Tracker mTracker;


	public static FirebaseDatabase getFBDatabase() {
		return fbdb;
	}
	
	public static AppDatabase getDatabase(Context context) {
		if (db == null)
			db = Room.databaseBuilder(context, AppDatabase.class, DATABASE_NAME).allowMainThreadQueries().build();
		return db;
	}
	
	public static Class getHomeClass() {
		Class start = MainActivity.class;
		//noinspection ConstantConditions
		if (BuildConfig.FLAVOR.equalsIgnoreCase("fewer")) {start = FewerActivity.class;}
		else if (BuildConfig.FLAVOR.equalsIgnoreCase("navigation")){start = NavigationDashedActivity.class; }
		else if (BuildConfig.FLAVOR.equalsIgnoreCase("sos")){start = SOSLauncherActivity.class; }
		return start;
	}
	
	// Called when the application is starting, before any other application objects have been created.
	// Overriding this method is optional!
	@Override
	public void onCreate() {
		super.onCreate();
		initializeFirebase(); // Initialize Firebase when application starts before activity shown
		startServices();
		applicationChecks();
	}
	
	@Override
	public void onTerminate(){
		stopServices();
		onExitLauncherCheck();
		super.onTerminate();
	}
	
	private void startServices() {
		LocationService.startLocationServices(this, true);
	}
	
	private void stopServices(){
		LocationService.stopLocationServices(this);
	}
	
	public void initializeFirebase(){
		// Check if Firebase app exists. If not, then initialised. Multiple calls to initialize will cause app to crash
		if (FirebaseApp.getApps(this).isEmpty()) FirebaseApp.initializeApp(this);
		// Initialise Firebase Database
		fbdb = FirebaseDatabase.getInstance();
		fbdb.setPersistenceEnabled(true);
		// Configure FB Plugins
		CrashReporter.getInstance(this);
		FirebasePerformance.getInstance().setPerformanceCollectionEnabled(!BuildConfig.DEBUG);
	}
	
	public void applicationChecks() {
		try {
			PackageInfo packageInfo = getPackageManager().getPackageInfo(getPackageName(), 0);
			final int versionCode = packageInfo.versionCode;
			// Significant change in storage requires a restart of preferences
			if (versionCode < 69 && !PrefsUtil.isReset(this)) {
				PrefsUtil.clearPreferences(getBaseContext());
				PrefsUtil.setIsReset(this, true);
			}
			if(PrefsUtil.getLastVersion(this) < 81){ //checks if user was logged in before version 83and forces them to log out
				PrefsUtil.clearPreferences(getBaseContext());
				PrefsUtil.setIsReset(this, true);
				PrefsUtil.setLastVersion(this, versionCode);
			}
			deleteZips();
		} catch (PackageManager.NameNotFoundException e) {
			e.printStackTrace();
		}
	}

	private void deleteZips(){
		//Delete incorrectly saved FirstAid.zip file
		String filePath = FileUtil.getFileUri("/FirstAid.zip").toString();
		File firstAid = new File(filePath);
		if(firstAid.exists() || FileUtil.directoryExists(filePath)){
			boolean deleted = firstAid.delete();
		}

		//Delete incorrectly saved Podcast.zip file
		filePath = FileUtil.getFileUri("/Podcast.zip").toString();
		File podcasts = new File(filePath);
		if(podcasts.exists() || FileUtil.directoryExists(filePath)){
			boolean deleted = podcasts.delete();
		}

		//Delete incorrectly extracted FirstAid Folder
		filePath = FileUtil.getFileUri("/FirstAid/FirstAid").toString();
		File firstaidFolder = new File(filePath);
		if(firstaidFolder.exists() || FileUtil.directoryExists(filePath)){
			filePath = FileUtil.getFileUri("/FirstAid").toString();
			firstaidFolder = new File(filePath);
			boolean deleted = firstaidFolder.delete();
		}
	}

	private void clearAppData(int versionCode){ //Will clear ALL preferences and close the app and thus not update the last version code.
		try{
			if (Build.VERSION_CODES.KITKAT <= Build.VERSION.SDK_INT) {
				((ActivityManager)getSystemService(ACTIVITY_SERVICE)).clearApplicationUserData();
				PrefsUtil.setLastVersion(this, versionCode);
			} else {
				String packagename = getApplicationContext().getPackageName();
				Runtime runtime = Runtime.getRuntime();
				runtime.exec("pm clear "+packagename);
				PrefsUtil.setLastVersion(this, versionCode);
			}
		} catch (Exception e){
			e.printStackTrace();
		}
	}
	
	synchronized public Tracker getDefaultTracker() {
		if (!BuildConfig.DEBUG) {
			Log.d(TAG, "Tracking is enabled");
			if (mTracker == null) {
				GoogleAnalytics analytics = GoogleAnalytics.getInstance(this);
				mTracker = analytics.newTracker(R.xml.global_tracker);
				mTracker.enableAutoActivityTracking(true);
			}
			return mTracker;
		} else Log.d(TAG, "Tracking is not enabled");
		return null;
	}

	/**
	 * Will double check What modules are enabled to update the Manifest for removing launchers
	 */
	public void onExitLauncherCheck(){

	}
}
