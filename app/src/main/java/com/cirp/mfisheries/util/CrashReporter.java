package com.cirp.mfisheries.util;


import android.content.Context;
import android.util.Log;

import com.cirp.mfisheries.BuildConfig;
import com.crashlytics.android.Crashlytics;
import com.crashlytics.android.core.CrashlyticsCore;

import io.fabric.sdk.android.Fabric;


/**
 * Putting Firebase Crash reporting in a wrapper class to make transition to Fabric easier
 */
public class CrashReporter {
	
	private static CrashReporter instance;
	
	private CrashReporter(Context context) {
		// You also need to disable the Crashlytics kit at runtime for debug builds by changing the way you initialize support for Fabric in your ap
		Crashlytics crashlyticsKit = new Crashlytics.Builder()
				.core(new CrashlyticsCore.Builder().disabled(BuildConfig.DEBUG).build())
				.build();
		Fabric.with(context, crashlyticsKit);
		// Debugging to help developers to check the status of debugging
		Log.d("CrashReporter", "Build is: "+ BuildConfig.DEBUG);
		if (!BuildConfig.DEBUG) Log.d("CrashReporter", "Production is enabled");

	}
	
	public CrashReporter log (int level, String tag, String message){
		Crashlytics.log(level, tag, message);
		return this;
	}
	
	public CrashReporter log (String message){
		Crashlytics.log(message);
		return this;
	}
	
	public CrashReporter report(Throwable throwable){
		Crashlytics.logException(throwable);
		return this;
	}
	
	public static CrashReporter getInstance(Context context) {
		if (instance == null)instance = new CrashReporter(context);
		return instance;
	}
}
