package com.cirp.mfisheries.util;


import android.content.Context;

import com.cirp.mfisheries.core.module.BaseModel;

import java.util.List;

@SuppressWarnings("WeakerAccess")
public class BaseUtils {
	protected final Context context;
	protected final String noInternetMsg;
	protected final String errorMsg;
	
	protected List<BaseModel> records;
	
	public BaseUtils(Context context) {
		this.context = context;
		errorMsg = "Unable to retrieve data.";
		noInternetMsg = " Phone not connected. Using Cached values.";
	}
}
