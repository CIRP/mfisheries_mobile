package com.cirp.mfisheries.util;

import android.util.Log;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

public class Zipper {
	
	private static final String TAG = "Zipper";
	private String zip_file;
	private String location;
	private OnZipFinishedListener listener;
	private boolean deleteAfterUnzip = true;

	public Zipper(String zipFile, String loc) {
		zip_file = zipFile;
		location = loc;

		if (makeDirectory(""))Log.d(TAG, "Parent directory created successfully for extraction");
	}

	public void unzip() {
		try {
			FileInputStream inputStream = new FileInputStream(zip_file);
			ZipInputStream zipStream = new ZipInputStream(inputStream);
			ZipEntry zEntry;
			while ((zEntry = zipStream.getNextEntry()) != null) {
				Log.d(TAG+"-Unzip", "Unzipping " + zEntry.getName());
				if (zEntry.isDirectory()) {
					makeDirectory(zEntry.getName());
				} else {
					FileOutputStream fout = new FileOutputStream(location + zEntry.getName());
					BufferedOutputStream bufout = new BufferedOutputStream(fout);
					byte[] buffer = new byte[1024];
					int read;
					while ((read = zipStream.read(buffer)) != -1) {
						bufout.write(buffer, 0, read);
					}

					zipStream.closeEntry();
					bufout.close();
					fout.close();
				}
			}
			zipStream.close();
			if (deleteAfterUnzip) {
				deleteFile();
			}
			listener.onFinished(true);
		} catch (Exception e) {
			Log.d("Unzip", "Unzipping failed");
			e.printStackTrace();
			listener.onFinished(false);
		}

	}

	private boolean makeDirectory(String dir) {
		File f = new File(location + dir);

		if (!f.isDirectory()) {
			return f.mkdirs();
		}
		
		return f.exists();
	}

	private boolean deleteFile() {
		File file = new File(zip_file);
		return file.delete();
	}

	public void setDeleteAfterUnzip(boolean deleteAfterUnzip) {
		this.deleteAfterUnzip = deleteAfterUnzip;
	}

	public void setOnZipFinishedListener(OnZipFinishedListener listener) {
		this.listener = listener;
	}

	public interface OnZipFinishedListener {

		void onFinished(boolean status);

	}
}
