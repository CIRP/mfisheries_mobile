package com.cirp.mfisheries.util;

import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.os.StatFs;
import android.provider.DocumentsContract;
import android.provider.MediaStore;
import android.support.annotation.RequiresApi;
import android.support.v4.content.CursorLoader;
import android.util.Log;

import com.squareup.moshi.JsonAdapter;
import com.squareup.moshi.Moshi;
import com.squareup.moshi.Types;

import org.json.JSONArray;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@SuppressWarnings({"WeakerAccess", "SameParameterValue", "TryFinallyCanBeTryWithResources", "UnusedReturnValue", "unused"})
public class FileUtil {
	
	private static final String TAG = "FileUtil";
	public static String FOLDER = "/mFisheries/";
	private static String MODULE_FILE_NAME = "modules.json";
	private static final String IMAGE_DIRECTORY_NAME = "mFisheriesImages";


	public static File getmFisheriesFolder(){
		final String folderStr = Environment.getExternalStorageDirectory() + FOLDER;
		Log.d(TAG, String.format("Attempting to retrieve folder at location: %s", folderStr));
		File dir = new File(folderStr);
		if (!dir.exists()) {
			if(dir.mkdirs())Log.d(TAG, "Path successfully created");
			else return null;
		}
		return  dir;
	}
	
	//reads a file containing json and returns it string
	public static String readJSONFile(String filePath) {
		//Read text from file
		StringBuilder text = new StringBuilder();
		if(!Environment.MEDIA_MOUNTED.equals(Environment.getExternalStorageState())){
			Log.d(TAG,"External SD card not mounted");
		}

		try {
			File sdcard = Environment.getExternalStorageDirectory();
			File dir = new File(sdcard + FOLDER);
			if (!dir.exists()) {
				if(dir.mkdirs())Log.d(TAG, "Path successfully created");
				else Log.d(TAG, "Unable to create path");
			}
			//Get the text file
			File file = new File(sdcard, FOLDER + filePath);
			Log.d(TAG, String.format("File Path is: %s and currently exists: %s", file.getAbsolutePath(), file.exists()));
			if (!file.exists()){
				if (file.createNewFile()) Log.d(TAG, "File was created successfully");
				else Log.d(TAG, "File was not Created");
			}
			
			BufferedReader br = new BufferedReader(new FileReader(file));
			String line;

			while ((line = br.readLine()) != null) {
				text.append(line);
				text.append('\n');
			}
			br.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return text.toString();
	}

	//writes a string of json to a file
	public static void writeJSONFile(String json, String location, String fileName) throws Exception {
		File sdcard = Environment.getExternalStorageDirectory();

		File dir = new File(sdcard + FOLDER + location);
		if (!dir.exists()) {
			if(dir.mkdirs())Log.d(TAG, "Path successfully created");
			else Log.d(TAG, "Unable to create path");
		}
		File file = new File(sdcard, FOLDER + location + fileName);
		FileOutputStream stream = new FileOutputStream(file);
		try {
			stream.write(json.getBytes());
		} finally {
			stream.close();
		}

	}

	//loads a json array and returns it as a List
	public static List <String> loadJSONArray(String json) {
		if (json != null && json.length() > 2) {
			Moshi moshi = new Moshi.Builder().build();
			Type listMyData = Types.newParameterizedType(List.class, String.class);
			JsonAdapter<List<String>> jsonAdapter = moshi.adapter(listMyData);
			Log.d(TAG, String.format("JSON was found to be: %s", json));
			try {
				return jsonAdapter.fromJson(json);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}else { Log.d(TAG, "JSON was empty"); }
		return new ArrayList<>();
	}

	//loads the selected modules from the cache
	public static List<String> loadSavedModules() {
		return loadJSONArray(readJSONFile(MODULE_FILE_NAME));
	}

	//saves the selected modules to the cache
	public static boolean saveModules(List<String> modules) {
		JSONArray mJSONArray = new JSONArray(modules);
		try {
			FileUtil.writeJSONFile(mJSONArray.toString(), "", FileUtil.MODULE_FILE_NAME);
			return true;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return false;
	}

	public static Uri getFileUri(String extra) {
		return Uri.parse(Environment.getExternalStorageDirectory().getPath() + FileUtil.FOLDER + extra);
	}

	public static Uri getAudioUri(String extra) {
		return Uri.parse("file://" + Environment.getExternalStorageDirectory().getPath() + FileUtil.FOLDER + extra);
	}

	public static String getAudioUriString(String extra) {
		return Environment.getExternalStorageDirectory().getPath() + FileUtil.FOLDER + extra;
	}

	public static Uri getOutputMediaFileUri(File mediaFile) {
		return Uri.fromFile(mediaFile);
	}

	public static boolean directoryExists(String dir) {
		File f = new File(dir);
		return f.isDirectory();
	}

	public static boolean fileExists(String dir) {
		Log.v(TAG, "Dir: " + dir);
		File f = new File(dir);
		return f.isFile();
	}

	public static String getAbsolutePath(Context context, Uri uri, String type) {
		try{
			Log.d(TAG, uri.toString() + " - " + uri.getPath());
			Cursor cursor = context.getContentResolver().query(uri, null, null, null, null);
			if (cursor != null && cursor.moveToFirst()) {
				String document_id = cursor.getString(0);
				document_id = document_id.substring(document_id.lastIndexOf(":") + 1);
				cursor.close();

				String sel = getSelector(type);
				Uri media = getMedia(type);

				cursor = context.getContentResolver().query(media, null, sel, new String[]{document_id}, null);
				if (cursor != null && cursor.moveToFirst()) {
					String path = cursor.getString(cursor.getColumnIndex(MediaStore.Images.Media.DATA));
					cursor.close();
					return path;
				}
			}
			throw new Exception("Unable to find absolute path in general method");
		}
		catch(Exception ex) {
			ex.printStackTrace();
			try {
				if (Build.VERSION.SDK_INT >= 19)
					return getAbsolutePathFromURI_API19(context, uri, type);
				return getAbsolutePathFromURI_API11to18(context, uri);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		if (uri!= null)
			return uri.getPath();
		else
			return "";
	}

	private static Uri getMedia(String type){
		Uri media = MediaStore.Images.Media.EXTERNAL_CONTENT_URI;
		switch (type) {
			case "image":
				return MediaStore.Images.Media.EXTERNAL_CONTENT_URI;
			case "audio":
				return MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;
			case "video":
				return MediaStore.Video.Media.EXTERNAL_CONTENT_URI;
		}
		return media;
	}

	private static String getSelector(String type){
		switch (type) {
			case "image":
				return MediaStore.Images.Media._ID + "=?";
			case "audio":
				return MediaStore.Audio.Media._ID + "=?";
			case "video":
				return MediaStore.Video.Media._ID + "=?";
		}
		return MediaStore.Images.Media._ID + "=?";
	}

	@RequiresApi(api = Build.VERSION_CODES.KITKAT)
	public static String getAbsolutePathFromURI_API19(final Context context, Uri uri, String type) {
		String filePath = "";
		final String wholeID = DocumentsContract.getDocumentId(uri);
		// Split at colon, use second item in the array
		final String id = wholeID.split(":")[1];
		// Columns
		final String[] column = {MediaStore.Images.Media.DATA};

		// where id is equal to
		String sel = getSelector(type);
		Uri media = getMedia(type);

		final Cursor cursor = context.getContentResolver().query(media, column, sel, new String[]{id}, null);

		if (cursor != null) {
			final int columnIndex = cursor.getColumnIndex(column[0]);
			if (cursor.moveToFirst()) {
				filePath = cursor.getString(columnIndex);
			}
			cursor.close();
		}
		return filePath;
	}

	public static String getAbsolutePathFromURI_API11to18(Context context, Uri contentUri) {
		String[] proj = {MediaStore.Images.Media.DATA, MediaStore.Audio.Media.DATA, MediaStore.Video.Media.DATA};
		String result = null;

		CursorLoader cursorLoader = new CursorLoader(context, contentUri, proj, null, null, null);
		Cursor cursor = cursorLoader.loadInBackground();

		if (cursor != null) {
			int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
			cursor.moveToFirst();
			result = cursor.getString(column_index);
		}
		return result;
	}

	public static String getFileType(String type, String path){
		String filetype= type + "/" + path.substring(path.indexOf(".") + 1, path.length());
		Log.d(TAG, filetype);
		return filetype;
	}

	public static String getFileNameFromPath(String path) {
		String[] pieces = path.split("/");
		return pieces[pieces.length - 1];
	}

	public static File getOutputMediaFile(Context context, String fileName) {
//		File mediaStorageDir = new File(Environment.getExternalStorageDirectory(), "mFisheriesImages");
//		File mediaStorageDir = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES), FileUtil.IMAGE_DIRECTORY_NAME);
		File mediaStorageDir = new File(context.getExternalFilesDir(Environment.DIRECTORY_PICTURES), FileUtil.IMAGE_DIRECTORY_NAME);
		if (!mediaStorageDir.exists()){
			if (!mediaStorageDir.mkdirs()){
				return null;
			}
		}
		
		if(fileName == null || fileName.equals("")) fileName = generateImageName(context);
		return new File(mediaStorageDir, fileName);
	}

	public static String generateImageName(Context context){
		long timeStamp = new Date().getTime();
		return "USR_" + PrefsUtil.getUserId(context) + "_IMG_" + timeStamp + ".jpg";
	}

	public static String generateVideoName(Context context){
		long timeStamp = new Date().getTime();
		return "USR_" + PrefsUtil.getUserId(context) + "_VID_" + timeStamp + ".mp4";
	}

	public static float megabytesAvailable() {
		File file = Environment.getExternalStorageDirectory();
		if (Build.VERSION.SDK_INT >= 19) {
			StatFs stat = new StatFs(file.getPath());
			long bytesAvailable = stat.getBlockSizeLong() * stat.getAvailableBlocksLong();
			Log.d(TAG, "Space: " + bytesAvailable);
			return bytesAvailable / (1024.f * 1024.f);
		} else {
			StatFs stat = new StatFs(file.getPath());
			long bytesAvailable = (long) stat.getBlockSize() * (long) stat.getAvailableBlocks();
			Log.d(TAG, "Space: " + bytesAvailable);
			return bytesAvailable / (1024.f * 1024.f);
		}
	}

	public static byte[] getByteArray(Context context, Uri uri){
		try {
			InputStream iStream = context.getContentResolver().openInputStream(uri);
			return getBytes(iStream);
		}
		catch (Exception e){
			Log.d(TAG, "Error: " + e.getMessage());
		}
		return null;
	}

	public static byte[] getBytes(InputStream inputStream) throws IOException {
		final ByteArrayOutputStream byteBuffer = new ByteArrayOutputStream();
		final int bufferSize = 1024;
		final byte[] buffer = new byte[bufferSize];

		int len;
		while ((len = inputStream.read(buffer)) != -1) {
			byteBuffer.write(buffer, 0, len);
		}
		return byteBuffer.toByteArray();
	}

}
