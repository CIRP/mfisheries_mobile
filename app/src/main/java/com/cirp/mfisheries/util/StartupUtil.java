package com.cirp.mfisheries.util;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.cirp.mfisheries.MainActivity;
import com.cirp.mfisheries.R;
import com.cirp.mfisheries.core.Country;
import com.cirp.mfisheries.core.CountryAdapter;
import com.cirp.mfisheries.core.NetworkChangeReceiver;
import com.cirp.mfisheries.core.config.ConfigUtil;
import com.cirp.mfisheries.core.download.InstallModuleService;
import com.cirp.mfisheries.core.location.LocationService;
import com.cirp.mfisheries.core.register.RegisterActivity;
import com.cirp.mfisheries.core.register.RegisterHelper;
import com.cirp.mfisheries.core.register.WebViewActivity;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.annotations.NonNull;

import static okhttp3.internal.Internal.instance;

@SuppressWarnings("ALL")
public class StartupUtil {

    private Context context;
    private String country;
    private String countryId;
    private static final String TAG = "StartUpUtil";
    private boolean SIGNED_IN;
    private boolean isInitiated;
    private SharedPreferences preferences;

    private CountryUtil countryUtil;
    public BroadcastReceiver registerBroadcastHandler;
    public BroadcastReceiver networkChangeReceiver;
    public BroadcastReceiver installReceiver;
    private static StartupUtil instance;



    private StartupUtil(@NonNull Context context){
        this.context = context;
        init();
    }
    public static StartupUtil getInstance(@NonNull Context context){
        if (instance == null)instance = new StartupUtil(context);
        else instance.context = context;
        return instance;
    }

    private void init() {
        preferences = PreferenceManager.getDefaultSharedPreferences(context);
        countryUtil = CountryUtil.getInstance(context);
        isInitiated = false;
    }

    private void startLocationServices() {
        LocationService.startLocationServices(context.getApplicationContext());
    }

    public void checkVersion(){
        (new ConfigUtil(context)).checkVersion();
    }

    /**
     * Retrieve the listing of available countries within mFisheries
     */
    public void loadCountries(final CountrySelectionHandler handler) {
        Log.d(TAG, "Started load countries");
        if (!NetUtil.isOnline(context)) {
            NetUtil.retryDialog(context, new NetUtil.OnRetryClicked() {
                public void retry() {
                    loadCountries(handler);
                }
            });
            return;
        }
        final ProgressDialog progress = new ProgressDialog(context);
        progress.setMessage(context.getString(R.string.loading_countries));
        progress.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progress.setIndeterminate(true);
        progress.setCancelable(true);
        progress.show();
        countryUtil.retrieveCountries(new CountryUtil.CountryRetrievedListener() {
            @Override
            public void processCountries(List<Country> countries) {
                Log.d(TAG, "Started load countries");
                if (countries != null) {
                    selectCountry(countries, handler);
                } else {
                    Toast.makeText(context, "Unable to retrieve countries", Toast.LENGTH_SHORT).show();
                    NetUtil.retryDialog(context, new NetUtil.OnRetryClicked() {
                        @Override
                        public void retry() {
                            loadCountries(handler);
                        }
                    });
                }
                progress.dismiss();
            }
        });
    }

    public void requestCountry(final CountrySelectionHandler handler){
        loadCountries(handler);
    }

    public interface CountrySelectionHandler{
        public void handleCountrySelection(Country selectedCountry);
    }

    public boolean isFirstLaunch(){
        return PrefsUtil.isFirstOpen(context) || PrefsUtil.getCountry(context).equals("") || PrefsUtil.getCountryId(context) == null || PrefsUtil.getCountryId(context).equals("-1");
    }

    public void selectCountry(final List<Country> countries, final CountrySelectionHandler handler) {
        //show country dialog
        for (Country c : countries) Log.d(TAG, c.toString());
        final CountryAdapter arrayAdapter = new CountryAdapter(context, countries);
        new AlertDialog
                .Builder(context)
                .setTitle(context.getResources().getString(R.string.dialog_country))
                .setCancelable(true)
                .setAdapter(arrayAdapter,
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                Country selectedCountry = countries.get(which);
                                country = selectedCountry.getName();
                                countryId = selectedCountry.getId();
                                Log.d(TAG, String.format("Country: %s", selectedCountry.getName()));
                                countryUtil.saveCountryData(selectedCountry);
                                handler.handleCountrySelection(selectedCountry);
                            }
                        })
                .show();
    }

    /**
     * //Formerly loadData function from main activity
     */
    public void checkRegistration(){
        // Check if Application Running for the First Time
        if ( isFirstLaunch()) {
            Log.d(TAG, "The mFisheries App was opened for the first time");
            Log.d(TAG, "Starting location services");
            startLocationServices();

        }else{
            Log.d(TAG, "The mFisheries App was opened previously");
            country = PrefsUtil.getCountry(context);
            countryId = PrefsUtil.getCountryId(context);
            // Get if user previously registered
            final boolean isRegistered = PrefsUtil.getIsRegistered(context);

            if (PrefsUtil.hasUserId(context) && PrefsUtil.getUserId(context) != -999) {
                Log.d(TAG, "User ID was set");
                SIGNED_IN = true;
                Log.d(TAG, "Starting location services");
                startLocationServices();
                if (!isRegistered) { // has id but profile not completed
                    // Relaunch web view activity
                    Log.d(TAG, "Launching web view activity");
//                    Intent signIn = new Intent(MainActivity.this, RegisterActivity.class);
//                    context.startActivityForResult(signIn, RegisterActivity.SIGN_IN_REQUEST);
                } else {
                    Log.d(TAG, "User id set but the system is not marked as registered");
                }
            }
            else {
                Log.d(TAG, "User ID was not set");
            }
        }
    }


    public void initiateListeners(){
        // Will Launch the Register Activity when authentication is successful
        registerBroadcastHandler = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
//				Intent webViewIntent = new Intent(MainActivity.this, RegisterActivity.class);
//				MainActivity.this.startActivity(webViewIntent);
                Log.d(TAG, "Received Request for Registration");
            }
        };

        networkChangeReceiver = new NetworkChangeReceiver();
        registerListeners();

    }

    public void signIn() {
        Log.d(TAG, "Signing In Process Request");
        Intent signIn = new Intent(context, RegisterActivity.class);
		context.startActivity(signIn);
    }


    /**
     * Used to assign the existing listeners
     */
    public void registerListeners() {
        // If the receiver is null (such as it cleared by memory) then re-instantiate
        if (installReceiver == null)initiateListeners();

        Log.d(TAG, "Registering Receivers");
        // Currently the only listener is to trigger the registration process after sign in is completed
        if (registerBroadcastHandler != null)context.registerReceiver(registerBroadcastHandler, new IntentFilter(RegisterActivity.BROADCAST_ID));
        if (networkChangeReceiver != null)context.registerReceiver(networkChangeReceiver, new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION));
    }

    public void unregisterListeners() {
        Log.d(TAG, "Unregister Receivers");
        if (registerBroadcastHandler != null) context.unregisterReceiver(registerBroadcastHandler);
        if (networkChangeReceiver != null) context.unregisterReceiver(networkChangeReceiver);
    }

}
