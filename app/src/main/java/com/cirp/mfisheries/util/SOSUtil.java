package com.cirp.mfisheries.util;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.net.Uri;
import android.support.v4.app.ActivityCompat;
import android.util.Log;

import java.util.Locale;

public class SOSUtil {

    private Context mContext;
    private static final String TAG = "SOSUtil";
    Location current_location = new Location("Test");
    
    public SOSUtil(Context context, double lat, double lon){
        mContext = context;
        current_location.setLatitude(lat);
        current_location.setLongitude(lon);
    }
    
    public String getSOSPrimary(){
        return PrefsUtil.getSOSMobile(mContext.getApplicationContext());
    }
    
    public String getSOSExtra(){
        return PrefsUtil.getSOSExtraPhone(mContext.getApplicationContext());
    }
    
    public String getSOSLandPrimary(){
        return PrefsUtil.getSOSLand(mContext.getApplicationContext());
    }
    
    public String generateSOSSMSMessage(){
        return PrefsUtil.getUser(mContext.getApplicationContext()) + " " +
                LocationUtil.convertLatitude(current_location.getLatitude()) + " " +
                LocationUtil.convertLongitude(current_location.getLongitude()) + " " +
                "Dir:" + String.format(Locale.US, "%.1f", current_location.getBearing()) + (char) 0x00B0 + " " +
                "Speed:" + String.format(Locale.US, "%.1f", current_location.getSpeed()) + "m/s";
    }

    public void performSmsCall() {
        String mobileNum = getSOSPrimary();
        String extraNum = getSOSExtra();
        Log.d("SOS:", "mobile: " + mobileNum + " extra:" + extraNum);

        //TODO Add the date to the SMS
        String message = generateSOSSMSMessage();

        if (!mobileNum.equals("")) {
//            SMSUtil.sendSMS(mContext.getApplicationContext(), message, mobileNum);
            SMSUtil.composeSMSMessage(mContext.getApplicationContext(), message, mobileNum);
        } else {
            Log.d("SOSService", "No number was specified");
        }

        if (!extraNum.equals("")) {
//            SMSUtil.sendSMS(mContext.getApplicationContext(), message, extraNum);
            SMSUtil.composeSMSMessage(mContext.getApplicationContext(), message, extraNum);
        } else {
            Log.d("SOSService", "No Extra Number specified");
        }

    }

    public void performVoiceCall() {
        if (ActivityCompat.checkSelfPermission(mContext.getApplicationContext(), Manifest.permission.CALL_PHONE) == PackageManager.PERMISSION_GRANTED) {
            String phone_number = getSOSLandPrimary();
//            String phone_number = "7545226"; //Hardcoded value for test. DO NOT USE in production
            Log.d("Phone", phone_number);
            Log.d("SOS voice", "Calling:  " + phone_number);
            Intent intent = new Intent(Intent.ACTION_CALL);
            intent.setData(Uri.parse("tel:" + phone_number));
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            intent.addFlags(Intent.FLAG_FROM_BACKGROUND);
            mContext.startActivity(intent);
        }
    }

}
