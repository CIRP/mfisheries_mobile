package com.cirp.mfisheries.util;

import android.content.Context;
import android.content.DialogInterface;
import android.net.ConnectivityManager;
import android.support.v7.app.AlertDialog;
import android.util.Log;

import com.cirp.mfisheries.BuildConfig;
import com.cirp.mfisheries.R;

import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.util.Calendar;
import java.util.Date;

@SuppressWarnings({"WeakerAccess", "unused"})
public class NetUtil {
//	production urls
//	public static String SITE_URL = "https://mfisheries.cirp.org.tt/";
//	development urls
//	public static final String SITE_URL = "http://mfisheries.herokuapp.com/";       // remote dev
//	public static final String SITE_URL = "http://10.0.2.2:8080/";                  // local for emulator
//	public static final String SITE_URL = "http://192.168.1.2:8080/";             // local dev
//	public static final String SITE_URL = "https://dev.mfisheries.cirp.org.tt/";    // remote dev
//    public static final String SITE_URL = "https://ewer.fish/";	// production
//  public static final String SITE_URL = "http://test.mfisheries.cirp.org.tt/";   // testing
//	public static final String SITE_URL = "http://test.mfisheries.cirp.org.tt/";   // testing
//	public static final String SITE_URL = "https://ewer.fish/";	//EWER.fish deployment
//	public static final String SITE_URL = "http://192.168.0.12:8080/";   // testing for Kwasi's machine via r1hd hotspot

	public static final String SITE_URL = BuildConfig.SITE_URL; // NB (Mikkel), the build config works as expected. Change the build variants to see the value change


	public static final String API_URL = SITE_URL + "api/";
	public static final String GOOGLE_AUTH = "oauth2:https://www.googleapis.com/auth/userinfo.profile";
	public static final String GET_COUNTRIES_PATH = "countries?auths=true";
	public static final String GET_MODULES_PATH = "country/modules";
	public static final String GET_USER_INFO = "#/user/info/";
	public static final String ADD_GOOGLE_USER = "add/google/user";
	public static final String POST_TRACK_URL = "add/user/tracks";
	public static final String POST_IMAGE_URL = "add/user/image";
	public static final String ADD_LEK_PATH = "lek";
	public static final String GET_LEK_PATH = "lek";

	//Alerts
	public static final String SUBSCRIBE = "group/subscribe";
	public static final String ALERTS = "alerts";
	public static final String API_KEY = "S2cI9EHrJtXQdSAx2FetiXLdA626KtFZ";
	public static final String API_KEY_PARAM = "apikey";
	public static final String MESSAGING_IMAGE_PATH = SITE_URL + "static/messaging/";

	//Add number
	public static final String ADD_ALERT_NUMBER = "addalertnumber";
	public static final String REMOVE_ALERT_NUMBER = "removealertnumber";
	public static final String TEST_BROADCAST = "testbroadcast";

    public static String USER_GROUPS(int id) {
		return "user/" + id + "/groups";
	}

	public static String GET_LEK(int id, String type, String category){
		return API_URL + "lek/" + id + "/" + type + "/" + category;
	}

	public static String GET_LEK_DATE(int id, String type, String category, int day, int month, int year){
		return API_URL + "lek/" + id + "/" + type + "/" + category + "/" + day + "/" + (month + 1) + "/" + year;
	}

	public static String GET_LEK_CATEGORIES(int userid){
		return API_URL + "lek/categories/" + userid;
	}

	public static String GET_LEK_DATE_RANGE(int id, String type, String category, Date oldDate, Date laterDate){
		Calendar cal1 = Calendar.getInstance();
		cal1.setTime(oldDate);

		Calendar cal2 = Calendar.getInstance();
		cal2.setTime(laterDate);

		return API_URL + "lek/" + id + "/" + type + "/" + category + "/default/" + cal1.get(Calendar.DAY_OF_MONTH) + "/"
				+ (cal1.get(Calendar.MONTH) + 1) + "/" + cal1.get(Calendar.YEAR) + "/" + cal2.get(Calendar.DAY_OF_MONTH) + "/"
				+ (cal2.get(Calendar.MONTH) + 1) + "/" + cal2.get(Calendar.YEAR);
	}

	public static boolean isOnline(Context context) {
		try {
			ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
			if (cm != null && cm.getActiveNetworkInfo() != null) {
				return cm.getActiveNetworkInfo().isConnectedOrConnecting();
			}
		} catch (Exception e) { e.printStackTrace(); }
		return false;
	}
	
	public static boolean isData(Context context){
		try {
			ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
			if (cm != null && cm.getActiveNetworkInfo() != null) {
				return cm.getActiveNetworkInfo().getType() == ConnectivityManager.TYPE_MOBILE;
			}
		} catch (Exception e) { e.printStackTrace(); }
		return false;
	}
	
	public static boolean isWiFi(Context context){
		try {
			ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
			if (cm != null && cm.getActiveNetworkInfo() != null) {
				return cm.getActiveNetworkInfo().getType() == ConnectivityManager.TYPE_WIFI;
			}
		} catch (Exception e) { e.printStackTrace(); }
		return false;
	}

	public static int getFileSize(String url) {
		try {
			URL url1 = new URL(url);
			URLConnection urlConnection = url1.openConnection();
			urlConnection.connect();
			int file_size = urlConnection.getContentLength();
			Log.d("FileSize", "Size: " + file_size);
			return file_size / 1048576;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return -1;
	}

	public static void retryDialog(Context context, final OnRetryClicked onRetryClicked) {
		AlertDialog.Builder builder = new AlertDialog.Builder(context);
		builder
				.setTitle(context.getResources().getString(R.string.no_conn_dialog))
				.setCancelable(false)
				.setPositiveButton("Retry", new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						onRetryClicked.retry();
					}
				})
				.setNegativeButton("Close", new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialogInterface, int i) {
						dialogInterface.dismiss();
					}
				})
				.show();
	}

	public static boolean exists(String url) {
		return exists(url, "GET");
	}

	public static boolean exists(String url, String requestMethod) {
		try {
			Log.d("Exists", "URL: "+url);
			URL u = new URL(url);
			HttpURLConnection huc = (HttpURLConnection) u.openConnection();
			huc.setRequestMethod(requestMethod);
			huc.connect();
			int code = huc.getResponseCode();
			Log.d("Exists", "Code: " + code);

			return code == 200;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}
	
	public interface OnRetryClicked {
		void retry();
	}
}
