package com.cirp.mfisheries.util;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.support.annotation.DrawableRes;
import android.support.annotation.NonNull;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.cirp.mfisheries.BuildConfig;
import com.cirp.mfisheries.R;

import java.util.Map;
import java.util.Random;

public class NotifyUtil {
	private static final String TAG = "NotifyUtil";
	

	public static final int LOC_NOTIFY_ID = 1;
	public static final int LOC_ALERT_ID = 2;
	public static final int LOC_MSG_ID = 3;
	
	private final Context context;
	private final NotificationManager notificationManager;
	private NotificationChannel channel;
	private NotificationCompat.Builder serviceNotification;
	private final String channelId;
	private Map<String, Map> actions;
	
	// https://code.tutsplus.com/tutorials/android-o-how-to-use-notification-channels--cms-28616
	public NotifyUtil(Context context){
		this.context = context;
		channelId = BuildConfig.APPLICATION_ID;
		notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
		initChannels();
	}
	
	
	private void initChannels() {
		if (Build.VERSION.SDK_INT < 26) {
			return;
		}
		Log.d(TAG, "Attempting to create Notification Channels");
		channel = new NotificationChannel(channelId, context.getString(R.string.app_name), NotificationManager.IMPORTANCE_DEFAULT);
		channel.setDescription("Notification For mFisheries System");
		channel.enableLights(true);
		channel.enableVibration(true);
		channel.setLockscreenVisibility(Notification.VISIBILITY_PUBLIC);
		
		if (notificationManager != null) {
			notificationManager.createNotificationChannel(channel);
		}
	}
	
	public void notify(String contentText){
		Log.d(TAG, "Attempting to Create a Notification with the text: " + contentText);
		serviceNotification = new NotificationCompat.Builder(context, channelId)
				.setSmallIcon(R.drawable.ic_map_marker_radius_black_24dp)
				.setContentTitle(context.getString(R.string.app_name))
				.setContentText(contentText);
		
		if (notificationManager != null){
			notificationManager.notify(context.getString(R.string.app_name), LOC_NOTIFY_ID, serviceNotification.build());
		}
	}

	public void weathernotif(final  String message){
		Log.d(TAG, "Attempting to Create a Notification with the text: " + message);
		serviceNotification = new NotificationCompat.Builder(context, channelId)
				.setSmallIcon(R.drawable.ic_info_outline_black_36dp)
				.setContentTitle(context.getString(R.string.app_name))
				.setContentText(message);

		if (notificationManager != null){
			notificationManager.notify(context.getString(R.string.app_name), LOC_NOTIFY_ID, serviceNotification.build());
		}
	}
	
	public int notify(final String title, final String message, @DrawableRes int icon, @NonNull final Intent intent) {
		Log.e(TAG, String.format("Received Message: %s: %s", title, message));
		
		// Strategy for getting a unique notification code for each type of request
		int notifyID = PrefsUtil.getNotifyIdFor(context, title);
		if (notifyID == -1){
			notifyID = new Random().nextInt(100) + 20; // TODO Check for collision
			PrefsUtil.setNotifyIdFor(context, title, notifyID);
		}
		
		
		final PendingIntent onSelectIntent = PendingIntent.getActivity(context, 0 , intent, PendingIntent.FLAG_ONE_SHOT);
		
		serviceNotification = new NotificationCompat.Builder(context, channelId)
				.setSmallIcon(icon)
				.setContentTitle(title)
				.setContentText(message)
				.setContentIntent(onSelectIntent)
				.setAutoCancel(true);
		
		if (this.actions != null && this.actions.entrySet().size() > 0){
			for (Map.Entry <String, Map> entry : this.actions.entrySet()){
				//TODO Complete implementation of action
			}
		}
		
		if (notificationManager != null){
			notificationManager.notify(context.getString(R.string.app_name), notifyID, serviceNotification.build());
			return notifyID;
		}
		return -1;
	}
	
	public void addActions(Map<String, Map> actions) {
		this.actions = actions;
	}
}
