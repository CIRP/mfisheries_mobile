package com.cirp.mfisheries.util;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.preference.PreferenceManager;
import android.util.Log;

import com.cirp.mfisheries.BuildConfig;
import com.cirp.mfisheries.core.register.RegisterActivity;

import java.util.Calendar;

@SuppressWarnings({"UnusedReturnValue", "WeakerAccess", "SameParameterValue"})
public class PrefsUtil {
    private final static String base_path = BuildConfig.FLAVOR; // base path used to distinguish preferences between flavours
    private final static String first_tag = base_path + "FIRST_OPEN";
    
    private final static String id_tag = base_path + "USER_ID";
    private final static String fname_tag = base_path + "FNAME";
    private final static String lname_tag = base_path + "LNAME";
    private final static String email_tag = base_path + "EMAIL";

    private final static String google_id_tag = base_path + "GOOGLE_ID";
    private final static String token_tag = base_path + "TOKEN";

    private final static String sos_email_tag = base_path + "SOS_EMAIL";
    private final static String sos_mobile_tag = base_path + "SOS_MOBILE";
    private final static String sos_land_tag = base_path + "SOS_LAND";
    private final static String sos_extra_tag = base_path + "SOS_EXTRA_NUMBER";
    private final static String sos_in_progress = base_path + "SOS_IN_PROGRESS";

    private final static String has_lek = base_path + "HAS_LEK";

    private final static String has_ms = base_path + "HAS_MS";

    private final static String has_dm = base_path + "HAS_DM";
	/*
	* *** Country Related Shared Preferences ***
	*/
	private final static String country_tag = base_path + "COUNTRY";
	private final static String path_tag = base_path + "PATH";
	private final static String country_id_tag = base_path + "COUNTRY_ID";
	private final static String all_country_tag = base_path + "COUNTRY_LIST";
	private final static String all_country_loc_tag = base_path + "COUNTRY_LOC_LIST";
	/* *** Emergency Contacts Module *** */
	private final static String emergency_contacts_phone = base_path + "emergency_phone_contacts_data";
	private final static String emergency_contacts = base_path + "emergency_contacts_data";
	private final static String emergency_contacts_cache_time = base_path + "emergency_contacts_time";
	/* *** Damage Reporting Module *** */
	private final static String dm_report = base_path + "dm_report";
	private final static String dm_report_categories = base_path + "_dm_report_cat";
	private final static String dm_contacts_cache_time = base_path + "_dm_report_time";
	private static final String user_dm_report = "dm_report_u";
	/* *** Weather Module *** */
	private static final String weather_src = "weather_source";
	private static final String weather_src_time = "weather_source_t";
	private static final String weather_read = "weather_readings";
	private static final String weather_read_time = "weather_readings_t";
    /* *** LEK Module *** */
    private static final String lek_user_post = "lek_u";
    private static final String lek_categories = base_path+"lek_cat";
    private static final String lek_post = base_path+"lek_post";
    private static final String lek_categories_cache_time = base_path+"_lek_cache_time";
    /* *** Missing Persons Module *** */
	private static final String ms_person_report = "missing_persons";
	private static final String user_ms_person_report = "missing_persons_u";
	private static final String ms_person_report_time = "missing_persons_t";
	private static String notify_token = "firebaseToken";
	
	public static boolean clearPreferences(Context context) {
		Log.d("PrefUtil", "Attempting to clear preferences");
		SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        boolean result = preferences.edit().clear().commit();
        return result;
	}
	
	public static boolean isReset(Context context) {
		SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
		return preferences.getBoolean("is_set", false);
	}
	
	public static boolean setIsReset(Context context, boolean set) {
		SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
		return preferences.edit().putBoolean("is_set", set).commit();
	}
	
	public static boolean isFirstOpen(Context context) {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        return preferences.getBoolean(first_tag, true);
    }
	
	public static void setFirstOpen(Context context, boolean bool) {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        preferences.edit().putBoolean(first_tag, bool).apply();
    }

    public static void setLastVersion(Context context, int version){
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        preferences.edit().putInt("last_version", version).apply();
	}

    public static int getLastVersion(Context context){
	    SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
	        return preferences.getInt("last_version", -1);
	}
	
	public static void setUserId(Context context, int id) {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        preferences.edit().putInt(id_tag, id).apply();
    }

    public static int getUserId(Context context) {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        return preferences.getInt(id_tag, -999);
        //return 62;
    }

    public static boolean hasUserId(Context context) {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        return preferences.contains(id_tag);
    }

    public static void setFirstName(Context context, String fname) {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        preferences.edit().putString(fname_tag, fname).apply();
    }

    public static String getFirstName(Context context) {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        return preferences.getString(fname_tag, "GUEST");
    }

    public static void setLastName(Context context, String lname) {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        preferences.edit().putString(lname_tag, lname).apply();
    }

    public static String getLastName(Context context) {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        return preferences.getString(lname_tag, "USER");
    }
	
	public static String getUser(Context context) {
        return getFirstName(context) +" "+ getLastName(context);
    }
	
	public static boolean setNotificationToken(Context context, String token) {
		SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
		return preferences.edit().putString(notify_token, token).commit();
	}
	
	public static String getNotificationToken(Context context) {
		SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
		return preferences.getString(notify_token, "-1");
	}
	
	public static boolean setAllCountries(Context context, String countries){
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        return preferences.edit().putString(all_country_tag, countries).commit();
    }
    
    public static String getAllCountries(Context context) {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        return preferences.getString(all_country_tag, null);
    }

    public static boolean setCountry(Context context, String country) {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        return preferences.edit().putString(country_tag, country).commit();
    }

    public static String getCountry(Context context) {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        return preferences.getString(country_tag, "");
    }

    public static boolean setPath(Context context, String path) {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        return preferences.edit().putString(path_tag, path).commit();
    }

    public static String getPath(Context context) {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        return preferences.getString(path_tag, "");
    }

    public static boolean hasCountry(Context context) {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        return preferences.contains(country_tag);
    }

    public static boolean setCountryId(Context context, String cId) {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        return preferences.edit().putString(country_id_tag, cId).commit();
    }

    public static String getCountryId(Context context) {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        return preferences.getString(country_id_tag, "-1");
    }
    
    public static boolean setCountryCacheTime(Context context, Calendar date){
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        return preferences.edit().putLong("country_time", date.getTimeInMillis()).commit();
    }
    
    public static Calendar getCountryCacheTime(Context context){
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        long time = preferences.getLong("country_time", 0);
        if (time > 0){
            Calendar cal =  Calendar.getInstance();
            cal.setTimeInMillis(time);
            return cal;
        }
        return null;
    }

    public static boolean setCountryLocCacheTime(Context context, Calendar date){
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        return preferences.edit().putLong("country_loc_time", date.getTimeInMillis()).commit();
    }

    public static Calendar getCountryLocCacheTime(Context context){
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        long time = preferences.getLong("country_loc_time", 0);
        if (time > 0){
            Calendar cal =  Calendar.getInstance();
            cal.setTimeInMillis(time);
            return cal;
        }
        return null;
    }

    public static String getAllCountryLocations(Context context) {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        return preferences.getString(all_country_loc_tag, null);
    }

    public static boolean setAllCountryLocations(Context context, String countriesStr) {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        return preferences.edit().putString(all_country_loc_tag, countriesStr).commit();
    }

    public static void setEmail(Context context, String email) {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        preferences.edit().putString(email_tag, email).apply();
    }

    public static String getEmail(Context context) {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        return preferences.getString(email_tag, "");
    }

    public static void setToken(Context context, String token) {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        preferences.edit().putString(token_tag, token).apply();
    }

    public static String getToken(Context context) {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        return preferences.getString(token_tag, "");
    }

    public static void setGoogleId(Context context, String gId) {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        preferences.edit().putString(google_id_tag, gId).apply();
    }

    public static String getGoogleId(Context context) {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        return preferences.getString(google_id_tag, "");
    }

    public static void setSOS(Context context, String mobile, String land, String email) {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        preferences.edit().putString(sos_mobile_tag, mobile).apply();
        preferences.edit().putString(sos_land_tag, land).apply();
        preferences.edit().putString(sos_email_tag, email).apply();
    }

    public static void setSOSMobile(Context context, String mobile) {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        preferences.edit().putString(sos_mobile_tag, mobile).apply();
    }

    public static String getSOSMobile(Context context) {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        return preferences.getString(sos_mobile_tag, "");
    }

    public static void setSOSLand(Context context, String land) {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        preferences.edit().putString(sos_land_tag, land).apply();
    }

    public static String getSOSEmail(Context context) {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        return preferences.getString(sos_land_tag, "");
    }

    public static void setSOSEmail(Context context, String email) {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        preferences.edit().putString(sos_email_tag, email).apply();
    }
	
	public static String getSOSLand(Context context) {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        return preferences.getString(sos_land_tag, "");
    }

    public static boolean setSOSExtraPhone(Context context, String number) {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        return preferences.edit().putString(sos_extra_tag, number).commit();

    }

    public static String getSOSExtraPhone(Context context) {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        return preferences.getString(sos_extra_tag, "");
    }
	
	public static boolean hasSOSExtraPhone(Context context) {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        return preferences.contains(sos_extra_tag);
    }
	
	public static void setLocService(Context context, boolean bool) {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        preferences.edit().putBoolean("loc_service_stopped", bool).apply();
    }
	
	public static boolean setIsRegistered(Context context, boolean bool) {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        return preferences.edit().putBoolean(RegisterActivity.HAS_REGISTER_SET, bool).commit();
    }
	
	public static boolean getIsRegistered(Context context) {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        return preferences.getBoolean(RegisterActivity.HAS_REGISTER_SET, false);
    }
	
	public static boolean removeUser(Context context) {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = preferences.edit();
        return editor
            .remove(RegisterActivity.HAS_REGISTER_SET)
            .remove(token_tag)
            .remove(google_id_tag)
            .remove(email_tag)
            .remove(lname_tag)
            .remove(fname_tag)
            .remove(id_tag)
            .commit();
    }
	
	public static boolean isSOSInProgress(Context context) {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        return preferences.getBoolean(sos_in_progress, false);
    }
	
	public static void setSOSInProgress(Context context, boolean val) {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        preferences.edit().putBoolean(sos_in_progress, val).apply();
    }

    public static boolean hasLekToSync(Context context) {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        return preferences.getBoolean(has_lek, false);
    }

    public static void setLekToSync(Context context, boolean hasLEK) {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        preferences.edit().putBoolean(has_lek, hasLEK).apply();
    }

    public static boolean setUserLEKPost(Context context, String json){
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        return preferences.edit().putString(lek_user_post, json).commit();
    }

    public static String getUserLEKPost(Context context){
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        return preferences.getString(lek_user_post,null);
    }

    public static  boolean hasMSToSend(Context context){
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        return preferences.getBoolean(has_ms, false);
    }

    public static void setMSToSend(Context context, boolean hasMS) {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        preferences.edit().putBoolean(has_ms, hasMS).apply();
    }

    public static boolean hasDMtoSend(Context context){
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        return preferences.getBoolean(has_dm, false);
    }

    public static void setDMtoSend(Context context, boolean hasDM){
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        preferences.edit().putBoolean(has_dm, hasDM).apply();
    }
	
	public static boolean setEmergencyPhoneContacts(Context context, String contacts){
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        return preferences.edit().putString(emergency_contacts_phone, contacts).commit();
    }
    
    public static String getEmergencyPhoneContacts(Context context){
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        return preferences.getString(emergency_contacts_phone, null);
    }
    
    public static boolean setEmergencyContacts(Context context, String contacts){
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        return preferences.edit().putString(emergency_contacts, contacts).commit();
    }
    
    public static String getEmergencyContacts(Context context){
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        return preferences.getString(emergency_contacts, null);
    }
    
    public static boolean setContactCacheTime(Context context, Calendar date){
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        return preferences.edit().putLong(emergency_contacts_cache_time, date.getTimeInMillis()).commit();
    }
    
    public static Calendar getContactCacheTime(Context context){
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        long time = preferences.getLong(emergency_contacts_cache_time, 0);
        if (time > 0){
            Calendar cal =  Calendar.getInstance();
            cal.setTimeInMillis(time);
            return cal;
        }
        return null;
    }
    
	public static Calendar getDMReportCacheTime(Context context) {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        long time = preferences.getLong(dm_contacts_cache_time, 0);
        if (time > 0){
            Calendar cal =  Calendar.getInstance();
            cal.setTimeInMillis(time);
            return cal;
        }
        return null;
	}
    
    public static boolean setDMReportCacheTime(Context context, Calendar date){
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        return preferences.edit().putLong(dm_contacts_cache_time, date.getTimeInMillis()).commit();
    }
    
    public static boolean setReports(Context context, String json) {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        return preferences.edit().putString(dm_report, json).commit();
    }
	
	public static String getReports(Context context){
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        return preferences.getString(dm_report, null);
    }

    public static boolean setUSerDMReport(Context context, String json, String fileJson){
	    SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
	    return preferences.edit().putString(user_dm_report, json+fileJson).commit();
    }

    public static String getUserDMReport(Context context){
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        return preferences.getString(user_dm_report, null);
    }
	
	public static Calendar getDMCategoryCacheTime(Context context) {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        long time = preferences.getLong(dm_contacts_cache_time, 0);
        if (time > 0){
            Calendar cal =  Calendar.getInstance();
            cal.setTimeInMillis(time);
            return cal;
        }
        return null;
    }

	public static boolean setDMCategoryCacheTime(Context context, Calendar date){
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        return preferences.edit().putLong(dm_contacts_cache_time, date.getTimeInMillis()).commit();
    }
    
    public static boolean setCategories(Context context, String json) {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        return preferences.edit().putString(dm_report, json).commit();
    }
    
    public static String getCategories(Context context){
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        return preferences.getString(dm_report, null);
    }
    
    public static boolean setWeatherSource(Context context, String countryid, String json){
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        final String key = weather_src +"_" +countryid;
        return preferences.edit().putString(key, json).commit();
    }
    
    public static boolean setWeatherSrcCacheTime(Context context, Calendar date){
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        return preferences.edit().putLong(weather_src_time, date.getTimeInMillis()).commit();
    }
    
    public static Calendar getWeatherSrcCacheTime(Context context) {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        long time = preferences.getLong(weather_src_time, 0);
        if (time > 0){
            Calendar cal =  Calendar.getInstance();
            cal.setTimeInMillis(time);
            return cal;
        }
        return null;
    }
    
    public static String getWeatherSource(Context context, String countryid){
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        final String key = weather_src +"_" +countryid;
        return preferences.getString(key, null);
    }
    
    public static boolean setWeatherReadings(Context context, String json) {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        return preferences.edit().putString(weather_read, json).commit();
    }
    
    public static String getWeatherReadings(Context context){
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        return preferences.getString(weather_read, null);
    }
    
    public static boolean setWeatherReadingsCacheTime(Context context, Calendar instance) {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        return preferences.edit().putLong(weather_read_time, instance.getTimeInMillis()).commit();
    }
	
	public static Calendar getWeatherReadingsCacheTime(Context context) {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        long time = preferences.getLong(weather_read_time, 0);
        if (time > 0){
            Calendar cal =  Calendar.getInstance();
            cal.setTimeInMillis(time);
            return cal;
        }
        return null;
    }
    
    public static boolean setMSReports(Context context, String json) {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        return preferences.edit().putString(ms_person_report, json).commit();
    }

    public static  boolean setUserMSReport(Context context, String json) {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        return preferences.edit().putString(user_ms_person_report, json).commit();
    }
    
    public static String getMSReports(Context context){
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        return preferences.getString(ms_person_report, null);
    }

    public static String getUserMSReport(Context context){
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        return  preferences.getString(user_ms_person_report, null);
    }
    
    public static boolean setMSRCacheTime(Context context, Calendar instance) {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        return preferences.edit().putLong(weather_read_time, instance.getTimeInMillis()).commit();
    }
    
    public static Calendar getMSRCacheTime(Context context) {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        long time = preferences.getLong(weather_read_time, 0);
        if (time > 0){
            Calendar cal =  Calendar.getInstance();
            cal.setTimeInMillis(time);
            return cal;
        }
        return null;
    }

    public static Calendar getLEKCategoryCacheTime(Context context) {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        long time = preferences.getLong(lek_categories_cache_time, 0);
        if (time > 0){
            Calendar cal =  Calendar.getInstance();
            cal.setTimeInMillis(time);
            return cal;
        }
        return null;
    }

    public static boolean setLEKCategoryCacheTime(Context context, Calendar date){
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        return preferences.edit().putLong(lek_categories_cache_time, date.getTimeInMillis()).commit();
    }

    public static boolean setLEKCategories(Context context, String json){
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        return  preferences.edit().putString(lek_categories, json).commit();
    }

    public static String getLEKCategories(Context context){
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        return preferences.getString(lek_categories, null);
    }

    public static boolean setLEKPosts(Context context, String json){
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        return preferences.edit().putString(lek_post, json).commit();
    }

    public static String getLEKPosts(Context context){
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        return  preferences.getString(lek_post, null);
    }
    
    private static final String notifyPrefix = "notify_";
    
    public static int getNotifyIdFor(Context context, String title) {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        return preferences.getInt(notifyPrefix + title.toLowerCase(), -1);
    }
    
    public static boolean setNotifyIdFor(Context context, String title, int notifyID) {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        return preferences.edit().putInt(notifyPrefix + title.toLowerCase(), notifyID).commit();
    }

    private static final String FIRST_RUN = "first_run";

    public static boolean isFirstRun(Context context){
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        return preferences.getBoolean(FIRST_RUN, true);
    }

    public static boolean setFirstRun(Context context, boolean firstRun){
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        return preferences.edit().putBoolean(FIRST_RUN, firstRun).commit();
    }


//    public static boolean setCurrentLatitude(Context context, double latitude){
//        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
//        return  preferences.edit().putLong("lat", Double.doubleToRawLongBits(latitude)).commit();
//    }
//
//    public static boolean setCurrentLongitude(Context context, double longitude) {
//        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
//        return  preferences.edit().putLong("lng", Double.doubleToRawLongBits(longitude)).commit();
//    }
//
//    public static double getCurrentLatitude(Context context){
//        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
//        Log.d("PrefUtil", "GOT" + preferences.getLong("lat", 0));
//        return 0;
////        Long value = preferences.getLong("lat", 0);
////        return value.doubleValue();
//    }
//
//    public static double getCurrentLongitude(Context context) {
//        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
//        return 0;
////        Long value = preferences.getLong("lng", 0);
////        return value.doubleValue();
//    }
    
}