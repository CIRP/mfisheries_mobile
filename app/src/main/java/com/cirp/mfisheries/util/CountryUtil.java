package com.cirp.mfisheries.util;

import android.content.Context;
import android.support.annotation.NonNull;
import android.util.Log;

import com.cirp.mfisheries.R;
import com.cirp.mfisheries.core.Country;
import com.cirp.mfisheries.core.CountryLoc;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;
import com.squareup.moshi.JsonAdapter;
import com.squareup.moshi.Moshi;
import com.squareup.moshi.Types;

import java.io.IOException;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

@SuppressWarnings("WeakerAccess")
public class CountryUtil {
	
	public static final String ANTIGUA = "Antigua and Barbuda";
	public static final String BARBADOS = "Barbados";
	public static final String BELIZE = "Belize";
	public static final String DOMINICA = "Dominica";
	public static final String GRENADA = "Grenada";
	public static final String GUYANA = "Guyana";
	public static final String STKITTS = "St Kitts and Nevis";
	public static final String STLUCIA = "Saint Lucia";
	public static final String STVINCENT = "St Vincent And The Grenadines";
	public static final String TRINIDAD = "Trinidad";
	public static final String TOBAGO = "Tobago";
	public static final String TNT = "Trinidad and Tobago";
	private static final String TAG = "CountryUtil";
	private static final int MAX_DAYS = 1;
	private static CountryUtil instance;
	private Context context;
	
	
	private CountryUtil(@NonNull Context context){
		this.context = context;
	}
	
	public static CountryUtil getInstance(@NonNull Context context){
		if (instance == null)instance = new CountryUtil(context);
		else instance.context =context;
		return instance;
	}
	
	public static List<Country> convertFromJSON(@NonNull JsonArray array) {
		try {
			Moshi moshi = new Moshi.Builder().build();
			Type listMyData = Types.newParameterizedType(List.class, Country.class);
			JsonAdapter<List<Country>> adapter = moshi.adapter(listMyData);
			return adapter.fromJson(array.toString());
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}
	
	public static boolean isNumeric(String str) {
		try {
			@SuppressWarnings("unused")
			double d = Double.parseDouble(str);
			return true;
		} catch (NumberFormatException nfe) {
			return false;
		}
	}
	
	public void retrieveCountries(@NonNull final CountryRetrievedListener listener){
		if (canUseCache()){
			Log.d(TAG, "Loading country from Cache");
			listener.processCountries(retrieveCountriesFromCache());
		}else {
			retrieveCountriesFromServer(new CountryRetrievedListener() {
				@Override
				public void processCountries(List<Country> countries) {
					if (countries != null)
						if (cacheCountries(countries))
							updateCacheUse();
					listener.processCountries(countries);
				}
			});
		}
	}
	
	/**
	 *
	 * @return true if data is available to be used from cache
	 */
	public boolean canUseCache(){
		// If we are on WiFi, use online
		Calendar currTime = Calendar.getInstance();
		Calendar preTime = PrefsUtil.getCountryCacheTime(context);
		// TODO - CountryUtil - Ensure that country location is accommodated as well
		if (preTime != null) {
			long diff = currTime.getTimeInMillis() - preTime.getTimeInMillis();
			long days = diff / (24 * 60 * 60 * 1000);
			if (days < MAX_DAYS)return true;
			else{
				// We are greater than the time to live, but we are offline then use cache
				if (!NetUtil.isOnline(context))return true;
				// We are greater than the time to live but we are using mobile data then use cache
				return !NetUtil.isWiFi(context);
			}
		}
		return false;
	}
	
	public void updateCacheUse(){
		Calendar currTime = Calendar.getInstance();
		PrefsUtil.setCountryCacheTime(context, currTime);
	}

	public void retrieveCountriesFromServer(@NonNull final CountryRetrievedListener listener){
		Log.d(TAG, String.format("Loading Countries from: %s", NetUtil.API_URL + NetUtil.GET_COUNTRIES_PATH));
		Ion.with(context)
				.load(NetUtil.API_URL + NetUtil.GET_COUNTRIES_PATH)
				.setHeader(NetUtil.API_KEY_PARAM, NetUtil.API_KEY)
				.asJsonArray()
				.setCallback(new FutureCallback<JsonArray>() {
					@Override
					public void onCompleted(Exception e, JsonArray result) {
						// If no error
						if (e == null){
							List<Country> countries = CountryUtil.convertFromJSON(result);
							listener.processCountries(countries);
							return;
						}else{
							Log.d(TAG, "Error Loading Countries: " + e.getMessage());
							CrashReporter.getInstance(context).report(e);
						}
						listener.processCountries(null);
					}
				});
	}
	
	public boolean cacheCountries(@NonNull List<Country> countries){
		String countriesStr = convertToJSONString(countries);
		return PrefsUtil.setAllCountries(context, countriesStr);
	}
	
	public List<Country> retrieveCountriesFromCache(){
		String countriesStr = PrefsUtil.getAllCountries(context);
		if (countriesStr != null){
			JsonParser parser = new JsonParser();
			JsonElement element = parser.parse(countriesStr);
			JsonArray list = element.getAsJsonArray();
			return convertFromJSON(list);
		}
		return null;
	}
	
	public String convertToJSONString(@NonNull List<Country> countries){
		return new Gson().toJson(countries);
	}
	
	public int getCountryFlagResourceID(@NonNull String country){
//		Log.d(TAG, "Attempting to find flag for: " + country);
		if (isNumeric(country)) {
			List<Country> countries = retrieveCountriesFromCache();
			if (countries != null && countries.size() > 0) {
				for (Country c : countries) {
					if (c.getId().equalsIgnoreCase(country)) {
						country = c.getName();
						break;
					}
				}
			}
		}
		// Did not use switch because we need cases insensitive comparison
		if (country.equalsIgnoreCase(ANTIGUA)){
			return R.drawable.flag_antigua_and_barbuda;
		}
		if (country.equalsIgnoreCase(DOMINICA)){
			return R.drawable.flag_dominica;
		}
		if (country.equalsIgnoreCase(GRENADA)){
			return R.drawable.flag_grenada;
		}
		if (country.equalsIgnoreCase(STKITTS)){
			return R.drawable.flag_st_kitts_and_nevis;
		}
		if (country.equalsIgnoreCase(STLUCIA)){
			return R.drawable.flag_st_lucia;
		}
		if (country.equalsIgnoreCase(STVINCENT)){
			return R.drawable.flag_st_vincent_and_the_grenadines;
		}
		if (country.equalsIgnoreCase(TRINIDAD) || country.equalsIgnoreCase(TOBAGO)){
			return R.drawable.flag_trinidad_and_tobago;
		}
		
		return R.drawable.flag_trinidad_and_tobago;
	}
	
	public String getCountryFromCode(@NonNull String code){
		code = code.toUpperCase();
		// using codes from: http://www.nationsonline.org/oneworld/country_code_list.htm
		switch (code){
			case "AG":
				return ANTIGUA;
			case "BB":
				return BARBADOS;
			case "BZ":
				return BELIZE;
			case "DM":
				return DOMINICA;
			case "GD":
				return GRENADA;
			case "GY":
				return GUYANA;
			case "KN":
				return STKITTS;
			case "LC":
				return STLUCIA;
			case "VC":
				return STVINCENT;
			case "TT":
				return TNT;
			default:
				return "Unknown";
		}
	}
	
	public void saveCountryData(@NonNull Country country) {
		// Store to preferences
		if (PrefsUtil.setCountry(context, country.getName()))
			Log.d(TAG, String.format("Country was set to %s", country.getName()));
		if (PrefsUtil.setPath(context, country.getPath()))
			Log.d(TAG, String.format("Path was set to %s", country.getPath()));
		if(PrefsUtil.setCountryId(context, country.getId()))
			Log.d(TAG, String.format("ID was set to %s", country.getId()));
		
		if (country.getAuthority() != null) {
			Country.Authority authData = country.getAuthority();
			boolean isSet = false;
			// fix issue here
			if (authData.getMobileNum() != null) {
				PrefsUtil.setSOSMobile(context, authData.getMobileNum());
				Log.d(TAG, "Configured Emergency Contact as: " + authData.getMobileNum());
				isSet = true;
			}
			
			if (authData.getHomeNum() != null) {
				PrefsUtil.setSOSLand(context, authData.getHomeNum());
				Log.d(TAG, "Configured Emergency Contact as: " + authData.getHomeNum());
				isSet = true;
			}
			
			if (authData.getEmail() != null) {
				PrefsUtil.setSOSEmail(context, authData.getEmail());
				Log.d(TAG, "Configured Emergency Contact as: " + authData.getEmail());
				isSet = true;
			}
			
			if (isSet)
				Log.d(TAG, "Successfully Configured Country Information");
//				Toast.makeText(context, "Successfully Configured Country Information", Toast.LENGTH_SHORT).show();
			
		}
	}

	// ************ Country Locations ***********
	/**
	 *
	 * @return true if data is available to be used from cache
	 */
	public boolean canUseCountryLocCache(){
		// If we are on WiFi, use online
		Calendar currTime = Calendar.getInstance();
		Calendar preTime = PrefsUtil.getCountryLocCacheTime(context);
		if (preTime != null) {
			long diff = currTime.getTimeInMillis() - preTime.getTimeInMillis();
			long days = diff / (24 * 60 * 60 * 1000);
			if (days < MAX_DAYS)return true;
			else{
				// We are greater than the time to live, but we are offline then use cache
				if (!NetUtil.isOnline(context))return true;
				// We are greater than the time to live but we are using mobile data then use cache
				return !NetUtil.isWiFi(context);
			}
		}
		return false;
	}
	
	public void updateCountryLocCacheUse(){
		Calendar currTime = Calendar.getInstance();
		PrefsUtil.setCountryLocCacheTime(context, currTime);
	}
	
	public List<CountryLoc> convertLocFromJSON(@NonNull JsonArray array){
		try{
			Moshi moshi = new Moshi.Builder().build();
			Type listMyData = Types.newParameterizedType(List.class, CountryLoc.class);
			JsonAdapter<List<CountryLoc>> adapter = moshi.adapter(listMyData);
			return adapter.fromJson(array.toString());
		}catch (IOException e){
			e.printStackTrace();
		}
		return null;
	}
	
	public String convertLocToJSONString(@NonNull List<CountryLoc> countries){
		return new Gson().toJson(countries);
	}
	
	public void retrieveCountryLocations(@NonNull final CountryLocRetrievedListener listener){
		if (canUseCountryLocCache()){
			Log.d(TAG, "Loading country locations from Cache");
			listener.processCountryLocations(retrieveCountryLocationsFromCache());
		}else {
			retrieveCountryLocationsFromServer(new CountryLocRetrievedListener() {
				@Override
				public void processCountryLocations(List<CountryLoc> countries) {
					if (countries != null)
						if (cacheCountryLocations(countries))
							updateCountryLocCacheUse();
					listener.processCountryLocations(countries);
				}
			});
		}
	}
	
	public void retrieveCountryLocation(final int countryId, @NonNull final CountryLocRetrievedListener listener){
		this.retrieveCountryLocations(new CountryLocRetrievedListener() {
			@Override
			public void processCountryLocations(List<CountryLoc> countryLocs) {
				if (countryLocs != null && countryLocs.size() > 0){
					List<CountryLoc> result = new ArrayList<>();
					for (CountryLoc loc : countryLocs){
						if (loc.countryid == countryId){
							result.add(loc);
							listener.processCountryLocations(result);
							return;
						}
					}
				}
				listener.processCountryLocations(null);
			}
		});
	}
	
	public void retrieveCountryLocationsFromServer(@NonNull final CountryLocRetrievedListener listener){
		final String url = NetUtil.API_URL + "countrylocs";
		Log.d(TAG, String.format("Loading Countries from: %s", url));
		Ion.with(context)
				.load(url)
				.setHeader(NetUtil.API_KEY_PARAM, NetUtil.API_KEY)
				.asJsonArray()
				.setCallback(new FutureCallback<JsonArray>() {
					@Override
					public void onCompleted(Exception e, JsonArray result) {
						// If no error
						if (e == null) {
							Log.d(TAG, "Retrieved from Server: " + result.toString());
							List<CountryLoc> countryLocs = convertLocFromJSON(result);
							listener.processCountryLocations(countryLocs);
						}else{
							Log.d(TAG, "Error Loading Country Locations: " + e.getMessage());
							e.printStackTrace();
							CrashReporter.getInstance(context).report(e);
						}
						listener.processCountryLocations(null);
					}
				});
	}
	
	private List<CountryLoc> retrieveCountryLocationsFromCache() {
		String countriesStr = PrefsUtil.getAllCountryLocations(context);
		if (countriesStr != null){
			JsonParser parser = new JsonParser();
			JsonElement element = parser.parse(countriesStr);
			JsonArray list = element.getAsJsonArray();
			return convertLocFromJSON(list);
		}
		return null;
	}
	
	private boolean cacheCountryLocations(@NonNull List<CountryLoc> countries) {
		String countriesStr = convertLocToJSONString(countries);
		return PrefsUtil.setAllCountryLocations(context, countriesStr);
	}
	
	public interface CountryRetrievedListener {
		void processCountries(List<Country> countries);
	}
	
	public interface CountryLocRetrievedListener{
		void processCountryLocations(List<CountryLoc> countryLocs);
	}
}
