package com.cirp.mfisheries.util;

import android.content.Context;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.Toast;

import com.cirp.mfisheries.core.config.ConfigUtil;
import com.cirp.mfisheries.core.module.Module;
import com.cirp.mfisheries.core.module.ModuleFactory;
import com.koushikdutta.ion.Ion;

import java.util.List;

public class ModuleUtil {

	public static String[] MODULE_ORDER = {"Navigation", "Weather", "Alerts", "SOS", "Tracking", "Messaging", "FEWER", "Photo Diary", "LEK", "First Aid", " " };

	public static String PLACEHOLDER_TILE = "BLANK";

	public static boolean needsRegistration(Context context, List<String> modules) {
		ModuleFactory factory = ModuleFactory.getInstance(context);
		for (String moduleName : modules) {
			Log.d("ModuleUtil", moduleName);
			Module module = factory.getModule(moduleName);
			if (module != null) {
				if (module.needsRegistration)
					return true;
			} else {
				Log.d("ModuleUtil", "Module does not exist");
				Toast.makeText(context, "Module selected does not exist", Toast.LENGTH_SHORT).show();
			}
		}
		return false;
	}
}
