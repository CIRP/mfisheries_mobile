package com.cirp.mfisheries.util;


import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.media.ThumbnailUtils;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.FileProvider;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.cirp.mfisheries.R;

import java.io.File;

import static android.provider.MediaStore.Video.Thumbnails.MINI_KIND;

@SuppressWarnings("WeakerAccess")
public class MediaUtils {
	
	private static final String TAG = "MediaUtils";
	public static final int COMPRESS_QUALITY = 45;
	
	final int REQUEST_IMAGE_GET = 0x1;
	final int REQUEST_AUDIO_GET = 0x3;
	final int REQUEST_VIDEO_GET = 0x2;
	final int REQUEST_NEW_IMAGE_GET = 0x4;
	final int REQUEST_NEW_VIDEO_GET = 0x5;
	
	public final String MEDIA_NEW_IMAGE = "new_photo";
	public final String MEDIA_NEW_VIDEO = "new_video";
	
	
	
	private final Activity activity;
	private File currentFile;
	private String currentFilePath;
	private Uri currentFileUri;
	private String currentFileType = "text/*";
	
	public MediaUtils(Activity activity){
		this.activity = activity;
	}
	
	
	public void request(String type){
		switch (type){
			case MEDIA_NEW_IMAGE:
				attachNewImage();
				break;
			case MEDIA_NEW_VIDEO:
				break;
		}
	}
	
	
	
	public void attach(View view) {
		AlertDialog.Builder builder = new AlertDialog.Builder(activity);
		String[] items = {
				"Attach Image"
				,"Attach Video"
				,"Attach Audio"
				,"Take Picture"
				,"Record Video"
		};
		
		final ArrayAdapter<String> arrayAdapter = new ArrayAdapter<>(activity, android.R.layout.simple_list_item_1, items);
		
		builder.setAdapter(arrayAdapter,
				new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						switch (which) {
							case 0:
								attachImage();
								return;
							case 1:
								attachVideo();
								return;
							case 2:
								attachAudio();
								return;
							case 3:
								attachNewImage();
								return;
							case 4:
								attachNewVideo();
						}
					}
				});
		builder.show();
	}
	
	//attach audio files
	public void attachAudio() {
		Intent intent = new Intent(Intent.ACTION_GET_CONTENT, MediaStore.Audio.Media.EXTERNAL_CONTENT_URI);
		intent.setType("audio/*");
		if (intent.resolveActivity(activity.getPackageManager()) != null) {
			activity.startActivityForResult(intent, REQUEST_AUDIO_GET);
		}
	}
	
	//attach video files
	public void attachVideo() {
		Intent intent = new Intent(Intent.ACTION_GET_CONTENT, MediaStore.Video.Media.EXTERNAL_CONTENT_URI);
		intent.setType("video/*");
		if (intent.resolveActivity(activity.getPackageManager()) != null) {
			activity.startActivityForResult(intent, REQUEST_VIDEO_GET);
		}
	}
	
	//attach image files
	public void attachImage() {
		Intent intent = new Intent(Intent.ACTION_GET_CONTENT, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
		intent.setType("image/*");
		if (intent.resolveActivity(activity.getPackageManager()) != null) {
			activity.startActivityForResult(intent, REQUEST_IMAGE_GET);
		}
	}
	
	private void setupFileForMedia(Intent intent, int requestCode, String filename){
		String msg;
		if (intent.resolveActivity(activity.getPackageManager()) != null){
			currentFile = FileUtil.getOutputMediaFile(activity, filename);
			if (currentFile != null) {
				currentFilePath = currentFile.getAbsolutePath();
				Log.d(TAG, "File Created as :" + currentFile.getAbsolutePath());
				currentFileUri = FileProvider.getUriForFile(activity,activity.getPackageName() + ".provider", currentFile);
				Log.d(TAG, "URI For file is: " + currentFileUri);
				intent.putExtra(MediaStore.EXTRA_OUTPUT, currentFileUri);
				activity.startActivityForResult(intent, requestCode);
				return;
			}else msg = "Unable to Create Media. Ensure to enable file permissions when requested.";
		}else msg = "Unable to Launch Camera. Please restart mFisheries application and try again.";
		Log.d(TAG, msg);
		Toast.makeText(activity, msg, Toast.LENGTH_SHORT).show();
		CrashReporter.getInstance(activity).log(Log.ERROR, TAG, msg);
	}
	
	// Based on - https://developer.android.com/training/camera/photobasics.html#TaskPath
	public void attachNewImage() {
		Log.d(TAG, "Making request for new image");
		Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
		setupFileForMedia(intent, REQUEST_NEW_IMAGE_GET, FileUtil.generateImageName(activity));
	}
	
	// Based on https://developer.android.com/training/camera/videobasics.html
	public void attachNewVideo() {
		Log.d(TAG, "Making Request for new Video");
		Intent intent = new Intent(MediaStore.ACTION_VIDEO_CAPTURE);
		setupFileForMedia(intent, REQUEST_NEW_VIDEO_GET, FileUtil.generateVideoName(activity));
	}
	
	public void handleActivityResult(int requestCode, int resultCode, Intent data, @Nullable MediaListener listener){
		Log.d(TAG, "Handle the Activity result of the activity request");
		// Handle Capturing Images from Camera
		if (requestCode == REQUEST_NEW_IMAGE_GET && resultCode == Activity.RESULT_OK) {
			if (listener  != null)listener.onMediaReceived(currentFileUri, currentFilePath, MEDIA_NEW_IMAGE);
		}
	}
	
	public void handleActivityResult(int requestCode, int resultCode, Intent data, final ImageView thumb,  @Nullable MediaListener listener) {
		thumb.setVisibility(View.VISIBLE);
		
		if (requestCode == REQUEST_IMAGE_GET && resultCode == Activity.RESULT_OK) {
			Uri uri = data.getData();
			currentFilePath = FileUtil.getAbsolutePath(activity, uri, "image");
			Glide.with(activity).load(new File(currentFilePath)).into(thumb);
			this.currentFileType = "image/*";
			if (listener != null)
				listener.onMediaReceived(uri, currentFilePath, currentFileType);
		}
		
		if (requestCode == REQUEST_AUDIO_GET && resultCode == Activity.RESULT_OK) {
			Uri uri = data.getData();
			thumb.setImageResource(R.drawable.ic_mic_none_white_48dp);
			thumb.setBackgroundColor(ContextCompat.getColor(activity, R.color.primary));
			currentFilePath = FileUtil.getAbsolutePath(activity, uri, "audio");
			this.currentFileType = "audio/*";
			
			if (listener != null)
				listener.onMediaReceived(uri, currentFilePath, currentFileType);
		}
		
		if (requestCode == REQUEST_VIDEO_GET && resultCode == Activity.RESULT_OK) {
			Uri uri = data.getData();
			currentFilePath = FileUtil.getAbsolutePath(activity, uri, "video");
			Glide.with(activity).load(new File(currentFilePath)).into(thumb);
			currentFileType = "video/mp4";
			if (listener != null)
				listener.onMediaReceived(uri, currentFilePath, currentFileType);
		}
		
		// Handle Capturing Images from Camera
		if (requestCode == REQUEST_NEW_IMAGE_GET && resultCode == Activity.RESULT_OK) {
			Glide.with(activity).load(new File(currentFilePath)).into(thumb);
			currentFileType = "image/*";
			if (listener != null)
				listener.onMediaReceived(currentFileUri, currentFilePath, currentFileType);
		}
		
		// Handling Capturing Video from camera
		if (requestCode == REQUEST_NEW_VIDEO_GET && resultCode == Activity.RESULT_OK) {
			thumb.setImageBitmap(ThumbnailUtils.createVideoThumbnail(currentFilePath, MINI_KIND));
			currentFileType = "video/mp4";
			if (listener != null)
				listener.onMediaReceived(currentFileUri, currentFilePath, currentFileType);
		}
	}
	
	
	public interface MediaListener{
		void onMediaReceived(Uri currentFileUri, String currentFilePath, String currentFileType);
	}
}
