package com.cirp.mfisheries.util;


import android.annotation.SuppressLint;
import android.content.Context;
import android.util.Log;

import com.cirp.mfisheries.BuildConfig;
import com.cirp.mfisheries.core.location.Geofencer;
import com.cirp.mfisheries.core.location.LocationService;

import java.io.File;
import java.io.FileOutputStream;

public class ExperimentUtil {
	private static final String TAG = "ExprUtil";
	private final Context context;
	@SuppressLint("StaticFieldLeak")
	private static ExperimentUtil instance;
	
	private ExperimentUtil(Context context) {
		this.context = context;
	}

	public boolean testUnzipFile(){
		try{
			Zipper zip = new Zipper(FileUtil.getFileUri("/" +"test.zip").toString(),FileUtil.getFileUri("").toString());
//			final File file = new File(FileUtil.getmFisheriesFolder()+"/"+filename);
			zip.setDeleteAfterUnzip(false);
			zip.setOnZipFinishedListener(new Zipper.OnZipFinishedListener(){
				@Override
				public void onFinished(boolean status) {
					Log.d("UnzipTest","Finished unzipping test.zip");
				}
			});
			zip.unzip();
		}catch (Exception e){
			e.printStackTrace();
		}
		return true;
	}
	
	private boolean writeFile(String filename, String data){
		try{
			// Ignoring Null possibilities for time being
			final File file = new File(FileUtil.getmFisheriesFolder() +"/"+ filename);
			Log.d(TAG, "Attempting to store results to: " + file.getAbsolutePath());
			FileOutputStream stream = new FileOutputStream(file);
			stream.write(data.getBytes());
			stream.close();
			return true;
		} catch (Exception e){
			e.printStackTrace();
		}
		return false;
	}

	public void pollingAdjust(){
		if(BuildConfig.DEBUG){
			Log.d(TAG, "Adjusting Polling interval");
			LocationService.LOC_TIME_INTERVAL = 6000;
		}
	}
	
	@SuppressLint("DefaultLocale")
	public void testLocationFunction(){
		if (BuildConfig.DEBUG) { // Only run tests in debugging mode
			Log.d(TAG, "Executing Location Tests");
			StringBuilder accuracyBuilder = new StringBuilder();
			accuracyBuilder.append("Test, Country, ANB, DOM, GRE, SKN, SLU, SVG, TOB, TRI, Time \n");
			String [] countries = {
					"Antigua & Barbuda",
					"Dominica",
					"Grenada",
					"St Kitts and Nevis",
					"St Lucia",
					"St Vincent & the Grenadines",
					"Tobago",
					"Trinidad"
			};
			// (LAT, LONG) - points extracted from geo fence
			double [][] locations = {
					{16.94375, -62.34375}, // ANB
					{15.628053665161133, -61.468055725097656}, // DOM
					{12.438055038452148, -61.49555969238281}, // GRE
					{17.177361, -62.625137}, // SKN
					{14.108195, -60.93486},  // SLU
					{13.018194, -61.222637}, // SVG
					{11.183610916137695, -60.73638916015625}, // TOB
					{10.7199847, -61.6477205} // TRI
			};
			long startTime;
			int comma;
			for (int ci = 0 ; ci < countries.length; ci++){
				startTime = System.currentTimeMillis();
				accuracyBuilder.append("In Country,").append(countries[ci]).append(",");
				// NB (X is longitude and Y is latitude)
				boolean res = Geofencer.isWithinCountry(context, locations[ci][0],  locations[ci][1], true);
				// Print commas before results
				for(comma = 0; comma < ci; comma++)accuracyBuilder.append(",");
				// Print result
				accuracyBuilder.append(res); // Should print True for all
				// Print remaining commas
				for(comma = ci+1; comma <= countries.length; comma++) accuracyBuilder.append(",");
				// Print time taken to process country
				double delta = (System.currentTimeMillis() - startTime);
				accuracyBuilder.append(String.format("%f", delta));
				accuracyBuilder.append("\n");
			}
			
			String results = accuracyBuilder.toString();
			Log.d(TAG, String.format("Results for test is: %s", results));
			if (writeFile("accuracy_results.csv", results))Log.d(TAG, "Results saved to file");
			else Log.d(TAG, "Results not saved to file");
			
			
			StringBuilder cBuilder = new StringBuilder();
			boolean testResult;
			cBuilder.append("country, result, time");
			startTime = System.currentTimeMillis();
			testResult = Geofencer.isWithinCountry(context, 10.455657, -61.228503, true);
			cBuilder.append("Trinidad")
					.append(",")
					.append(testResult)
					.append(",")
					.append(System.currentTimeMillis() - startTime)
					.append("\n");
			
			startTime = System.currentTimeMillis();
			testResult = Geofencer.isWithinCountry(context, 12.089624, -61.681652, true);
			cBuilder.append("Grenada")
					.append(",")
					.append(testResult)
					.append(",")
					.append(System.currentTimeMillis() - startTime)
					.append("\n");
			
			startTime = System.currentTimeMillis();
			testResult = Geofencer.isWithinCountry(context, 11.183559, -60.776726, true);
			cBuilder.append("Tobago")
					.append(",")
					.append(testResult)
					.append(",")
					.append(System.currentTimeMillis() - startTime)
					.append("\n");
			
			startTime = System.currentTimeMillis();
			testResult = Geofencer.isWithinCountry(context, 15.453270, -61.353806, true);
			cBuilder.append("Dominica")
					.append(",")
					.append(testResult)
					.append(",")
					.append(System.currentTimeMillis() - startTime)
					.append("\n");
			
			startTime = System.currentTimeMillis();
			testResult = Geofencer.isWithinCountry(context, 13.210350, -61.168451, true);
			cBuilder.append("SVG")
					.append(",")
					.append(testResult)
					.append(",")
					.append(System.currentTimeMillis() - startTime)
					.append("\n");
			
			startTime = System.currentTimeMillis();
			testResult = Geofencer.isWithinCountry(context, 13.913813, -61.028205, true);
			cBuilder.append("St Lucia")
					.append(",")
					.append(testResult)
					.append(",")
					.append(System.currentTimeMillis() - startTime)
					.append("\n");
			
			startTime = System.currentTimeMillis();
			testResult = Geofencer.isWithinCountry(context, 17.357296, -62.776445, true);
			cBuilder.append("St Kitts & Nevis")
					.append(",")
					.append(testResult)
					.append(",")
					.append(System.currentTimeMillis() - startTime)
					.append("\n");
			
			results = cBuilder.toString();
			Log.d(TAG, String.format("Results for test is: %s", results));
			if (writeFile("basic_results.csv", results))Log.d(TAG, "Results saved to file");
			else Log.d(TAG, "Results not saved to file");
		}
	}
	
	public static ExperimentUtil getInstance(Context context) {
		if (instance ==null)instance = new ExperimentUtil(context);
		return instance;
	}
}
