package com.cirp.mfisheries.util;

import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.util.Log;
import android.view.View;

import com.cirp.mfisheries.core.location.LocationService;
import com.cirp.mfisheries.core.module.ModuleFactory;
import com.cirp.mfisheries.nav.MapMarkerDM;
import com.cirp.mfisheries.nav.NavigationActivity;

import java.util.Locale;

public class LocationUtil {

    private static final String TAG = "LocationUtil";

    public static String convertLatitude(double latitude) {
        double abs_latitude = Math.abs(latitude);
        int deg = (int) abs_latitude;
        int min = (int) Math.abs((abs_latitude - deg) * 60);
        float sec = (float) Math.abs((((abs_latitude - deg) * 60) - min) * 60);
        if (latitude > 0) {
//            return (String.valueOf(deg) + (char) 0x00B0 + " " + String.valueOf(min) + "' " + String.format(Locale.US,"%.2f",sec)  + '"' + " N");
            return (String.valueOf(deg) + (char) 0x00B0 + " " + String.valueOf(min) + "N");

        } else {
//            return (String.valueOf(deg) + (char) 0x00B0 + " " + String.valueOf(min) + "' " + String.format(Locale.US,"%.2f",sec)  + '"' + " S");
            return (String.valueOf(deg) + (char) 0x00B0 + " " + String.valueOf(min) + "S");

        }
    }

    public static String convertLongitude(double longitude) {
        double abs_longitude = Math.abs(longitude);
        int deg = (int) abs_longitude;
        int min = (int) Math.abs((abs_longitude - deg) * 60);
        float sec = (float) Math.abs((((abs_longitude - deg) * 60) - min) * 60);
        if (longitude < 0) {
//            return (String.valueOf(deg) + (char) 0x00B0 + " " + String.valueOf(min) + "' " + String.format(Locale.US,"%.2f",sec) + '"' + " W");
            return (String.valueOf(deg) + (char) 0x00B0 + " " + String.valueOf(min) + "W");

        } else {
//            return (String.valueOf(deg) + (char) 0x00B0 + " " + String.valueOf(min) + "' " + String.format(Locale.US,"%.2f",sec) + '"' + " E");
            return (String.valueOf(deg) + (char) 0x00B0 + " " + String.valueOf(min) + "E");
        }
    }

    @NonNull
    public static String getLocationString(double lat, double lng) {
        Location location = new Location("prov");
        location.setLongitude(lng);
        location.setLatitude(lat);
        return convertLatitude(lat) + " " + convertLongitude(lng);
    }

    public static void startLocationServices(Context context) {
        LocationService.startLocationServices(context);
    }


    public interface mFisheriesLocationListener{
        void onLocationChanged(Location loc);
    }
    
    public static void displayLocation(final Context context, final MapMarkerDM loc, final View view){
        Log.d(TAG, "Checking if the navigation module was installed to determine what map to display");
        
        if (ModuleFactory.getInstance(context).isModuleInstalled("Navigation")) {
            Intent intent = new Intent(context, NavigationActivity.class);
            intent.putExtra("name", loc.name);
            intent.putExtra("details", loc.details);
            intent.putExtra("lat", loc.latitude);
            intent.putExtra("lng", loc.longitude);
            context.startActivity(intent);
        }else{
            Log.d(TAG, "No Navigation module installed. Give user the user the option");
        
            Snackbar.make(view, "The mFisheries Navigation module is not installed.", Snackbar.LENGTH_LONG)
                    .setAction("Open Google Maps", new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            String urlStr = String.format("google.streetview:cbll=%s,$s", loc.latitude, loc.longitude);
                            Uri gmmIntentUri = Uri.parse(urlStr); // Create a Uri from an intent string. Use the result to create an Intent.
                            Intent mapIntent = new Intent(Intent.ACTION_VIEW, gmmIntentUri);
                            mapIntent.setPackage("com.google.android.apps.maps");
                            mapIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                            context.startActivity(mapIntent);
                        }
                    })
                    .show();
        }
    }
    
}
